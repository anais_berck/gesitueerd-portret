## Bewegingen stam

![](/static/img/sensors/ems-600x.jpg)

### Herkenbare patronen

Dag & nacht
:   overdag krimpt de stam tijdens de fotosynthese, door het snelle transport van sappen van de kruin naar de bladeren en andsersom. 's Nachts vult de stam zich opnieuw met water vanuit de wortels en zet die uit. Dit regelmatige patroon is enkel zichtbaar bij de Rode Beuk.

Zon & regen
:   bij de oude bomen met dikke bast meet de dendrometer in de eerste plaats de beweging van het dode hout van de bast. Wanneer het regent, zet die uit; waneer de zon schijnt krimpt die. Je kent dit fenomeen vast bij klemmende houten deuren in de herfst en de winter.

We zouden de groei bij de oude bomen met dikke bast kunnen meten door over de omtrek van de stam een stuk van de bast te verwijderen. Het ligt niet in de aard van dit project om te snijden in de bast van oud loofbomen. De meeste studies met dendrometers worden gedaan op beuken, die een heel dunne bast hebben.

Seizoen
:   bij de Rode Beuk zie je een gestage groei per week; bij de andere bomen merk je de groei pas over veel langere termijn.


### Waarom een dendrometer

De dendrometer laat toe om het 'ademen' van een boom te registreren, iets wat wij met het blote oog niet kunnen waarnemen. De boom beweegt voortdurend. Overdag, wanneer hij actief is met het aanmaken van suikers via fotosynthese, krimpt de stam. 's Nachts, wanneer hij rust, vult de stam zich met water en zet weer uit.

De productie van suikers in combinatie met het water en de mineralen uit de bodem zorgt voor de aanmaak van cellen en de groei van de boom. In een jaar met veel zon, voldoende regen, geen ziektes of onverwachte gebeurtenissen, groeit de boom meer. Hij maakt dan een bredere boomring aan. Wanneer een boom geveld wordt, kan je de jaarringen precies tellen. Een dunne jaarring betekent een moeizaam jaar voor de boom. Het weer was dan veel te droog of veel nat.

Dendrochronologen boren een sample uit de stam van een levende boom om precies te weten hoe oud die is, en om de weersomstandigheden van het verleden te analyseren. Bomen zijn dus levende klimaatarchieven!

### Type sensor

Voor dit project gebruiken we banddendrometers. Een metalen band wordt aangespannen om de stam. De zwarte cilinder is het meetinstrument dat beweeglijk op en neer gaat en de metingen via het LTE-netwerk of mobiele telefoonnetwerk doorstuurt.
