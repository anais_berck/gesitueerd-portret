## Bodemtemperatuur

![](/static/img/sensors/sensoterra-600x.jpg)

### Herkenbare patronen

Dag & nacht
:   wanneer de zon ondergaat, wordt het frisser en vochtiger. Dit heeft effect op de bodemtemperatuur

Warm & droog
:   dit effect is het duidelijkst aan het strand, waar op warme dagen het zand letterlijk te heet onder je voeten kan zijn

Stand van de zon
:   de sensor meet de bodemtemperatuur in het bovenste deel; dit is beschermd met een plastic kap. Wanneer de zon recht op de kap schijnt, warmt de plastic snel op en kan je gele bollen ontdekken in de visualisatie, met temperaturen tot wel 40°C. Op die manier kan je, dankzij de afwijking in de sensor, de stand van de zon volgen.

### Bodemtemperatuur & Bomen

De bodemtemperatuur is van groot belang voor het leven van bomen en planten. Het beïnvloedt de stofwisseling in de wortelcellen en de bacteriële activiteiten van de daarmee verbonden zwammen. Het heeft effect op het ophopen, afbreken en mineraliseren van organisch materiaal, op de beschikbaarheid van water en voedingsstoffen voor planten en microben, de beluchting van de bodem, de vochtigheid, de efficiëntie van meststoffen, de afbraak van pesticiden, enz. Bij elke graad temperatuurverandering verandert er ongeveer 2% van de verschillende parameters in de bodem.

Bij planten beïnvloedt de temperatuur de ontkieming van zaden, de groei en versteviging van cellen, de opname van water en voedingsstoffen, en het voorkomen van ziekten en insecten. De snelheid van chemische en biochemische reacties verdubbelt met elke 10 °C temperatuurstijging. De bodem is dan ook de opslagplaats en bron van voedingsstoffen en thermische energie op dagelijkse of seizoensbasis.

### Type sensor
De bodemtemperatuur wordt gemeten met sensoren van de Nederlandse start-up Sensoterra. Ze meten de temperatuur in de kop van de sensor, onder de plastic hoes.

De sensoren zijn geplaatst op de 'drip line' van de bomen. Dat is de ingebeelde lijn die loopt langs de uiterste bladeren van de kruin. Langs de dripline groeien de meest levendige oppervlaktewortels.
