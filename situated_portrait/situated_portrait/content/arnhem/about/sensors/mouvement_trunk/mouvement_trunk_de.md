## Bewegung Baumstamm

![](/static/img/sensors/ems-600x.jpg)

### Erkennbare Muster

Tag und Nacht
:   Tagsüber schrumpft der Stamm während der Photosynthese, da der Saft schnell von der Krone zu den Blättern und umgekehrt transportiert wird. In der Nacht füllt sich der Stamm mit Wasser aus den Wurzeln und dehnt sich aus. Dieses regelmäßige Muster ist nur bei der Rotbuche zu beobachten.

Sonne und Regen:
:   Bei alten Bäumen mit dicker Rinde misst das Dendrometer vor allem die Bewegung des Totholzes der Rinde. Wenn es regnet, dehnt es sich aus, wenn die Sonne scheint, schrumpft es. Wir kennen dieses Phänomen von Holztüren, die sich im Herbst und Winter verziehen und ‘stecken bleiben’.

Bei alten Bäumen mit dicker Rinde könnte man das Wachstum messen, indem man ein Stück Rinde am Umfang des Stammes entfernt. Im Rahmen dieses Projektes wurde aber darauf verzichtet, in die Rinde alter Laubbäume zu schneiden. Die meisten Studien mit Dendrometern werden an Buchen durchgeführt, die eine sehr dünne Rinde haben.

Saison
:   Bei der Rotbuche ist ein gleichmäßiges Wachstum pro Woche zu erkennen, bei den anderen Bäumen ist das Wachstum über einen viel längeren Zeitraum zu beobachten.

### Warum ein Dendrometer

Mit dem Dendrometer lässt sich die ‘Atmung’ eines Baumes aufzeichnen, etwas, das wir mit bloßem Auge nicht beobachten können. Der Baum ist ständig in Bewegung. Tagsüber, wenn er aktiv Zucker durch Photosynthese produziert, schrumpft der Stamm. In der Nacht, wenn er sich ausruht, füllt sich der Stamm mit Wasser und dehnt sich wieder aus.

Die Erzeugung von Zucker in Verbindung mit Wasser und Mineralien aus dem Boden sorgt für die Zellproduktion und das Wachstum des Baumes. In einem Jahr mit viel Sonnenschein und ausreichend Regen, ohne Krankheiten oder unerwarteten Ereignissen, wächst der Baum mehr. Er bildet dann einen breiteren Baumring. Wenn ein Baum gefällt wird, kann man die Jahresringe zählen. Ein dünner Jahresring bedeutet es war ein schwieriges Jahr für den Baum. Das Wetter war dann zu trocken oder zu nass.

Dendrochronologen bohren eine Probe aus dem Stamm eines lebenden Baumes, um genau zu wissen, wie alt er ist, und um vergangene Wetterbedingungen zu analysieren. Bäume sind also lebende Klimaarchive!

### Type Sensor

Für dieses Projekt verwenden wir Band-Dendrometer. Ein Metallband wird um den Baumstamm geschnallt. Der schwarze Zylinder ist das Messgerät, das sich auf und ab bewegt und die Messungen über das LTE-Netz oder das Mobiltelefonnetz überträgt. 
