Wanneer is magie ontstaan
waar komt magie vandaan
hoe kwamen die vlinders tot stand
Hoe kwam mijn hart in brand
Gevoel van warmte overspoelde mij 
Jij maakte mij ongelofelijk blij
Niet alleen woorden maar ook daden
Daardoor kruisten onze paden 
Toen liepen we hand in hand
Zo kwam de magie tot stand.
door J.strijbos, 2004

De magie van kaarsen
Twaalf kaarsen branden
rondom
de woorden die ik schrijf
De draak
bewaakt mijn elfen
in de kamer waar ik verblijf
Vele vlammen dansen
als magische wezens
mijn wereld in
Hun eigen taal
ieder een eigen verhaal
tekens een nieuw begin.
door Almaresáre, 2005

Vloeiend zacht
spreiden haar ogen
hun warme blik
Iedereen voelt de
licht intieme klik
als zijn ziel ontwaakt
Het contact met
gelijken is weer even
in de hemel verblijven
Het goddelijk moment
waarvan de magie 
overal wordt herkend
Veraf en toch 
intens dichtbij zo delen 
wij haar mystieke zijn.
door wil melker, 2019

Ben jij
de schepper van de beelden
die je aanreikt in woorden
De god
die uit het niets
een werkelijkheid creëert
Of schenk jij ons
met stem en ogen 
de ongeziene wereld van vandaag
Met highlights
en een andere modulatie
raak jij ons met nieuw elan
Alleen poëzie 
heeft dan de magie
van het luisterrijk bekijken.
door wil melker, 2013

Een pad in het bos
paddenstoel tovert uit hoed
diverse sporen
door Catherine Boone, 2023

Tover nooit uit dozen
heb niet
met magie gespeeld
tover nooit uit dozen
ben ooit een
goochelaar geweest
blijf in mystiek geloven
alle sprookjes
kwamen boven van
goede fee tot boze geest
samen hebben we genoten
aan de rand van het bos de
oude vos is er nog steeds
steelt in plots bewegen
de magie van het ogenblik
door wil melker, 2008

Ik kreeg opeens een shot 
adrenaline 
Die rush ging als vanzelf, 
puur intuïtie 
Een solo uit het niets - 
geen exercitie 
Pure magie, da capo 
tot al fine.
door Maxim, 2020

Ons leven vol illusies
maar is het niet heerlijk
om juist de magie
ervan
vast te houden
in de palm 
van je hart
te dromen
dat je
de wind hoort fluisteren
en je eigen schaduw
je toe lacht.
door windwhisper, 2009

Ik ben een vrouw,
ik ben krachtig;
Mijn magie is machtig:
ik klauw en ik miauw,
ik stoei spinnend met jou
en ben dan drachtig;
Ik ben een vrouw,
ik ben krachtig;
In mij groeit aandachtig
het leven dat ik brouw,
het is prachtig.
door Jack Stoop, 2023

Toen
wat dagen geleden
verdwenen verdwaalde regen
onder een bloedrode maan
waarheen wist geen mens
en waarvandaan komt een geest
Een fles binnen
met een liefdevolle boodschap
alles zwicht onder licht
dat op een manier
van magie gebonden
is
door marcus neuborg, 2014

Podium
woorden vol magie 
de lucht 
met schittertjes 
echo's 
luisteren niet 
Stilte flitst 
in tover 
neem ogen mee 
steel gedachten 
open maar 
Lach is een 
ontlading waard 
hemel wordt 
voor eventjes geaard
door wil melker, 2005

Het nachtelijk blauw
vlijt zich als een donker kleed
over de aarde
de wereld in een andere
dimensie hullend
Hoorbare
stilte
omhulling
en
weidsheid
biedend
De magie
van de nacht
neemt mij
bij de hand
laat mij
een moment
het contact
met de eeuwigheid 
ervaren
door mine stemkens, 2010
