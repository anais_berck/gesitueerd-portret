from situated_portrait.models import Tree, Sensor, Observation, get_range
import datetime
import pytz
from collections import defaultdict

# --- GLOBAL SUBTITLES ACROSS ALL TREES
# --- 1 sensor with highest/lowest last value to indicate direction
# --- maybe TODO: comparing value to previous value ???

## download laatste versie van database:
# scp debian@91.134.100.3:/srv/bomenvertellen/gesitueerd-portret/situated_portrait/db.sqlite3 ./

## run script
# ./manage.py shell < exercises/subtitles_global.py

def find_time(nr):
    # --- CHOOSE TIME WINDOWS
    now = datetime.datetime.now(pytz.utc)
    yesterday = now - datetime.timedelta(days = nr)
    return now, yesterday

# TREE CLOCK FUNCTION:
# a tree clock contains the observations
# linked to a tree
# over a choosen window of time
# alongside its min, max and average over that window of time
# and sorted by sensor type

# Find last and second last values for all sensors at all trees using the tree clock function
# nr sets amount of days you want to go back
def last_values_all_trees(nr):
    collection = []
    last_observations = defaultdict(list)
    # find time
    now, yesterday = find_time(nr)
    # select all trees
    trees = Tree.objects.all()
    # get clock observations for all trees in set time
    for tree in trees:
        tree.clock = tree.get_clock(yesterday, now)
        # get all last values of all trees with their timestamp and unit
        for sensor_type, observation_per_tree in tree.clock.items():
            if 'serie' in observation_per_tree and len(observation_per_tree['serie']) > 0:
                data = [tree.local_name, observation_per_tree["unit"], observation_per_tree["last"], observation_per_tree["hour_last"], observation_per_tree["second_last"],  observation_per_tree["hour_second_last"], observation_per_tree['max'], observation_per_tree['min']]
                couple = observation_per_tree["sensor_quality"], data
                collection.append(couple)
    for k, v in collection:
        last_observations[k].append(v)
    last_observations = list(last_observations.items())
    return(last_observations)

# find highest value for specific sensor quality
# this function returns 4 lists, each list contains:
# name of tree, unit, last value, hour of last value, second last value, hour of second last value
def high_low(list, update):
    values_last = []
    values_second_last = []
    for item in list:
        # find specific sensor quality & all values for all trees related to that quality
        if item[0] == update:
            values = item[1]
            # collect last and second last values in 1 list
            values_last = [value[2] for value in values]
            values_second_last = [value[4] for value in values]
            # find maximum & min values
            max_last = max(values_last)
            max_second_last = max(values_second_last)
            min_last = min(values_last)
            min_second_last = min(values_second_last)
            # find other information
            for v in values:
                if max_last in v:
                    max_list = v
                if max_second_last in v:
                    second_max_list = v
                if min_last in v:
                    min_list = v
                if min_second_last in v:
                    second_min_list = v
    return max_list, second_max_list, min_list, second_min_list


def compare(list):
    last = list[2]
    second = list[4]
    if last >= second:
        difference = last - second
        ratio = "meer"
    else:
        difference = second - last
        ratio = "minder"
    return difference, ratio

def symphony_subtitles(update, nr):
    translations = {'circular increment': ['stambeweging', 'ik', 'ervaar'], 'air humidity': ['luchtvochtigheid', 'bladeren', 'ervaren'], 'air pressure': ['luchtdruk', 'bladeren', 'ervaren'], 'air temperature': ['temperatuur', 'bladeren', 'ervaren'], 'moisture': ['bodemvochtigheid', 'wortels', 'ervaren'], 'temperature': ['bodemtemperatuur', 'wortels', 'ervaren'], 'Sweet chestnut': 'Kastanje', 'European oak': 'Boseik', 'Douglas fir': 'Douglas', 'Red beech': 'Rode Beuk', 'Solitary oak': 'Allenige Eik', 'European horse-chestnut': 'Paardenkastanje'}
    # # Find last and second last values for all sensors at all trees using the tree clock function
    # nr sets amount of days you want to go back
    last_observations = last_values_all_trees(nr)
    # get observations with last and second last highes/lowest values
    max_list, second_max_list, min_list, second_min_list = high_low(last_observations, update)
    # difference with second last value
    difference_max, ratio_max = compare(max_list)
    difference_min, ratio_min = compare(min_list)
    # write subtitles
    # highest
    subtitles_high = "De hoogste {} vind je bij {}: {} {} om {} u. \n Dat is {} {} {} dan de vorige meting om {} u.".format(translations[update][0], translations[max_list[0]], "{:.2f}".format(max_list[2]), max_list[1], max_list[3], "{:.2f}".format(difference_max), max_list[1], ratio_max, max_list[5])
    # lowest
    subtitles_low = "De laagste {} vind je bij {}: {} {} om {} u. \n Dat is {} {} {} dan de vorige meting om {} u.".format(translations[update][0], translations[min_list[0]], "{:.2f}".format(min_list[2]), min_list[1], min_list[3], "{:.2f}".format(difference_min), min_list[1], ratio_min, min_list[5])

    return subtitles_high, subtitles_low
# --- -------------------------


# find highest/lowest value and second last value for specific sensor quality
# updates can be (type sensor): circular increment, air humidity, air pressure, air temperature, co2, moisture, temperature
update = 'circular increment'

# difference with second last value
subtitles_high, subtitles_low = symphony_subtitles(update, 1)
#print(subtitles_high, subtitles_low)


# ---- COMMENTS

'''
# --- POSSIBLE SENSOR TYPES

sensoterra-temperature
sensoterra-moisture
sensecap-air_temperature
sensecap-air_humidity
sensecap-co2
ems_brno-circular_increment
milesight-temperature
milesight-humidity
milesight-co2
milesight-pressure
decentlab-senseair-lp8-co2
# decentlab-implexx-sapflow-sap-flow
# decentlab-apogee-sp110-pyr


# --- POSSIBLE SENSOR QUALITIES

temperature
moisture
air humidity
co2
air pressure
air temperature
Circular increment
'''
