First script to extract data through the sensoterra API.

To install:
1. Install the requirements through the `requirements.txt`
2. Set the username and password through the local settings file. Save `local_settings.example.py` as `local_settings.py` and enter username and password.