## Meet a Gatekeeper and five other unusual trees

Until December you can meet six Arnhem trees in a unique way. Listen to a 374-year-old Poortwachter (Gatekeeper) in Gulden Bodem. What stories did a tree whisperer pick up? How does the Red Beech in Zypendaal breathe? And does he grow every day? What is normally hidden from us can now be discovered. As part of the 125the anniversary of Sonsbeek Park, there is a special walking route "Arnhemse Bomen Vertellen". Visit the artistic installation from 15 June to 15 December 2024 in Sonsbeekpark, Zijpendaal and Gulden Bodem.

Experience the parks and their trees in an innovative way. Establish a relationship with the trees and become more aware of the natural and cultural heritage the parks have to offer. This art project is a creation by [Anaïs Berck](https://www.anaisberck.be/), a collective of humans, trees and algorithms.

### Moving trees

We usually just walk past the trees of Arnhem. They seem pretty regular to us and if we notice them at all, they always look the same – whether yesterday, today or tomorrow. Our houses are made with wood. Trees also give us medicine, food, warmth, oxygen, and we seek their shade during hot summers.

During ‘Arnhemse Bomen Vertellen’, you will meet trees as you meet a work of art. The three parks are home to very special and old trees. Using sensors installed in six trees, ‘Arnhemse Bomen Vertellen’ makes visible what is otherwise hidden from the human eye: the movements of the trunk, the CO2 and water supply, the temperature in the air and at the roots, and the amount of sunlight that helps the trees in their photosynthesis.

### Always different

From hour to hour, you can see changes in the trunk of the tree. The installation invites you to appreciate these special trees as active, growing, changing living beings – beings you can also enter into a relationship with.

You will get to know the intimate life of the Solitary Oak in the Moerasweide, the Horse Chestnut near the Witte Villa, the European Oak on the way to the Belvedere Tower, one of the Red Beeches near Zijpendaal Castle, one of the Gatekeepers at Gulden Bodem, and a Douglas Fir in Schaarsbergen-Zuid.

### QR code in bench

Near each of the trees, a wooden bench has been placed with a QR code on it. These benches are made of wood from trees of Arnhem. By scanning the QR code, you will be guided to a web page where story fragments, an artistic soundscape and digital images will provide insight into the life and history of the tree. What you get to see and hear depends on your location and the data sent by the sensors. Let yourself be surprised – every hour, every day.

## Practical

The starting point is [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), where you will find a paper map with the route:

- a 7 km walk along the six trees
- a 4 km walk along five trees, accessible for the disabled

For regular visitors to the parks, it is possible to meet one or more trees on a daily basis, to get to know them and make new, lifelong friendships. Visitors who don’t have a smartphone or prefer not to walk can visit the portraits of the six trees using the touchscreen in the visitor centre at Molenplaats Sonsbeek. Here you will also find a small exhibition with artistic visualizations based on the data sent by the sensors.

Visitor centre [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/) is open from Wednesday till Saturday from 10.00 till 16.00h. Sunday from 11.00 till 16.00h.


## Credits

<small markdown>

This project was realised by [Anaïs Berck](https://www.anaisberck.be/), a collective of people, trees and algorithms:

- Artists collaborating on this project: An Mertens (concept, coordination, tree clerk), Gijs de Heij & Doriane Timmermans (code, design & visualizations - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (sound), Bram Goots (photos), Loïc Lachaize (mastering), Floris van Hintum (benches/stand), Patrick Lennon & Karin Ulmer (proofreading)
- Trees participating in this project: Gatekeeper Chestnut, Red Beech Zijpendaal, Horse Chestnut Witte Villa, Solitary Oak Sonsbeek, European Oak Ronde Weide Sonsbeek, Douglas Fir Schaarsbergen-Zuid
- algorithms collaborating on this project: Decentlab sap flow sensor, sunlight sensor, CO2 & temperature sensor; EMS Brno tape drometers, Sensoterra soil moisture and temperature sensors, Milesight CO2 sensors, SenseCap CO2 sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

We thank for their advice: dr Ute Sass-Klaassen (Hogeschool Van Hall Larenstein), Hélène van Ginderachter, ir Erik Mertens, Joost Verhagen (Cobra-Groen in zicht), Judith Van Ginneken, Marleen Verschueren, Erik De Wilde.

We thank for sharing knowledge, experiences, and stories: park guide Eddy van Heeckeren, forest expert Simon Klingen, ecologist Hans Van den Bos, artist Boudewijn Corstiaensen, park administrators Jan Floor, Rob Borst, senior administrative adviser on green spaces Marcel Wenker, nature guide and biologist Pim van Oers, park guide Gerard Herbers, artist Albert van der Weide, forester Nina Kreisler, author Jibbe Willems, city ecologist Ineke Wesseling, artist Bartaku Vandeput (Baroa belaobara).

‘Arnhemse Bomen Vertellen’ is grateful for the cooperation and support of the municipality of Arnhem and Natuurcentrum Arnhem.

The sensors and benches were installed with the very kind help of the tree care staff from the municipality of Arnhem. Thank you for your help and patience: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

This project was made possible thanks to the support of Marko Van der Wel (Nature, Environment, Care, Local Food, Water), park manager Rob Borst and senior management advisor green living environment Marcel Wenker. Communication was ensured by Sanne Blok, Hanneke Busscher and William Hulstein; production by Karlijn Michielsen. Thank you!

Arnhemse Bomen Vertellen, by Anaïs Berck, Arnhem, June 2024.
All code, images, sounds and texts are published to a Creative Commons Attribution 4.0 licence (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). You may reuse, distribute, reproduce, and adapt this work in any medium, except for commercial purposes, provided you give attribution to the copyright holder, provide a link to the Creative Commons licence, and indicate if changes have been made.

All code, images, sounds and texts have also been released under a CC4r licence, available here: <https://constantvzw.org/wefts/cc4r.en.html>.

The data captured during this work is published to a Creative Commons 1.0 licence (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). You can copy, modify, distribute and perform the data, even for commercial purposes, all without asking permission.

</small>
