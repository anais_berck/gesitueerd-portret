Situated Portrait is an artistic research project that aims to focus on trees like a work of art. Using sensors that we place in trees, we artistically try to make visible what is otherwise hidden from the human eye: the movement of the trunk, the CO2 and water supply, the temperature in the air and at the roots.

With the support of Vlaamse Overheid, Kunsten & Erfgoed.

![](/static/img/trees/beuk.jpg)

<!-- More information about: -->

- [the project](project/)
- [the web app](webapp/)
- [the sensors](sensors/)
- [the tree](trees/)
- [the artists and all the people involved in the project](artists/)
- [the licence](licence/)
