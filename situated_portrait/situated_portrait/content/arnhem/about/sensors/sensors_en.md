# Sensors

As artists, we want to connect park visitors with different trees. We create a unique and aesthetic experience for people from diverse backgrounds, ages and cultures.

To do this, we 'improperly' use sensors developed to make trees and plants grow ‘optimally’. For this project, it is neither interesting nor possible to make statements about the well-being of trees. Trees are complex creatures. Measurements and setting ‘standards’ depend on soil type, location, tree species and the age of the tree.

That is why we make the sensors visible, audible and tangible in the moment. This is how you see that trees are alive.

<!-- Sensors used in this project: -->

- [Soil moisture](soil_moisture/)
- [Soil temperature](soil_temperature/)
- [Movements trunk](mouvement_trunk/)
- [Sap flow](sapflow/)
- [Air humidity](air_humidity/)
- [Air temperature](air_temperature/)
- [CO2](co2/)
- [Sunlight](sunlight/)
