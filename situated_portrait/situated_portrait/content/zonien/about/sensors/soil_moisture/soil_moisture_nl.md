## Bodemvochtigheidsensor

![](/static/img/sensors/sensoterra-600x.jpg)

### Herkenbare patronen

Bodemtype
:   elke sensor wordt ingesteld op het specifieke bodemtype waarin de metingen worden gedaan, in dit geval gaat het om een leembodem.

Regen
:   hoe meer regen, hoe natter de grond. De aanhoudende regenval van de voorbije winter en lente zorgt voor uitzonderlijk vlakke lijnen in de grafieken over langere tijd.

Warm & droog
:   op een warme dag is de bodem minder vochtig


### Bodemvochtigheid & Bomen

Bomen hebben veel water - en mineralen - nodig voor het aanmaken van suikers via fotosynthese (zie CO2-sensor). De bomen transporteren het water van de wortels naar de kruin via het xyleem. Dat zijn houtvaten net onder de schors, die na elk seizoen dood hout worden en zo de jaarringen van de boom vormen. Op een warme zomerdag kan een Beuk bijvoorbeeld tot 800 liter water per dag transporteren.

### Type sensor

De bodemvochtigheid wordt gemeten met sensoren van de Nederlandse start-up Sensoterra. Ze meten de vochtigheid van de grond op 30cm diepte.

De sensoren zijn geplaatst op de 'drip line' van de bomen. Dat is de ingebeelde lijn die loopt langs de uiterste bladeren van de kruin. Langs de dripline groeien de meest levendige oppervlaktewortels.
