import markdown
from parse_stories import make_stories_json
import json
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os.path, os
import shutil

# Images are attached to parts and organized in folders
# Discover those images and return a data structure with these images
# {
#   'roots': [ str:path, ... ],
#   'trunk': [ str:path, ... ],
#   'canopy': [ str:path, ... ]
# }
# Assume it's only jpgs
def discover_images (path):
    parts = ['roots', 'trunk', 'canopy']
    images = { part: [] for part in parts }
    allowed_extensions = ['.jpg', '.jpeg', '.png', '.gif', '.webp']
    
    for part in parts:
        if os.path.isdir(os.path.join(path, part)):
            for img in os.listdir(os.path.join(path, part)):
                ext = os.path.splitext(img)[1]
                if ext and ext.lower() in allowed_extensions:
                    images[part].append(os.path.join(path, part, img))
    
    return images

# Make backup of old site
if os.path.exists('output'):
    if os.path.exists('output.bak'):
        shutil.rmtree('output.bak')    

    shutil.move('output', 'output.bak')

# Make new output folder
os.mkdir('output')

# Copy over static files
shutil.copytree('static', 'output/static')

# Ensure theres is a data dir
if not os.path.exists('output/static/data'):
    os.mkdir('output/static/data')

if not os.path.exists('output/static/data/stories'):
    os.mkdir('output/static/data/stories')

# Make stories
stories_json_paths = make_stories_json('stories', 'output/static/data/stories', 'static/data/stories')

env = Environment(
    loader=FileSystemLoader("templates"),
    autoescape=select_autoescape()
)

# Load page data for the trees, holds general about and 
# introduction of the trees and generate pages
page_data = json.load(open("static_pages.json", "r"))
template = env.get_template("tree.html")

languages = ['nl', 'en']

for qr_id, tree in page_data['trees'].items():
    ctx = { 
        'image': tree['image'],
        'images': discover_images(os.path.join('static/img/trees', tree['slug'])),
        'stories_json_path': stories_json_paths[tree['slug']],
        'translations': [{
                'language': language,
                'tree': tree[language],
                'about': markdown.markdown(page_data['about'][language]) ## parse markdown
        } for language in languages]
    }
    with open(f"output/{ qr_id }.html", "w") as h:
        h.write(template.render(ctx))



# for tree in trees:
