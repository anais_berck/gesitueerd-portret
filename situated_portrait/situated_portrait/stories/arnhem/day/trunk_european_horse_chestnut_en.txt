I have lived here for many years. It is impossible for me to leave here.
We plants are sedentary. We cannot flee.
That makes us built differently from you, humans.
To have one heart in one well-defined place, or one lung, or one place for one set of brains,
that would very soon kill us.
We wouldn’t be able to defend ourselves against an enemy who tears off a piece of our body.
We therefore grow thousands of sex organs, and as many noses as there are leaves, trunk and roots.
We always have a Plan B.
We are modular.
We are not one I, but thousands of Is at once.
We can say I with any particle of our body.

With the voyages of discovery and the creation of botanical gardens, you humans became interested in exotic tree species. In the eighteenth century, when rich people could not yet show off expensive cars, we became status symbols. Large gardens and private parks showed prosperity, with trees brought in from far away. I too was planted because I was special, even though the first White Horse Chestnut was imported into the Netherlands as early as 1557. In the Balkans, you can still find us in the wild.

See those bright yellow spots with cups on them on my trunk? That is the Orange Lichen. It is a successful collaboration between fungi and algae. One finds water and minerals in my bark; the alga protects the fungus from the Sun and produces sugars through photosynthesis. 
The Orange Lichen has not lived here very long. It chooses places with lots of exhaust gas. You will find it in other places in the city. After all, it loves nitrogen. 

I was planted here in 1836 by a gardener of Baron van Heeckeren van Enghuizen, who purchased this estate with the inheritance of Elisa Hope, his wife, a wealthy banker's daughter. At the time, more gardeners worked here. The baron had the entire estate converted into an English-style park landscape: hills were moved, ponds built, romantic vistas and peepholes created. Incalculable treasures were spent here, the consequences of which you can still see today.

My childhood was spent during relatively major works on the villa, the entrance, the park. Baron van Heeckeren van Enghuizen and his wife Elisa Hope had four children, three of whom died in infancy. The fourth, who succeeded his father in managing this estate in 1862, was Louis Evert. The boy was already saddened by the loss of his brothers and sister, but when he also had to say goodbye to his wife after six years of marriage, this part of Sonsbeek was shrouded in gloom. 

My beloved but sad master, Louis Evert van Heeckeren van Enghuizen, died in an insane asylum at the age of 53 in 1883. His son, Willem Frederik, aged 25, then became the caretaker of this estate. The boy loved travelling, hunting and spending money. Large parts of this estate were sold and parcelled out to meet his needs. 

I almost lost my life at the end of the nineteenth century. It was the then land agent, Jacques Mulder, who saved my life. Baron van Heeckeren, who was staying in Paris, was in need of money. He wrote the land agent a telegram instructing him to cut down and sell all the rising timber on Sonsbeek. Jacques and his father went to Paris. They met the baron in a luxurious hotel. If he wanted to cut down Sonsbeek, they would resign. The baron came to his senses and, on their advice, decided to sell a large number of other lands.

At the end of the nineteenth century, my life was at risk a second time. Two property developers nurtured a plan to exploit this estate as a building site. 
Fortunately, we could count on the commitment of a visionary civil servant. The director of the municipal works department, Mr J.W.C. Tellegen, managed to convince the mayor and aldermen to buy this estate and open it as a public park for the residents of Arnhem. They spent a large part of the municipal budget and took out a hundred-year loan for a million guilders. This year, we too, all the old trees that escaped clear-cutting, are celebrating Sonsbeek Park's 125th anniversary with great joy. 

After the park was set up for the residents of Arnhem in 1899, my life changed considerably. Just one year later, for instance, the first lanterns were installed. Even today, their light makes me work longer hours. Benches also appeared, attracting many visitors, who talk, laugh, drink and smoke until late in the summer. It took many generations for all the birds, insects and spiders even to adapt to so many presences. 

A hundred and twenty-five years ago, when Sonsbeek had become the property of the municipality of Arnhem, I began to witness regularly all kinds of large and small events. Large-scale operas and operettas, congresses, parades, circuses, competitions, markets, fireworks – anything is possible here. Recently, the ground under my crown got a serious treatment. So much passage from you humans had compacted the soil, leaving my roots barely able to move.
