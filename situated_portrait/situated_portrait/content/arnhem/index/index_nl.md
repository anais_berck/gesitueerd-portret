## Ontmoet een Poortwachter en vijf andere bijzondere bomen

Nog tot december kan je Arnhemse bomen op een unieke manier ervaren. Luister naar een 374 jaar oude Poortwachter in Gulden Bodem. Welke verhalen heeft een bomenfluisteraar opgevangen? Hoe ademt de Rode Beuk in Zypendaal? En groeit hij elke dag? Wat normaal voor ons verborgen blijft valt nu te ontdekken. In het kader van 125 jaar Sonsbeekpark is er een bijzondere wandelroute “Arnhemse bomen vertellen”. Bezoek de artistieke installatie van 15 juni tot 15 december 2024 in Sonsbeekpark, Zijpendaal en Gulden Bodem.

Beleef de parken en haar bomen op een vernieuwende manier. Ga een relatie aan met de bomen en word je meer bewust van het natuurlijk en cultureel erfgoed dat de parken te bieden hebben. Dit kunstproject is een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes.

### Bewegende bomen
Arnhemse bomen, we lopen er doorgaans gewoon voorbij. We vinden ze normaal en als we ze al opmerken, zien ze er hetzelfde uit als gisteren, als morgen en overmorgen. Onze huizen zijn gemaakt met hout. Bomen geven ons ook medicijnen, eten, warmte, zuurstof en we zoeken hun schaduw op tijdens hete zomers. Wist je dat een boom van uur tot uur beweegt? Dat kan je met ‘Arnhemse Bomen Vertellen' ervaren.

Tijdens ‘Arnhemse Bomen Vertellen’ sta je stil bij bomen, zoals bij een kunstwerk. In de drie parken leven zeer bijzondere en oude bomen. Aan de hand van sensoren die in zes bomen zijn geïnstalleerd, maakt ‘Arnhemse Bomen Vertellen’ zichtbaar wat anders verborgen blijft voor het menselijke oog: het bewegen van de stam, de CO2- en wateraanvoer, de temperatuur in de lucht en bij de wortels, de hoeveelheid zonlicht die de bomen helpt bij hun fotosynthese.

### Altijd anders
Van uur tot uur zie je veranderingen in de stam van de boom, waardoor die boom gaat leven. De installatie nodigt bezoekers dan ook uit om deze bijzondere bomen te waarderen als actieve, groeiende, veranderende levende wezens, waarmee je ook een relatie kan aangaan.

Bezoekers maken kennis met het intieme leven van de Allenige Eik in de Moerasweide, de Paardenkastanje bij de Witte Villa, de Boseik onderweg naar de Belvedère-toren, één van de Rode Beuken bij het Kasteel van Zypendaal, één van de Poortwachters bij Gulden Bodem en een Douglasspar in Schaarsbergen-Zuid.

### Bankjes met QR-code
Bij elk van de bomen is een houten bankje geplaatst met daarop een QR-code. Als je die scant, word je naar een webpagina geleid, waar verhaalfragmenten, een artistieke soundscape en digitale beelden je inzicht geven in het leven en de geschiedenis van de boom. Wat je te zien en te horen krijgt, hangt af van je locatie en van de data die de sensoren verzenden. Laat je verrassen, elk uur, elke dag.



## Praktisch

Het vertrekpunt is [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), waar je een kaart met de route vindt:

- wandeling van +-7km langs de 6 bomen
- wandeling van 4km langs 5 bomen, toegankelijk voor mensen met een beperking.

Voor regelmatige bezoekers van de parken bestaat de mogelijkheid om één of meerdere bomen dagelijks te ontmoeten, hen te leren kennen en nieuwe, levenslange vriendschappen te sluiten. Bezoekers die geen smartphone hebben of liever niet wandelen, kunnen de portretten van de zes bomen bezoeken via een touchscreen dat opgesteld staat in bezoekerscentrum Molenplaats Sonsbeek. Daar vind je ook een kleine tentoonstelling met artistieke visualisaties op basis van de data die de sensoren uitsturen.

Bezoekerscentrum [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/) is open van woensdag t/m zaterdag van 10.00 tot 16.00 uur. Zondag van 11.00 tot 16.00 uur.

## Credits

<small markdown>

Dit project wordt gerealiseerd door [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes:

- kunstenaars die meewerken aan dit project: An Mertens (concept, coördinatie, boomgriffier), Gijs de Heij & Doriane Timmermans (code, ontwerp & visualisaties - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (geluid), Bram Goots (foto’s), Loïc Lachaize (mastering), Floris van Hintum (bankjes/meubel), Patrick Lennon & Karin Ulmer (eindredactie EN/D)
- bomen die meewerken aan dit project: Poortwachter Kastanje, Rode Beuk Zijpendaal, Paardenkastanje Witte Villa, Allenige Eik Sonsbeek, Boseik Ronde Weide Sonsbeek, Douglasspar Schaarsbergen-Zuid
- algoritmes die meewerken aan dit project: Decentlab sapstroomsensor, zonlichtsensor, CO2 & temperatuursensor; EMS Brno banddendrometers, Sensoterra bodemvochtigheids- en temperatuursensors, Milesight CO2-sensors, SenseCap CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programmeertaal, html, css.

Wij danken voor hun advies: dr. Ute Sass-Klaassen (Hogeschool Van Hall Larenstein), Hélène van Ginderachter, ir. Erik Mertens, Joost Verhagen (Cobra-Groen in zicht), Judith Van Ginneken, Marleen Verschueren, Erik De Wilde.

Wij danken voor het delen van kennis, ervaringen en verhalen: parkgids Eddy van Heeckeren, bosdeskundige Simon Klingen, ecoloog Hans Van den Bos, kunstenaar Boudewijn Corstiaensen, parkbeheerders Jan Floor, Rob Borst, senior bestuursadviseur groene leefomgeving Marcel Wenker, natuurgids en bioloog Pim van Oers, parkgids Gerard Herbers, kunstenaar Albert van der Weide, boswachter Nina Kreisler, auteur Jibbe Willems, stadsecoloog Ineke Wesseling, kunstenaar Bartaku Vandeput (Baroa belaobara).

“Arnhemse Bomen Vertellen” is dankbaar voor de medewerking en ondersteuning van de gemeente Arnhem en Natuurcentrum Arnhem.

Wij danken de boomverzorgers van de Gemeente Arnhem voor het installeren van de sensoren en de bankjes: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

Dit project kwam tot stand dankzij ondersteuning van Marko Van der Wel (wethouder Natuur, Milieu, Zorg, Lokaal Voedsel, Water), parkbeheerder Rob Borst en Senior bestuursadviseur Groene Leefomgeving Marcel Wenker. Communicatie werd verzekerd door Sanne Blok, Hanneke Busscher en William Hulstein; productie door Karlijn Michielsen. Met dank!

Arnhemse Bomen Vertellen, door Anaïs Berck, Arnhem, juni 2024.
Alle code, afbeeldingen, geluiden en teksten zijn gepubliceerd onder een Creative Commons Attribution 4.0-licentie (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). Je kan dit werk op elk medium hergebruiken, verspreiden, reproduceren en aanpassen, maar niet voor commerciële doeleinden, en op voorwaarde dat je de auteur vermeldt, evenals een link naar de Creative Commons-licentie en vermelding van de wijzigingen die je maakte.

Alle code, afbeeldingen, geluiden en teksten zijn ook gepubliceerd onder een CC4r-licentie, die hier beschikbaar is: <https://constantvzw.org/wefts/cc4r.en.html>.

De gegevens die tijdens dit werk werden gegenereerd, zijn gepubliceerd onder een Creative Commons 1.0-licentie (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). Je kan deze data kopiëren, wijzigen, en opnieuw verspreiden, zelfs voor commerciële doeleinden, zonder toestemming te vragen.
</small>
