(function () {
  browser.runtime.onMessage.addListener((message) => {
    if (message.command === "loadMoreRecords") {
      console.log("Getting more records?");
      let button = document.querySelector('[data-analytics-label="device detail_data history tab load more"]');
      console.log(button);
      if (button) {
        // button.dispatchEvent(new Event('click'));
        button.click();
      }
    }
  });
})();