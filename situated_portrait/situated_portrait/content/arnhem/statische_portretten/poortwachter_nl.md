## Poortwachter

![](/static/img/trees/3watchers-600x.jpg)

Meer dan 375 jaar geleden werden wij, drie tamme Kastanjes, aangeplant als eerbetoon aan de Romeinen die onze soort tot in het Noorden brachten. We staan hier bijna even lang als de Eusebiuskerk. Ooit kregen we bezoek van Rembrandt. Heb je ons eenmaal ontmoet, dan reizen we graag mee in je herinnering.
Als Poortwachter ben ik een leermeester over de Tijd in al haar belevingen. Vanuit mijn celgeheugen deel ik verhalen over bomen in wereldse mythen en tradities. 's Nachts reciteer ik gedichten over Tijd.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
