Meine Früchte sind seit Jahren nicht mehr geerntet worden.
Früher war das anders.
Eine würdevolle alte Dame, die gestern mit kurzen Schritten hier vorbeikam
erinnerte sich.
Als Kind kam sie im Herbst jeden Tag, um meine Eicheln zu sammeln.
Sie haben sie getrocknet und geröstet,
zermalmt,
und sie in Getreidekaffee verwandelt.
Es hat furchtbar geschmeckt.
Es war also Krieg.

Wir sind Lichtwesen. Wir ernähren uns vom Licht. Unsere Zellen, unser Holz, unsere Früchte sind aus Sonnenlicht gemacht.
Wann immer Sie etwas Pflanzliches essen, sollten Sie wissen, dass Sie sich von Licht ernähren. 
Lassen Sie sich erleuchten!

Ich habe viele Bewohner und treue Besucher.
Ich bin wie das New York der Niederlande, der Baum, in dem die meisten Vogel-, Insekten-, Pilz-, Moos- und Tierarten Schutz und Nahrung finden. Wenn Sie sich meine Wurzeln, meinen Stamm und meine Krone als Abbild vorstellen, würden Sie einen Wolkenkratzer neben dem anderen sehen.
Dagegen hat ein Neuankömmling wie die Platane kaum Besucher oder Bewohner.
Aber eine uralte Spezies wie ich, die hier seit mehr als zehntausend Jahren gedeiht, ist mit allem, was lebt, bis in die kleinsten Fasern verwoben.

Die Eichelhäher sind gute Freunde von mir. Vielleicht finden Sie eine ihrer blauschwarz gestreiften Federn, wenn Sie den Park besuchen.
Niederländischsprachige haben ihm den Namen 'der ständig scharrende Eichelsucher' gegeben.
Er ist sehr laut, aber das vor allem im Herbst, wenn er meine Eicheln für den Winter im Boden versteckt.
Mit etwas Glück vergisst er dann, sie wieder auszugraben. So können meine kleinen Sprösslinge an den unerwarteten Orten auswachsen.

In diesem Frühjahr hatte ich wieder das Vergnügen, ein Krähennest hoch oben in meinen Ästen zu beherbergen.
Zweig für Zweig schichteten und flochten die Krähen sie zu einer Plattform, die sie zusätzlich mit Erde und dickeren Ästen befestigten.
Jahr für Jahr überraschen sie mich auch mit den Materialien, die sie zum Auskleiden ihrer Nester finden. Früher rupften sie Wollfetzen aus dem Vlies der Schafe, die weiter weg lebten. Jetzt finden sie Wolle, Draht und Papier, die von Menschen verarbeitet wurden. Und natürlich viele Federn und Gräser, die so hoch oben in meiner Krone einen besonderen Duft verbreiten.

Meine Früchte wurden einst zur Schweinemast verwendet. Sie sind sehr nahrhaft und enthalten mehr als ein Drittel Fett. ‘Der beste Speck wächst auf Eichen’, ist ein altes niederländisches Sprichwort, das ich treu in meinem Zellgedächtnis bewahre. Dank der Schweine genossen wir auch einige Privilegien: Wir durften nicht gefällt werden, und wir Stieleichen, die mehr und größere Eicheln produzieren als die Traubeneiche, wurden weiterverbreitet.

Jeden Sommer bekommen wir Besuch von ein paar Baumfalken. Sie lieben die Bäume hier rund um den Wildpark. Sie brüten etwas später und nutzen die große Anzahl an Libellen in diesem Park. Mit diesen füttern sie ihren Nachwuchs. Wenn die Libellenpopulation Ende August verschwindet, verjagen die Eltern ihre Jungen aus dem Territorium. Dann müssen sie sich selbst Nahrung suchen. Einige jagen Stare, andere Fledermäuse. Wenn Sie in der Abenddämmerung kommen, sehen Sie vielleicht einen Baumfalken auf meinem Ast sitzen, der darauf wartet, dass Fledermäuse auftauchen.

Eichhörnchen sind meine treuen Freunde. Nein, ich bin nicht naiv: Ich weiß, dass es zum Teil daran liegt, dass sie meine Früchte lieben. Leider können sie sie nicht einfach aus meiner Krone pflücken. Meine Eicheln enthalten dafür zu viel Blausäure, die für sie ziemlich giftig ist. Nur Wildschweine vertragen sie.
Eichhörnchen vergraben meine Eicheln als Nahrung für den Winter. Und die Blausäure ist dafür nützlich; sie hilft dabei, meine Eicheln zu konservieren. Mit der Zeit verschwindet die Blausäure. Manchmal sehe ich im Winter ein Eichhörnchen eine meiner Eicheln ausgraben, daran riechen und ein Stück abknabbern. Um sicherzugehen, dass sie essbar ist!

Die schönen Eichelhäher mit ihren blauschwarz gestreiften Federn lieben meine Eicheln. Manche gehen dabei sehr hinterlistig vor. Sie verstecken sich hoch oben in meiner Baumkrone und beobachten, wie das Eichhörnchen darunter seinen Eichelvorrat ausgräbt. Der Eichelhäher kann dann einen Alarmton ausstoßen, der das Eichhörnchen zur Flucht veranlasst. Nach zwei oder drei Malen lässt sich das Eichhörnchen so nicht mehr täuschen. Aber der Eichelhäher ist ein Meister der Nachahmung. Mit dem Ruf eines Bussards oder Habichts kann er immer noch an den Vorrat des Eichhörnchens gelangen.

Alles ändert sich ständig, auch mein Rhythmus. Früher habe ich alle fünf Jahre viele Eicheln produziert. Ihr Menschen nennt das unser Mastjahr. Ich habe mir dafür die wärmeren Jahre ausgesucht. Und dann habe ich so viele Eicheln produziert, dass alle Tiere davon naschen konnten und immer noch welche übrig blieben. Man muss sagen, seit der globalen Erwärmung ist es fast jeden Sommer so warm. Seitdem kann ich nicht anders, als fast jedes Jahr so viele Früchte zu produzieren. Das kostet mich viel Energie, aber es braucht noch mehr Kraft und Zeit, um mich an diese neue Umgebung und Klima anzupassen.

Ein Neuling in diesem Park ist der Wiedehopf. Er ist leicht an seinen rotbraunen Federn und seinem langen, schwarzspitzigen Kamm zu erkennen, den er aufstellt, wenn er aufgeregt ist. Der Wiedehopf hält sich gern in Südeuropa auf, aber aufgrund der globalen Erwärmung fühlt er sich hier zunehmend wohl. Er baut sein Nest gern in Eichenhöhlen. Manchmal pickt er sogar in das Loch eines alten Spechts.
