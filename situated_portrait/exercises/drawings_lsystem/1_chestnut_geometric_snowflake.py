from turtle import *
import tkinter as tk
from situated_portrait.models import Tree, Sensor, Observation
from datetime import date, timedelta

# Source pattern geometric snowflake: https://lsystem.netlify.app/

# Turtle Graphics -- Code by Isabel Beach
# Source code: https://github.com/ibeach/ibeach.github.io/blob/master/code/turtle/turtle.m
# different formulas: https://ibeach.github.io/turtle/

## run script
# ./manage.py shell < experiments/drawings/geometric_snowflake.py

# TURTLE

#  Apply the string replacement rule to word with specific amount of iterations and maximum length of word
def getTurtleString(word, rule, iterMax, wordMax):
    # replace characters in word
    iteration = 0
    while iteration < iterMax and len(word) < wordMax:
        word = word.replace('f', rule)
        iteration += 1
    return word

# draw the text pattern
def getTurtleCurve(word, len_forward, angle_left, angle_right):
    for character in word:
        if character == 'f':
            # go forward
            turtle.forward(len_forward)
        elif character == '-':
            # turn left
            turtle. left(angle_left)
        elif character == '+':
            # turn right
            turtle.right(angle_right)
        elif character == '0':
            turtle.pencolor("black")
        elif character == '1':
            turtle.pencolor("brown")
        elif character == '2':
            turtle.pencolor("green")
        elif character == '3':
            turtle.pencolor("red")
        else:
            print("invalid character")

# DJANGO DATABASE

### Find the date of today
def dates(day):
    year = int(day.strftime("%Y"))
    month = int(day.strftime("%m"))
    day = int(day.strftime("%d"))
    return year, month, day

### Find the  last observation
def last_observation(sensor):
    for observation in Observation.objects.filter(sensor = sensor).order_by("-timestamp")[:1]:
        # get hour
        time = observation.timestamp.strftime("%H:%M")
        # get value
        value = observation.value
    return time, value

# ------
# actions

## Sensoterra moisture % Sweet Chestnut
chestnut = Sensor.objects.get(pk=8)
## Find date of today
today = date.today()
## Find last observation
time, value = last_observation(chestnut)

## declare turtle variables

# declare pen / turtle : You can program the turtle to move around the screen.
turtle = getturtle()
# set background canvas to transparent
root = (turtle
    ._screen
    .getcanvas()
    .winfo_toplevel())
root.attributes('-alpha', 0.3)
# declare canvas - turtle screen
s = turtle.getscreen()

# change form Turtle
turtle.shape("circle")
# increase size of turtle to see colours more clearly
turtle.shapesize(0.1,0.1,0.1)
# set colour turtle
turtle.fillcolor("green")
# set speed pen
turtle.speed(15)

# sends the turtle back to the point (0,0)
turtle.home()
# draw a line from your current position to the point (100,100) on the screen.
#t.goto(100,100)

# iterMax: number of time to apply the string replacement rule.
iterMax = value
# wordMax: maximum allowed length of the string.
wordMax = 500
# word: string of initial word.
word = 'f-f-f-f'
# formula to obtain certain generated pattern, see https://ibeach.github.io/turtle/
rule = '3ff-2f--1ff-3f'
#lineColor = [0, 0, 0]
len_forward = 5
angle_left = 90
angle_right = 90

#  Apply the string replacement rule to word with specific amount of iterations and maximum length of word
word = getTurtleString(word, rule, iterMax, wordMax)
getTurtleCurve(word, len_forward, angle_left, angle_right)
s.getcanvas().postscript(file="drawing_sensoterra_chestnut.eps")
