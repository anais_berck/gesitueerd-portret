## Paardenkastanje

![](/static/img/trees/european_horse_chestnut-600x.jpg)

Hallo, ik ben een Witte Paardenkastanje, in 1900 aangeplant door de toenmalige eigenaar van de Witte Villa. Je herkent me aan mijn gewonde stam. Ik kreeg veel ongenodigde bezoekers, schimmels en bacteriën. Maar je ziet hoe gezond en levendig mijn kruin nog is. En zag je mijn mooie witte kaarsbloemen deze lente?
Doorheen de jaren veranderden de bewoners van de villa en de gebruikers van het park. Vanuit mijn stam deel ik verhaalfragmenten over die geschiedenis. 's Nachts reciteer ik gedichten over de Dood.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
