## Red Beech

![](/static/img/trees/beuken-600x.jpg)

Here, at Zijpendaal Castle, it looks like there is one giant Beech, but we are five. Our thin bark can’t tolerate the Sun. So we zip ourselves up in a coat of leaves. The space between our trunks is like a temple, quiet and cool. I am the big Red Beech on the front left.
The research on Traces of Slavery recently published awakens old memories in my trunk. I have known Anna van Vossenburg personally, and share those experiences with you. At night, I give you poems on Magic.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
