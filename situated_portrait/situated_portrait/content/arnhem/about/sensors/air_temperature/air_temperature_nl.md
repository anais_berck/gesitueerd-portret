## Luchttemperatuursensor

![](/static/img/sensors/milesight-600x.jpg)

### Herkenbare patronen

Zonlicht
:   als mensen zijn we erg vertrouwd met deze sensor; we leven met het weer. De luchttemperatuur bepaalt onze activiteiten, kledingstijl en vaak ook ons humeur. De temperatuur stijgt wanneer er meer zonlicht is.

Dag & nacht
:   de temperatuur golft mee op het ritme van dag en nacht. Aangezien er 's nachts geen zonlicht is, koelt de lucht af en daalt de luchttemperatuur.

### Luchttemperatuur & Bomen

Door met hun bladeren zonnestraling op te nemen, water te laten verdampen en schaduw te creëren, verlagen bomen de temperatuur in hun directe omgeving. Gemiddeld is het in een bos 4°C koeler en bij een hittegolf soms wel 10 °C. Uit onderzoek van Wageningen Universiteit en TU Delft blijkt dat één grote boom voor net zoveel verkoeling zorgt als tien airco's.

Je kan het effect vergelijken met net uit de zee komen. Het verdampen van zeewater onttrekt energie aan onze huid en dat geeft ons een koud gevoel. Hetzelfde gebeurt bij bomen. Water verdampen kost de boom energie. Een eik of een beuk heeft een dichter bladerdek dan een den. Hoe meer bladeren, hoe meer schaduw en hoe meer water er ook verdampt. Onder een eik zal je dus meer verfrissing vinden dan onder een naaldboom. Jonge bomen bieden minder verfrissing dan oude exemplaren.

Bomen kunnen ook de luchtstroom beïnvloeden, wat kan helpen de warmte te verspreiden en de temperatuur te verlagen. Als lucht over de bladeren van bomen beweegt, kan het koeler worden. Voornamelijk bomen met samengesteld blad (Acacia-achtigen) hebben relatief veel bladrand en zijn dus hittetolerant.

### Effect klimaatopwarming

Wetenschappers voorspellen dat de luchttemperatuur de volgende twintig jaar zal toenemen met 0.2 °C. Afhankelijk van bestaande klimaatmodellen zou de luchttemperatuur tot 1.5 à 3.5 °C kunnen stijgen aan het einde van deze eeuw. Daarom wordt onderzoek gedaan naar het effect van verhoogde luchttemperatuur op de groei van bomen.

Over het algemeen lijkt het erop dat bomen sneller groeien bij een hogere temperatuur - met uitzondering van tropische bomen. De reden is dat tropische bomen al aan de maximumtemperatuur leven die ze aankunnen, terwijl er een marge is bij bomen in gematigde klimaten. Bij hogere temperaturen groeien bomen hoger en smaller, terwijl ze meer bladeren aanmaken en minder wortels. Dit geldt vooral voor loofbomen en minder voor naaldbomen. De boom past transpireert minder bij hoge temperaturen, terwijl de fotosynthese hetzelfde blijft.

Je zou kunnen zeggen dat hoge temperaturen dus beter zijn voor onze bomen, maar helaas komt de temperatuursverhoging in de klimaatverandering niet alleen. Verstoringen zoals bosbranden, insectenuitbraken en stormen nemen toe in een warmere wereld, zoals de voorbije jaren al hebben aangetoond. De snelle groei kan dus teniet gedaan worden door boomsterfte.

### Type sensor

De luchttemperatuur wordt gemeten door de sensor die ook CO2 en luchtvochtigheid meet. Die sensor werd opgehangen in de kruin van de boom.
