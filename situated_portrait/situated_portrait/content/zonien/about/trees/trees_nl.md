# Boom

## Merkwaardige Beuk

![](/static/img/trees/beuk.jpg)

77% van het Zoniënwoud bestaat uit beuken. Ze zijn erfgenamen van de beuken die in de 18de eeuw door de Oostenrijkse landschapsarchitect Jacques Zinner werden aangeplant. Ze geven het typische aanzicht van de beukenkathedraal. Ze groeien zo recht mogelijk en vormen brede zuilen. Hun flinterdunne schors verdraagt geen zon. Daarom ritsen zij zich dicht met een jas van bladeren. Tussen hen in, krijgen weinig andere planten de kans om te groeien.

Deze beuk is een merkwaardige beuk, omdat hij behoort tot de categorie van de 'zuilvormige beuk met een stamomtrek bomen van 291 centimeter'. De inventaris van merkwaardige bomen in het Zoniënwoud werd in 2015 opgesteld door Leefmilieu Brussel in samenwerking met de `Association Protectrice des Arbres en Forêt de Soignes' (Vereniging voor de
Bescherming van de Bomen in het Zoniënwoud).

“Merkwaardige bomen zijn bomen met een sterke persoonlijkheid. […] Zij trekken de aandacht en zetten hun soortgenoten die het bosbestand vormen in de schaduw.” In het Zoniënwoud werd gekozen voor een 'emotionele aanpak' voor de keuze van de merkwaardige bomen. Een merkwaardige boom onderscheidt zich van de andere bomen door “zijn morfologie: wonderlijke slankheid, indrukwekkende stamomtrek, enorme takken […] of door zijn misvorming: gezwel, knoest, struikgewas, stompe punt.” Deze benadering van merkwaardige bomen is gebaseerd op ‘de emotionele impact’ of ‘de liefde op het eerste gezicht’ bij de persoon die de inventaris samenstelt. G. Feterman, voorzitter van de vereniging A.R.B.R.E.S. heeft dit proces zeer goed beschreven: “Een merkwaardige boom is in de eerste plaats een emotie, liefde op het eerste gezicht, de adem die stokt bij het zien van een ongelooflijke getuige van het verleden die plots in een bocht van de weg voor u opduikt”.

Op het gebied van bosbeheer genieten de merkwaardige en bijzondere bomen een bijzonder statuut. De bomen binnen het bestand worden behouden tot aan hun fysiek verval. Zo worden zij meegerekend in het quotum voor behoud van de oude reserves en dragen zij bij tot de natuurbehoudsdoelstellingen.

Bron: [https://erfgoed.brussels/links/digitale-publicaties/pdf-versies/artikels-van-het-tijdschrift-erfgoed-brussel/nummer-14/artikel-14-6](https://erfgoed.brussels/links/digitale-publicaties/pdf-versies/artikels-van-het-tijdschrift-erfgoed-brussel/nummer-14/artikel-14-6)
