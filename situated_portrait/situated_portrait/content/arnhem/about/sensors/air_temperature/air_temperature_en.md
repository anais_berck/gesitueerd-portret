## Air temperature sensor

![](/static/img/sensors/milesight-600x.jpg)

### Recognizable patterns

Sunlight
:   as humans, we are very familiar with this sensor; we live with the weather. The air temperature determines our activities, clothing style and often our mood. The temperature rises when there is more sunlight.

Day & night
:   the temperature undulates with the rhythm of day and night. Since there is no sunlight at night, the air cools down and the air temperature drops.

### Air temperature & Trees

By absorbing solar radiation with their leaves, evaporating water and creating shade, trees lower the temperature in their immediate surroundings. On average, it is 4°C cooler in a forest and sometimes as much as 10°C during a heat wave. Research by Wageningen University and TU Delft has shown that one large tree provides as much cooling as ten air conditioners.

You can compare the effect to getting out of the sea. The evaporation of seawater drains energy from our skin and that makes us feel cold. The same thing happens to trees. Evaporating water costs the tree energy. An Oak or a Beech has denser foliage than a Pine. The more leaves, the more shade, and the more water also evaporates. So you will find more refreshment under an Oak tree than under a Conifer. Young trees offer less refreshment than old ones.

Trees can also affect airflow, which can help disperse heat and lower temperatures. As air moves over the leaves of trees, it can become cooler. Mainly trees with compound leaves (Acacia-like trees) have relatively large leaf margins and are thus heat-tolerant.

### Effect climate change

Scientists predict that air temperature will increase by 0.2°C over the next twenty years. Depending on existing climate models, air temperatures could rise up to 1.5 to 3.5°C by the end of this century. Therefore, the effect of increased air temperature on tree growth is being researched.

In general, it seems that trees grow faster at higher temperatures - with the exception of tropical trees. The reason is that tropical trees already live at the maximum temperature they can handle, while there is a margin in trees in temperate climates. At higher temperatures, trees grow taller and narrower, while producing more leaves and fewer roots. This is especially true for deciduous trees, less so for Conifers. The tree transpires less at high temperatures, while photosynthesis remains the same.

So you could say that high temperatures are better for our trees, but unfortunately the temperature increase in climate change doesn't come alone. Disturbances such as forest fires, insect outbreaks and storms increase in a warmer world, as recent years have shown. So rapid growth can be negated by tree mortality.

### Sensor type

Air temperature is measured by the sensor that also calculates CO2 and air humidity. That sensor is suspended in the canopy of the tree.
