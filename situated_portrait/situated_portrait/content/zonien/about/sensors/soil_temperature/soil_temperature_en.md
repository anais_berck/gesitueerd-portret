## Soil temperature sensor

![](/static/img/sensors/sensoterra-600x.jpg)

### Recognizable patterns

Day & night
:   when the Sun sets, it becomes cooler and more humid. This has an effect on soil temperature

Hot & dry
:   this effect is most obvious at the beach, where on hot days the sand can literally be too hot under your feet

Position of the Sun
:   the sensor measures the soil temperature in the upper part, which is protected by a plastic cap. When the Sun shines straight onto the cap, the plastic heats up quickly and you can discover yellow spheres in the visualization, with measurements up to 40°C. This way, thanks to the bias of the sensor, you can follow the position of the Sun.

### Soil temperature & Trees

The soil temperature is of great importance for the life of trees and plants. It affects metabolism in root cells and the bacterial activities of associated fungi. It influences the accumulation, breakdown and mineralization of organic matter, the availability of water and nutrients for plants and microbes, soil aeration, humidity, fertilizer efficiency, pesticide degradation, etc.

There is an approximately 2% change in various parameters present in the soil with every degree change in the temperature. In plants, the soil temperature influences the seed germination, plant development and turgidity, water and nutrient uptake, and disease and insect occurrence. The rate of chemical and biochemical reactions doubles with every 10°C rise in temperature. Soil acts as a store as well as a sink of nutrients and thermal energy on a day-to-day or seasonal basis.

### Sensor type

Soil temperature is measured with sensors from the Dutch start-up Sensoterra. They measure the temperature of the soil at the top of the sensor, under the plastic cap.

The sensors are placed on the trees' 'drip line'. That is the imagined line that runs along the extreme leaves of the crown. It is along the drip line that the most lively surface roots grow.
