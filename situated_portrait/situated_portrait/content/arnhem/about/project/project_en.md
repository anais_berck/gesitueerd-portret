# The project

### Moving trees

We usually just walk past the trees of Arnhem. They seem pretty regular to us and if we notice them at all, they always look the same whether yesterday, today or tomorrow. Our houses are made with wood. Trees also give us medicine, food, warmth, oxygen and we seek their shade during hot summers.

During 'Arnhemse Bomen Vertellen', you will meet trees as you meet a work of art. The three parks are home to very special and old trees. Using sensors installed in six trees, 'Arnhemse Bomen Vertellen' makes visible what is otherwise hidden from the human eye: the mouvements of the trunk, the CO2 and water supply, the temperature in the air and at the roots, and the amount of sunlight that helps the trees in their photosynthesis.

### Always different

From hour to hour, you can see changes in the trunk of the tree. The installation invites you to appreciate these special trees as active, growing, changing living beings, beings you can also enter into a relationship with.

You will get to know the intimate life of the Solitary Oak in the Moerasweide, the Horse Chestnut near the Witte Villa, the European Oak on the way to the Belvedere Tower, one of the Red Beeches near Zypendaal Castle, one of the Gatekeepers at Gulden Bodem, and a Douglas Fir in Schaarsbergen-Zuid.

### QR code in bench

Near each of the trees, a wooden bench has been placed with a QR code on it. These benches are made of wood from trees of Arnhem. By scanning the QR code you will be guided to a web page where story fragments, an artistic soundscape and digital images will provide insight into the life and history of the tree. What you get to see and hear depends on your location and the data sent by the sensors. Let yourself be surprised every hour, every day.

## Practical

The starting point is [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), where you will find a paper map with the route:

- a 7 km walk along the six trees
- a 4 km walk along five trees, accessible for the disabled

For regular visitors to the parks, it is possible to meet one or more trees on a daily basis, to get to know them and make new, lifelong friendships. Visitors who don’t have a smartphone or prefer not to walk can visit the portraits of the six trees using the touchscreen in the visitor centre Molenplaats Sonsbeek. Here you will also find a small exhibition with artistic visualizations based on the data sent by the sensors.


## Credits

<small markdown>

This project was realized by [Anaïs Berck](https://www.anaisberck.be/), a collective of people, trees and algorithms:

- Artists collaborating on this project: An Mertens (concept, coordination, tree whisperer), Gijs de Heij & Doriane Timmermans (code, design & visualizations - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (sound), Bram Goots (photos), Loïc Lachaize (mastering), Floris van Hintum (benches/stand), Patrick Lennon & Karin Ulmer (proofreading)
More about the artists and other humans who collaborated in the project (link artists page)
- Trees participating in this project: Gatekeeper Chestnut, Red Beech Zijpendaal, Horse Chestnut Witte Villa, Solitary Oak Sonsbeek, European Oak Ronde Weide Sonsbeek, Douglas Fir Schaarsbergen-Zuid
- algorithms collaborating on this project: Decentlab sap flow sensor, sunlight sensor, CO2 & temperature sensor; EMS Brno tape drometers, Sensoterra soil moisture and temperature sensors, Milesight CO2 sensors, SenseCap CO2 sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

'Arnhemse Bomen Vertellen' is grateful for the cooperation and support of the municipality of Arnhem and Natuurcentrum Arnhem.

</small>
