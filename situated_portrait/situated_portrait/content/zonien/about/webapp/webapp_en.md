# Web app

When you open the web app, you are introduced to the guide. It makes the transition between the forest and your phone as gentle as possible. The guide leads you through the app. We hope we can arouse enough curiosity and wonder so that you feel like coming back.

In a second instance, you can zoom in on a story and a visualization. If you want to explore more, you also get an overview of the data - in the form of a simple graph. You can always return to the guide from any other page.

### Story
This is a screen where you can read a text, told from the tree's point of view.
During the day, you will be told fragments from the roots, trunk or crown of the tree. A fragment from the roots is an invitation to ground and connect with the tree. From the crown, the tree tells about relationships with other living things such as birds and insects. The trunk stores the tree's memory.
The text changes every 10 minutes.
At night, the tree will give you poems!

### Imaged
Imaged shows a clock that goes back 24 hrs in time. At the top is the now moment. Anticlockwise, you can see what measurements the sensors recorded at the tree. The clock is like a live portrait. It is updated every hour. This page also has subtitles. With each new update, a sentence appears, indicating how much a type of sensor measures. You can find more information about the different measurements at ‘About the sensors’.

### Data
For this project, we created a database which collects all measurements from all the different sensors.
This screen allows the data to be viewed as a graph in a simple way.

### Map
Here you find a screen with a map and the location of the tree.
