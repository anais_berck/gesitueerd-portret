��    9      �  O   �      �     �     �     �             :        F     R     X     _     w     �  >   �  <   �  
     :     7   K  	   �     �     �     �     �     �     �     �     �  
             +     4     <     B     O     \     l     y     �     �     �     �     �     �     �     �     �     �     �     �     �                     0     9     E     K  B  R  
   �	     �	     �	     �	     �	  <   �	  	   �	     
     	
     
  	   
     (
  '   -
  B   U
  
   �
  V   �
  >   �
     9     B     K     Y     i     �     �     �     �     �     �  	   �  
   �            	   -     7     F     W  	   f     p     w     �     �     �     �     �     �     �     �  	   �     �     �     �          !     -     =     D     8             7          -                  	   
       ,                         "      4              6   $       !   %   &   /         0         +             #       2   9   *   (          3   '                              .   1          )   5                                     24 uur 3 dagen 7 dagen Beech Data De boom heeft de afgelopen 20 minuten geen data verstuurd. Douglas fir Duits Engels European horse-chestnut European oak Foto Gebruik oordopjes of een hoofdtelefoon voor de beste ervaring. Interactive art installation on trees in three Arnhem parks. Nederlands Observaties van de laatste 24 uur, van binnen naar buiten: Raak het scherm aan<br />om de symfonie te beluisteren. Red beech Sensoren Solitary oak Stop met spelen Sunlight (at douglas fir) Sweet chestnut Symfonie Terug naar de gids Toon data van de laatste Toon kaart Toon luchtfoto Verbeeld Verhaal about air humidity air pressure air temperature air_humidity air_temperature artists benches choose a tree circular increment co2 licence logo moisture mouvement_trunk now project sapflow sensors soil temperature soil_moisture soil_temperature sunlight temperature trees webapp Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 24 Stunden 3 Tage 7 Tage Buche Daten Der Baum hat in den letzten 20 Minuten keine Daten gesendet. Douglasie Deutsch English Rosskastanie Waldeiche Bild Use headphones for the best experience. Interaktive Kunstinstallation auf Bäumen in drei Arnheimer Parks. Nederlands Beobachtungen der letzten 24 Stunden, gegen den Uhrzeigersinn, von innen nach ausssen: Berühren Sie den Bildschirm,<br />um die Symnfonie zu hören. Rotbuche Sensoren Einsame Eiche Spielen beenden Sonnenlicht (an der Douglasie) Kastanie Symfonie Zurück zum Reiseführer Anzeige der Daten des letzten Karte anzeigen Luftbild anzeigen Abbildung Erzählung Über dieses Projekt Luftfeuchtigkeit Luftdruck Lufttemperatur Luftfeuchtigkeit Lufttemperatur Künstler Bänke Wählen Sie einen Baum Bewegung Stamm Co2 Lizenz Logo Bodenfeuchtigkeit Bewegung Stamm jetzt Projekt Saftstrom Sensoren Bodentemperatur Bodenfeuchtigkeit Bodentemperatur Sonnenlicht Bodentemperatur Bäume Webapp 