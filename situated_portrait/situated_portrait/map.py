from django.contrib.staticfiles import finders
from django.conf import settings
import overpass
import json
import os
import gpxpy
from django.utils.translation import gettext as _

try:
    from functools import cache
    
    @cache
    def make_overpass_query (*args, **kwargs):
        api = overpass.API(endpoint="https://overpass-api.de/api/interpreter")
        return api.get(*args, **kwargs)
except ImportError:
    def make_overpass_query (*args, **kwargs):
        api = overpass.API(endpoint="https://overpass-api.de/api/interpreter")
        return api.get(*args, **kwargs)


# overpass is the API we need to get OSM object
# we use a python wrapper for overpass query as they use their own language
# https://github.com/mvexel/overpass-api-python-wrapper
api = overpass.API(endpoint="https://overpass-api.de/api/interpreter")

emptyFeatureCollection = {"type": "FeatureCollection", "features": []}

def get_map_labels(installation):

    POI = json.loads(installation.POI)
    return POI["ADD"]

def relabel_featureCollection (featureCollection, relabel_lookup):
    for feature in featureCollection['features']:
        id = str(feature['id'])
        if id in relabel_lookup:
            feature['properties']['name'] = relabel_lookup[id]
    return featureCollection

def get_poi(installation):
    # getting the points of interests

    POI = json.loads(installation.POI)
    map_poi = [str(poi) for poi in POI["POI"]]
    map_poi_relabeling = POI["POI_LABEL"]

    if map_poi:
        r_poi = 'way(id:' + ','.join(map_poi) + ');'
        poi_data = make_overpass_query(r_poi, verbosity = 'geom')
        poi_data = relabel_featureCollection(poi_data, map_poi_relabeling)
    else:
        return emptyFeatureCollection

    return poi_data

def get_park(installation):

    POI = json.loads(installation.POI)
    map_park = POI['PARK']

    if map_park:
        r_park = 'way(id:' + ','.join(map_park) + ');(._;>;);'
        park_data =  make_overpass_query(r_park, verbosity = 'geom')
    else:
        return emptyFeatureCollection
    
    return park_data

def get_bound(trees):
    # getting a bounding box in which we're going to request
    # all object of a certain type
    margin = [0.0055, 0.001, 0.002, 0.005]
    # top right bottom left
    trees_latitudes = list(map(lambda t : t.latitude, trees))
    trees_longitudes = list(map(lambda t : t.longitude, trees))
    bounding_box = [str(min(trees_latitudes) - margin[2]),
                    str(min(trees_longitudes) - margin[3]),
                    str(max(trees_latitudes) + margin[0]),
                    str(max(trees_longitudes) + margin[1])]
    r_bounding_box = '(' + ','.join(bounding_box) + ')'

    return r_bounding_box

def get_ways(trees):

    r_bounding_box = get_bound(trees)
    r_ways = '(' + 'way[highway]' + r_bounding_box + ';' + ');(._;>;);'
    ways_data =  make_overpass_query(r_ways, verbosity = 'geom')

    return ways_data

def get_water(trees):
    r_bounding_box = get_bound(trees)
    r_water= '(' + 'way[natural=water]' + r_bounding_box + ';'  + 'rel[natural=water]' + r_bounding_box + ';' + ');(._;>;);'
    water_data =  make_overpass_query(r_water, verbosity = 'geom')

    return water_data

def get_focus(tree):
    return {
        'latitude': tree.latitude,
        'longitude': tree.longitude,
        'name': _(tree.local_name)
    }

def get_points_from_gpx(gpx_path):
    points = []
    print(gpx_path)
    with open(gpx_path) as gpx_file:
        gpx = gpxpy.parse(gpx_file)

        for track in gpx.tracks:
            for segment in track.segments:
                for point in segment.points:
                    points.append((point.latitude, point.longitude))

    return points


def get_walks(installation):
    walks_data = {}
    for subdir, dirs, files in os.walk('situated_portrait/static/gpx/' + installation.slug + '/'):
        for file in files:
            filepath = subdir + file
            walks_data[file] = get_points_from_gpx(filepath)
    return walks_data