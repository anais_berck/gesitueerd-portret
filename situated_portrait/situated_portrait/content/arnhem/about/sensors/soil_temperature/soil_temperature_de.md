## Bodentemperatur

![](/static/img/sensors/sensoterra-600x.jpg)

### Erkennbare Muster

Tag und Nacht
:   Wenn die Sonne untergeht, wird es kühler und feuchter. Dies hat Auswirkungen auf die Bodentemperatur.

Heiß und trocken
:   Am deutlichsten wird dieser Effekt am Strand, wo an heißen Tagen der Sand buchstäblich zu heiß unter den Füßen sein kann.

Position der Sonne
:   Der Sensor misst die Bodentemperatur im oberen Teil, der durch eine Kunststoffkappe geschützt ist. Wenn die Sonne direkt auf die Kappe scheint, erwärmt sich der Kunststoff schnell und Sie können gelbe Bereiche in der Visualisierung entdecken, mit Messungen bis zu 40°C. Dank der Ausrichtung des Sensors können Sie auf diese Weise die Position der Sonne verfolgen.

### Bodentemperatur & Baume

Die Bodentemperatur ist für das Leben von Bäumen und Pflanzen von großer Bedeutung. Sie beeinflusst den Stoffwechsel in den Wurzelzellen und die bakteriellen Aktivitäten der assoziierten Pilze. Sie beeinflusst die Anhäufung, den Abbau und die Mineralisierung organischer Stoffe, die Verfügbarkeit von Wasser und Nährstoffen für Pflanzen und Mikroben, die Bodenbelüftung, die Feuchtigkeit, die Wirksamkeit von Düngemitteln, den Abbau von Pestiziden, usw.

Mit jedem Grad Temperaturveränderung ändern sich die verschiedenen Parameter im Boden um etwa 2 Prozent. Bei Pflanzen beeinflusst die Bodentemperatur die Keimung der Samen, die Entwicklung der Pflanzen und ihre Spannkraft, die Wasser- und Nährstoffaufnahme sowie das Auftreten von Krankheiten und Insekten. Die Geschwindigkeit chemischer und biochemischer Reaktionen verdoppelt sich mit jeder Temperaturerhöhung von 10°C. Je nach Tages- oder Jahreszeit fungiert der Boden sowohl als Speicher als auch als Senke für Nährstoffe und Wärmeenergie.

### Sensortype

Die Bodentemperatur wird mit Sensoren des niederländischen Start-ups Sensoterra gemessen. Sie messen die Temperatur des Bodens an der Spitze des Sensors, unter der Kunststoffkappe.

Die Sensoren werden an der "Tropflinie" der Bäume angebracht. Das ist die gedachte Linie, die entlang der äußersten Blätter der Krone verläuft. Entlang der Tropflinie wachsen die lebendigsten Oberflächenwurzeln.
