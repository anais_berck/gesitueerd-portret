## Kohlendioxid

![](/static/img/sensors/milesight-600x.jpg)

### Erkennbare Muster

Tag und Nacht
:   Tagsüber nehmen Bäume Kohlendioxid (CO2) auf, nachts geben sie es wieder ab.

Heiß und trocken
:   Wenn es sehr heiß ist, schließen die Bäume die Spaltöffnungen ihrer Blätter, um eine übermäßige Wasserverdunstung zu vermeiden. Das bedeutet, dass sie auch kein Kohlendioxid aufnehmen und somit mehr CO2 in der Luft bleibt.

### CO2 & Baume

Bäume und Pflanzen sind die einzigen lebenden Organismen, die ihre eigene Nahrung produzieren.

Wie der Mensch atmen auch Bäume durch winzige Öffnungen an der Unterseite ihrer Blätter. Durch diese Spaltöffnungen atmen sie CO2 ein. Die Blätter fangen das Sonnenlicht ein. Wasser wird von den Wurzeln in die Krone geleitet. Wasser, Sonnenlicht und CO2 lösen in den Blättern - genauer gesagt in den Chloroplasten - die Photosynthese aus. Durch die Photosynthese werden Zucker produziert, aus denen Fasern entstehen und die es dem Baum ermöglichen, über und unter der Erde weiter zu wachsen. Der Abfall aus diesem Produktionsprozess ist Wasserstoff und Sauerstoff. Letzteren atmen wir wieder ein. Nachts geben die Bäume etwa die Hälfte des Kohlenstoffs durch Atmung wieder an die Luft ab.

### Auswirkungen des Klimawandels

Bäume sind daher wichtige Gehilfen im Kampf gegen die Klimakrise. Landpflanzen entziehen der Luft etwa 29 Prozent aller CO2-Emissionen, die sonst zum Anstieg des CO2-Gehalts in der Atmosphäre beitragen würden. Und vor allem alte Bäume, wie die an diesem Projekt beteiligten, absorbieren CO2. Es dauert mehrere Jahrzehnte, bis ein Baum ausreichend genug CO2 aufgenommen hat. Es ist auch erwiesen, dass eine einfache Anreihung von Bäumen derselben Art viel weniger CO2 absorbiert als reiche, artenreiche Wälder. Die massiven Baumpflanzungsprojekte großer Unternehmen sollten daher kritisch betrachtet werden. Zumal wir weiterhin immer noch überall auf der Welt alte, artenreiche Wälder abholzen. Diese zu schützen, wäre viel wirksamer.

Bäume speichern CO2 in ihren Holzzellen. Wenn ein Baum gefällt und verbrannt wird, wird dieses CO2 wieder freigesetzt. Wenn der Baum geschützt wird oder wenn das Holz des gefällten Baumes in Häusern, Dächern und Möbeln weiterleben darf, wird das CO2 für viele Jahre gespeichert.

### Type Sensor

Die CO2 wird von diesem Sensor gemessen, der auch die Lufttemperatur und Luftfeuchtigkeit berechnet. Dieser Sensor ist in der Baumkrone aufgehängt.

Der CO2-Sensor misst Werte in ppm (parts per million). Im Mai 2024 beträgt die globale durchschnittliche CO2-Konzentration in der Atmosphäre 425,22 ppm. Dies ist ein Anstieg um 50 Prozent seit Beginn der industriellen Revolution, Verglichen mit 280 ppm in den 10.000 Jahren vor der Mitte des 18.Jahrhunderts.
