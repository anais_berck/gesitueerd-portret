Flarden mist
Heiige weilanden
Een stille wereld
Vroeg in de morgen
Langzaam verschijnt een lichtpunt
Kleurt de hemel
In een zacht oranje gloed
Verlicht de stille wereld
De wereld ontwaakt
Gaapt en rekt zich uit
Kijkt nog slaperig om zich heen
De stilte wordt langzaam verbroken
door Arnaud Klein, 2005

Het kind van de zon fluistert de sterren weg
doet de maan verdwijnen
bloost een wolkje
laat de dag weer schijnen
door Inge Verschueren, 2007

Vroeg in de morgen 
glinsterende dauwdruppels 
parels in het gras
door Ineke Dijkhuis, 2022

Jij hebt wel trekjes
van de hoge
ochtendwind
die in de vroege
morgen al wat
witte strepen vindt
ze dubbelsporig vouwt 
want atmosferisch is het
op die hoogte erg koud
ze blijven zo langer
in het gareel maar kleuren
met zonsopgang rood mee
door Wil Melker, 2022

Een zuchtje wind,
geruis,
geritsel van de bladeren.
Het is stil.
Golvend water,
stromend,
in een riviertje.
Het is stil.
De zonsopgang,
de warme gloed,
belicht de savanne.
Het is stil.
Plots...
De vogels ontwaken,
gefluit,
een melodie,
bij het ochtendlicht.
Een kudde buffels,
stormt voorbij,
opgeschrikt door leeuwen.
De natuur is wakker geworden.
De stilte is gebroken.
door Sandy Stevens, 2002

Nu zonsopgang het land bekleedt
sla ik deuren achter me dicht
in een stormloop van windkracht acht
loop ik te dromen
van schoonheid weelde en levenslust 
wat is dit allemaal toch mooi gemaakt
een wervelwind van gouden bladeren
hebben mij in hun kring opgenomen
hun sierlijke rondedans tovert zacht
een glimlach van rust op mijn gezicht
die werkelijkheid van kleurenpracht
heeft mij van binnen diep geraakt
door Cor van Vliet, 2015

Ik wiste 
de mist boven
water en weide
klaarde de hemel
voor de komst
van de zon
liep door de kou
van de nacht in 
afnemende schemer
zacht wekte ik het
licht en riep op tot
geritsel en fluister
in hun eerste tonen
zongen de vogels
verlangend uit bomen
een zomerse dag
zo eentje die de
iedereen graag zag
naar Wil Melker, 2017

Gouden zonnenstraal
weerkaatst het licht in water
geeft een gulden gloed
door Tonja van Hoek, 2007

In het uur dat verloren lijkt te lopen
tussen het einde van de nacht
en het aanvangen van de dag
ontwaakt in breekbaar licht
de morgen
door Hilde, 2008

Met strepen grijs
de deur al op een kier
schoorvoetend komt ze binnen
het ochtendlicht in eerst kwartier
ze vaagt met strepen grijs
waarin het rood 
al naar de hemel wijst
stoeit met resten zwart
die uit de lange nacht
maar langzaam willen wijken
dan kimt de zon
schijnt met warme stralen
de laatste kou uit nevelige dalen
door Wil Melker, 2011

Schitterende zon
Hoog in de blauwe hemel
Onze oude tijd
Het vallende blad
Dat vrolijk danst op de wind
Op weg naar beneden
Mooie regenboog
Ik zie je soms heel even
Kleuren zonder eind
Verre horizon
Met je mooie zonsopgang
Waar is je einde?
door Mira, 2003

Stel, het wordt nooit meer licht,
de zon verbergt haar aangezicht.
Weg met bloei en kleurenpracht,
vanaf morgen blijft het altijd nacht.
Weg blauwe lucht en regenboog,
niets streelt langer nog ons oog.
Weg horizon en vergezicht,
stel, het wordt nooit meer licht!
Stel, het wordt nooit meer nacht,
wie heeft daaraan ooit gedacht.
Weg de sterren en de maan,
alsof zij nooit hebben bestaan.
Dromen zijn niet meer van belang
want straks de laatste zonsopgang.
Duisternis verliest haar macht,
stel, het wordt nooit meer nacht!
Stel, er is geen dag of nacht,
geen avond en geen 'slaap maar zacht'.
Geen gloren van de dageraad,
geen orde en geen regelmaat.
Geen dagelijks einde of begin,
alleen maar iets er tussenin.
Hoelang zouden wij overleven,
stel u dit eens voor, heel even!
door Frans Vanhove, 2009

Onhoorbaar
schudt de nacht
stukjes donker van de snaren
als het eerste licht
de schaduwen doet trillen
in aanzet voor het zonconcert
gelijkgestemd
in ochtendrood legt
de nieuwe dag haar uren bloot
ontwakend waaiert wind
de klanken rond van
deze schitterende morgenstond
door Wil Melker, 2012

De vale ochtendschemer
De warmte van je lijf
Jouw armen om me heen
In ons knusse slaapverblijf
Het bonzen van jouw hart
Mijn vingers door je haar
De kleding die je draagt
Als ik stiekum naar je staar
De geur van verse koffie
Het ritselen van de krant
Gezamenlijk ontbijt
De poesjes in de mand
Het ochtendritueel
Onze lichaamstaal
De schone zonsopgang
Een warme zonnestraal
Het luisteren naar je stem
Als zachtjes staat te zingen
Geluk is een verzameling
Van hele kleine dingen
door Henk Hoek, 2013


Dag ventje met de fiets op de vaas met de bloem
	ploem ploem
dag stoel naast de tafel
dag brood op de tafel
dag visserke-vis met de pet
		pet en pijp
 	van het visserke-vis
		goeiendag
DAA-AG VIS
dag lieve vis
dag klein visselijn mijn
door Paul van Ostaijen, 1935
