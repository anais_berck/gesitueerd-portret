import os
import os.path
import datetime
import random
import yaml
from django.conf import settings
# Use numpy to be able to provide a seed for the random number generator
import numpy

# indicate the time window around sunrise and sunset
delta = datetime.timedelta(minutes = 30)

# every x time, we pick another story that would be the same for everyone
reshuffle = datetime.timedelta(minutes = 10)

langs = ['nl', 'en', 'de']

def get_period_of_day(now):
    sunrise_start = now.replace(hour=6, minute=00)
    sunrise_end = sunrise_start + delta
    sunset_end = now.replace(hour=22, minute=00)
    sunset_start = sunset_end - delta
    print(sunrise_start, sunrise_end, sunset_start, sunset_end)

    if now < sunrise_start:
        return 'night'
    elif now < sunrise_end:
        return 'sunrise'
    elif now < sunset_start:
        return 'day'
    elif now < sunset_end:
        return 'sunset'
    else:
        return 'night'

def pick_story(path, lang):
    # return a list of story from each file, 
    # that will be picked at random
    # but be the same for everyone and change every 10min
    full_path = path + '_' + lang + '.txt'

    if os.path.exists:
        # opening the file
        with open(full_path, 'r') as file:
            splitted_stories = file.read().strip().split('\n\n')
            
            # if we didn't picked one already, do it
            return numpy.random.choice(splitted_stories).strip()
    else:
        return f"ERROR {full_path} doesn't exist."

def discover_story(slug, lang, tree_name, period):
    # the directory in which we look for txt files
    directory = 'situated_portrait/stories/' + slug + '/' + period + '/'
    
    part = None
    
    # for day and night we have different choices of stories for every tree
    # for day we have a prefix with a part of the tree
    if period == 'day':
        part = numpy.random.choice(['canopy', 'trunk', 'roots'])

        # but for the roots the tree itself is not taken in account
        if part == 'roots':
            path = directory + '_'.join([part])  
        else:
            path = directory + '_'.join([part, tree_name]) 
    # for night 
    elif period == 'night':
        path = directory + '_'.join([tree_name])
    # for sunset and sunrise
    else:
        path = directory + '_'.join([period])

    story = pick_story(path, lang)

    return (part, story)

def get_intro(slug, now, lang, tree_name, period, part):
    # get the intro phrase from stories/legend.yaml
    with open('situated_portrait/stories/' + slug + '/legend.yaml', 'r') as file:

        introductions = yaml.safe_load(file)

        intro = introductions[lang][period]

        # pick the appropriate sentence according to the part of the tree
        if period == 'day':
            intro = intro[part]
        
        # replace with actual hour and minutes
        intro = intro.replace('$', now.astimezone(settings.TZ_INSTALLATION).strftime('%H:%M'))

        if period == 'night':
            # pick the appropriate concept according to the tree
            concept = introductions['concept'][tree_name][lang]
            intro =  intro.replace('%', concept)
    
        return intro

def get_image(slug, tree_name, part):
    tree_name = tree_name
    extend = part if part else 'others'

    # the directory in which we look for image files
    directory = 'situated_portrait/static/img/' + slug + '/pictures/' + tree_name + '/' + extend

    if os.path.exists(directory):
        # pick one in there
        images = os.listdir(directory)
        picked = numpy.random.randint(0, len(images))
        directory = '/' + '/'.join(directory.split('/')[1:])
        
        return directory + '/' + images[picked]
    else:
        return None

def get_stories(slug, tree, lang, at=None):
    
    now = at if at else datetime.datetime.now().astimezone(settings.TZ_INSTALLATION)
    # now = now.replace(hour=21, minute=00)

    seed = int(now.timestamp() / reshuffle.total_seconds())
    numpy.random.seed(seed)

    period = get_period_of_day(now)
    tree_name = tree.local_name.lower().replace(' ', '_').replace('-', '_')
    # look for corresponding story plain text file
    part, content = discover_story(slug, lang, tree_name, period)
        
    content = content
    intro = get_intro(slug, now, lang, tree_name, period, part)
    image = get_image(slug, tree_name, part)
            
    return {
        'intro': intro,
        'content': content,
        'image': image,
        'tree_name': tree_name,
        'part': part,
        'period': period
    }