# gesitueerd-portret

Gesitueerd Portret (Situated Project) is a research project in different nature reserves and in the parks of Arnhem.
This repository gathers all code developed for the webapp of 'Arnhemse Bomen Vertellen', an installation in Arnhem from 15 June till 15 September 2024.

During ‘Arnhemse Bomen Vertellen’, you will meet trees as you meet a work of art. The three parks are home to very special and old trees. Using sensors installed in six trees, ‘Arnhemse Bomen Vertellen’ makes visible what is otherwise hidden from the human eye: the movements of the trunk, the CO2 and water supply, the temperature in the air and at the roots, and the amount of sunlight that helps the trees in their photosynthesis.


## Application

The application is based on [Django](https://www.djangoproject.com/) and [Django rest framework](https://www.django-rest-framework.org/)

## get the database from the server

        scp debian@bomenvertellen.net:/srv/bomenvertellen/gesitueerd-portret/situated_portrait/db.sqlite3 db.sqlite3

## Installation

All steps assume an open terminal, `cd`'ed into the folder containing this readme.

First, install the requirements. For example through pip:
```
pip install -r requirements.txt
```

Then, to set up a database run the migrate command through the django management script:
```
python manage.py migrate
```

*Note:* by default the application will use sqlite3. The database file `db.sqlite3` will be made in the same folder as this readme. To adjust the name, location, or engine of the database adjust the file `situated_portrait/settings.py`. See also the [documentation of Django](https://docs.djangoproject.com/en/4.2/ref/settings/#databases).

## Running

During development it is the easiest to the start the application through the django management script:

```
python manage.py runserver
```

The web interface should be accesible at: <http://localhost:8000/>

The web interface for the scrap API should be accesible at: <http://localhost:8000/???>

*Note:* it is possible to bind the development server to a [different ip address and / or port](https://docs.djangoproject.com/en/4.2/ref/django-admin/#examples-of-using-different-ports-and-addresses). By binding to 0.0.0.0 the API will be accessible within the local network (at the ip-address of the device):

```
python manage.py runserver 0.0.0.0:8000
```


## create super user to connect to the admin pannel

        python manage.py createsuperuser

## Using the API

???

## API Endpoints

???

## CORS

By default the API allows all origins.

## Licence

Arnhemse Bomen Vertellen is a creation by Anaïs Berck, Arnhem, June 2024.

All code, images, sounds and texts are published to a Creative Commons Attribution 4.0 licence (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). You may reuse, distribute, reproduce, and adapt this work in any medium, except for commercial purposes, provided you give attribution to the copyright holder, provide a link to the Creative Commons licence, and indicate if changes have been made.

All code, images, sounds and texts have also been released under a CC4r licence, available here: <https://constantvzw.org/wefts/cc4r.en.html>.

The data captured during this work is published to a Creative Commons 1.0 licence (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). You can copy, modify, distribute and perform the data, even for commercial purposes, all without asking permission.

## Notes 9 - 2

Adding sensor

- setup_devices

Retrieve devices from services and add them.

Adding sensors fails:
- EMS Brno doesn't check whether the sensor already exists, multiple sensors are added
- Sensecap device is added. But the sensors are not. It thinks they already exist?
- On retrieving data from a sensecap sensor the from_date matters. If the date lies before the date of first data sensecap API returns an error message (or the date was too long ago, at moment of test September 2nd asked for dates since May 2nd).