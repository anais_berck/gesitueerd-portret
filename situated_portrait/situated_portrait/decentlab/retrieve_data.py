from situated_portrait.decentlab.decentlab_utils import query
from situated_portrait.models import Sensor, Observation

def retrieve_data (device, timestamp_from):
  domain = device.deviceType.authentication['domain']
  api_key = device.deviceType.authentication['apitoken']

  print("Retrieving data for {} since {}".format(str(device), timestamp_from.strftime('%Y-%m-%d %H:%M:%S')))

  df = query(domain=domain,
    api_key=api_key,
    time_filter="time > '{}'".format(timestamp_from.strftime('%Y-%m-%d %H:%M:%S')),
    device='/{}/'.format(device.remote_identifier),
    sensor='//',
    agg_func='mean',
    agg_interval='15m')

  for sensorname in df.columns:
    sensor = Sensor.objects.get(remote_identifier=sensorname, device=device)
    observations = []
    if sensor:
      for x, row in enumerate(df[sensorname]):
          observations.append(
             Observation(
              sensor=sensor,
              timestamp=df.index[x],
              value=row
             )
          )
          
    # print(observations)
    print("Received {} observation(s)".format(len(observations)))
    Observation.objects.bulk_create(observations, ignore_conflicts=True)