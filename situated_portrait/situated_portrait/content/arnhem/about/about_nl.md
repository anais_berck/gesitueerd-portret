Met 'Arnhemse Bomen Vertellen' kan je de parken en haar bomen op een vernieuwende manier beleven. Je gaat een relatie aan met de bomen en word je meer bewust van het natuurlijk en cultureel erfgoed dat de parken te bieden hebben.
Dit kunstproject is een creatie van Anaïs Berck, een collectief van mensen, bomen en algoritmes.

![](/static/img/trees/eik1-600x.jpg)

<!-- Meer informatie: -->

- [Over dit project](project/)
- [Over de webapp](webapp/)
- [Over de sensors](sensors/)
- [Over de bomen](trees/)
- [Over de bankjes](benches/)
- [Over de kunstenaars en alle mensen die betrokken werden bij dit project](artists/)
- [Over de licentie](licence/)

<!--
    note for An:
    in between parentheses is the name of the next subfolder you want to go into
-->
