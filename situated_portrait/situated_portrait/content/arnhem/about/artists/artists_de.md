# Kunstler und alle an dem Projekt beteiligten Personen

## Anaïs Berck
[Anaïs Berck] (https://www.anaisberck.be/) ist ein Pseudonym und steht für die Zusammenarbeit zwischen Menschen, Algorithmen und Bäumen. Als Kollektiv eröffnen sie einen Raum, in dem die menschliche Intelligenz einen Platz neben der pflanzlichen und künstlichen Intelligenz erhält.

Für dieses Projekt sind die folgenden Personen an der Zusammenarbeit beteiligt:

### An Mertens (Brüssel, BE)
An ist eine Medienkünstlerin und Baumgriffier. An konzipierte und koordinierte dieses Projekt, recherchierte, schrieb den literarischen Code und verfasste und redigierte die Texte und die Erzählungen.

### Gijs de Heij (Brüssel, BE) und Doriane Timmermans (Brüssel, BE)
Beide sind Mitglieder des experimentellen Grafikdesignstudios [Open Source Publishing] (http://osp.kitchen/) in Brüssel. Sie kombinieren Grafikdesign mit Programmierung und künstlerischem Schaffen.
[Gijs de Heij](https://gijsdeheij.com/) studierte Grafikdesign am ArtEZ in Arnheim. Er programmiert seit seinem 11. Lebensjahr.
Doriane Timmermans hat einen Master-Abschluss in Mathematik von der Université libre de Bruxelles und studierte digitale Kunst an der ERG in Brüssel. Sie erforscht das literarische Potenzial von css: [Declarations](https://declarations.style/).

### Bram Goots (Antwerpen, BE)
Bram ist Fotograf. In der Agence Future hat Bram Erfahrung darin gesammelt, verschiedene Kontexte an verschiedenen Orten auf der ganzen Welt einzufangen, immer mit einem ausgeprägten Sinn für Licht, Details und Komposition.

### Christina Clar (AU)
[Christina](https://christinaclar.net/) ist eine Klangkünstlerin. Sie hat zwanzig Jahre Erfahrung mit Aufnahmen in der Natur und der Schaffung vieldeutiger Klanglandschaften. Christina lebte bis vor ein paar Jahren in Brüssel, wo sie für Organisationen wie QO2 und das Opernhaus De Munt tätig war. Obwohl sie zwischenzeitlich nach Österreich umgezogen ist, kommt sie oft nach Brüssel.

### Floris van Hintum (Arnhem, NL)
Floris bezeichnet sich selbst gerne als Baum-, Wald-, Natur- und Landschaftsmanager. Die Bänke und Möbel im Molenplaats Sonsbeek hat er aus einheimischem Holz gefertigt. Mit [Landbeeld](http://www.landbeeld.nl/) entwickelt er Projekte, die Natur und Kunst miteinander verbinden. Außerdem stellt er mit WILLOW Hifi Soundsysteme her.

## Baumpfleger der Gemeinde Arnheim
Die Sensoren und Bänke wurden mit freundlicher Unterstützung der Baumpfleger der Gemeinde Arnheim installiert. Vielen Dank für Ihre Hilfe und Geduld: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

## Wissenschaftlicher Rat
In verschiedenen Gesprächen vor und während der Gestaltung des Kunstprojektes erhielten wir sehr wertvolle Ratschläge von der Baumringforscherin Dr. Ute Sass-Klaassen und ihrem Kollegen Linar Akhmetzyanov (Van Hall Larenstein University of Applied Sciences, Leeuwarden). Wir danken Ihnen!

## Fachwissen und Erfahrung über Bäume und Parks
Während des Entstehungsprozesses dieses Projekts hatte An das Privileg und das Vergnügen, dem Fachwissen und der Erfahrung der folgenden Personen zuzuhören und Zeit mit ihnen zu verbringen: Parkführer Eddy van Heeckeren, Waldexperte Simon Klingen, Ökologe Hans Van den Bos, Künstler Boudewijn Corstiaensen, Verwalter Jan Floor und Rob Borst, Senior Management Adviser Green Living Environment Marcel Wenker, Naturführer und Biologe Pim van Oers, Parkführer Gerard Herbers, Künstler Albert van der Weide, Försterin Nina Kreisler, Autor Jibbe Willems, Stadtökologin Ineke Wesseling, Künstler Bartaku Vandeput (Baroa belaobara). Wir danken Ihnen!

## Weitere Beratung
Wir konnten auch auf den praktischen und ethischen Rat von Hélène van Ginderachter, Ingenieur Erik Mertens, Joost Verhagen (Cobra-Groen in Zicht), Judith Van Ginneken, Marleen Verschueren und Erik De Wilde zählen. Das Korrekturlesen wurde von Patrick Lennon und Karin Ulmer übernommen. Wir danken Ihnen!

## Finanzierung, Produktion, Kommunikation
Dieses Projekt wurde dank der Unterstützung von Marko van der Wel (Natur, Umwelt, Pflege, lokale Lebensmittel, Wasser), Parkmanager Rob Borst und dem leitenden Verwaltungsberater für ‘grünes Lebensumfeld’ Marcel Wenker ermöglicht. Für die Kommunikation sorgten Sanne Blok, Hanneke Busscher und William Hulstein; die Produktion lag in den Händen von Karlijn Michielsen. Wir danken Ihnen!

## Logo

<!-- ![](/static/img/logos/arnhemse-bomen-vertellen-groen.png) -->

Das Logo von 'Arnhemse Bomen Vertellen' verwendet die Schriftart ‘Computer Regular’.

Diese Schriftart wurde von [Marianne Noordzij](https://www.mariannenoordzij.nl) entworfen, die ihr Studium als Grafikdesignerin bei Artez abgeschlossen hat und anschließend KI an der Universität Utrecht studierte. Diese Schrift stellt eine Verbindung zwischen Maschinen und Mehr-als-menschlichen-Wesen als ‘Weggefährtin’ her.

Uns gefällt das zufällige Zusammentreffen mit Marianne die aus Arnheim stammt, und uns gefällt die Art und Weise, wie die Schrift organische und technische Elemente kombiniert. In dieser Hinsicht spiegelt sie die Identität des Projekts sehr gut wieder.
