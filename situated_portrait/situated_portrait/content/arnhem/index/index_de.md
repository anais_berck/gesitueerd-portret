## Sie können einem Pförtnerkastanie treffen und fünf anderen ungewöhnlichen Bäumen

Bis Dezember können Sie sechs Arnheimer Bäume auf einzigartige Weise kennen lernen. Hören Sie einem 374 Jahre alten Pförtnerkastanie in Gulden Bodem zu. Welche Geschichten hat ein Baumflüsterer aufgeschnappt? Wie sehen Sie die Buche atmen? Und wächst sie immer noch jeden Tag? Was uns normalerweise verborgen bleibt, kann jetzt entdeckt werden. Im Rahmen des 125-jährigen Jubiläums des Sonsbeek-Parks gibt es eine spezielle Wanderroute „Arnhemse Bomen Vertellen“. Besuchen Sie die künstlerische Installation vom 15. Juni bis 15. Dezember 2024 im Sonsbeekpark, in Zijpendaal und Gulden Bodem.

Sie sind eingeladen, die Parks und ihre Bäume auf innovative Weise zu erleben, eine Beziehung zu den Bäumen aufzubauen und sich des natürlichen und kulturellen Erbes, das die Parks zu bieten haben, stärker bewusst zu werden. Dieses Kunstprojekt ist eine Kreation von [Anaïs Berck](https://www.anaisberck.be/), einem Kollektiv aus Menschen, Bäumen und Algorithmen.

### Baum Bewegung

Normalerweise gehen wir einfach vorbei an den Bäumen von Arnheim. Sie kommen uns ziemlich gewöhnlich vor, und wenn wir sie überhaupt wahrnehmen, sehen sie immer gleich aus - ob gestern, heute oder morgen. Unsere Häuser sind aus Holz gebaut. Bäume geben uns auch Medizin, Nahrung, Wärme, Sauerstoff, und wir suchen ihren Schatten in heißen Sommern.

Bei 'Arnhemse Bomen Vertellen' begegnen Sie den Bäumen wie einem Kunstwerk. Die drei Parks beherbergen ganz besondere und alte Bäume. Mit Hilfe von Sensoren, die in sechs Bäumen installiert sind, macht "Arnhemse Bomen Vertellen" sichtbar, was dem menschlichen Auge sonst verborgen bleibt: die Bewegungen des Stammes, die CO2- und Wasserversorgung, die Temperatur in der Luft und an den Wurzeln sowie die Menge an Sonnenlicht, die den Bäumen bei der Photosynthese hilft.

### Immer anders

Von Stunde zu Stunde kann man Veränderungen im Stamm des Baumes beobachten. Die Installation lädt dazu ein, diese besonderen Bäume als aktive, wachsende, sich verändernde Lebewesen zu betrachten - Wesen, denen man begegnen und auch eine Beziehung eingehen kann.

Sie lernen das intime Leben der Einsamen Eiche in der Moerasweide, der Rosskastanie bei der Witte-Villa, der Waldeiche auf dem Weg zum Belvedere-Turm, einer der Rotbuchen beim Schloss Zijpendaal, eines der Pförtnerkastanie am Gulden Bodem und einer Douglasie in Schaarsbergen-Zuid kennen.

### QR-Code auf der Bank

In der Nähe eines jeden Baumes wurde eine Holzbank mit einem QR-Code aufgestellt. Diese Bänke sind aus Holz von Bäumen aus Arnheim gefertigt. Wenn Sie den QR-Code scannen, werden Sie auf eine Webseite weitergeleitet, auf der Fragmente von Geschichten, eine künstlerische Klanglandschaft und digitale Bilder einen Einblick in das Leben und die Geschichte des Baumes geben. Was Sie zu sehen und zu hören bekommen, hängt von Ihrem Standort und den von den Sensoren gesendeten Daten ab. Lassen Sie sich überraschen - jede Stunde, jeden Tag neu.

## Praktisch

Der Startpunkt ist [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), wo Sie einen Papierabzug mit der Route finden:

- eine 7 km lange Wanderung entlang der sechs Bäume
- ein 4 km langer Spaziergang entlang von fünf Bäumen, der auch für Behinderte zugänglich ist

Regelmäßige Besucher der Parks haben die Möglichkeit, täglich einen oder mehrere Bäume zu treffen, sie kennenzulernen und neue, lebenslange Freundschaften zu schließen. Besucher, die kein Smartphone haben oder nicht laufen möchten, können die Porträts der sechs Bäume über den Touchscreen im Besucherzentrum am Molenplaats Sonsbeek kennenlernen. Hier gibt es auch eine kleine Ausstellung mit künstlerischen Visualisierungen auf der Grundlage der von den Sensoren gesendeten Daten.

Das Besucherzentrum [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/) ist von Mittwoch bis Samstag von 10.00 bis 16.00 Uhr geöffnet. Sonntags von 11.00 bis 16.00 Uhr.

## Impressum

<small markdown>

Dieses Projekt  wurde von [Anaïs Berck] (https://www.anaisberck.be/), einem Kollektiv aus Menschen, Bäumen und Algorithmen, realisiert.

- Künstler, die an diesem Projekt mitarbeiten: An Mertens (Konzept, Koordination, Baumgriffier), Gijs de Heij & Doriane Timmermans (Code, Design & Visualisierungen - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (Ton), Bram Goots (Fotos), Loïc Lachaize (Mastering), Floris van Hintum (Bänke/Stand), Patrick Lennon & Karin Ulmer (Korrekturlesen)
- An diesem Projekt beteiligte Bäume: Pförtnerkastanie, Rotbuche Zijpendaal, Rosskastanie Witte Villa, Einsame Eiche Sonsbeek, Waldeiche Ronde Weide Sonsbeek, Douglasie Schaarsbergen-Zuid
- Algorithmen, die an diesem Projekt mitarbeiten: Decentlab Saftstromsensor, Sonnenlichtsensor, CO2- und Temperatursensor; EMS Brno Bandmessgeräte, Sensoterra Bodenfeuchte- und Temperatursensoren, Milesight CO2-Sensoren, SenseCap CO2-Sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

Wir danken für ihre Rat: Dr. Ute Sass-Klaassen (Hogeschool Van Hall Larenstein), Hélène van Ginderachter, Erik Mertens, Joost Verhagen (Cobra-Groen in zicht), Judith Van Ginneken, Marleen Verschueren, Erik De Wilde.

Wir danken für den Austausch von Kenntnissen, Erfahrungen und Geschichten: Parkführer Eddy van Heeckeren, Forstexperte Simon Klingen, Ökologe Hans Van den Bos, Künstler Boudewijn Corstiaensen, Parkverwalter Jan Floor, Rob Borst, leitender Verwaltungsberater für Grünflächen Marcel Wenker, Naturführer und Biologe Pim van Oers, Parkführer Gerard Herbers, Künstler Albert van der Weide, Försterin Nina Kreisler, Autor Jibbe Willems, Stadtökologine Ineke Wesseling, Künstler Bartaku Vandeput (Baroa belaobara).

„Arnhemse Bomen Vertellen“ ist dankbar für die Zusammenarbeit und Unterstützung der Gemeinde Arnheim und des Natuurcentrum Arnheim.

Die Sensoren und Bänke wurden mit freundlicher Unterstützung der Baumpfleger der Gemeinde Arnheim installiert. Vielen Dank für Ihre Hilfe und Geduld: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

Dieses Projekt wurde dank der Unterstützung von Marko Van der Wel (Natur, Umwelt, Pflege, lokale Lebensmittel, Wasser), Parkmanager Rob Borst und dem leitenden Verwaltungsberater für ‘grünes Lebensumfeld’ Marcel Wenker ermöglicht. Für die Kommunikation sorgten Sanne Blok, Hanneke Busscher und William Hulstein; die Produktion lag in den Händen von Karlijn Michielsen. Wir danken Ihnen!

Arnhemse Bomen Vertellen, von Anaïs Berck, Arnhem, Juni 2024.
Sämtlicher Code, alle Bilder, Sounds und Texte werden unter einer Creative Commons Attribution 4.0-Lizenz (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>) veröffentlicht. Sie dürfen dieses Werk außer für kommerzielle Zwecke in jedem beliebigen Medium wiederverwenden, verteilen, reproduzieren und anpassen, vorausgesetzt, Sie geben den Urheber des Werks an, stellen einen Link zur Creative Commons-Lizenz bereit und geben an, ob Änderungen vorgenommen wurden.

Sämtlicher Code, alle Bilder, Sounds und Texte wurden auch unter einer CC4r-Lizenz veröffentlicht, die hier verfügbar ist: <https://constantvzw.org/wefts/cc4r.en.html>.

Die während dieser Arbeit erfassten Daten werden unter einer Creative Commons 1.0-Lizenz (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>) veröffentlicht. Sie können die Daten kopieren, ändern, verteilen und aufführen, auch für kommerzielle Zwecke, und das alles, ohne um Erlaubnis zu bitten.

</small>
