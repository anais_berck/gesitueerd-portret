Hi boy with the bike on the vase on the bloom
                                                          ploom ploom
hi chair by the table
hi bread on the table
hi fisher-of-fish with the pipe
                       and
hi fisher-of -fish with cap
              cap and pipe
    of the fisher-of-fish
H i i i —i fish
hi little fish
hi tiny fishy-fine of mine
by Paul van Ostaijen, 1935. Translation: 1982, James Holmes

What was thy dream, sweet Morning? for, behold,’
Thine eyes are heavy with the balm of night,
And, as reluctant lilies to the light,
The languid lids of lethargy unfold.
Was it the tale of Yesterday retold —
An echo wakened from the western height,
Where the warm glow of sunset dalliance bright
Grew, with the pulse of waning passion, cold?
Or was it some heraldic vision grand
Of legends that forgotten ages keep
In twilight, where the sundering shoals of day
Vex the dim sails, unpiloted, of Sleep,
Till, one by one, the freighting fancies gay,
Like bubbles, vanish on the treacherous strand?
by John B. Tabb

Rising sun captures horizon
dominating the sky in the east
bright red absorbing the blue
turns purple:
room for the king!
by Anonymous

Bleeding through moments
Spent alone and lost
Sinking in thoughts
That fill my heart with frost
Dreading another sunrise
Another day to live through
Enduring this bitter loneliness
While I try to find my way to you
by Shruti Atri

I look for you in the sun rise,
Your face in cloud formation.
I feel your kiss as the light crests,
Your soul shining on the horizon.
Yet the sun does not warm my skin,
The way your breath warms my face;
You holding my head in your hands,
As we lock in our embrace.
by Fireheart

morning light
calls out to me
inside the first
breath of the day
there is clarity
the sun greeting
tired eyes from
the distant horizon
awakens reality
as clear as glass
before daydreams
and judgements
have time to pass
you will never be
so sure, of what you want
than in that moment
what are you wishing?
to gaze upon once
your eyes flutter open?
by Nicole M Mutchler

Now the stars have faded
In the purple chill,
Lo, the sun is kindling
On the eastern hill.
Tree by tree the forest
Takes the golden tinge,
As the shafts of glory
Pierce the summit’s fringe.
Like a shining angel
At my cabin door,
Shod with hope and silence,
Day is come once more.
by Bliss Carman

From the lips of Morning,
Where the blossoms lie,
Petulantly scorning,
Breathed a little sigh;
“Sunrise flowers wither,
Quickly turn to gray;
Whither fly they? Whither
Pass from light away?”
From the sunset splendor,
Glowing soft and clear,
Came a whisper tender:
“Morning, we are here!”
by Amos Russel Wells

I forgot what sunrise looked like
Before I met you
I’m not a morning person
But for you I’ll do
You helped me find my way home
When the world seemed gray
Carried the sun straight to the sky
And turned my night into day
by FrankieM

sunlight shatters on my shoulders
blessing my soul with hope
by Anonymous

Silver shimmers
reflected by the sea
When dawn kisses each
silhouetted tree.
As sunrise pierces
fleecy clouds of blush
A canvass bides the
eager artist’s brush.
Our Banyon bids adieu
to fowl in flight…
Their songs reverberate
in morning light
Attesting to the joy
each dawn excites
In explosive colors
which Sol ignites.
As breezes blow through
azure skies above
I share divinely Dawn’s
sweet gifts of love.
by Connie Marcum Wong

I lay here searching for wakefulness
hours after sunrise.
Outside, remnants of a soft pink rest
among new-born grey clouds, 
And embers of a morning sun fade away.
I admire the transitioning sky, remembering how this life 
is a mystifying blur made of hellos and goodbyes.
by Allesha Eman

Something pulls my eyes
up up up and around.
What vision of the world reflects?
off my eyes?
So, I recognize the smallness
of my life under a sky
that I can’t run towards the end of,
and also, the magnitude
of being under that same sky,
as it opens from all sides
to reveal the color
of morning.
by Eleora Timberlake

Day’s sweetest moments are at dawn;
Refreshed by his long sleep, the Light
Kisses the languid lips of Night,
Ere she can rise and hasten on.
All glowing from his dreamless rest
He holds her closely to his breast,
Warm lip to lip and limb to limb,
Until she dies for love of him.
by Ella Wheeler Wilcox

They are rattling breakfast plates in basement kitchens,
And along the trampled edges of the street
I am aware of the damp souls of housemaids
Sprouting despondently at area gates.
The brown waves of fog toss up to me
Twisted faces from the bottom of the street,
And tear from a passer-by with muddy skirts
An aimless smile that hovers in the air
And vanishes along the level of the roofs.
by T. S. Eliot

Silent skies awaken, as stars bid adieu,
The world comes alive, bathed in hues so true.
With each brushstroke of light, darkness fades,
And nature, in harmony, serenades.
Oh, Aurora, hear my plea,
To embrace life's dawn, eternally free.
As the sun ascends, a new day begins,
With hope in my heart, my spirit sings.
by Emily Davidson

Soft tendrils of dawn gently unfold,
As the sun's radiance, the earth does enfold.
The world awakens with a sigh of delight,
As darkness succumbs to the morning's bright.
A symphony of birds, their songs fill the air,
Dancing on sunbeams, without a care.
Nature's chorus, a melodious blend,
Welcoming the day, as darkness rescinds.
In the arms of the sunrise, I find solace and peace,
A moment of stillness, as worries cease.
With each ray of light, a new hope is born,
In the gentle embrace of the sun's warm morn.
by Robert Anderson

Whispers of the sunrise caress my face,
As dawn's tender touch, my soul does embrace.
Colors unfold, like a delicate dance,
Painting the sky with a celestial expanse.
Golden rays breathe life into each petal,
As flowers awaken, embracing the celestial.
Nature's canvas, a masterpiece so divine,
A surreal tableau, frozen in time.
In the morning's embrace, dreams take flight,
With renewed vigor, darkness takes its flight.
Oh, sunrise, you are a beacon of hope,
Guiding us forward, with strength to cope.
by Sofia Ramirez




