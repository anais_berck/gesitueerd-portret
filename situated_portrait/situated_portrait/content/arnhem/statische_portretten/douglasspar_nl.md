## Douglasspar

![](/static/img/trees/douglas_fir-600x.jpg)

Aangenaam, ik behoor tot een van de meest aangeplante boomsoorten in Nederland. Mijn hout werd gebruikt om de gangen in de mijnen te stutten. Je herkent me aan mijn kurkachtige schors en mijn dennenappels dragen grappige muizenstaartjes bij elke schub.
Vanuit mijn kernhout deel ik verhaalfragmenten over het aanplanten en beheren van bossen, over exoten als wij die veel winst opleveren. 's Nachts reciteer ik gedichten over Natuur.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
