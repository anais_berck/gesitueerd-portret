��    9      �  O   �      �     �     �     �             :        F     R     X     _     w     �  >   �  <   �  
     :     7   K  	   �     �     �     �     �     �     �     �     �  
             +     4     <     B     O     \     l     y     �     �     �     �     �     �     �     �     �     �     �     �     �                     0     9     E     K  B  R     �	     �	     �	     �	     �	  :   �	     �	     �	     
     
     
     $
  >   )
  8   h
  
   �
  L   �
  7   �
  	   1     ;     D     Q     a          �     �     �  
   �     �     �     �     �     �  	   
          %     6     G     S     [     i     w     �     �     �     �     �     �  	   �     �     �     �     �                     &     8             7          -                  	   
       ,                         "      4              6   $       !   %   &   /         0         +             #       2   9   *   (          3   '                              .   1          )   5                                     24 uur 3 dagen 7 dagen Beech Data De boom heeft de afgelopen 20 minuten geen data verstuurd. Douglas fir Duits Engels European horse-chestnut European oak Foto Gebruik oordopjes of een hoofdtelefoon voor de beste ervaring. Interactive art installation on trees in three Arnhem parks. Nederlands Observaties van de laatste 24 uur, van binnen naar buiten: Raak het scherm aan<br />om de symfonie te beluisteren. Red beech Sensoren Solitary oak Stop met spelen Sunlight (at douglas fir) Sweet chestnut Symfonie Terug naar de gids Toon data van de laatste Toon kaart Toon luchtfoto Verbeeld Verhaal about air humidity air pressure air temperature air_humidity air_temperature artists benches choose a tree circular increment co2 licence logo moisture mouvement_trunk now project sapflow sensors soil temperature soil_moisture soil_temperature sunlight temperature trees webapp Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 24 uur 3 dagen 7 dagen Beuk Data De boom heeft de afgelopen 20 minuten geen data verstuurd. Douglasspar Deutsch English Paardenkastanje Boseik Foto Gebruik oordopjes of een hoofdtelefoon voor de beste ervaring. Interactieve installatie rond bomen in 3 Arnhemse parken Nederlands Observaties van de laatste 24 uur, tegen de klok in, van binnen naar buiten: Raak het scherm aan<br />om de symfonie te beluisteren. Rode Beuk Sensoren Allenige Eik Stop met spelen Zonlicht (bij de douglasspar) Kastanje Symfonie Terug naar de gids Toon data van de laatste Toon kaart Toon luchtfoto Verbeeld Verhaal Over dit project Luchtvochtigheid Luchtdruk Luchttemperatuur Luchtvochtigheid Luchttemperatuur Kunstenaars Bankjes Kies een boom Beweging stam Co2 concentratie Licentie Logo Bodemvochtigheid Beweging stam nu Project Sapstroom Sensoren Bodemtemperatuur Bodemvochtigheid Bodemtemperatuur Zonlicht Bodemtemperatuur Bomen Web app 