# Licentie

Gesitueerd Portret, door Anaïs Berck, Brussel, augustus 2024.

Alle code, afbeeldingen en teksten zijn gepubliceerd onder een Creative Commons Attribution 4.0-licentie (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). Je kan dit werk op elk medium hergebruiken, verspreiden, reproduceren en aanpassen, maar niet voor commerciële doeleinden, en op voorwaarde dat je de auteur vermeldt, evenals een link naar de Creative Commons-licentie en vermelding van de wijzigingen die je maakte.

Alle code, afbeeldingen, geluiden en teksten zijn ook gepubliceerd onder een CC4r-licentie, die hier beschikbaar is: <https://constantvzw.org/wefts/cc4r.en.html>.

De gegevens die tijdens dit werk werden gegenereerd, zijn gepubliceerd onder een Creative Commons 1.0-licentie (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). Je kan deze data kopiëren, wijzigen, en opnieuw verspreiden, zelfs voor commerciële doeleinden, zonder toestemming te vragen.
