## Sonnenlicht

![](/static/img/sensors/solar_radiation.jpg)

### Erkennbares Muster

Tag und Nacht
:   Die Erde dreht sich in etwa 365 Tagen um die Sonne. Gleichzeitig dreht sich die Erde auch um ihre eigene Achse, in 24 Stunden oder einem Tag.

In den Niederlanden ist es im Sommer viel länger hell als im Winter. Das liegt daran, dass die Erde im Verhältnis zur Sonne leicht geneigt ist.

Im Sommer steht die Sonne senkrecht zur Nordhalbkugel. Daher scheint im Sommer mehr Sonne auf diese Hemisphäre und die Tage sind länger. Im Winter verhält es sich genau umgekehrt.

### Sonnenlicht & Baume

Bäume wachsen buchstäblich dem Licht entgegen. Ihre Blätter fangen so viel Sonnenlicht wie möglich ein, um durch Photosynthese Zucker zu produzieren. Wenn man darüber nachdenkt, bilden Sonne, Wasser und CO2 die Grundlage für unser Leben, unsere Nahrung, unsere Materialien und unsere Medikamente. Jede Pflanze wächst dank der Sonne und der Photosynthese. Menschen und Tiere wiederum ernähren sich von den Pflanzen.

### Sensortype

Der Sonnensensor ist eine kleine Kugel von der Größe eines Auges, der horizontal im offenen Gelände angebracht ist. Er misst die Bestrahlungsstärke der Sonne. Die Einheit der Bestrahlungsstärke ist Joule pro Sekunde und Quadratmeter (W/m²). Die in der Solarbranche häufig verwendete Einheit ist Kilowattstunden pro Quadratmeter (kWh/m²). Joule benennt die Energiemenge, während Watt die Leistung angibt. Wenn Sie eine bestimmte Zeit lang Energie verwenden (z. B. pro Stunde), haben Sie eine bestimmte Menge an Joule verbraucht oder erzeugt.

Der Sensor verwendet als Einheit Joule pro Sekunde und Quadratmeter (W/m²). Geteilt durch 1000 ergibt dies kWh/m².

Die Sonnenscheindauer ist der Zeitraum, in dem die direkte Sonneneinstrahlung den Grenzwert von 120 W/m² überschreitet. Die Strahlungsintensität variiert stark, von etwa 50 W/m² an einem stark bewölkten Wintertag bis zu mehr als 1000 W/m² bei klarem Himmel im Sommer.
