function randrange (min, max) {
  return min + Math.random() * (max - min);
}

function choice (options) {
  return options[Math.floor(randrange(0,options.length))];
}

window.soundscape = {
  play_window: 20 * 60,
  parallel_sensors: 2,
  time_delta: 0
};

class Controller {
  constructor (tree, sensors, ontick) {
    this.tree = tree;
    this.sensors = sensors;
    this.is_playing = false;
    window.setInterval(this.tick.bind(this), 500);
    this.ontick = ontick;
    this.parallel_sensors = 0;
  }

  tick () {
    if (this.is_playing) {
      const sensors_playing = this.sensors.filter((sensor) => sensor.is_playing || sensor.is_loading);
  
      if (this.tree.can_play && !this.tree.is_playing && !this.tree.is_loading) {
        this.tree.start();
      }
  
      if (sensors_playing.length < this.parallel_sensors) {
        const candidates = this.sensors.filter((sensor) => !sensor.is_playing && sensor.can_play);
        if (candidates.length > 0) {
          const next_sound = choice(candidates);
          next_sound.start();
        } 
      }

      this.ontick(this.tree, sensors_playing);
    }
  }

  add_parallel_sensor () {
    if (this.parallel_sensors < window.soundscape.parallel_sensors) {
        this.parallel_sensors += 1;
        window.setTimeout(this.add_parallel_sensor.bind(this), randrange(2000,5000));
    }
  }

  start () {
    this.parallel_sensors = 0;
    window.setTimeout(this.add_parallel_sensor.bind(this), randrange(2000,5000));
    console.log('starting');
    this.is_playing = true;
    this.tree.mute(false);
    this.sensors.forEach((sensor) => sensor.mute(false));
  }

  stop () {
    this.is_playing = !this.is_playing;
    this.tree.mute(true);
    this.sensors.forEach((sensor) => sensor.mute(true));
  }
}

class Sensor {

  constructor (pk, name, type, tree, location, sounds, observation, interval) {
    this.pk = pk;
    this.name = name;
    this.type = type;
    this.location = location;
    this.tree = tree;
    
    this.observation = null;
    this.can_play = false;
    // Tracks whether this sensor can play
    
    this.is_playing = false;
    this.is_loading = false;
    // Tracks whether this sensor is actually playing
    // timeout is not considered to play
    
    this.current_sound = null;

    this.is_muted = false;
    // Whether it is muted
    
    this.interval = interval;
    // Time in between plays of a sound

    this.load_sounds(sounds);
    this.set_observation(observation);
  }

  start () {
    // Pick a sound
    let sound = choice(this.sounds);
    this.current_sound = sound;
    this.set_new_volume();
    if (sound.state() == 'unloaded') {
      this.is_loading = true;
      sound.on('load', () => {
        this.is_loading = false;
        sound.play();
      });
      sound.load();
    }
    else {
      sound.play();
    }
  }

  mute (is_muted) {
    this.is_muted = is_muted;
    this.sounds.map((sound) => sound.mute(is_muted));
  }

  load_sounds (sounds) {
    this.sounds = sounds.map((path) => (new Howl({
      src: [path],
      autoplay: false,
      loop: false,
      volume: 1,
      onplay: () => this.onplay(),
      onend: () => this.onend(),
      preload: false
    })));
  }

  set_observation (observation) {
    this.observation = observation;
    const time_since_observation = (Math.round(Date.now() / 1000) - window.soundscape.time_delta) - this.observation.timestamp

    if (this.observation && time_since_observation < window.soundscape.play_window) {
      this.can_play = true;
    }
    else {
      this.can_play = false;
    }
  }

  update_distance () {}

  set_new_volume () {
    const nowInSeconds = Math.round(Date.now() / 1000) - window.soundscape.time_delta;
    const volume = 1 - Math.max(0, (nowInSeconds - this.observation.timestamp) / window.soundscape.play_window);

    console.log('New volume ', volume);

    this.sounds.forEach((sound) => {
      sound.volume(volume);
    });
  }

  /**
   * Get delay time in milliseconds
   * @returns int
   */
  get_delay () {
    // define delay logic
    return randrange(this.delay[0], this.delay[1]);
  }

  /**
   * Get delay time in milliseconds
   * @returns int
   */
  get_interval () {
    // define delay logic
    return randrange(this.interval[0], this.interval[1]);
  }

  onplay () {
    console.log('Sensor started playing.')
    this.is_playing = true;
  }

  onend () {
    console.log('Sensor sound finished')
    let interval = this.get_interval();
    if (interval) {
      window.setTimeout(() => {
        console.log('is_playing marked false', this);
        this.is_playing = false;
      }, interval);
    }
    else {
      console.log('is_playing marked false', this);
      this.is_playing = false;
    }
  }

}

class Tree {

  constructor (name, location, sounds, observation, interval) {
    this.name = name;
    this.location = location;
    
    this.observation = null;
    this.can_play = false;
    // Tracks whether this sensor can play
    
    this.is_playing = false;
    this.is_loading = false;
    // Tracks whether this sensor is actually playing
    // timeout is not considered to play
    
    this.is_muted = false;
    // Whether it is muted
    
    this.interval = interval;
    // Time in between plays of a sound
    
    this.current_sound = null;

    this.load_sounds(sounds);
    this.set_observation(observation);
  }

  start () {
    // Pick a sound
    this.current_sound = choice(this.sounds);
    this.set_new_volume();
    if (this.current_sound.state() == 'unloaded') {
      this.is_loading = true;
      this.current_sound.on('load', () => {
        this.current_sound.play();
        this.is_loading = false;
      });
      this.current_sound.load();
    }
    else {
      this.current_sound.play();
    }
  }

  mute (is_muted) {
    this.is_muted = is_muted;
    this.sounds.map((sound) => sound.mute(is_muted));
  }

  load_sounds (sounds) {
    this.sounds = sounds.map((path) => (new Howl({
      src: [path],
      autoplay: false,
      loop: false,
      volume: 1,
      onplay: () => this.onplay(),
      onend: () => this.onend(),
      preload: false
    })));
  }

  set_observation (observation) {
    this.observation = observation;
    const time_since_observation = (Math.round(Date.now() / 1000) - window.soundscape.time_delta) - this.observation.timestamp

    if (this.observation && time_since_observation < window.soundscape.play_window) {
      this.can_play = true;
    }
    else {
      this.can_play = false;
    }
  }

  update_distance () {}

  set_new_volume () {
    const nowInSeconds = Math.round(Date.now() / 1000) - window.soundscape.time_delta;
    const volume = 1 - Math.max(0, nowInSeconds / window.soundscape.play_window);

    console.log('New volume ', volume);

    this.sounds.forEach((sound) => {
      sound.volume(volume);
    });
  }
  
  /**
   * Get delay time in milliseconds
   * @returns int
   */
  get_interval () {
    // define delay logic
    return randrange(this.interval[0], this.interval[1]);
  }

  onplay () {
    console.log('Tree started playing.')
    this.is_playing = true;
  }

  onend () {
    // console.log('Tree sound finished')
    let interval = this.get_interval();
    this.is_playing = false;
    this.can_play = false;
    if (interval) {
      window.setTimeout(() => {
        this.can_play = true;
        // console.log('is_playing marked false', this);
      }, interval);
    }
    else {
      this.can_play = true;
      // console.log('is_playing marked false', this);
    }
  }

}
