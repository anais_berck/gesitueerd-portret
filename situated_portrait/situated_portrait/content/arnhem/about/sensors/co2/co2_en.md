## CO2-sensor

![](/static/img/sensors/milesight-600x.jpg)

### Recognizable patterns

Day & night
:   during the day, trees absorb carbon dioxide (CO2), at night they emit it again.

Hot & dry
:   when it is very hot, trees close the stomata of their leaves to avoid excess water evaporation. This means they also do not absorb carbon dioxide and so more CO2 remains in the air.

### CO2 & Trees

Trees and plants are the only living organisms that produce their own food.

Like humans, trees breathe through tiny mouths on the underside of their leaves. Through these stomata, they breathe in CO2. The leaves capture sunlight. Water is sent from the roots to the crown. Water, sunlight and CO2 trigger photosynthesis in the leaves - in the chloroplasts to be precise. The photosynthesis produces sugars, which create fibres and allow the tree to continue growing above and below ground. The waste from this production process is hydrogen and oxygen. The latter we breathe in again. At night, trees release about half that carbon back in the air through respiration.

### Effects of climate change

Trees are therefore important tools in the fight against the climate crisis. Land plants remove about 29 per cent of all our CO2 emissions from the air, emissions that would otherwise contribute to the increase in atmospheric CO2 levels. And especially old trees absorb CO2, like the ones participating in this project. It takes several decades for a tree to absorb enough CO2. It has also been proven that a simple row of the same kind of trees absorb much less CO2 than rich, biodiverse forests. The massive tree-planting projects of big companies should therefore be viewed critically. Especially since we are still cutting down ancient, biodiverse forests all over the world. Protecting these would be a lot more efficient.

Trees store CO2 in their wood cells. When a tree is felled and burned, that CO2 is released again. When the tree is protected and when the wood from the cut tree is allowed to live on in houses, roofs and furniture, the CO2 is stored for many years to come.

### Sensortype

Air temperature is measured by the sensor that also calculates CO2 and air humidity. That sensor is suspended in the canopy of the tree.

The CO2 sensor measures values in ppm (parts per million). As of May 2024, the global average concentration of CO2 in the atmosphere is 425.22 ppm. This is a 50% increase since the start of the Industrial Revolution, compared to 280 ppm during the 10,000 years preceding the mid-eighteenth century.
