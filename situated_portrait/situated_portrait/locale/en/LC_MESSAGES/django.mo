��    +      t  ;   �      �     �     �     �     �     �  :   �          "     (     /     G     T  >   Y  <   �  
   �  :   �  7     	   S     ]     f     s     �     �     �     �     �  
   �     �     �                         ,     <     J     ]     a     j     n     v     �  B  �     �     �     �     �     �  2   �     +     7     ?     G  
   V     a  '   g  <   �  
   �  N   �  ,   &	  	   S	     ]	     e	     r	     	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     !
     1
     ?
     N
     R
     `
     d
     l
     }
        $                                                              +   %                           &                  	      
         !          )          *      "          '                     #   (    24 uur 3 dagen 7 dagen Beech Data De boom heeft de afgelopen 20 minuten geen data verstuurd. Douglas fir Duits Engels European horse-chestnut European oak Foto Gebruik oordopjes of een hoofdtelefoon voor de beste ervaring. Interactive art installation on trees in three Arnhem parks. Nederlands Observaties van de laatste 24 uur, van binnen naar buiten: Raak het scherm aan<br />om de symfonie te beluisteren. Red beech Sensoren Solitary oak Stop met spelen Sunlight (at douglas fir) Sweet chestnut Symfonie Terug naar de gids Toon data van de laatste Toon kaart Toon luchtfoto Verbeeld Verhaal about air humidity air pressure air temperature choose a tree circular increment co2 moisture now sapflow soil temperature temperature Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 24 hours 3 days 7 days Beech Data The tree has not sent data in the last 20 minutes. Douglas Fir Deutsch English Horse Chestnut Forest Oak Image Use headphones for the best experience. Interactive art installation on trees in three Arnhem parks. Nederlands Obervations of the last 24 hours, counter-clockwise, from the center outwards: Touch the screen<br />to start the symphony. Red Beech Sensors Solitary Oak Stop playing Sunlight (at douglas fir) Sweet Chestnut Symphony Back to the guide Show data of the last Show map Show aerial photo Imaged Story About Air Humidity Air Pressure Air Temperature Choose a tree Trunk movement Co2 Soil moisture now Sapflow Soil temperature Soil temperature 