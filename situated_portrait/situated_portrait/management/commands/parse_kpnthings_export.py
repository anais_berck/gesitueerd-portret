from situated_portrait.milesight.decoder import decode_message, hexstring_to_bytes
from situated_portrait.models import Device, DeviceType, Observation, Sensor
from django.core.management.base import BaseCommand, CommandError, CommandParser
import json
import datetime
from django.db.utils import IntegrityError


class Command (BaseCommand):
  help = 'Import export from KPN Things dashboard'

  def add_arguments(self, parser):
    parser.add_argument("exports", nargs='+', type=open)

  def handle (self, *args, **options):
    # timeMin = datetime.datetime(2024, 8, 12, 17, 10, 45, tzinfo=datetime.timezone.utc)
    # timeMax = datetime.datetime(2024, 10, 31, 17, 5, 0, tzinfo=datetime.timezone.utc)
    for path in options['exports']:
      chunks = json.load(path)
      
      for chunk in chunks:
        for record in chunk:
          if record['messageDirection'] == 'UP':
            try:
              payload = record['content']['payload']
              timestamp = datetime.datetime.strptime(record['timestamp'], '%Y-%m-%d %H:%M:%S.%fZ').astimezone(datetime.timezone.utc)
              
              # if timestamp > timeMin and timestamp < timeMax:
              data = decode_message(hexstring_to_bytes(payload))
              deviceId = record['deviceIdentification']

              device = Device.objects.get(remote_identifier=deviceId)

              if device.active:
                for (sensor_type, value) in data.items():
                  sensor = device.sensors.get(type=sensor_type)
                  observation = Observation(sensor=sensor, value=value, timestamp=timestamp, extra_data=json.dumps(data))
                  try:
                    observation.save()  
                  except IntegrityError:
                    print('Already exists', timestamp, data)
                    pass

            except KeyError:
              print("Could not parse", record)