dialogue = {
    'arnhem': {
        'en': [
            '{installation}',
            'Welcome to {installation}',
            'An installation that invites you to get to know some remarkable trees',
            'You discover their stories and also their mouvements, thanks to a series of sensors that are installed in the trees',
            'You are currently at the <strong>{tree}</strong>',
            'How would you like to meet this tree today?',
            [
                'group-direction',
                ['<strong>Story</strong> <br/>Read a story by the tree', 'story'],
                ['<strong>Imaged</strong> <br/>Look at the latest observations', 'portrait'],
                ['<strong>Symphony</strong> <br/>Listen to the sensors updating', 'soundscape'],
                # ['<strong>Data</strong> <br/>Enjoy an overview of the sensor data', 'data'],
                ['<strong>Map</strong> <br/>Walk to another tree', 'map'],
                ['<strong>About</strong> <br/>More about this project', 'about'],
            ],
            # [
            #     'group-about',
            #     'This project is an intitiative by Anais Berck, an artistic collective of humans, trees and algrithms; and receives the collaboration and support of Gemeente Arnhem.',
            #     ['How can I meet a tree?', "#p-5"]
            # ]
        ],
        'nl':[
            '{installation}',
            'Welkom bij {installation}',
            'Een installatie die je uitnodigt om een aantal bijzondere bomen te leren kennen',
            'Je ontdekt hun verhalen, en ook hun bewegingen dankzij een reeks sensoren die bij de bomen zijn bevestigd',
            'Je bent nu bij de <em>{tree}</em>',
            'Hoe zou je deze boom graag willen ontmoeten?',
            [
                'group-direction',
                ['<strong>Verhaal</strong> Lees een getuigenis van de boom', 'story'],
                ['<strong>Verbeeld</strong> Kijk naar de laatste metingen', 'portrait'],
                ['<strong>Symfonie</strong> Luister naar de updates van de sensoren', 'soundscape'],
                # ['<strong>Data</strong> Krijg een overzicht van de sensordata', 'data'],
                ['<strong>Kaart</strong> Wandel naar een andere boom', 'map'],
                ['<strong>Info</strong> Meer over dit project', 'about'],
            ]
            # [
            #     'group-about',
            #     'Dit project is een initiatief van Anais Berck, een artistiek collectief van mensen, bomen en algoritmes; en krijgt de medewerking en ondersteuning van de Gemeente Arnhem.',
            #     ['Hoe kan ik een boom ontmoeten?', ('guide', '#p-5')]
            # ]
        ],
        'de': [
            '{installation}',
            'Willkommen bei {installation}',
            'Eine Installation, die dazu einlädt, einige bemerkenswerte Bäume kennen zu lernen',
            'Sie entdecken ihre Erzählungen und auch ihre Bewegungen, dank einer Reihe von Sensoren, die in den Bäumen installiert sind',
            'Sie befinden sich derzeit am <em>{tree}</em>',
            'Wie würde es Ihnen gefallen, diesen Baum heute kennenzulernen?',
            [
                'group-direction',
                ['<strong>Erzählung</strong> <br/>Laß den Baum mir eine Erzählung erzählen', 'story'],
                ['<strong>Abgebildet</strong> <br/>Zeigen Sie mir die neuesten Beobachtungen', 'portrait'],
                ['<strong>Sinfonie</strong> <br/>Sensor-Updates anhören', 'soundscape'],
                #['<strong>Data</strong> <br/>Verschaffen Sie sich einen Überblick über die Sensordaten', 'portrait'],
                ['<strong>Karte</strong> <br/>Gehen Sie zu einem anderen Baum', 'map'],
                ['<strong>Info</strong> <br/>Mehr über dieses Projekt', 'about'],
            ],
            # [
            #     'group-about',
            #     'Dieses Projekt ist eine Initiative von Anais Berck, einem künstlerischen Kollektiv aus Menschen, Bäumen und Algorithmen; und erhält die Zusammenarbeit und Unterstützung von Gemeente Arnhem.',
            #     ['Wie kann ich einen Baum treffen?', ('guide', '#p-5')]
            # ]
        ]
    },
    'zonien': {
        'en': [
            '{installation}',
            'Welcome to {installation}',
            'An installation that invites you to get to know the remarkable Beech who lives diagonally to your left.',
            'It has a metal and a black band around its trunk.',
            'You discover his stories and also his mouvements, thanks to a series of sensors that are installed in the tree',
            'How would you like to meet this Beech today?',
            [
                'group-direction',
                ['<strong>Story</strong> <br/>Read a story by the Beech', 'story'],
                ['<strong>Imaged</strong> <br/>Look at the latest observations', 'portrait'],
                ['<strong>Symphony</strong> <br/>Listen to the sensors updating', 'soundscape'],
                ['<strong>Map</strong> <br/>Continue the walk', 'map'],
                ['<strong>About</strong> <br/>More about this project', 'about'],
            ]
        ],
        'nl':[
            '{installation}',
            'Welkom bij {installation}',
            'Een installatie die je uitnodigt om de Bijzondere Beuk te leren kennen, die schuin links voor je leeft.',
            'Zijn stam is voorzien van een metalen en een zwarte band.'
            'Je ontdekt zijn verhalen, en ook zijn bewegingen dankzij een reeks sensoren die bij de boom zijn bevestigd',
            'Hoe zou je deze Beuk graag willen ontmoeten?',
            [
                'group-direction',
                ['<strong>Verhaal</strong> Lees een getuigenis van de Beuk', 'story'],
                ['<strong>Verbeeld</strong> Kijk naar de laatste metingen', 'portrait'],
                ['<strong>Symfonie</strong> Luister naar de updates van de sensoren', 'soundscape'],
                ['<strong>Kaart</strong> Wandel verder', 'map'],
                ['<strong>Info</strong> Meer over dit project', 'about'],
            ]
        ]
    }
}
