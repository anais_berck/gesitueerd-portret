Wat donkere wolken drijven samen
de hemel wordt plots hel verlicht
gekraak, gedruis … een bliksemschicht
een foto waard, die luchtopname.
in mum van tijd, wat tellen later
als hemelsluizen opengaan
verlangen akkers drooggestaan...
naar bakken vol met regenwater.
de regen daalt nu heftig neder
het levensvocht voor vele dingen
laat dier en planten opnieuw zingen
met druppels nat, doch... zacht en teder!
door Jonhy Donovan, 2020

Tussen grijs en grauw
brengt één ijspauw kleur in
de morgenstilte.
door joz. le bruyn, 2024

De dag is te ruste gegaan op het talud vandaag
het zijn de mooie herinneringen die ik meedraag.
Het naambordje siert nog steeds de muur
glinstert na in de zon in dit avonduur.
Wie hier allemaal zijn geweest en door de deur zijn gegaan
het zijn na vandaag herinneringen die ook weer vergaan.
door annemieke steenbergen, 2020

Oh, ranke stengel
omringd door
groene schilden 
je groeit en bloeit
zo recht omhoog.
Zoekend naar licht
wijst je bloem
met gele kroon, 
bereidwillig
naar het oosten.
Een hart van goud
met duizend draden,
gesponnen echt ragfijn,
kunstwerk van jewelste,
je mag er best wel zijn.
door jonhy donovan, 2016

veel lichter als veder
dwarrelend in blauwe lucht
een losgekomen pluisje
geniet van zweven
speurt naar goede aarde
op zijn eenzame vlucht
door speelse wind
wat verder afgedreven...
strandt op rotsgrond
in de luwte
het voelt meteen,
dit is niet mijn thuis
beseft zijn lot,
maakt een kruis
mijn leven is sterven
op het zwarte gruis.
door jonhy donovan, 2017

De wind leek te huilen
om wat was geweest
grote vrienden met het blad
dat meeging
niet meer terug, maar hoog
de lucht in
als een langverwachte zucht
de wind en het blad
samen sterk en toch zo zwak
stralen geel kwamen door
waar wolken snel verdwenen
in het niets, op de vlucht
en met een laatste zucht
blies de wind z'n laatste adem
te zacht om het blad
te doen verdwijnen in het niets
te hard om te kunnen terugkeren
naar hoe het ooit was.
door Mariska Greven, 2005

Stel dat de sterren niet meer zouden stralen,
En de zon eens niet meer scheen.
Of als de maan nu eens voor altijd,
Achter de horizon verdween.
Hoe zou de wereld er dan uitzien,
Verdord en doods misschien?
Wees zuinig op wat ons is gegeven
Laat alles wat leeft leven.
door moeke, 2003

De geur van bloemen wel duizend zo schoon
geen mens die ’t ziet
ze zijn enorm gehaast
lopen de schoonheid der natuur voorbij
geen plant of boom kan hen verbinden
de schoonheid zien ze niet meer
explosies van natuurlijke kleuren
geen neus ophalend voor de heerlijke bos geuren
druk en een leven van verveling en haast
hetgeen mij steeds verbaast
alles in de natuur adembenemend mooi.
maar mens zit geketend rondom zijn eigen
gebouwde kooi
Overdenkend wat wel of niet is geweest
geen natuur of jaargetijden geeft 
hen nog een vreugdefeest.
door Guusje v.d.Wiel, 2004

Frisse dauwdruppels
stralende diamantjes
in de ochtendzon.
door Johan Defourny, 2004

Groen
puur
levend
duur
‘t mooiste 
op aarde
de natuur
door Monique Smit, 2003

Als adem
witte wolken waait
langs lusteloze luchten
dan dampt 
prachtig przewalski paard
van vogelvrije vluchten
Als aarde
groene grassen geeft
met machtig mooie mossen
dan drinkt de
vliegensvlugge vink
bij bont beboomde bossen
Als avond
rode ruikers ruilt
voor vederlichte veren 
dan danst de
zomerzotte zwaan
met maanverlichte meren.
door Jeroen Swaan, 2003

Hangende roze bottels,
veelkleurige asters
weelderig gras,
allen zoeken ze nog wat warmte.
De ene leunt tegen de andere,
hun stengels zijn te zwak.
Heesters en struiken verliezen
hun blad.
Sommigen wroeten om te blijven bestaan.
Hun leven hangt aan een zijden draadje.
Dood zullen ze gaan,
eenjarigen hebben nu eenmaal
een kort bestaan.
Wij danken hen voor de kleurenpracht,
die zij de ganse zomer 
hebben gebracht.
door claire vanfleteren, 2006

Het vertrouwen was groot
die dag in het bos
bij de kabbelende beek
zij sliep en hij keek
over het mos
wat de natuur hem bood
door Francis Portugaels, 2007

Ik luister naar de wind
het is net of die een liedje zingt
met een hele hoge toon
de bladeren ritselen in de boom
vogels zitten te fluiten
het is zo mooi hier buiten
het water beukt tegen de kade
in mijn hoofd wordt dit een serenade
overal hoor ik muziek
en ik ben het enigste publiek
ik zit op een bankje te rusten
en luister naar de natuur met al zijn lusten
door Thea Boerrigter, 2007


