from situated_portrait.models import Sensor, Observation
from django.db.utils import IntegrityError
from situated_portrait.emsbrno.find_daily_dendrometer_delta import select_min_max_per_day
import datetime

types = [
    ('growth_per_day', 'mm/24h', 'growth'),
    ('delta_per_day', 'mm/24h',  'delta'),
    ('max_per_day', 'mm', 'max'),
    ('min_per_day', 'mm', 'min')
]


def setup_synthetic_sensor (sensor, synthetic_type, unit):
  try:
    synthetic_sensor = Sensor(
      device = sensor.device,
      type = sensor.type,
      unit = unit,
      synthetic = True,
      synthetic_type = synthetic_type
    ).save()

    return synthetic_sensor
  except IntegrityError:
    print('Could not create synthetic sensor {} for {}'.format(synthetic_type, sensor))
  
  return None


def setup (sensor):
  for (synthetic_type, unit, _) in types:
    setup_synthetic_sensor(sensor, synthetic_type, unit)


def compute (sensor):
  today = datetime.date.today()
  synthetic_sensors = []

  # Loop through synthetic types. Make sure there is a synthetic sensor.
  # If it doesn't exist make one.
  # If it exists retrieve last observation to extract date of last observation
  for (synthetic_type, unit, key) in types:
    date_last_observation = None

    try:
      synthetic_sensor = Sensor.objects.get(device=sensor.device, synthetic=True, unit=unit, synthetic_type=synthetic_type)
      last_observation = synthetic_sensor.get_last_observation()
      if last_observation:
        date_last_observation = last_observation.timestamp.date()
    except Sensor.DoesNotExist:
      synthetic_sensor = setup_synthetic_sensor(sensor, synthetic_type, unit)
    
    synthetic_sensors.append((synthetic_sensor, key, date_last_observation))

  # Make list of dates of last observations
  dates_last_observation = list(map(lambda o: o[2], synthetic_sensors))

  # Get the smallest last observation from the recorded timestamps.
  # Unless one of the sensors did not have any observations.
  # Then try to go back as far as possible.
  if not None in dates_last_observation:
    # Define since based on that
    date_last_observation = min(dates_last_observation)
    since_date = date_last_observation + datetime.timedelta(days=1)
    since = datetime.datetime(since_date.year, since_date.month, since_date.day, 0, 0, 0, tzinfo=datetime.timezone.utc)
  else:
    since = None
  
  observations = []
  for row in select_min_max_per_day(sensor.pk, since):
    timestamp = datetime.datetime(row['date'].year, row['date'].month, row['date'].day, 12, 0, 0, tzinfo=datetime.timezone.utc)
    
    for (synthetic_sensor, key, date_last_observation) in synthetic_sensors:
      if (date_last_observation is None or row['date'] > date_last_observation) and row['date'] < today:
        observations.append(Observation(
          timestamp=timestamp,
          sensor=synthetic_sensor,
          value=row[key]
        ))

  Observation.objects.bulk_create(observations, ignore_conflicts=True)


def applicable (sensor):
    if sensor and sensor.device.deviceType.model == 'DRL26D' \
    and sensor.type == 'circular increment' \
    and sensor.unit == 'mm':
        return True
    else:
        return False
