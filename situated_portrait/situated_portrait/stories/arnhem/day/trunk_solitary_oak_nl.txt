Ik ben een Eik.
Jullie noemen me een inheemse soort hier in Nederland.
En toch ben ook ik ooit van elders gekomen.
Na de laatste Ijstijd reisden mijn voorouders
uit Zuid-Spanje en Zuid-Italië hierheen.
Wij waren dus ooit migrant.
Weet jij toevallig hoe lang het duurt bij jullie voor iemand geen migrant meer is?

Ik leef hier al 225 jaar.
Ooit werd ik aangeplant door een oudere man.
Hij werkte in opdracht van de toenmalige eigenaren van de Witte Villa.
Al meer dan twee eeuwen adem en observeer ik,
ontvang en groei ik,
verrijk ik jullie met zuurstof.
Beeld je eens in hoe de wereld er hier toen uitzag.
Kan je tien elementen opsommen die er niet waren toen ik een peuter was?

Ik ken het geluk
om als vrijstaande Eik te leven.
Wie mij ziet,
blijft even verrukt staan.
Ik ben een Eik die zich in alle richtingen
heeft kunnen ontwikkelen
naar wens en welbevinden
zonder enige hinder van andere bomen, gebouwen, rioleringen of ondergrondse parkings
Ik ben het toonbeeld
van de ideale persoonlijke ontwikkeling.
Het staat je vrij om je ten allen tijde
aan mij te spiegelen.

Ik leef in deze Moerasweide.
Het is hier altijd lekker nat en drassig.
De Beuk even verderop zou het hier niet redden.
Zijn oppervlakkige wortels zouden al snel beginnen rotten.
Mijn wortels reiken dieper.
En net zoals een van mijn takken altijd als een centrale speer de hoogte in zal groeien,
is er altijd een wortel die de diepte ingaat
sterk, krachtig, lang en dik.
Het is mijn penwortel, mijn duurzame anker.

Ik leef hier al vele jaren. Het is onmogelijk voor me om hier te vertrekken.
Wij planten zijn sedentair. We kunnen niet vluchten.
Dat maakt dat we anders gebouwd zijn dan jullie, mensen.
Om één hart te hebben op één welbepaalde plaats, of één long, of één plek voor één stel hersens,
dat zou al heel snel onze dood betekenen.
We zouden ons niet kunnen weren tegen een vijand die een stuk van ons lichaam afrukt.
Wij groeien dan ook duizenden geslachtsorganen, en evenveel neuzen als er bladeren, stam en wortels zijn.
Wij hebben altijd een Plan B.
Wij zijn modulair.
Wij zijn niet één ik, maar duizenden 'ikjes' tegelijk.
Wij kunnen 'ik' zeggen met eender welk deeltje van ons lichaam.

Veel parkbezoekers houden van me, omdat mijn verschijning lijkt op die van de Levensboom, een belangrijk symbool in vrijwel alle grote religies en culturen. De kosmische boom maakt de verbinding tussen Hemel en Aarde, tussen goden en mensen. Je zou kunnen zeggen dat mijn wortels verbinding maken met de Onderwereld. En met mijn takken die naar de hemel reiken, sta ik in verbinding met het Universum. Je zou me dus kunnen inschakelen als een bemiddelaar tussen jezelf en onzichtbare wezens in andere dimensies.

Mijn breed uitwaaierende gelijkmatige takken herinneren parkbezoekers aan de Levensboom. Die symboliseert het eeuwige leven. Je kan de wisseling van de seizoenen erin lezen het: uitlopen van de knoppen in de lente, de groei in de zomer, de vruchten en het verval in de herfst en het afsterven in de winter. Dat afsterven is maar een schijnbaar sterven. Elke lente loop ik weer uit, en krijg ik nieuwe groene blaadjes. Zo word ik symbool van de overwinning op de dood.

Odysseus, het hoofdpersonage uit de Griekse avonturenroman van Homerus, was de koning van Ithaka. Jarenlang reisde hij rond. Toen hij weer thuiskwam, zag hij eruit als een bedelaar, een zwerver met vuile kleren en een lange baard. Zijn vrouw, koningin Penelope, kon niet geloven dat hij haar verloren gewaande echtgenoot was. Bij wijze van test vertelde ze hem dat ze hun bed had laten verplaatsen. Odysseus, die het bed zelf had gesneden uit een Eik die zijn wortels had in de fundering van het huis zelf, barstte uit van woede. Penelope huilde van geluk.

Als Eik worden me vele mythische verhalen toegedicht. Laat me er nog een met je delen, over Wodan, de Germaanse oppergod die in Eiken werd vereerd. Naar hem zijn de Wodanseiken van Wolfheze genoemd. Onder die reusachtige Eiken werd vroeger recht gesproken, omdat de bladeren van die bomen de aanklagers het recht zouden influisteren.

Ooit, wanneer mijn takken van vermoeidheid neerwaarts buigen en ik een begin zal maken aan mijn einde, hoop ik het gezelschap te krijgen van de maretak, ook al is het een parasiet die zal wortelen in mijn hout en daar water en mineralen af zal tappen.
Bij de Kelten en Germanen was de maretak die op een Eik groeide, een heilige plant. Met een gouden sikkel sneed de druïde in de midwinterceremonie de maretak uit de Eik. De afgesneden plant mocht de grond niet raken en werd in witte doeken opgevangen. Daarna dompelde de druïde de maretak in water. Dat werd als bescherming tegen ziekten en onheil gebruikt. Zou het niet mooi zijn als toekomstige parkbezoekers opnieuw maretak in hun huizen zouden hangen?

Ooit was de natuur heilig voor de volkeren uit deze streken. De Kelten bouwden geen kerken of tempels, maar legden een offersteen in het woud. Ze organiseerden hun rituelen in open lucht, onder het ruisende loof van natuurlijke tempels. Hun oppergod Dagda, die de seizoenen conroleerden, vereerden ze in een Eikenboom.

Een Eik zoals ik, van 200 jaar oud, heeft zowat tien miljoen eikels voortgebracht. Kan je je voorstellen hoe belangrijk die rijke oogst was voor jullie voorouders? Toen graan nog niet bekend was, vormden eikels het basisvoedsel voor de mens, die ze maalde en tot brood verwerkte. Verrast het je dan dat wij Eiken als heilige bomen werden vereerd?

Ooit lang geleden hadden bezoekers de gewoonte om me toestemming te vragen voor het gebruik van mijn bladeren. Ze zijn heel efficiënt. Neem mijn verse bladeren, stamp ze klein, kook ze in water en gebruik dat water voor het schoonmaken van een wonde. Het vermijdt ontstekingen, zuivert en heelt de wonde.

Eik is een Oud-Germaans woord dat ‘boom’ betekent. De boomgriffier vergeleek het met het woord dat jullie bijna dagelijks gebruiken: googelen. De eigennaam wordt soortnaam omdat het zo aanwezig is.
Het Griekse woord voor Eik is ‘drus’. Het lijkt op het woord ‘druïde’ voor de Keltische priesters, die ook wel Eikmensen genoemd werden. Eiken werden door de Kelten vereerd als Heilige bomen.

Voor veel parkbezoekers ben ik hun favoriete boom. Een vrijstaande Eik als ik staat symbool voor kracht, seksualiteit, macht en een lang leven. Ik ben in jullie onderbewustzijn verbonden met vuur en vruchtbaarheid. Jullie warmen jezelf en bouwen met mijn hout, voeden je met mijn vruchten, en dit al eeuwen lang. Dat maakt me ook een zinnebeeld van generositeit.
