Elke dag vangen mijn bladeren indrukken op van jullie soortgenoten. 'Die bomen staan in de weg,' zegt een enkele als grap. 'Wat zijn ze mooi,' zegt iemand anders. Ze worden even stil. En dan komt steevast de vraag 'Hoe oud zouden ze zijn?'. Veel passanten hebben wel iets over ons gelezen. 'Ze zijn meer dan 350 jaar oud,' zeggen ze dan en ze lijken het gewoon te vinden.
Kan je je inbeelden wat wij in 370 jaar zoal hebben zien veranderen rondom ons?

Een oude knar als ik heeft een huid die gespleten en gegroefd is doorheen de eeuwen.
Mijn dikke gegroefde schors zit vol gaatjes, haakjes en scheurtjes.
Spinnen vinden dat heerlijk:
ze hebben de grootste keuze aan aanhechtingspunten voor hun web.
Ik huisvest dan ook - niet zonder enige fierheid - een groot aantal webben.
Heb je ze al eens geteld?

Koolmezen, pimpelmezen, merels, spreeuwen, vinken, winterkoninkjes,
ze vinden allemaal de weg naar mijn takken.
Ze komen hier rusten, nacht na nacht.
Sommige broeden hier ook.
Die holtes - waar ooit takken zaten - zijn de perfecte schuilplaatsen.

Samen met mijn kameraden zijn we de oudste tamme kastanjes in
Nederland. We zijn grillig, en hebben veel holtes.
Daarom zijn we populair bij dieren en
vogels die hier graag komen rusten.
Ik kan met enige eer zeggen
dat ik één van de populairste rustplekken ben van de bosuilen die hier in het bos leven.
Je kan hen hier 's avonds ontmoeten, wanneer ze komen slapen.
Een enkele keer hebben ze hier ook gebroed.
Ook de boommarters maken gretig gebruik van de holtes.
Ik voel me als een reusachtig gastenverblijf:
elke van mijn holtes wordt door verschillende dieren gebruikt.

Ondanks mijn hoge leeftijd
maak ik nog elk jaar tamme kastanjes aan.
Eekhoorns, gaaien, kraaien, muizen,
ze vinden mijn vruchten heerlijk en voedzaam.
Jullie mensen rapen ze ook wel eens.
Voorbije herfst vertelde een mens aan zijn jongen dat hij ze verrukkelijk vindt,
gepoft, geroosterd, zelfs rauw.

Wij zijn lichtwezens.
Wij voeden ons met licht.
Onze cellen, ons hout, onze vruchten zijn gemaakt met zonlicht.
Telkens wanneer je iets plantaardig eet, weet dan dat je je voedt met licht.
Laat je verlichten!

Vinken zijn een van mijn trouwe bewoners, jaar na jaar. In hun veren voeren ze geuren en beelden van andere bomen en dieren in het verre Zuiden. Het is altijd een genoegen o hun zang weer te horen tijdens de lente en de zomer. Ze zingen altijd hetzelfde intense en melodieuze riedeltje, soms wel tien keer per minuut.

De Spinnen die hun web weven tussen mijn bladeren en in mijn holtes zijn een lekkernij voor de Winterkoninkjes. Deze vogels maken hun nest in de lage struiken in de buurt. Je zal ze amper te zien krijgen, zo klein zijn ze. Maar hun zang is opvallend luid, helder, met vibrerende scherpe trillers. Eens je die heb gehoord, vergeet je die nooit meer. Ze zijn ook talrijk. Ze broeden soms tot drie keer per jaar.

Weet je dat ik onderdak biedt aan een Vlinder die zich hier zo goed voelt, dat hij zo’n vierhonderd eitjes per week legt? De rupsen van de Dwergbladroller leven in mijn blad, dat ze handig oprollen of samenvouwen. Ze voeden zich met mijn loof en knoppen. Gelukkig heb ik er daarvan meer dan genoeg.

Onder mijn bladeren leeft de Kastanjesnuitkever. Hij legt eitjes in wat er nog rest van mijn kernhout. Ze zijn een van de redenen dat ik zo hol ben geworden vanbinnen. Zijn larven voeden zich met dit zachte, volle hout. In de herfst komen ze uit en overwinteren ze in de bodem bij mijn wortels. In het lente zie ik hen weer verschijnen, groot en volwassen. Dan volgt de grote trek terug naar mijn kruin.
