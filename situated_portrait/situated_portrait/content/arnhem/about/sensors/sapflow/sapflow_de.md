## Saftstrom

![](/static/img/sensors/sapflow.jpg)

### Erkennbares Muster

Sonnenlicht und Luftfeuchtigkeit
:   Messungen des Saftflusses zeigen, wie empfindlich die Bäume auf ihre Umwelt reagieren. Eine Wolke, ein Regenschauer, eine sonnige Periode wirken sich sofort auf den Saftfluss aus. Man kann zum Beispiel sehen, dass der Saft an warmen, sonnigen Tagen viel schneller fließt - und an kühlen, regnerischen Tagen langsamer.

Das hat mit der Photosynthese zu tun: Je mehr Sonne, desto mehr Zucker wird produziert und desto mehr Wasser fließt von den Wurzeln in die Baumkronen. Oben angekommen, trägt das Wasser zur Erzeugung von Zucker in den Blättern bei und verdunstet. Der Zucker wird über einen zweiten Saftstrom zu den Wurzeln transportiert.

eachten Sie, dass die Bäume an zu heißen Tagen, wenn das Gleichgewicht zwischen Sonnenlicht und Wassermenge nicht ausgeglichen ist, ihre Spaltöffnungen (kleine Öffnungen auf der Rückseite der Blätter) schließen, die Photosynthese beenden und den Saftstrom verlangsamen.

### Warum Saftstrom

Der Saftstrom eines Baumes kann mit unserem Blutkreislauf verglichen werden, nur dass er in zwei Richtungen fließt: von den Wurzeln zur Baumkrone (Transport von Wasser und Mineralien) und von der Baumkrone zu den Wurzeln (Transport von Zuckern). Die Wassermenge ist für eine Vielzahl von Prozessen in Pflanzen entscheidend: Photosynthese, Verdunstung, Fortpflanzung, Wachstum, Organentwicklung, Zellstruktur und Zellfunktion und vieles mehr.

Die Menge und die Geschwindigkeit des Saftflusses geben auch Aufschluss über die Fähigkeit der Bäume, auf Bodenwassermangel, Trockenheit und extreme Wetterbedingungen zu reagieren.

### Sensortype

Der Saftstrom wird auf der Grundlage der Fließgeschwindigkeit in den Transportzellen direkt unter der Baumrinde berechnet. Der Sensor besteht aus drei in den Baumstamm gebohrten Nadeln. Die mittlere Nadel gibt ein Wärmesignal ab. Die Nadeln oberhalb und unterhalb des Wärmesignals messen die Temperatur stromabwärts und stromaufwärts der Wärmequelle. Die Reisezeit des Wärmesignals wird also an zwei Stellen gemessen. Wasser ist ein starker Leiter. Durch die Messung der Zeit, die das Wärmesignal benötigt, um beide Stellen zu erreichen, können wir die Geschwindigkeit des Wärmesignals bestimmen.

Der Sensor wendet dann einige mathematische Formeln an: Die Wärmesignalgeschwindigkeiten werden in Saftflussdichten umgewandelt und auf den Querschnitt des Stammes angewandt, um eine Vorstellung vom gesamten Saftfluss zu erhalten. In Online-Foren diskutieren die Entwickler aktiv darüber, welche mathematischen Formeln die besten Ergebnisse liefern.

Wir haben einen Saftstromsensor an der Douglasie angebracht, weil für den Sensor drei Löcher in die Rinde gebohrt werden müssen. Douglasien produzieren schnell Harz, wenn ihre Rinde verletzt wird, und können sich damit besser schützen vor einer Invasion von Bakterien und Pilzen als alte Laubbäume.
