An warmen Sommerabenden strömen Hunderte von jungen Menschen in diesen Teil des Parks.
Sie spielen, flattern, lachen, trinken, tanzen.
Manche suchen sich sogar einen geschützten Platz, um sich zu paaren.
Es ist wie ein Festival an meinen Wurzeln.
Hoch in meiner Krone findet ein weiteres Fest statt, das der blauen Eichen-Zipfelfalter.
Männliche Schmetterlinge verstecken sich zwischen meinen Blättern.
Auf der Suche nach einem Partner fliegen die Weibchen hoch hinaus.
Die Männchen flattern den Weibchen nach und liefern sich Luftkämpfe mit Rivalen.
Hunderte von Schmetterlingen flattern und schwirren um meine Blätter, manchmal bis spät in die Nacht.
Manche finden eine dunkle Stelle in meiner Krone, um sich zu paaren.

Ich habe viele Bewohner und treue Besucher.
Ich bin wie das New York der Niederlande, der Baum, in dem die meisten Vogel-, Insekten-, Pilz-, Moos- und Tierarten Schutz und Nahrung finden. Wenn Sie sich meine Wurzeln, meinen Stamm und meine Krone als Abbild vorstellen, würden Sie einen Wolkenkratzer neben dem anderen sehen.
Dagegen hat ein Neuankömmling wie die Platane kaum Besucher oder Bewohner.
Aber eine uralte Spezies wie ich, die hier seit mehr als zehntausend Jahren gedeiht, ist mit allem, was lebt, bis in die kleinsten Fasern verwoben.

Die Eichelhäher sind gute Freunde von mir. Vielleicht finden Sie eine ihrer blauschwarz gestreiften Federn, wenn Sie den Park besuchen.
Niederländischsprachige haben ihm den Namen 'der ständig scharrende Eichelsucher' gegeben.
Er ist sehr laut, aber das vor allem im Herbst, wenn er meine Eicheln für den Winter im Boden versteckt.
Mit etwas Glück vergisst er dann, sie wieder auszugraben. So können meine kleinen Sprösslinge an den unerwarteten Orten auswachsen.

In diesem Frühjahr hatte ich wieder das Vergnügen, ein Krähennest hoch oben in meinen Ästen zu beherbergen.
Zweig für Zweig schichteten und flochten die Krähen sie zu einer Plattform, die sie zusätzlich mit Erde und dickeren Ästen befestigten.
Jahr für Jahr überraschen sie mich auch mit den Materialien, die sie zum Auskleiden ihrer Nester finden. Früher rupften sie Wollfetzen aus dem Vlies der Schafe, die weiter weg lebten. Jetzt finden sie Wolle, Draht und Papier, die von Menschen verarbeitet wurden. Und natürlich viele Federn und Gräser, die so hoch oben in meiner Krone einen besonderen Duft verbreiten.

Besonders stolz bin ich auf den jungen Holunder, der einen günstigen Platz in der Nähe meines Stammes gefunden hat.
Er liebt die feuchte Erde. Sein Samen ist hier gelandet, dank der Kohlmeise, einer anderen treuen Besucherin, die gerne einen meiner unteren Zweige benutzt, um sich zu erleichtern. Holunderbäume sind bekannt als Schutzbaum vor bösen Geistern und Wunderheiler. Mit der Zunahme des ermüdenden Verkehrslärms habe ich jahrelang geduldig darauf gewartet, dass ein Holunder wächst und die Atmosphäre vor negativen Einflüssen schützt. Also ermutige ich sie, schnell zu wachsen und viel Platz auf und unter der Erde einzunehmen.

Meine Früchte sind seit Jahren nicht mehr geerntet worden.
Früher war das anders.
Eine würdevolle alte Dame, die gestern mit kurzen Schritten hier vorbeikam
erinnerte sich.
Als Kind kam sie im Herbst jeden Tag, um meine Eicheln zu sammeln.
Sie haben sie getrocknet und geröstet,
zermalmt,
und sie in Getreidekaffee verwandelt.
Es hat furchtbar geschmeckt.
Es war also Krieg.

Wir sind Lichtwesen. Wir ernähren uns vom Licht. Unsere Zellen, unser Holz, unsere Früchte sind aus Sonnenlicht gemacht.
Wann immer Sie etwas Pflanzliches essen, sollten Sie wissen, dass Sie sich von Licht ernähren. Lassen Sie sich erleuchten!

Meine Früchte wurden einst zur Schweinemast verwendet. Sie sind sehr nahrhaft und enthalten mehr als ein Drittel Fett. ‘Der beste Speck wächst auf Eichen’, ist ein altes niederländisches Sprichwort, das ich treu in meinem Zellgedächtnis bewahre. Dank der Schweine genossen wir auch einige Privilegien: Wir durften nicht gefällt werden, und wir Stieleichen, die mehr und größere Eicheln produzieren als die Traubeneiche, wurden weiterverbreitet.

Sehen Sie die junge Eberesche am Fuße meines Stammes? Sie ist ein Neuling. Sie wächst auf jedem Boden und bei jedem Wetter. Weil sie Kälte so gut verträgt, war sie einer der ersten Bäume, die nach der Eiszeit in den Niederlanden auftauchten. Ich freue mich schon auf den Tag, an dem sie Blüten und Beeren trägt. Dann werde ich noch mehr Vögel zu Besuch haben und Sie können aus den Beeren köstliche Marmelade und Tee machen.

Neben meinem Stamm wächst eine Pflanze mit zarten weißen Blüten: Klettenlabkraut, auch Klebriges Gras genannt, der Liebling der Kinder im Sommer. Die Pflanze klebt und haftet an allem, was an ihr vorbeischlendert. Ihr Stamm und ihre Früchte sind voller Fanghaken. Sie bleiben im Fell der Tiere und an der Kleidung haften. So verbreitet diese clevere Pflanze ihre Früchte über weite Strecken.

Wenn Sie näherkommen, sehen Sie eine Menge Klee im Gras unter meiner Krone wachsen. Dafür bin ich dankbar: Klee kann durch Bakterien in seinen Wurzeln Stickstoff aus der Atmosphäre aufnehmen. Klee ist der perfekte Bodenverbesserer. Pro Hektar kann er dem Boden zwischen 55 und 170 kg Stickstoff hinzufügen. Parkbesucher sammeln die Blüten auch, um daraus köstlichen Tee zuzubereiten.

In den Ästen in meiner Krone leben viele Spinnen. Ihre Webkünste erstaunen mich immer wieder. Eine sitzt gerade etwa auf halber Höhe meines Baumkronendachs. Ungestört spinnt sie einen langen Seidenfaden und lässt ihn vom Wind zu einem parallelen Ast etwas weiter oben tragen. Wenn sie den Eindruck hat, dass der Faden fixiert ist, gleitet sie gelassen auf die andere Seite, wo sie den Faden zurechtrückt, verstärkt und ungerührt weiterspinnt. Sie baut ein feines neues Netz, das beim Wachsen sanft im Wind wogt. Bald, wenn es fertig ist, wird sie dort Mücken und Fliegen als leckeres Fingerfood fangen. Und in den Fäden hält sie so ziemlich alles fest, was hier in dieser Höhe passiert. Sie ist die Archivarin der Wirbellosen.

Einige meiner Gäste verstecken sich tagsüber in meinen Blättern. Nachts kommen sie hervor und fliegen dem Licht entgegen. Es sind die Eichenblattroller, kleine grüne Motten. Nach dem Winter, wenn die Raupen schlüpfen, kann es heikel werden. Sie fressen dann meine jungen Triebe. Später machen sie eine Kerbe in eines meiner Blätter, rollen sich hinein und verpuppen sich dort. Wenn sie einmal Schmetterlinge sind, schlafen sie tagsüber noch in meinen Blättern. Nachts fliegen sie heraus. Dann sammeln sie sich in großen grünen Schwärmen.

Darf ich Euch meine treuen Gäste vorstellen? Eichelkäfer sind Rüsselkäfer, die meine Früchte in Kinderstuben umwandeln. Sie haben eine lange Schnauze, mit der sie ein Loch in meine Eicheln bohren. Dort legt das Weibchen seine Eier ab. Die geschlüpfte Larve fühlt sich dort sicher und findet in meinem Keim auch leckere Nahrung. Die Larve verpuppt sich und nach einiger Zeit werde ich zusehen, wie aus meinen Früchten ein neuer Käfer schlüpft. Eicheln kann ich zum Glück in Hülle und Fülle produzieren.

Habt Ihr neulich meine Maigäste hier gesehen? Die Maikäfer finden meine Blätter köstlich. Sie sind nicht zu beneiden, wohlgemerkt. Sie leben bis zu drei Jahre unter der Erde. Dort fressen sie die Wurzeln der Gräser und knabbern manchmal auch an meinen. Nach der Verpuppung leben sie noch ein paar Wochen. Dann bin ich ihr Lieblingsrestaurant. Es gab Jahre, da haben sie es richtig übertrieben. Es waren so viele, dass ich eingreifen musste und Wirkstoffe produziert habe, die sie nicht mögen. Die Eiche hier neben mir riecht dann den Stoff und ist wiederum alarmiert. Deshalb sieht man Maikäfer manchmal auf andere Bäume abwandern.

Seit etwa dreißig Jahren bekomme ich auch Besuch vom Eichenprozessionsspinner, der als unscheinbarer brauner Falter auftaucht. Die Raupen können aufgrund der Brennhaare, mit denen sie sich verteidigen, gefährlich werden, auch für uns. Einige meiner anderen treuen Besucher, die Kohlmeisen, sind manchmal überrascht. Wenn sie mit ihrem kurzen Schnabel eine dieser Raupen schnappen, berührt ihr Kopf die brennenden Haare, was schmerzhaft sein kann. Die Besucher, die mit Raupen gut umgehen können, sind die Baumläufer. Mit ihren langen, gebogenen Pinzettenschnäbeln schnappen sie die Raupen hinter dem Kopf, schaben die Haare ab und voilà, guten Appetit!
