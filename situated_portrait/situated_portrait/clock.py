import datetime
import pytz
from .models import Tree, get_range

def remap_list(observations, min, max, new_min, new_max):
    return [((o.value - min) / (max - min) * (new_max - new_min) + new_min) if (max - min) != 0 else 0 for o in observations]

def remap_clock(sensor_clock, min, max, negative = False, compare = False):

    if min != max:

        if compare:
            # this is the value 24h before now
            sensor_clock['baseline'] = list(sensor_clock['serie'])[-1].value

            # this remove the baseline from everything so we can see 
            # what went above and below during the last 24h 
            for obs in sensor_clock['serie']:
                old_value = obs.value
                obs.value = obs.value - sensor_clock['baseline']
                # print(str(old_value) + 'mm', obs.value)

            # redefining min and max
            min = round(min - sensor_clock['baseline'], 2)
            max = round(max - sensor_clock['baseline'], 2)
                
            sensor_clock['remapped'] = list(map(lambda obs : obs.value, sensor_clock['serie']))

        else:
            sensor_clock['remapped'] = remap_list(
                sensor_clock['serie'],
                min, max,
                0 if not negative else -1, 1)
        
        sensor_clock['min'] = min
        sensor_clock['max'] = max

    return sensor_clock

def opacity(i, m):
    return (min(((m - i) / m), 0.25) * 4) ** 2

def angle(now, yesterday, t):
    range = now.timestamp() - yesterday.timestamp()
    t = now.timestamp() - t.timestamp()
    return ((t / range) * -1) - 0.25

def generate_clock(tree, now, yesterday, longtimeago):

    tree.clock = tree.get_clock(yesterday, now)
    
    # --- CROSS-TREE EXTREMUMS PER SENSOR TYPES
    # today_range = get_range(
    #     round(yesterday.replace(minute=0,second=0,microsecond=0).timestamp()),
    #     round(now.replace(minute=0,second=0,microsecond=0).timestamp())
    # )
    long_range = get_range(
        round(longtimeago.replace(hour=0,minute=0,second=0,microsecond=0).timestamp()),
        round(now.replace(hour=0,minute=0,second=0,microsecond=0).timestamp())
    )
    # print(long_range)

    # --- REMAPPING CLOCK VALUES BETWEEN 1 AND 0 (OR -1)
    unavailable = []
    for sensor_key, sensor_clock in tree.clock.items():
        if not 'serie' in sensor_clock or len(sensor_clock['serie']) == 0:
            unavailable.append(sensor_key)
        else:

            if 'co2' in sensor_clock['classes']:
                sensor_clock = remap_clock(sensor_clock, sensor_clock['min'], sensor_clock['max'])
            elif 'circular' in sensor_clock['classes']:
                sensor_clock = remap_clock(sensor_clock, sensor_clock['min'], sensor_clock['max'], True, True)
            elif 'sapflow' in sensor_clock['classes']:
                sensor_clock = remap_clock(sensor_clock, sensor_clock['min'], sensor_clock['max'], True, True)

            elif 'humidity' in sensor_clock['classes']:
                print(sensor_key)
                print(long_range[sensor_key]['min'], long_range[sensor_key]['max'])
                sensor_clock = remap_clock(sensor_clock, long_range[sensor_key]['min'], long_range[sensor_key]['max'])

            elif 'temperature' in sensor_clock['classes']:
                print(sensor_key)
                print(long_range[sensor_key]['min'], long_range[sensor_key]['max'])
                sensor_clock = remap_clock(sensor_clock, long_range[sensor_key]['min'], long_range[sensor_key]['max'], True)

            # adding the opacity
            # and the angle of rotation 
            if 'remapped' in sensor_clock and len(sensor_clock['remapped']) > 0:
                m = len(sensor_clock['remapped'])
                sensor_clock['remapped'] = [ 
                    {
                        'v': sensor_clock['remapped'][i], 
                        't': sensor_clock['serie'][i].timestamp,
                        'd': sensor_clock['serie'][i].value,
                        'a': angle(now, yesterday, sensor_clock['serie'][i].timestamp),
                        'o': opacity(i,m),
                    } 
                    for i in range(m)
                ]
    for sensor_key in unavailable:
        del tree.clock[sensor_key]

    return tree