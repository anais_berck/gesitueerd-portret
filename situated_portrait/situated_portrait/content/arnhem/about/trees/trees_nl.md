# Bomen

<!-- Zes belangwekkende bomen nemen deel aan dit project: -->

## Allenige Eik

![](/static/img/trees/eik1-600x.jpg)

Deze indrukwekkende Eik in de Moerasweide is de publiekstrekker van Sonsbeekpark. Sterk, gezond en evenwichtig. Zijn takken lopen breed en grillig uit. Je herkent de boom ook aan zijn gegroefde schors en zijn gelobde bladeren.
De Allenige Eik biedt ons een spiegel voor zelfontwikkeling in alle vrijheid. Vanuit zijn stam deelt deze boom dan ook verhaalfragmenten over de Levensboom en de Eik als belangrijk symbool in onze geschiedenis. 's Nachts reciteert hij gedichten over Liefde.

## Paardenkastanje

![](/static/img/trees/european_horse_chestnut-600x.jpg)

Deze boom bij de Witte Villa herken je aan zijn gewonde stam. Zij kreeg veel ongenodigde bezoekers, schimmels en bacteriën. Maar je ziet hoe gezond en levendig haar kruin nog is. En zag je haar mooie witte kaarsbloemen deze lente?
De Paardenkastanje leeft al 188 jaar bij de Witte Villa. Doorheen de jaren veranderden de bewoners van de villa en de gebruikers van het park. Deze boom deelt vanuit haar stam verhaalfragmenten over die geschiedenis. 's Nachts reciteert zij gedichten over de Dood.

## Boseik

![](/static/img/trees/european_oak-600x.jpg)

Deze Eik, onderweg naar de Belvédère-toren, staat een beetje achterin, voorbij de jonge aangeplante boompjes. Hij is 200 jaar oud, net als alle grote Eiken in dit stukje bos. Verwar de Eiken niet met Beuken. Die laatste hebben een grijze gladde schors. En de kruin van een Eik groeit veel grilliger en artistieker dan die van de beuk.
Deze Eik is oud en leeft erg rustig. Samen met zijn soortgenoten observeert hij de effecten van de klimaatverandering en reflecteert daarover. De verhalen die hij deelt vanuit zijn stam zijn toekomstverhalen. 's Nachts reciteert hij gedichten over Stilte.

## Rode Beuk

![](/static/img/trees/beuken-600x.jpg)

Bij het kasteel van Zypendaal lijkt één reusachtige Beuk te leven, maar het zijn er vijf. Hun flinterdunne schors verdraagt geen zon. Daarom ritsen zij zich dicht met een jas van bladeren. Tussen hen in is het als in een tempel, rustig en koel.
Het onderzoek naar Sporen van Slavernij dat onlangs werd gepubliceerd, maakt oude herinneringen wakker bij deze Rode Beuk. Hij heeft Anna van Vossenburg persoonlijk gekend, en deelt die ervaringen vanuit zijn stam. 's Nachts geeft hij je gedichten over Magie.

## Poortwachter / Kastanje

![](/static/img/trees/3watchers-600x.jpg)

375 jaar geleden werden deze drie Kastanjes aangeplant als eerbetoon aan de Romeinen die de Tamme Kastanje tot in het Noorden brachten. De drie Poortwachters staan hier bijna even lang als de Eusebiuskerk. Ooit kregen ze bezoek van Rembrandt. Heb je hen eenmaal ontmoet, dan reizen ze graag mee in je herinnering.
Deze Poortwachter is een leermeester over de Tijd in al haar belevingen. Vanuit zijn celgeheugen deelt hij verhalen over bomen in wereldse mythen en tradities. 's Nachts reciteert hij gedichten over Tijd.

## Douglasspar

![](/static/img/trees/douglas_fir-600x.jpg)

Deze spar behoort tot een van de meest aangeplante boomsoorten in Nederland. Zijn hout werd gebruikt om de gangen in de mijnen te stutten. Je herkent hem aan zijn kurkachtige schors en zijn dennenappels dragen grappige muizenstaartjes bij elke schub.
De Douglasspar deelt vanuit zijn kernhout verhaalfragmenten over het aanplanten en beheren van bossen, over exoten als hij die veel winst opleveren. 's Nachts reciteert hij gedichten over Natuur.
