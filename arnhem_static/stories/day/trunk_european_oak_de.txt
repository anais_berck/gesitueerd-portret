Ich mag Zukunftsszenarien.
Diese Fassung wurde mir von der Autorin Jibbe Willems anvertraut.
Dies ist eine weitere Warnung an Sie, die Menschen:
Unsere Geduld reißt Beton auf,
zerbricht die dicksten Mauern,
überwuchert die höchsten Gebäude,
unsere Triebe durchbrechen die Steinschichten,
verschleißen Straßen und Gehwegen.
Und den Rohling, den Schuft und Abschaum, die sich für
besser halten als die Natur,
werden wir überwuchern und ersticken.
Wir werden ihnen den Atem aus den Lungen ziehen
und uns an ihren verwesenden Leichen laben.
Und dann wird
alles, was einst zweibeinig war
als Gras zurückkehren.
Unsere Geduld bricht Beton auf,
und
wir werden die Erde erben.

Ich bin eine Eiche.
Ich wurde hier vor 225 Jahren gepflanzt, im Jahr 1799, zusammen mit all den anderen ähnlichen Eichen, die Sie hier sehen.
Die Besitzer des Anwesens hofften, viel Geld mit uns zu verdienen.
Es ist anders gekommen. Wir sind immer noch hier und werden Sie überleben.

Weil wir Eichen hier so dicht beieinanderstehen, sind wir schmal und hochgewachsen und haben das Licht gesucht.
Wir sind so alt wie die Einsame Eiche, die auf der Sumpfwiese steht.
Ihr Stamm und ihre Äste sind viel dicker als unsere, aber sie haben die gleiche Anzahl von Jahresringen.
Sie wächst dort allein, wir bilden eine Gruppe.

Ich würde das Leben in diesem Teil des Parks als friedlich bezeichnen.
Seit die bewaldeten Ufer gebaut wurden, ist es hier wieder gemütlich geworden.
Das reine Wasser, das von der Veluwe herunterfließt
wird dadurch ein wenig zurückgehalten und versickert im Boden, zwischen unseren Wurzeln.
Dankenswerterweise transportieren wir jeden Tag durchschnittlich 800 Liter Wasser zu unserer Krone.

Anfang der 1980er Jahre las ein Lehrer hier ein Stück aus einer Zukunftsgeschichte vor.
Nach den großen Überschwemmungen, Stürmen und Aussterben entstand im hohen Norden ein neues Königreich.
Die Führung lag in den Händen von Frauen.
Das Klima dort war mediterran.
Und wir Bäume wurden wie Götter verehrt.

Ich lebe schon seit vielen Jahren hier. Es ist unmöglich für mich, von hier wegzugehen.
Wir Pflanzen sind sesshaft. Wir können nicht fliehen.
Deshalb sind wir anders gebaut als Sie, die Menschen.
Ein Herz an einer genau definierten Stelle, eine Lunge oder das Gehirn an genau einem Ort,
Das wäre tödlich für uns.
Wir wären nicht in der Lage, uns gegen einen Feind zu verteidigen, der uns ein Stück unseres Körpers abreißt.
Deshalb wachsen uns Tausende von Geschlechtsorganen und ebenso viele Nasen, wie es Blätter, Stämme und Wurzeln gibt.
Wir haben immer einen Plan B.
Wir sind modular.
Wir sind nicht ein Ich, sondern Tausende von Ichs, viele in einem, alle auf einmal.
Wir können mit jedem Teilchen unseres Körpers Ich sagen.

Ich lebe seit 200 Jahren hier und bin dafür sehr dankbar. Ich habe vor, noch so viele Jahre wie möglich an diesem Ort zu verbringen. Inzwischen ändern sich die Dinge. Durch meine Wurzeln hat mich die Nachricht erreicht, dass viele Eichen wegen der Versauerung des Bodens in Schwierigkeiten sind. Ihre Wurzeln sind verkürzt und sie können die notwendige Wasser- und Mineralversorgung nicht mehr besorgen. Diese Beobachtung veranlasste meine Zellen, so weit in die Zukunft zu blicken, wie wir in die Vergangenheit blicken können: genau 224 Jahre.

Sehen Sie all die Setzlinge vor mir? Sie wurden erst vor kurzem gepflanzt. Etliche Buchen mussten ihnen weichen. Im Boden sind noch Stümpfe ihrer Stämme zu finden. Buchen sind unter den Bäumen recht dominant. Sie nehmen alles Licht weg. Ihre Blätter sind so gebräunt, dass sie lange brauchen, um zu verrotten. Die Blätter der jungen Linden, Ahornbäume, Ebereschen und Kirschbäume werden schnell verdaut. Sie machen den Boden luftiger und mineralreicher. Deshalb heiße ich diese Setzlinge von ganzem Herzen willkommen.

Der Regen trägt Geschichten aus fernen Ländern herbei. Ich habe zum Beispiel gelernt, dass ihr Menschen glaubt, wir Bäume würden ewig leben. Ihr braucht euch keine Sorgen um uns zu machen. Das Gegenteil ist der Fall. Solange ihr durch eure Landwirtschaft, Abgase und Industrie weiterhin Stickstoff produziert, werden wir sterben. Eichen auf sandigen Böden leiden bereits jetzt darunter. Der Boden versauert und Mineralien werden freigesetzt, aber der Regen wäscht sie so tief weg, dass die Eichen sie nicht mehr erreichen können. Und so kommt es zu Hungersnot und Sterben.

Am liebsten würde ich nochmals 224 Jahre hier leben, also noch einmal so viele. Aber es besteht die Möglichkeit, dass Eichen wie ich aus der Landschaft verschwinden werden, ebenso wie Buchen und Fichten. Der Klimawandel und die Umweltverschmutzung spielen uns Streiche. Es braucht nur einen Borkenkäfer, und wir sind alle weg. Viele von Ihnen glauben, dass andere Bäume die Eiche zwangsläufig ersetzen werden, wie die Esskastanie.

Es besteht eine echte Chance, dass einer Ihrer Nachkommen der letzte auf der Erde sein wird, ohne dass andere Lebewesen übrigbleiben. Aber die Situation könnte sich auch umkehren. Auf Hawaii zum Beispiel hörte ich von einem Firebug mit entfernten Nachkommen, dass viele einheimische Arten ausgestorben sind. Ihr Menschen habt dort auch viele neue Arten eingeführt. Trotz der Tatsache, dass sie aus sehr unterschiedlichen Regionen stammen, scheinen sie Beziehungen zueinander aufzubauen, sodass ein neues Ökosystem entsteht. Ihr Menschen habt das zusammengesetzt, teilweise zufällig, teilweise mit Absicht, aber es könnte in gewisser Weise funktionieren.

Blumentopfmanagement, nannte ein Mann in Uniform den Trend zur Einhegung in den Niederlanden. Er lehnte sich an meinen Stamm und sprach seinen Kollegen ernsthaft an: ‘Wir müssen uns in Richtung Renaturierung bewegen. Das bedeutet, der Natur so weit wie möglich ihren Lauf zu lassen und sich auf natürliche Prozesse zu konzentrieren. Es kann nicht nur darum gehen, dieses kleine Stück Natur intensiv zu bewirtschaften und zu versuchen, es in dem Zustand zu erhalten, in dem wir Menschen es haben wollen.’ Ich schickte diesem Menschen meine wärmsten Energien.

Finken, Stare, Wiesenpieper, Blässgänse, Kiebitze, Rauchschwalben, Kormorane, Feldlerchen, Singdrosseln – sie alle ziehen im Oktober weg. Für sie existieren die Niederlande nicht mehr. Sie leben auf der ganzen Erde. Sie ziehen weit weg. Auch das könnte man vermehrt tun. Anstatt zu versuchen, Landschaften und Arten zu erhalten oder wiederherzustellen, die in den Niederlanden selten werden, während sie anderswo in Europa und der Welt spontan wieder entstehen, können Sie die Natur bei ihrer Transformation begleiten. Und Sie werden überrascht sein.
