## CO2-sensor

![](/static/img/sensors/milesight-600x.jpg)

### Herkenbare patronen

Dag & nacht
:   overdag nemen bomen koolstofdioxide (CO2) op, 's nachts stoten ze die weer uit.

Warm & droog
:   wanneer het erg warm is, sluiten de bomen de huidmondjes van de bladeren af om een teveel aan waterverdamping te vermijden. Dat betekent dat ze ook geen koolstofdioxide opnemen en er meer CO2 in de lucht blijft hangen.

### CO2 & Bomen

Bomen en planten zijn de enige levende organismen die hun eigen voedsel aanmaken.

Net zoals mensen ademen bomen door kleine mondjes aan de onderkant van het blad. Via deze huidmondjes ademt de boom CO2 in. De bladeren vangen het zonlicht op. Het water wordt vanuit de wortels naar de kruin gestuurd. Water, zonlicht en koolstofdioxide zorgen voor de fotosynthese in de bladeren. In de bladgroenkorrels om precies te zijn. Die fotosynthese produceert suikers, waarmee vezels worden aangemaakt en de boom verder kan groeien boven en onder de grond. Het afval van dit productieproces is waterstof en zuurstof. Dat laatste ademen wij weer in. 's Nachts geven de bomen de helft van de CO2 weer af.

### Effect klimaatverandering

Bomen zijn dan ook belangrijke hulpmiddelen in de strijd tegen de klimaatcrisis. Landplanten halen ongeveer 29% van al onze CO2-emissies uit de lucht, emissies die anders zouden bijdragen aan de stijging van het CO2-niveau in de atmosfeer. Vooral oude bomen nemen CO2 op, zoals de bomen die deelnemen aan dit project. Het duurt enkele tientallen jaren voor een boom voldoende CO2 kan opnemen. Het is ook bewezen dat een simpele rij van dezelfde soort bomen veel minder CO2 opnemen dan rijke, biodiverse bossen. De massale boomaanplant-ideeën van grote bedrijven moet je dan ook kritisch bekijken. Zeker omdat we overal ter wereld nog volop doorgaan met het kappen van oeroude, biodiverse bossen. Die beschermen zou een stuk efficiënter zijn.

Bomen bewaren de CO2 in hun houtcellen. Wanneer een boom gekapt wordt en verbrand, komt die CO2 weer vrij. Wanneer het hout van de boom mag doorleven in huizen, daken, meubels, blijft de CO2-opslag nog vele jaren bewaard.

### Sensortype

De CO2 wordt gemeten door de sensor die ook luchttemperatuur en luchtvochtigheid meet. Die sensor werd opgehangen in de kruin van de boom.

De CO2-sensor meet waarden in ppm (parts per million). De wereldwijde gemiddelde concentratie van CO2 in de atmosfeer is 425.22 ppm sinds mei 2024. Dit is een stijging van 50% sinds het begin van de Industriële Revolutie, tegenover 280 ppm gedurende de 10.000 jaar voorafgaand aan het midden van de 18e eeuw.
