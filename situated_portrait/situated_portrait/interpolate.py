from scipy.interpolate import CloughTocher2DInterpolator
import numpy as np
from situated_portrait.models import Tree


"""
  Interpolate located observations into a grid with given size.
"""
def interpolate_grid (located_observations, grid_size=[100,100]):
    x, y, z = zip(*[(o['longitude'], o['latitude'], o['value']) for o in located_observations])
    
    # Set up the grid for interpolation based on x and y extrimities and amount of steps
    X, Y = np.meshgrid(
        np.linspace(min(x), max(x), grid_size[0]),
        np.linspace(min(y), max(y), grid_size[1]))  # 2D grid for interpolation
    # Interpolate values. Make an interpolator and call it.
    Z = CloughTocher2DInterpolator(list(zip(x, y)), z)(X, Y)

    return (X, Y, Z)



def interpolated_grid_for_sensortype (sensortype, grid_size=[100,100], synthetic=None):
    located_observations = []

    for tree in Tree.objects.all():
        observation = tree.observations_for_sensor_type(sensortype, synthetic=synthetic).order_by('-timestamp').first()
        
        if observation:
            located_observations.append({
                'longitude': tree.longitude,
                'latitude': tree.latitude,
                'value': observation.value
            })

    # Flatten grid and replace points with isnan value for False
    x, y, z = interpolate_grid(located_observations, grid_size)
    return [ False if np.isnan(z) else (x,y,z) for (x,y,z) in zip(x.flatten(), y.flatten(), z.flatten())]