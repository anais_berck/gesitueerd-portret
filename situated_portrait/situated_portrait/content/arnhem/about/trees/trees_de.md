# Baume

<!-- An diesem Projekt sind sechs Bäume beteiligt: -->

## Einsame Eiche

![](/static/img/trees/eik1-600x.jpg)

Diese majestätische Eiche in Moerasweide ist eine der Hauptattraktionen des Sonsbeekparks. Stark, gesund und symmetrisch. Seine weit ausladenden Äste wachsen weit und breit. Man erkennt sie auch an ihrer gefurchten Rinde und den gelappten Blättern.
Die Einsame Eiche bietet uns einen Spiegel für die Selbstentfaltung in aller Freiheit. Von seinem Stamm aus erzählt dieser Baum daher Erzählungen über den Baum des Lebens und die Eiche als wichtiges Symbol in unserer Geschichte. Nachts rezitiert er Gedichte über die Liebe.

## Rosskastanie

![](/static/img/trees/european_horse_chestnut-600x.jpg)

Sie können diese Kastanie in der Nähe der Witte Villa an ihrem verwundeten Stamm erkennen. Sie hatte viele ungebetene Besucher, Pilze und Bakterien. Aber Sie können sehen, wie gesund und lebendig die Baumkrone noch ist. Und haben Sie ihre schönen weißen Blüten in diesem Frühjahr gesehen?
Die Rosskastanie lebt seit 188 Jahren in der Weißen Villa. Im Laufe der Jahre haben sich die Bewohner der Villa und die Nutzer des Parks verändert. Aus ihrem Stamm erzählt dieser Baum Bruchstücke dieser Geschichte. Nachts trägt sie Gedichte über den Tod vor.

## Waldeiche

![](/static/img/trees/european_oak-600x.jpg)

Diese Eiche auf dem Weg zum Belvédère-Turm steht ein wenig im Hintergrund, jenseits der neu gepflanzten Bäume. Sie ist 200 Jahre alt, wie all die anderen großen Eichen in diesem kleinen Wald. Verwechseln Sie Eichen nicht mit Buchen. Letztere haben eine graue, glatte Rinde. Und die Krone der Eiche hat ein viel ausgedehnteres und kunstvolleres Muster als die der Buche.
Diese Eiche ist alt und lebt sehr zurückgezogen. Zusammen mit Gleichaltrigen beobachtet er die Auswirkungen des Klimawandels und denkt darüber nach. Die Erzählungen, die er aus seinem Baumstamm erzählt, sind Erzählungen über die Zukunft. Nachts rezitiert er Gedichte über die Stille.

## Rotbuche

![](/static/img/trees/beuken-600x.jpg)

Zunächst sieht es so aus, als gäbe es nur eine einzige riesige Buche auf Schloss Zypendaal, aber es sind fünf. Ihre dünne Rinde kann die Sonne nicht vertragen. Also hüllen sie sich in einen Mantel aus Blättern. Im Raum zwischen ihren Stämmen ist es wie in einem Tempel, ruhig und kühl.
Die kürzlich veröffentlichte Recherche zu Traces of Slavery weckt in diesem Rotbuchen alte Erinnerungen. Er hat Anna van Vossenburg persönlich gekannt und teilt diese Erfahrungen aus seinem Stamm mit. Nachts trägt er Gedichte über Magie vor.

## Esskastanie

![](/static/img/trees/3watchers-600x.jpg)

Diese drei Kastanien wurden vor 375 Jahren als Tribut an die Römer gepflanzt, die die Edelkastanie in den Norden brachten. Sie stehen hier schon fast so lange wie die Eusebius-Kirche. Rembrandt hat sie besucht. Wenn Sie sie einmal kennengelernt haben, werden sie Sie gerne in Ihrer Erinnerung behalten.
Diese Esskastanie ist ein Lehrer der Zeit in all ihren Aspekten. Aus seinem Zellgedächtnis erzählt er Fragmente über Bäume in den Mythen und Traditionen der Welt. Nachts rezitiert er Gedichte über die Zeit.

## Douglasie

![](/static/img/trees/douglas_fir-600x.jpg)

Diese Tanne gehört zu einer der am häufigsten angepflanzten Baumarten in den Niederlanden. Ihr Holz wurde zum Abstützen von Tunneln in Bergwerken verwendet. Man erkennt diesen Baum an seiner korkartigen Rinde und seinen Tannenzapfen mit ihren lustigen Mäuseschwänzen an jeder Schuppe.
Aus seinem Kernholz erzählt diese Douglasie Bruchstücke von Erzählungen über die Anpflanzung und Bewirtschaftung von Wäldern, über Exoten wie ihn, die viel Profit bringen. Nachts rezitiert er Gedichte über die Natur.
