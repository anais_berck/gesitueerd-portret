# Kunstenaars en alle mensen die betrokken werden bij dit project

## Anaïs Berck
[Anais Berck](https://www.anaisberck.be/) is een pseudoniem en staat voor de samenwerking tussen mensen, algoritmes en bomen. Als collectief openen ze een ruimte waarin menselijke intelligentie een plaats krijgt in gezelschap van plantaardige intelligentie en artificiële intelligentie.

Voor dit project maken volgende mensen deel uit van de samenwerking.

### An Mertens (Brussel, BE)
An is mediakunstenaar en boomgriffier. An bedacht en coördineerde dit project, verzorgde onderzoek, literaire code en redactie van teksten en verhalen.

### Gijs de Heij (Brussel, BE) en Doriane Timmermans (Brussel, BE)
Beiden zijn lid van het experimentele ontwerperscollectief [Open Source Publishing](http://osp.kitchen/) in Brussel. Ze combineren grafisch ontwerp met programmeren.
[Gijs de Heij](https://gijsdeheij.com/) studeerde grafisch ontwerp aan ArtEZ in Arnhem. Hij programmeert al sinds zijn 11e.
Doriane Timmermans behaalde een master in de Wiskunde aan de Université Libre de Bruxelles en studeerde daarna digitale kunst aan de academie erg in Brussel. Ze doet onderzoek naar het literaire potentieel van css: [Declarations](https://declarations.style/).

## Boomverzorgers van Leefmilieu Brussel
De sensoren werden geïnstalleerd met de fijne hulp van de boswachter en boomverzorgers van Leefmilieu Brussel / Environnement Bruxelles: Willy Vandevelde, Sébastien Billon, Didier Noben en Guillaume van Cutsem.

## Wetenschappelijk advies
Tijdens verschillende gesprekken voor en tijdens het creatieproces kregen we zeer waardevol advies van boomringenonderzoekster dr. Ute Sass-Klaassen en haar collega Linar Akhmetzyanov (Hogeschool Van Hall Larenstein, Leeuwarden). Met dank!

## Kennis en ervaringen over het Zoniënwoud
Tijdens het creatieproces van dit project had An het genoegen om tijd door te brengen en te luisteren naar de kennis en ervaringen van Frederik Vaes, Departementshoofd Leefmilieu Brussel, Afdeling Bouwkundig patrimonium - Bos en Natuur

## Meer advies
We konden ook rekenen op het praktische en morele advies van Hélène van Ginderachter, ir. Erik Mertens, Judith Van Ginneken, Marleen Verschueren, Erik De Wilde. Eindredactie van de teksten werd verzorgd door Patrick Lennon.

## Financiering, productie, communicatie
Dit project kwam tot stand dankzij de financiële steun van de Vlaamse Overheid, Kunsten & Erfgoed. Dit toonmoment kreeg de logistieke steun van Leefmilieu Brussel en het team van GC Wabo in Bosvoorde.
