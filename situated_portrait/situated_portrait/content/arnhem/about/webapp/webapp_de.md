# Web app

Wenn Sie die Web-App öffnen, werden Sie mit dem Reiseführer bekannt gemacht. Er wird den Übergang zwischen Park und Umgebung und Ihrem Telefon so sanft wie möglich gestalten. Der Guide wird Sie durch die App führen. Wir hoffen, dass er genug Neugierde und Staunen wecken kann, damit Sie gerne wiederkommen.

In einem zweiten Schritt können Sie eine Geschichte, eine Visualisierung oder eine Geräuschkulisse heranzoomen. Wenn Sie mehr erfahren möchten, erhalten Sie auch einen Überblick über die Daten - in Form von einfachen Diagrammen. Sie können jederzeit von einer anderen Seite zum Leitfaden zurückkehren.

### Erzählung
Auf diesem Bildschirm können Sie Fotos von Bram Goots sehen und einen Text lesen, der aus der Sicht des Baumes erzählt wird.
Im Laufe des Tages werden Ihnen Fragmente aus den Wurzeln, dem Stamm oder der Krone des Baumes erzählt. Ein Fragment aus den Wurzeln ist eine Einladung, sich zu erden und mit dem Baum zu verbinden. Aus der Krone erzählt der Baum von seinen Beziehungen zu anderen Lebewesen wie Vögeln und Insekten. Der Stamm speichert das Gedächtnis des Baumes. Jeder Baum erzählt aus seinen Holzzellen nach seinem eigenen Thema.
Der Text variiert alle 10 Minuten.
Nachts schenken Ihnen die Bäume Gedichte!

### Abbildung
Die Abbildung zeigt eine Uhr, die 24 Stunden in der Zeit zurückgeht. Oben befindet sich der aktuelle Zeitpunkt. Gegen den Uhrzeigersinn können Sie sehen, welche Messungen die Sensoren an einem bestimmten Baum aufgezeichnet haben. Die Uhr ist wie ein Live-Porträt. Sie wird jede Stunde aktualisiert. Diese Seite ist auch mit Untertiteln versehen. Bei jeder neuen Aktualisierung erscheint ein Text, der angibt, wie viel ein bestimmter Sensortyp misst. Weitere Informationen zu den verschiedenen Messungen finden Sie unter 'Über die Sensoren'.

### Symphonie
Die Klangkünstlerin Christina Clar hat für jeden Baum und jede Art von Sensor spezifische Klänge geschaffen.

Für die Bäume wählte sie klassische Instrumente wie Harfe, Klavier, Schlagzeug, usw. Für die Sensoren schuf sie synthetische Klänge.
Jedes Mal, wenn ein Sensor bei einem bestimmten Baum aktualisiert wird, werden die verschiedenen Töne in einer Schleife abgespielt, wobei der letzte Sensor die meiste Aufmerksamkeit erhält. Die Geräusche der anderen Bäume werden je nach Standort leise im Hintergrund abgespielt.

### Daten
Für dieses Projekt haben wir eine Datenbank erstellt, in der alle Messungen der verschiedenen Sensoren gesammelt werden.
Auf diesem Bildschirm können die Daten auf einfache Weise als Diagramme angezeigt werden.

### Karte
Hier finden Sie einen Bildschirm mit einer Karte und der Route, die Sie zum nächsten Baum führt. Sie können den Baum je nach den höchsten oder niedrigsten Sensormesswerten auswählen.
