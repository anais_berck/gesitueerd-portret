## Solitary Oak

![](/static/img/trees/eik1-600x.jpg)

Hello, nice to meet you. I'm an Oak and one of the main attractions of Sonsbeekpark. I was planted in 1900. I'm still strong, healthy and balanced. My sprawling branches grow far and wide. You can also recognize me by my grooved bark and lobed leaves.
I offer you, humans, a mirror for self-development in all freedom. From my trunk, therefore, I share story fragments about the Tree of Life and the Oak as an important symbol in our history. At night, I recite poems about Love.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
