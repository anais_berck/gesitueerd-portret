# Sensoren

Als kunstenaars willen we bezoekers graag in verbinding brengen met bomen.

Hiervoor maken we oneigenlijk gebruik van sensoren die ontwikkeld zijn om bomen en planten 'optimaal' te doen groeien. Voor dit project is het niet interessant en ook quasi onmogelijk om uitspraken te doen over het welzijn van de bomen. Bomen zijn complexe wezens. Metingen en het vastleggen van ‘standaarden’ zijn afhankelijk van bodemtype, locatie, boomsoort en leeftijd van de boom.

Daarom maken wij de sensoren in het moment zelf zichtbaar.
Zo zie je dat de bomen leven.

<!-- Sensoren die worden gebruikt in dit project: -->

- [Bodemvochtigheid](soil_moisture/)
- [Bodemtemperatuur](soil_temperature/)
- [Beweging stam](mouvement_trunk/)
- [Luchtvochtigheid](air_humidity/)
- [Luchttemperatuur](air_temperature/)
- [CO2](co2/)
