import json

treenames = [
    'douglas_fir',
    'european_horse_chestnut',
    'european_oak',
    'red_beech',
    'solitary_oak',
    'sweet_chestnut'
]


parts = [
    'canopy',
    'roots',
    'trunk'
]

languages = [
    'nl',
    'en'
]

introductions = {
  'en': {
    'sunrise': 'It is $, let me share a poem about sunrise with you.',
    'day': {
      'canopy': 'It is $, let me share a story related to my canopy.',
      'trunk': 'It is $, let me share a story I keep in my trunk.',
      'roots': 'It is $, let me share a story related to my roots.'
    },
    'sunset': 'It is $, let me recite a poem about sunsets.',
    'night': {
      'solitary_oak': 'It is $, let me recite a poem about love.',
      'european_horse_chestnut': 'It is $, let me recite a poem about death.',
      'european_oak': 'It is $, let me recite a poem about silence.',
      'red_beech': 'It is $, let me recite a poem about magic.',
      'sweet_chestnut': 'It is $, let me recite a poem about time.', 
      'douglas_fir': 'It is $, let me recite a poem about nature.',
    }
  },
  'nl': {
    'sunrise': 'Het is $, ik deel graag een gedicht over zonsopgang met je.',
    'day': {
      'canopy': 'Het is $, ik deel graag een verhaal vanuit mijn kruin.',
      'trunk': 'Het is $, ik deel graag een verhaal dat ik bewaar in mijn kernhout.',
      'roots': 'Het is $, ik deel graag een verhaal vanuit mijn wortels.'
    },
    'sunset': 'Het is $, ik deel graag een gedicht over zonsondergang met je.',
    'night': {
      'solitary_oak': 'Het is $, ik deel graag een gedicht over liefde.',
      'european_horse_chestnut': 'Het is $, ik deel graag een gedicht over dood.',
      'european_oak': 'Het is $, ik deel graag een gedicht over stilte.',
      'red_beech': 'Het is $, ik deel graag een gedicht over magie.',
      'sweet_chestnut': 'Het is $, ik deel graag een gedicht over tijd.', 
      'douglas_fir': 'Het is $, ik deel graag een gedicht over natuur.'
    }
  }
}

trees = { t: {
    l: {
        'sunrise': { 'introduction': '', 'stories': [] },
        'day': {
            'canopy':  { 'introduction': '', 'stories': [] },
            'trunk':  { 'introduction': '', 'stories': [] },
            'roots':  { 'introduction': '', 'stories': [] }
        },
        'sunset':  { 'introduction': '', 'stories': [] },
        'night':  { 'introduction': '', 'stories': [] }
    } for l in languages
} for t in treenames }


def parse_story_file (path):
    stories = []
    with open(path, 'r') as h:
        buff = []

        for line in h:
            if line.strip() == '':
                stories.append(buff)
                buff = []
            else:
                buff.append(line.strip('\n'))

    return stories

def make_stories_json(basepath_input, basepath_output, basepath_public):
    for part_of_day in ['sunrise', 'day', 'sunset', 'night']:
        for l in languages:
            if part_of_day == 'sunset' or part_of_day == 'sunrise':
                path = f'{basepath_input}/{part_of_day}/{part_of_day}_{l}.txt'
                stories = parse_story_file(path)
                for tree in trees:
                    trees[tree][l][part_of_day]['introduction'] = introductions[l][part_of_day]
                    trees[tree][l][part_of_day]['stories'] = stories
            
            if part_of_day == 'night':
                for tree in trees:
                    path = f'{basepath_input}/{part_of_day}/{tree}_{l}.txt'
                    trees[tree][l][part_of_day]['introduction'] = introductions[l][part_of_day][tree]
                    trees[tree][l][part_of_day]['stories'] = parse_story_file(path)

            if part_of_day == 'day':
                  for part_of_tree in parts:
                      if part_of_tree == 'roots':
                          path = f'{basepath_input}/{part_of_day}/{part_of_tree}_{l}.txt'
                          stories = parse_story_file(path)
                          
                          for tree in trees:
                            trees[tree][l][part_of_day][part_of_tree]['introduction'] = introductions[l][part_of_day][part_of_tree]
                            trees[tree][l][part_of_day][part_of_tree]['stories'] = parse_story_file(path)
                      else:            
                          for tree in trees:
                            path = f'{basepath_input}/{part_of_day}/{part_of_tree}_{tree}_{l}.txt'
                            stories = parse_story_file(path)
                            trees[tree][l][part_of_day][part_of_tree]['introduction'] = introductions[l][part_of_day][part_of_tree]
                            trees[tree][l][part_of_day][part_of_tree]['stories'] = parse_story_file(path)

                          
    generated = {}

    for tree, stories in trees.items():
        json.dump(stories, open( f'{basepath_output}/{tree}.json', 'w'), ensure_ascii=False)
        generated[tree] =  f'{basepath_public}/{tree}.json'

    return generated

if __name__ == '__main__':
    make_stories_json()