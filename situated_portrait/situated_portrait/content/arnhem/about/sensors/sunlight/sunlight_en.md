## Sunlight sensor

![](/static/img/sensors/solar_radiation.jpg)

### Recognizable pattern

Day & night
:   the Earth revolves around the Sun in about 365 days. Meanwhile, the Earth also rotates on its own axis, in 24 hours or one day.

In the Netherlands, it is light much longer in summer than in winter. This is because the Earth is slightly tilted in relation to the Sun. In summer, the Sun is perpendicular and therefore closest to the northern hemisphere.

As a result, there is more Sun in this hemisphere in summer and the days are therefore longer. In winter, the exact opposite happens.

### Sunlight & Trees

Trees literally grow towards the light. Their leaves capture as much sunlight as possible to produce sugars through photosynthesis. If you think about it, the Sun, water and CO2 form the basis of our life, food, materials and medicines. Every plant grows thanks to the Sun and photosynthesis. Humans and animals feed on plants.

### Sensor type

The sunshine sensor is a small sphere, the size of an eye, mounted horizontally in an open space. It measures the irradiance of the Sun. The unit of irradiance is joules per second per square metre (W/m-²). The unit that is often used in the solar industry is kilowatt hours per square metre (kWh/m²). Joules express the amount of energy, while watts indicate power. If you apply power for a certain time (e.g. per hour), then you have consumed or generated a certain amount of joules.

The sensor uses joules per second per square metre (W/m-2) as the unit. Divided by 1000, this gives kWh/m².

Sunshine duration is the period when direct solar radiation exceeds the limit of 120 W/m². The radiation intensity varies greatly, from about 50 W/m² on a heavily cloudy winter day to more than 1000 W/m² with a clear sky in summer.
