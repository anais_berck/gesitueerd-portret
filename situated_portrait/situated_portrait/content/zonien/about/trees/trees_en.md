# Tree


## Remarkable Beech

![](/static/img/trees/beuk.jpg)

77% of the Sonian forest consists of beeches. They are heirs to the beeches planted by Austrian landscape architect Jacques Zinner in the 18th century. They give the typical appearance of the beech cathedral. They grow as straight as possible and form broad columns. Their wafer-thin bark does not tolerate sunlight. Therefore, they zip themselves shut with a coat of leaves. Between them, few other plants get a chance to grow.

This beech is a remarkable beech because it belongs to the category of 'columnar beech with a trunk circumference trees of 291 centimetres'. The inventory of remarkable trees in the Sonian Forest was compiled in 2015 by Bruxelles Environnement in collaboration with the `Association Protectrice des Arbres en Forêt de Soignes' (Association for the Protection of Trees in the Sonian Forest).

"Remarkable trees are trees with a strong personality. [...] They attract attention and put their peers who make up the forest stock in the shade." In the Sonian Forest, an "emotional approach" was taken to the selection of remarkable trees. A remarkable tree is distinguished from other trees by "its morphology: curious slenderness, impressive trunk circumference, huge branches [...] or by its deformity: growth, knot, scrub, blunt tip." This approach to remarkable trees is based on ‘the emotional impact’ or ‘love at first sight’ of the person compiling the inventory. G. Feterman, chairman of the A.R.B.R.E.S. association, has described this process very well: “A remarkable tree is first and foremost an emotion, love at first sight, that catches your breath when you see an incredible witness to the past that suddenly appears in front of you on a bend in the road”.

In the field of forest management, remarkable and special trees enjoy a special status. These trees are preserved until they physically decay. In this way, they are included in the quota for the preservation of the old reserves and contribute to the nature conservation objectives.

Source: [https://erfgoed.brussels/links/digitale-publicaties/pdf-versies/artikels-van-het-tijdschrift-erfgoed-brussel/nummer-14/artikel-14-6](https://erfgoed.brussels/links/digitale-publicaties/pdf-versies/artikels-van-het-tijdschrift-erfgoed-brussel/nummer-14/artikel-14-6)
