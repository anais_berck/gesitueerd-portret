# Web app

Als je de webapp opent, maak je kennis met de gids. Die maakt de overgang tussen het bos en je telefoon zo zacht mogelijk. De gids leidt je door de app. We hopen dat we voldoende nieuwsgierigheid en verwondering kan wekken, zodat je zin krijgt om later nog eens terug te komen.

In een tweede instantie kan je inzoomen op een verhaal en een visualisatie. Als je meer wil ontdekken, krijg je ook een overzicht van de data – in de vorm van een eenvoudige grafiek.

Je kan vanuit elke andere pagina altijd terugkeren naar de gids.

### Verhaal
Dit is een scherm waarop je een tekst kan lezen, verteld vanuit het standpunt van de boom.
Overdag krijg je fragmenten verteld vanuit de wortels, de stam of de kruin van de boom. Een fragment vanuit de wortels is een uitnodiging tot aarding en verbinding met de boom. Vanuit de kruin vertelt de boom over relaties met andere levende wezens zoals vogels en insecten. In de stam ligt het geheugen van de boom opgeslagen.
De tekst verandert elke tien minuten.
's Nachts brengt de boom je gedichten!

### Verbeeld
Verbeeld toont een klok die 24u teruggaat in de tijd. Bovenaan is het nu-moment. In tegenwijzerzin zie je welke metingen de sensoren bij de boom registreerden. De klok is als een live-portret. Elk uur wordt ze ge-updated. Deze pagina is ook voorzien van ondertitels. Bij elke nieuwe update verschijnt er een zinnetje dat aangeeft hoeveel een type sensor meet. Je vindt meer informatie over de verschillende metingen bij 'Over de sensoren'.

### Data
Voor dit project creëerden we een databank, waarin alle metingen van alle verschillende sensoren verzameld worden.
Dit scherm laat toe om de data op een eenvoudige manier als een grafiek te bekijken.

### Kaart
Hier vind je een scherm met een kaart die de locatie van de boom aangeeft.
