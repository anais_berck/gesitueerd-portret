# Benches and stand

Floris van Hintum created the benches in the park and the stand at Molenplaats Sonsbeek using local wood. So these objects also have their own tree story.

## Benches: Elm & Robinia

![](/static/img/foto_bankje.jpg)

The trunks on which the benches rest come from the Robinias that were felled on Parkstraat in the Spijkerkwartier. Most of the trees were ill. The sewer is being replaced and the street redesigned.

The boards came from Elms affected by Dutch Elm disease. The result is the wilting of branches and the death of the tree. The disease is caused by fungi that grow in wood vessels of the tree. As the fungus moves from one vessel to another, the vessels clog up and the tree dies. To prevent the spread of Dutch Elm disease, these trees were 'peeled'.

When this project ends on 15 September 2024, the benches will be reused in Arnhem's public parks. If you are interested in placing a bench in your neighbourhood, please contact anais[att]anaisberck[dot]be.

## Stand: Linden

The trunk of the furniture in Molenplaats Sonsbeek, on which a touchscreen is mounted, comes from a Lime tree that lived on Onderlangs, opposite the Artez art academy. How nice that a large piece of the tree is getting an artistic repurposing.
