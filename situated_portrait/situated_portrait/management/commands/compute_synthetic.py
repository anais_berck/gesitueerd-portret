from django.core.management.base import BaseCommand
from situated_portrait.models import Device, Sensor, Observation
from situated_portrait.decentlab.retrieve_data import retrieve_data as decentlab_retrieve_data
from django.conf import settings
import numpy
from django.db.utils import IntegrityError
from situated_portrait.emsbrno import synthetic_growth_sensor

# Adding one synthetic sensor per synthetic type
# ====================================================================

synthetic_types = ['moving_average']

def add_synthetic_sensors(sensor):

    for s_type in synthetic_types:
    
        # there is not a synthetic sensors  with that type
        if not Sensor.objects.filter(device = sensor.device, type = sensor.type, synthetic_type = s_type).exists():
            
            synthetic_sensor = Sensor(
                device = sensor.device,
                type = sensor.type,
                unit = sensor.unit,
                synthetic = True,
                synthetic_type = s_type, 
            )
            try:
                synthetic_sensor.save()
            except IntegrityError:
                print('Tried to store double sensor (synthetic)')

            print(f'  new synthetic sensor added {synthetic_sensor}')

# Computing the new values for every sensors
# ====================================================================

# n = 96 ---> 2 days
def moving_average(a, n = 96):
    ret = numpy.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def compute_synthetic(synthetic_sensor, sensor):

    # get original observations
    observations = Observation.objects.filter(sensor = sensor).order_by('-timestamp')
    
    values = [observation.value for observation in observations]
    s_values = moving_average(values)

    s_observations = []
    for i in range(len(s_values)):
        s_observation = Observation(
            sensor = synthetic_sensor,
            timestamp = observations[i].timestamp,
            value = s_values[i]
        )
        s_observations.append(s_observation)

    Observation.objects.bulk_create(s_observations, ignore_conflicts=True)
    print(f'computed {synthetic_sensor}')


# GLOBAL COMMAND WITH SWITCH CASES
# ====================================================================

class Command(BaseCommand):
    args = ''
    help = 'Compute synthetic sensors based on the observations available'

    # we loop on the devices, not on the sensor
    # the reason is that for some devices, one request is enough to retrieve all its sensors
    def handle(self, *args, **options):

        # iterating on physical and adding new synthetic when needed
        for sensor in Sensor.objects.filter(synthetic = False):
            if sensor.device.deviceType.brand == 'EMS Brno':
                add_synthetic_sensors(sensor)
            if synthetic_growth_sensor.applicable(sensor):
                synthetic_growth_sensor.compute(sensor)

            

        # iterating on synthetic and computing new value
        for synthetic_sensor in Sensor.objects.filter(synthetic = True):
            if synthetic_sensor.synthetic_type in synthetic_types:
                sensor = Sensor.objects.filter(
                    type = synthetic_sensor.type, 
                    device = synthetic_sensor.device,
                    synthetic = False)[0]
                compute_synthetic(synthetic_sensor, sensor)