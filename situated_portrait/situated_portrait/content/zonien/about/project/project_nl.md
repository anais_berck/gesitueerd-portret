# Dit project

### Bewegende bomen
Bomen, we lopen er doorgaans gewoon voorbij. We vinden ze normaal en als we ze al opmerken, zien ze er hetzelfde uit als gisteren, als morgen en overmorgen. Onze huizen zijn gemaakt met hout. Bomen geven ons ook medicijnen, eten, warmte, zuurstof en we zoeken hun schaduw op tijdens hete zomers.

Tijdens deze installatie sta je stil bij een boom, zoals bij een kunstwerk. In het Zoniënwoud leven bijzondere en oude bomen. Aan de hand van sensoren die in deze beuk zijn geïnstalleerd, maakt Gesitueerd Portret zichtbaar wat anders verborgen blijft voor het menselijke oog: het bewegen van de stam, de CO2- en wateraanvoer, de temperatuur in de lucht en bij de wortels.

### Altijd anders
Van uur tot uur zie je veranderingen in de stam van de boom, waardoor die boom gaat leven. De installatie nodigt je dan ook uit om deze bijzondere boom te waarderen als een actief, groeiend, veranderend levend wezen, waarmee je ook een relatie kan aangaan.

### QR-code
Bij de picnicbank is een houten bankje geplaatst met daarop een QR-code. Als je die scant, word je naar een webpagina geleid, waar verhaalfragmenten en artistieke digitale beelden je inzicht geven in het leven en de geschiedenis van de boom. Wat je te zien krijgt, hangt af van de data die de sensoren verzenden. Laat je verrassen, elk uur, elke dag.

### Praktisch
Je vndt de Beuk in het Zoniënwoud tegenover de picnic-bank aan de Graafdreef, net voorbij het kruispunt van de Tumuliweg, de Sint-Hubertusdreef en de Groene Spechtweg: [locatie](https://www.openstreetmap.org/#map=18/50.77950/4.41103)

## Credits

<small markdown>

Dit project wordt gerealiseerd door [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes:

- kunstenaars die meewerken aan dit project: An Mertens (concept, coördinatie, boomgriffier), Gijs de Heij & Doriane Timmermans (code, ontwerp & visualisaties - [Open Source Publishing](https://osp.kitchen/))
Meer over de kunstenaars en de mensen die betrokken zijn bij dit project (link artists page)
- boom die meewerkt aan dit project: beuk (Fagus sylvatica)
- algoritmes die meewerken aan dit project: EMS Brno banddendrometer, Sensoterra bodemvochtigheids- en temperatuursensor, Milesight CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programmeertaal, html, css.

Gesitueerd Portret is mogelijk dankzij de financiële steun van de Vlaamse Overheid, Kunsten & Erfgoed. Dit toonmoment kreeg de logistieke steun van Leefmilieu Brussel en het team van GC Wabo in Bosvoorde.



</small>
