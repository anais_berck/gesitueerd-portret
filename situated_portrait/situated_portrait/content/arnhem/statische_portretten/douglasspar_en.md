## Douglas Fir

![](/static/img/trees/douglas_fir-600x.jpg)

Hello, nice to meet you! I belong to one of the most planted tree species in the Netherlands. My wood was used to shore up tunnels in mines. You can recognize me by my cork-like bark and my pine cones with their funny mousetails at each scale.
From my heartwood, I share story fragments about planting and managing forests, about exotics like me that bring much profit. At night, I recite poems about Nature.

[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
