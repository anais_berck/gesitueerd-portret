# Sensoren

Als Künstler wollen wir die Parkbesucher mit verschiedenen Bäumen in Verbindung bringen. Wir schaffen eine einzigartige und ästhetische Erfahrung für Menschen mit unterschiedlichem Hintergrund, Alter und Kultur.

Dazu verwenden wir die Sensoren zweckwidrig, die eigentlich entwickelt wurden, um Bäume und Pflanzen ‘optimal’ wachsen zu lassen. Für dieses Projekt jedoch ist es weder von Interesse noch ist es möglich, Aussagen über das Wohlbefinden von Bäumen zu machen. Bäume sind komplexe Lebewesen. Messungen und die Festlegung von ‘Standards’ hängen von der Bodenart, dem Standort, der Baumart und dem Alter des Baumes ab.

Deshalb machen wir die Messungen der Sensoren zu jedem Zeitpunkt sichtbar, hörbar und spürbar. So sehen Sie, dass Bäume lebendig sind.

<!-- In diesem Projekt verwendete Sensoren: -->

- [Feuchtigkeit des Bodens](soil_moisture/)
- [Bodentemperatuur](soil_temperature/)
- [Bewegung Baumstamm](mouvement_trunk/)
- [Saftstrom](sapflow/)
- [Luftfeuchtigkeit](air_humidity/)
- [Lufttemperatur](air_temperature/)
- [CO2](co2/)
- [Sonnenlicht](sunlight/)
