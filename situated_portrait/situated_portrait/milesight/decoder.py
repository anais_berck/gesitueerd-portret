def decode_message (bytes):
  i = 0

  decoded = {}

  while i < len(bytes):
    channel_id = bytes[i]
    channel_type = bytes[i+1]

    i += 2  
    
    # Battery
    if channel_id == 0x01 and channel_type == 0x75:
      # decoded.battery = bytes[i]
      i += 1
    # TEMPERATURE
    elif channel_id == 0x03 and channel_type == 0x67:
      # ℃
      # # decoded.temperature = readInt16LE(bytes.slice(i, i + 2)) / 10;
      decoded['air temperature'] = float(int.from_bytes(bytes[i:i+2], byteorder='little')) / 10.0
      i += 2
    # HUMIDITY
    elif channel_id == 0x04 and channel_type == 0x68:
      # # decoded.humidity = bytes[i] / 2;
      decoded['air humidity'] = float(int.from_bytes(bytes[i:i+1], byteorder='little')) / 2.0
      i += 1
    # CO2
    elif channel_id == 0x05 and channel_type == 0x7d:
      # decoded.co2 = readUInt16LE(bytes.slice(i, i + 2));
      decoded['co2'] = int.from_bytes(bytes[i:i+2], byteorder='little')
      i += 2
    # PRESSURE
    elif channel_id == 0x06 and channel_type == 0x73:
      # decoded.pressure = readUInt16LE(bytes.slice(i, i + 2)) / 10;
      decoded['air pressure'] = float(int.from_bytes(bytes[i:i+2], byteorder='little')) / 10.0
      i += 2
    # TEMPERATURE CHANGE ALARM
    elif channel_id == 0x83 and channel_type == 0xd7:
      # decoded.temperature = readInt16LE(bytes.slice(i, i + 2)) / 10;
      # decoded.temperature_change = readInt16LE(bytes.slice(i + 2, i + 4)) / 10;
      # decoded.temperature_alarm = readTempatureAlarm(bytes[i + 4]);
      i += 5
    # HISTORY
    elif channel_id == 0x20 and channel_type == 0xce:
      # var data = {};
      # data.timestamp = readUInt32LE(bytes.slice(i, i + 4));
      # data.co2 = readUInt16LE(bytes.slice(i + 4, i + 6));
      # data.pressure = readUInt16LE(bytes.slice(i + 6, i + 8)) / 10;
      # data.temperature = readInt16LE(bytes.slice(i + 8, i + 10)) / 10;
      # data.humidity = bytes[i + 10] / 2;
      i += 11

      # decoded.history = # decoded.history || [];
      # decoded.history.push(data);
    else:
      break

  return decoded

def hexstring_to_bytes (hex):
  return bytes.fromhex(hex)

if __name__ == '__main__':
  print(decode_message(hexstring_to_bytes("057de9010673e2260367c300046878")))
