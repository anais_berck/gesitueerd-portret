## Luftfeuchtigkeit

![](/static/img/sensors/milesight-600x.jpg)

### Erkennbare Muster

Regen
:   Als Menschen sind wir mit diesem Sensor sehr vertraut. Apps wie ‘Rainviewer’ geben uns Orientierung. Regen bestimmt unsere Aktivitäten, unseren Kleidungsstil und oft auch unsere Stimmung. Regen ist Teil des Wasserkreislaufes, in dem auch Bäume eine wichtige Rolle spielen.

Tag und Nacht
:   Die Luftfeuchtigkeit schwankt im Rhythmus von Tag und Nacht. In der Nacht kühlt es ab. Die warme Luft vom Tag kondensiert. Diese Feuchtigkeit setzt sich auf stofflich festen Elementen wie Gras oder Bänken im Park ab.

### Luftfeuchtigkeit & Baume

Wasser ist das Lebenselixier unseres Planeten. Vom Regenwald bis zur Wüste, von der Prärie bis zur Tundra - überall bestimmt die Menge des verfügbaren Wassers, was wächst. Kein lebender Organismus kann ohne Wasser als Grundlage für alle Lebensprozesse auskommen.

Sobald der Regen auf den Boden fällt, nehmen die Pflanzen das Wasser auf. Bäume wie Buchen haben braun gefärbte Blätter, die das Wasser zum Stamm leiten, von wo aus es zu den Wurzeln fliest. Auf diese Weise nutzen sie den Regen optimal. Die Pflanzen müssen jedoch viel mehr Wasser aufnehmen, als sie für ihren Stoffwechsel benötigen, denn sie verlieren viel Wasser durch Verdunstung und Transpiration. Wasser ist ihr internes Transportmittel.

Die Bedeutung von Wasser für Bäume ist eine zyklische Geschichte. Bäume sind für den Wasserkreislauf unverzichtbar. Die Transpiration (Ausdunstung durch ihre Blätter) ist der Hauptmechanismus, mit dem die Luft feucht gehalten wird. Diese Luft strömt von den Meeresküsten ins Landesinnere. Würden die Pflanzen nicht so viel Wasser verdunsten, wäre das Innere der Kontinente größtenteils Wüstenland. Es besteht ein direkter Zusammenhang zwischen der Abholzung der Wälder und der Wüstenbildung.

Unter den Pflanzen sind die Bäume die effektivsten Verdunster. Neben den Ozeanen bilden die Bäume die andere Hälfte des globalen Systems, das als Wasser- oder Regenkreislauf bekannt ist. Ein durchschnittlicher Baum kann durch die riesige Oberfläche seiner Blätter 100 Liter Wasser pro Tag verdunsten.

Selbst in Meeresnähe können Bäume entscheidend für Feuchtigkeit und Regen sein. Als europäische Siedler die Wälder voller hoher Bäume auf der Maui Hawaii-Insel abholzten, wurde die benachbarte, einst vollständig bewaldete Insel Kahoolawe schnell zu einer Wüste, da dort der Regen von den Bäumen auf Maui kam - und nicht vom Ozean, der beide Inseln umgibt.

### Sensortype

Die Luftfeuchtigkeit wird von dem gleichen Sensor gemessen, der auch CO2 und die Lufttemperatur misst. Dieser Sensor ist in der Baumkrone befestigt.
