## Lufttemperatur

![](/static/img/sensors/milesight-600x.jpg)

### Erkennbare Muster

Sonnenlicht
:   Als Menschen sind wir mit diesem Sensor sehr vertraut; wir leben mit dem Wetter. Die Lufttemperatur bestimmt unsere Aktivitäten, unseren Kleidungsstil und oft auch unsere Stimmung. Die Temperatur steigt, wenn es mehr Sonnenlicht gibt.

Tag und Nacht
:   Die Temperatur schwankt im Rhythmus von Tag und Nacht. Da es nachts kein Sonnenlicht gibt, kühlt sich die Luft ab und die Lufttemperatur sinkt.

### Lufttemperatur & Baume

Indem sie mit ihren Blättern die Sonnenstrahlung absorbieren, Wasser verdunsten und Schatten spenden, senken Bäume die Temperatur in ihrer unmittelbaren Umgebung. Im Durchschnitt ist es in einem Wald 4°C kühler, bei einer Hitzewelle sogar bis zu 10°C. Untersuchungen der Universität Wageningen und der TU-Delft haben gezeigt, dass ein großer Baum so viel Kühlung bietet wie zehn Klimaanlagen.

Man kann diesen Effekt damit vergleichen, aus dem Meer zu steigen. Durch die Verdunstung des Meerwassers wird unserer Haut Energie entzogen, und das führt dazu, dass uns kalt wird. Das Gleiche passiert mit Bäumen. Die Verdunstung von Wasser kostet den Baum Energie. Eine Eiche oder eine Buche hat dichteres Laub als eine Kiefer. Je mehr Blätter, desto mehr Schatten, und desto mehr Wasser verdunstet dabei. Daher finden Sie unter einer Eiche mehr Erfrischung als unter einem Nadelbaum. Junge Bäume bieten weniger Erfrischung als alte Bäume.

Bäume können auch die Luftströmung beeinflussen, was dazu beitragen kann, die Wärme zu verteilen und die Temperatur zu senken. Wenn sich die Luft über die Blätter hinwegbewegt, kann sie kühler werden. Vor allem Bäume mit gefiederten Blättern (akazienartige Bäume) haben relativ große Blattränder und sind daher hitzetolerant.


### Auswirkungen des Klimawandels

Wissenschaftler sagen voraus, dass die Lufttemperatur in den nächsten zwanzig Jahren um 0,2°C steigen wird. Nach den bestehenden Klimamodellen könnte die Lufttemperatur bis zum Ende dieses Jahrhunderts um 1,5 bis 3,5°C steigen. Daher werden die Auswirkungen der erhöhten Lufttemperatur auf das Wachstum von Bäumen erforscht.

Im Allgemeinen scheint es, dass Bäume bei höheren Temperaturen schneller wachsen - mit Ausnahme der tropischen Bäume. Der Grund dafür ist, dass tropische Bäume bereits bei der maximalen Temperatur leben, die sie ertragen können, während es bei Bäumen in gemäßigten Klimazonen noch einen Spielraum gibt. Bei höheren Temperaturen werden die Bäume höher und schmaler, während sie mehr Blätter und weniger Wurzeln bilden. Dies gilt vor allem für Laubbäume, weniger für Nadelbäume. Der Baum verdunstet bei hohen Temperaturen weniger Wasser, während die Photosynthese gleichbleibt.

Man könnte also sagen, dass hohe Temperaturen besser für unsere Bäume sind, aber leider kommt der Temperaturanstieg im Zuge des Klimawandels nicht allein. Störungen wie Waldbrände, Insektenbefall und Stürme nehmen in einer wärmeren Welt zu, wie die letzten Jahre gezeigt haben. Ein schnelleres Baumwachstum kann also durch vermehrtes Baumsterben zunichte gemacht werden.


### Type Sensor

Die Lufttemperatur wird von dem Sensor gemessen, der auch den CO2-Gehalt und die Luftfeuchtigkeit berechnet. Dieser Sensor ist in der Baumkrone aufgehängt.
