# Sensoren

Als kunstenaars willen we parkbezoekers graag in verbinding brengen met de verschillende bomen. We creëren een unieke en esthetische ervaring voor mensen van uiteenlopende achtergronden, leeftijden en culturen.

Hiervoor maken we oneigenlijk gebruik van sensoren die ontwikkeld zijn om bomen en planten 'optimaal' te doen groeien. Voor dit project is het niet interessant en ook quasi onmogelijk om uitspraken te doen over het welzijn van de bomen. Bomen zijn complexe wezens. Metingen en het vastleggen van ‘standaarden’ zijn afhankelijk van bodemtype, locatie, boomsoort en leeftijd van de boom.

Daarom maken wij de sensoren in het moment zelf zichtbaar, hoorbaar en voelbaar.
Zo zie je dat de bomen leven.

<!-- Sensoren die worden gebruikt in dit project: -->

- [Bodemvochtigheid](soil_moisture/)
- [Bodemtemperatuur](soil_temperature/)
- [Beweging stam](mouvement_trunk/)
- [Sapstroom](sapflow/)
- [Luchtvochtigheid](air_humidity/)
- [Luchttemperatuur](air_temperature/)
- [CO2](co2/)
- [Zonlicht](sunlight/)
