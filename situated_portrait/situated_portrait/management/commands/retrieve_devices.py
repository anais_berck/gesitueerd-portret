from django.core.management.base import BaseCommand
from situated_portrait.models import Device, Sensor, Observation
from situated_portrait.decentlab.retrieve_data import retrieve_data as decentlab_retrieve_data
from django.conf import settings
import requests
import datetime
import time
from django.db.utils import IntegrityError
import json
import os

# SENSOTERRA
# ====================================================================

def retrieve_sensoterra(device, datetime_from):

  device_type = device.deviceType

  # --- SENSOTERRA AUTHENTIFICATION
  headers = {
    'accept': 'application/json',
    'language': 'en'
  }
  auth = requests.post(device_type.url + 'customer/auth', headers=headers, json=device_type.authentication)
  authdata = auth.json()
  api_key = authdata['api_key']
  headers['api_key'] = api_key

  # --- datetime range 2 ISO format
  # Database date already has a timezone attached
  timestamp_from = datetime_from.isoformat()
  timestamp_to = datetime.datetime.now().isoformat() + '+01:00'

  # --- RETRIEVE
  probe_response = requests.get(device_type.url + 'probe/{}/{}/{}'.format(device.remote_identifier, timestamp_from, timestamp_to), headers=headers, params={
    'aggregate_by': 'none',
    'limit': 1000,
    'skip': 0,
    # we want an additional data not there by default: the sensoterra index (SI)
    'unit': '%,SI'
  })
  
  if probe_response.ok:
    probe_data = probe_response.json()
    for api_observation in probe_data:

      # each observations looks like this:
      # {'timestamp': '2024-02-05T09:36:48+01:00', 'height': -30, 'unit': 'SI', 'value': 0}
      
      # --- get the associated sensor
      if Sensor.objects.filter(device = device, unit = api_observation['unit']).exists():

        sensor = Sensor.objects.filter(device = device, unit = api_observation['unit'])[0]

        # --- create Observation
        observation = Observation(
          sensor = sensor,
          timestamp = datetime.datetime.fromisoformat(api_observation['timestamp']),
          value = api_observation['value'],
        )
        try:
          observation.save()
          print(f'Added: {observation}')
        except IntegrityError as e:
          print(e)

      else:
        print('No sensor associated with this device and observation unit:', device, api_observation['unit'])

  else:
    print(probe_response.request.url)
    print(probe_response.status_code, probe_response.reason)


# SENSECAP
# ====================================================================

def translate_sensecap_unit (measurement_id):
  lookup = {
    '4113': 'mm/hour',
    '4112': '°C',
    '4111': 'ds/m²',
    '4110': '%',
    '4109': 'mg/L',
    '4108': 'dS/m²',
    '4107': 'umol/㎡s',
    '4106': 'PH',
    '4105': 'm/s',
    '4104': '°',
    '4103': '%RH',
    '4102': '°C',
    '4101': 'Pa',
    '4100': 'ppm',
    '4099': 'Lux',
    '4098': '%RH',
    '4097': '°C',
  }
  try:
    return lookup[measurement_id]
  except KeyError:
    return None

def retrieve_sensecap(device, datetime_from):
  # https://sensecap-docs.seeed.cc/device_data.html

  headers = {
    'accept': 'application/json',
    'language': 'en'
  }

  if not datetime_from:
    # Request does not work
    # # Initial retrieval. Use activation date as the datetime_from
    # device_response =  requests.post(device.deviceType.url + 'view_devices',
    #                              headers=headers,
    #                              data={ 'device_euis': [ device.remote_identifier ] },
    #                              auth=( device.deviceType.authentication['id'], device.deviceType.authentication['key']))
    # if device_response.ok:
    #   device_data = device_response.json()
    #   timestamp_from = device_data[0]['sim']['activateTime']
    #   datetime_from = datetime.datetime.fromisoformat(timestamp_from.replace('Z', '+00:00'))
    datetime_from = datetime.datetime(2024, 11, 10)

  # time in unixtimestamp
  time_start = int(datetime_from.timestamp() * 1000)
  time_end = int(time.time() * 1000)

  device_type = device.deviceType
  
  
  probe_response =  requests.get(device_type.url + 'list_telemetry_data',
                                 headers=headers,
                                 params = { 'device_eui': device.remote_identifier,
                                            'time_start': time_start,
                                            'time_end': time_end },
                                 auth=( device_type.authentication['id'], device_type.authentication['key']))

  if probe_response.ok:
    probe_data = probe_response.json()
    
    if 'data' in probe_data:
      # --- list[0] are the measurment_id in order
      for i in range(len(probe_data['data']['list'][0])):
        measurement_id = probe_data['data']['list'][0][i][1]
        unit = translate_sensecap_unit(measurement_id)

        # --- get the associated sensor
        try:
          sensor = Sensor.objects.get(device=device, unit=unit)

          # --- list[1] are the data for each measurment_id in the same order
          for api_observation in probe_data['data']['list'][1][i]:
            # print(api_observation, unit)

            iso_time = api_observation[1].replace('Z', '+00:00')

            # --- create Observation
            observation = Observation(
              sensor = sensor,
              value = api_observation[0],
              timestamp = datetime.datetime.fromisoformat(iso_time)
            )
            try:
              observation.save()
              print(f'Added: {observation}')
            except IntegrityError as e:
              print(e)

        except Sensor.DoesNotExist:
          print('No sensor associated with this device and observation unit:', device, unit)
    else:
      print(probe_data)

  else:
    print(probe_response.status_code, probe_response.reason)


# EMS Brno
# ====================================================================


# API documention: http://www.emsbrno.cz/p.axd/en/Api.html#GET._v_data

def retrieve_emsbrno(device, datetime_from):

  # --- loop over all the logs files in the EMS data
  if not os.path.exists(settings.EMS_DATA_DIR):
    return
  
  for root, dirs, files in os.walk(settings.EMS_DATA_DIR):
    for name in files:

      if not '(raw)' in name:

        # logs files are name like so: "locName - timestamp.txt"
        (basename, ext) = os.path.splitext(name)
        [locName, timestamp] = basename.split(' - ')[:2]
        time_start = int(datetime_from.timestamp())

        # only open the file associated to this device
        # and with a timestamp after the last registered data
        [shortLocName, longLocName] = device.remote_identifier.split(' - ')[:2]
        name_condition = shortLocName == locName
        time_condition = time_start < int(timestamp)

        if name_condition and time_condition:
          print(shortLocName, str(datetime.datetime.fromtimestamp(int(timestamp))), 
                'is after', str(datetime.datetime.fromtimestamp(time_start)))

          with open(os.path.join(root,name), 'r') as file:
            data_callback = json.load(file)
            
            lookback_days = (datetime.datetime.now(datetime.timezone.utc) - datetime_from).days
            
            if lookback_days <= 2:
              lookback = 2
            elif lookback_days <= 5:
              lookback = 5
            elif lookback_days <= 10:
              lookback = 10
            elif lookback_days <= 30:
              lookback = 30
            elif lookback_days <= 60:
              lookback = 60

            # get the 60days url from this file
            url = data_callback['data'][str(lookback)]['url']
            print('requesting...', url)

            # # --- finally make the request
            headers = {
              'accept': 'application/json',
              'language': 'en'
            }
            # we have to ask for json explicitely
            probe_response =  requests.get( url, headers = headers,
                                            params = { 'format': 'JSON' })

            if probe_response.ok:
              probe_data = probe_response.json()

              # --- the data looks like that:
              #   probe_data['time'] = [...] (Array of measurement timestamps in ISO 8601 format.)
              #   probe_data['channels'] = {'40107': {'id': '40107', 'site': 'VP1967_BE', 'locName': 'RB', 'values': [...]}, ...}
              # every callback has 3 channels

              times = probe_data['time']
              for channel in probe_data['channels'].values():
                print(channel['id'])

                # --- get the associated sensor, using remote identifiers
                try:
                  sensor = Sensor.objects.get(device = device, remote_identifier = channel['id'])
                  values = channel['values']
                  baseline = sensor.extra_data['baseline'] if sensor.extra_data and 'baseline' in sensor.extra_data else None

                  # --- for every observations in this time serie
                  for i in range(len(times)):
                    observation_timestamp = datetime.datetime.fromisoformat(times[i] + '+01:00')

                    if observation_timestamp > datetime_from:
                      try:
                        parsed_value = float(values[i])
                        
                        if baseline is not None:
                          # --- if the sensor has a baseline set encode value in database based on it
                          observation = Observation(
                            sensor = sensor,
                            value = parsed_value-baseline,
                            timestamp = observation_timestamp,
                            extra_data = { 'raw_value': parsed_value, 'baseline_applied': True }
                          )
                        else:
                          # --- create Observation
                          observation = Observation(
                            sensor = sensor,
                            value = parsed_value,
                            timestamp = observation_timestamp
                          )
                          
                        try:
                          observation.save()
                          print(f'Added: {observation}')
                        except IntegrityError as e:
                          print(e)
                      except ValueError:
                        print(f'Could not parse value {values[i]} at row {i}.')
                        pass
                      except TypeError:
                        print(f'Could not parse None value at row {i}.')
                        pass
                except Sensor.DoesNotExist:
                  print('No sensor associated with this device and channel id:', device, channel['id'])

            else:
              # URLS has expired so we delete the file
              # Note that there is a risk that it's another error
              # but anyways we get a file every 2 hour with an URL for 60 days

              print(probe_response.status_code, probe_response.reason)
              os.remove(os.path.join(root,name))


# GLOBAL COMMAND WITH SWITCH CASES
# ====================================================================

class Command(BaseCommand):
  # args = ''
  help = 'Retrieve data data from sensors registerd in the app.'

  def add_arguments(self, parser):
    # Positional arguments
    parser.add_argument("brands", nargs="*", type=str)


  # we loop on the devices, not on the sensor
  # the reason is that for some devices, one request is enough to retrieve all its sensors
  def handle(self, *args, **options):
    for device in Device.objects.filter(active=True):

      # --- get the time of the last observation
      last_observation = Observation.objects.filter(sensor__device__pk = device.pk).order_by("-timestamp")[:1]
      if not last_observation:
        if Device.start_date_data_fetch:
          datetime_from = device.start_date_data_fetch
        else:
          datetime_from = None
      else:
        datetime_from = last_observation[0].timestamp
      
      if device.deviceType.brand == 'Sensoterra' and \
        (not options['brands'] or device.deviceType.brand.lower() in options['brands']):
        if not datetime_from:
          datetime_from = datetime.datetime(2024, 2, 5, 9, 0)
        retrieve_sensoterra(device, datetime_from)

      elif device.deviceType.brand == 'Sensecap' and \
        (not options['brands'] or device.deviceType.brand.lower() in options['brands']):
        retrieve_sensecap(device, datetime_from)

      elif device.deviceType.brand == 'Decentlab' and \
        (not options['brands'] or device.deviceType.brand.lower() in options['brands']):
        if not datetime_from:
          datetime_from = datetime.datetime(2024, 2, 5, 9, 0)
        decentlab_retrieve_data(device, datetime_from)

      if device.deviceType.brand == 'EMS Brno' and \
        (not options['brands'] or device.deviceType.brand.lower() in options['brands']):
        if not datetime_from:
          datetime_from = datetime.datetime(2024, 2, 5, 9, 0)
        retrieve_emsbrno(device, datetime_from)

      print('')
