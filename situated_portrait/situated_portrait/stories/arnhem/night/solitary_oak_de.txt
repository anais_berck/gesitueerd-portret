Zwei Tannenwurzeln groß und alt 
unterhalten sich im Wald. 
Was droben in den Wipfeln rauscht, 
das wird hier unten ausgetauscht. 
Ein altes Eichhorn sitzt dabei 
und strickt wohl Strümpfe für die zwei. 
Die eine sagt: knig. Die andre sagt: knag. 
Das ist genug für einen Tag.
___ Christian Morgenstern

Das gelbe Laub erzittert,
Es fallen die Blätter herab;
Ach, alles, was hold und lieblich,
Verwelkt und sinkt ins Grab.
Die Gipfel des Waldes umflimmert
Ein schmerzlicher Sonnenschein;
Das mögen die letzten Küsse
Des scheidenden Sommers sein.
Mir ist, als müsst ich weinen
Aus tiefstem Herzensgrund;
Dies Bild erinnert mich wieder
An unsre Abschiedsstund'.
Ich musste von dir scheiden,
Und wusste, du stürbest bald;
Ich war der scheidende Sommer,
Du warst der kranke Wald.
___ Heinrich Heine, 1834


Am Brunnen vor dem Tore,
Da steht ein Lindenbaum.
Ich träumt in seinem Schatten
So manchen süßen Traum.
Ich schnitt in seine Rinde
So manches liebe Wort,
Es zog in Freud und Leide
Zu ihm mich immer fort.
Ich musst auch heute wandern
Vorbei in tiefer Nacht,
Da hab ich noch im Dunkel
Die Augen zugemacht.
Und seine Zweige rauschten,
Als riefen sie mir zu:
Komm her zu mir, Geselle,
Hier findst du deine Ruh!
Die kalten Winde bliesen
Mir grad ins Angesicht,
Der Hut flog mir vom Kopfe,
Ich wendete mich nicht.
Nun bin ich manche Stunde
Entfernt von jenem Ort,
Und immer hör ich's rauschen:
Du fändest Ruhe dort!
____ Wilhelm Müller 1823

Bäume sind Gedichte, 
die die Erde in den Himmel schreibt. 
____ Khalil Gibran

Wüsst ich genau, wie dies Blatt 
Aus seinem Zweig hervorkam, 
Schwieg ich auf ewige Zeit still, 
denn ich wüsste genug. 
___ Hugo von Hofmannsthal
