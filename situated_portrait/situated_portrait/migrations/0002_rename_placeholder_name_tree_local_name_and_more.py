# Generated by Django 4.2.11 on 2024-04-15 14:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('situated_portrait', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tree',
            old_name='placeholder_name',
            new_name='local_name',
        ),
        migrations.AddField(
            model_name='tree',
            name='environment',
            field=models.CharField(default='park', max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tree',
            name='latitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tree',
            name='longitude',
            field=models.FloatField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tree',
            name='remote_identifier',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='tree',
            name='species',
            field=models.CharField(default='specy', max_length=250),
            preserve_default=False,
        ),
    ]
