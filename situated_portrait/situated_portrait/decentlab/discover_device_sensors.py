import requests
import time
from situated_portrait.models import Sensor

"""
  Execute query on decentlab's api
"""
def query_api (query, apiurl, apitoken):
  return requests.get(
    apiurl + '/api/datasources/proxy/uid/main/query',
    {'db': 'main',  'epoch': 'ms', 'q': query},
    headers={ 'Authorization': 'Bearer {}'.format(apitoken) }
  )

"""
  Get unit for a sensor from decentlab API

  Example API-reply with data:

  {
    'results': [
      {
        'statement_id': 0,
        'series': [
          {
            'name': 'measurements',
            'columns':
              ['key', 'value'],
            'values': [
              ['unit', '°C']
            ]
          }
        ]
      }
    ]
  }

  Example API-reply without data:

  {
    'results': [
      {
        'statement_id': 0
      }
    ]
  }
"""
def get_sensor_unit (device_id, sensorname, apiurl, apitoken):
  response = query_api('SHOW TAG VALUES WITH KEY = unit WHERE node =~ /^{}$/ AND sensor =~ /^{}$/'.format(device_id, sensorname), apiurl, apitoken)
  data = response.json()

  if 'results' in data and 'series' in data['results'][0]:
    return data['results'][0]['series'][0]['values'][0][1]

  return None

"""
  List sensors attached to a device on decentlab API

  API-reply with data
{
  'results': [
    {
      'statement_id': 0,
      'series': [
        {
          'name': 'measurements',
          'columns': ['key', 'value'], 
          'values': [
            ['sensor', 'senseair-lp8-co2'],
            ['sensor', 'senseair-lp8-co2-filtered']
          ]
        }
      ]
    }
  ]
}
"""
def get_device_sensors (device_id, apiurl, apitoken):
  response = query_api('SHOW TAG VALUES WITH KEY = sensor WHERE node =~ /^{}$/ AND channel !~ /^link-/'.format(device_id), apiurl, apitoken)
  data = response.json()
  result = data['results'][0]

  if 'series' in result and len(result['series']) > 0:
    values = result['series'][0]['values']
    _, sensornames = zip(*values)

    for sensor in sensornames:
      yield(sensor)

"""
  List and add device
"""
def discover_and_add_device_sensors (device):
  sensors = []

  apiurl = device.deviceType.authentication['apiurl']
  apitoken = device.deviceType.authentication['apitoken']


  for sensorname in get_device_sensors(device.remote_identifier, apiurl, apitoken):
    sensor_remote_identifier = '{}.{}'.format(device.remote_identifier, sensorname)
    time.sleep(.1) # Let's not DDOS decentlab
    unit = get_sensor_unit(device.remote_identifier, sensorname, apiurl, apitoken)

    if unit is None:
      unit = 'number'

    if not Sensor.objects.filter(device=device, remote_identifier=sensor_remote_identifier).exists():
      print("Found new sensor: {} for {}, with unit {}".format(sensorname, str(device), unit))
      sensors.append(Sensor(
        device=device,
        type=sensorname,
        unit=unit,
        remote_identifier=sensor_remote_identifier,
        synthetic=False
      ))

  Sensor.objects.bulk_create(sensors)
