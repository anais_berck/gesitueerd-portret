import requests
from local_settings import username, password
import datetime

baseurl = 'https://monitor.sensoterra.com/api/v3/'

headers = {
    'accept': 'application/json',
    'language': 'en'
}

auth = requests.post(baseurl + 'customer/auth', headers=headers, json={
    'email': username,
    'password': password
})

authdata = auth.json()
api_key = authdata['api_key']
headers['api_key'] = api_key

# # probe?limit=100&skip=0&include_shared_data=NO
# probe
probes = requests.get(baseurl + 'probe', headers=headers, params={
    'limit': 100,
    'skip': 0,
    'include_shared_data': 'NO' 
})

probesdata = probes.json()

# # probe/57630/2024-02-05T09%3A00%3A00%2B01%3A00/2024-02-05T18%3A00%3A00%2B01%3A00?aggregate_by=none&limit=1000&skip=0&include_shared_data=NO&format=json
# probe/from/to

for probe in probesdata:
    probe_id = probe['id']
    timestamp_from = datetime.datetime(2024, 2, 5, 9, 0).isoformat() + '+01:00'
    timestamp_to = datetime.datetime(2024, 2, 15, 18, 0).isoformat() + '+01:00'

    # 2024-02-05T09:00:00+01:00

    print(timestamp_from)
    print(timestamp_to)

    probe_data = requests.get(baseurl + 'probe/{}/{}/{}'.format(probe_id, timestamp_from, timestamp_to), headers=headers, params={
      'aggregate_by': 'none',
      'limit': 1000,
      'skip': 0,
      'unit': '%,SI'
    })
    
    for observation in probe_data.json():
        if observation['unit'] == '%':
            print(observation['value'])