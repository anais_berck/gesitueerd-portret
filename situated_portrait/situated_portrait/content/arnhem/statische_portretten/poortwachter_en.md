## Sweet Chestnut

![](/static/img/trees/3watchers-600x.jpg)

We were planted three hundred and seventy-five years ago as a tribute to the Romans who brought the Sweet Chestnut to the North. We have been here almost as long as the Church of Eusebius. Rembrandt visited us. Once you have met us, we will gladly travel with you in your memory.
I am a teacher of Time in all its aspects. From my cellular memory, I share stories about Trees in world myths and traditions. At night, I recite poems about Time.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
