from situated_portrait.models import Sensor, Observation

dendrometers = Sensor.objects.filter(type='circular increment', device__deviceType__brand='EMS Brno', synthetic=False)

for dendrometer in dendrometers:
    print(dendrometer)
    # Fetch observation at April 5th 00:00
    
    baseline = dendrometer.extra_data['baseline']
    print('Found baseline: {}'.format(baseline))
    
    observations = dendrometer.observations.all()
    observations_to_update = []

    for observation in observations:
        if not observation.extra_data or 'baseline_applied' not in observation.extra_data:
            observation.extra_data = { 'raw_value': observation.value, 'baseline_applied': True }
            observation.value = observation.value - baseline
            observations_to_update.append(observation)
  
    Observation.objects.bulk_update(observations_to_update, ['value',  'extra_data'])

    print('Updated {} observations'.format(len(observations_to_update)))