# Web app

When you open the web app, you will be introduced to the guide. It will make the transition between park and environment and your phone as gentle as possible. The guide will lead you through the app. We hope we can arouse enough curiosity and wonder so that you feel like coming back.

In a second instance, you can zoom in on a story, a visualization, a soundscape. If you want to explore more, you will also get an overview of the data - in the form of simple graphs. You can always return to the guide from any other page.

### Story
This is a screen where you can see photos by Bram Goots and read a text, told from the tree's point of view.
During the day, you will be told fragments from the roots, trunk or crown of the tree. A fragment from the roots is an invitation to ground and connect with the tree. From the crown, the tree tells about relationships with other living things such as birds and insects. The trunk stores the tree's memory. Each tree narrates from its wood cells according to his own theme.
The text changes every 10 minutes.
At night, the trees will give you poems!

### Imaged
Imaged shows a clock that goes back 24 hrs in time. At the top is the now moment. Anticlockwise, you can see what measurements the sensors recorded at a particular tree. The clock is like a live portrait. It is updated every hour. This page also has subtitles. With each new update, a sentence appears, indicating how much a type of sensor measures. You can find more information about the different measurements at ‘About the sensors’.

### Symphony
Sound artist Christina Clar created specific sounds for each tree and each type of sensor.
For the trees, she chose classical instruments, such as harp, piano, drums... For the sensors, she created synthetic sounds.
Each time a sensor updates at a specific tree, the different sounds are played in a loop, with the last sensor getting the most attention. The sounds of the other trees play softly in the background, depending on the location.

### Data
For this project, we created a database which collects all measurements from all the different sensors.
This screen allows the data to be viewed as graphs in a simple way.

### Map
Here you will find a screen with a map and the route guiding you to the next tree. You can choose the tree according to the highest or lowest sensor readings.
