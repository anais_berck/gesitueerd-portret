Zit je comfortabel? Met je beide voeten op de grond?
Zie je mijn kruin? Zo ver als mijn kruin strekt, strekken mijn wortels.
En veel en veel dieper.
Beeld je in, dat uit jouw voeten wortels groeien, recht de grond in.
Ze reiken steeds dieper en dieper.
Ze verstrengelen met mijn wortels.
We steunen elkaar.
We versterken elkaar.
Zolang als jij dat zelf wil.
Je bent verankerd in de aarde.
Geen storm die je omver kan waaien.
Je lichaam beweegt alleen een beetje, flexibel, meegaand.
Hoe is het voor je om hier zo te zitten?
Hoe is het voor je om zo samen naar de wereld te kijken?

Kijk naar ons, zit bij ons, neem de tijd en zie wat er gebeurt.
Luister naar de vogels, de wind in onze bladeren en naar het geraas van de auto's.
Merk op hoe snel anderen aan ons voorbijlopen, alsof het ongemakkelijk is om lang bij ons te zijn.
Dat is het ook, want wij gaan enkel intieme relaties aan.

Wij bomen rennen niet van hot naar her.
We kennen geen haast.
We nodigen je uit om even mee te reizen in onze tijd.
Zet een timer als je wil.
Maak het stil.
En kijk tien minuten lang naar alles wat beweegt, hoe klein ook.

Ik ben een levend wezen,
diep verbonden met de aarde,
wijds reikend naar de hemel.
In mijn aanwezigheid is het voor jou eenvoudiger
om verbinding te maken
met jouw diepe wortels
met jouw grote hart
met de melkweg ver weg
volg mijn lichaam
adem diep in en uit
en als je het voelt,
neem dan potlood en papier
en teken, schrijf, kribbel.
Creëer een spoor van dit unieke moment.

Het gebeurt wel vaker
dat een mens passeert
met de fiets of te voet.
Soms zien ze me.
Soms stoppen ze om naar me te kijken.
Niet lang.
Ze nemen hun telefoon, maken een snelle foto
en vertrekken weer.
Als ik je vraag om heel langzaam een foto van me te maken,
welk beeld zou je dan kiezen?

Hier kan je gewoon even blijven zitten.
Je mag hier niksen.
Een tiental minuten of langer.
De geluiden die je hier hoort, de kleuren die je hier ziet, de boodschappen die je hier ontvangt,
ze brengen je rust.
Een bezoeker fluisterde het ons gisteren nog toe,
dat jullie, moderne stadsmensen, per dag evenveel prikkels te verwerken krijgen
als een oermens in heel zijn leven.

Zit je gemakkelijk?
Neem dan even de tijd om te kijken waar je zoal aan denkt.
Ben je onrustig over alles wat je nog te doen hebt?
Of over een van je naasten die het niet zo goed stelt?
Heb je misschien zelf zorgen?
Ik nodig je uit om je blik op een van mijn bladeren te laten rusten.
Even.
Concentreer je nu op wat je hoort.
Even.
Let eens op wat je kan ruiken.
Of proeven.
Valt het je op hoe beetje bij beetje elke andere gedachte uit je hoofd verdwijnt?
Je komt tot rust.
En je hebt weer ruimte vrij voor nieuwe, creatieve ideeën.

Ik stel voor, bij wijze van ontmoeting, dat je een paar keer diep in en uit ademt.
Jij ademt de zuurstof in die ik uitstoot.
Jij ademt de CO2 uit die ik opneem met mijn bladeren.
We zijn in voortdurende uitwisseling met elkaar.
Aangename kennismaking.

Hier ben je helemaal omringd door levende wezens.
Niet alleen mensen.
Ook grassen, planten, insecten, vogels.
Zelfs elk van mijn bladeren en mijn wortels leven.
Zou je willen proberen om je open te stellen voor ons?
Dan kan een van ons jou roepen.
Kijk eens om je heen.
Is er een grasspriet die je aandacht trekt?
Of een mos? Een rups? Of een lieveheersbeestje?
Voel je vrij om het te groeten.
Voel je vrij om aandachtig te luisteren.
Misschien kan je de boodschap wel ontcijferen die het je stuurt.

Ben je in gezelschap?
Hebben jullie zin in een experiment?
Probeer dan eens om een tijdje niets tegen elkaar te zeggen.
En zie wat er gebeurt, wat je voelt, wat je hoort.
Het zal je deugd doen, dat weet ik uit ervaring.

Wij hoorden je al van ver.
Wij, dat zijn al mijn wortels en ook de zwammen die met mijn wortels samenleven.
We wisselen voortdurend voedingsstoffen en informatie uit.
De zwammen krijg je in de herfst te zien, wanneer hun vruchten de kop opsteken.
Onder je voeten leven we verder als een gigantisch netwerk aan flinterdunne draadjes.
Samen houden we de bodem luchtig en rijk.
Zo kunnen er miljoenen andere levende wezentjes overleven.
Misschien denk je wel aan ons, wanneer je straks weer de ene voet voor de andere zet.

Heb je beide voeten stevig op de grond?
Of heb je zin om erbij te gaan liggen?
Voel je vrij.
En kijk dan eens vanaf de voet van mijn stam naar boven.
Observeer mijn duizenden bladeren. Zie je hun kleur?
Wat denk je, zijn mijn bladeren allemaal even groen?

Wees welkom, beste bezoeker.
Maak het je comfortabel.
Zit je goed?
Dan nodig ik je uit om je ogen te sluiten.
Wat hoor je?
Valt het je op dat het concert rijker wordt, wanneer je langer luistert?

Dank om hier te zijn.
Dank om ons aandacht te geven.
Laat je blik eens ronddwalen.
Kijk eens welke andere boom je aandacht trekt.
Groeit die recht of krom? Is die jong of oud? Donker of licht?
Zou hij de spiegel kunnen zijn van een gevoel of een situatie die je bezighoudt?
Als dat zo is, aarzel dan niet om je hart bij hem te luchten.
Wij bomen transformeren alles wat ons gegeven wordt.

Heel lang geleden, in de tijd van onze voorouders, hadden jullie mensenwezens de gewoonte om onze raad in te winnen.
We vonden dat fijn.
Er bestaan nog plekken in de wereld waar mensen dagelijks vragen stellen aan bomen.
Het is heel eenvoudig.
Eerst groet je ons, zoals je een mens op straat zou groeten.
Dan kan je je met ons verbinden.
Beeld je in dat uit je voeten wortels groeien, die zich verstrengelen met de onze.
Wanneer je goed verankerd ben, kan je je vraag stellen.
Leg dan je linkerhand op onze schors - als dat niet mogelijk is, kan je het je inbeelden.
Het eerste wat verschijnt, is ons antwoord.
Dat kan een gedachte zijn, een geluid, een briesje, een gevoel...
Misschien lijkt het niet meteen op een helder antwoord, maar na een tijd zal je het begrijpen.

Mijn bladeren nemen CO2 op, maken er in samenwerking met de zon suikers van die ik naar mijn wortels stuur.
Het afvalproduct van dat proces is zuurstof.
Sluit je ogen even en beeld het je in.
Jij ademt ons frisse afval in.
En de lucht die je uitademt, is veel warmer.
Voel je dat?
De lucht die jij uitademt, is ontdaan van zuurstof en boordevol CO2,
waarmee ik weer verder kan.
Grote dank daarvoor.

Ik nodig je graag uit om eens goed naar me te kijken.
Zonder gêne.
Ik stel voor dat je een van mijn takken zo lang mogelijk blijft volgen,
recht de hoogte in.
Zo zie je hoe wij - net als jij - zoveel mogelijk groeien naar het licht.

Wil je graag een glimp opvangen van mijn ritme?
Adem dan 7 tellen door je neus.
Dit is de lente, waarin ik actief reserves opbouw.
Rust 4 tellen door je adem in te houden.
Dit is de zomer, waarin ik weer even vertraag.
Adem nu uit door je mond in 8 tellen.
Dit is de herfst, waarin ik alles loslaat wat mij niet meer dient.
Rust weer 4 tellen.
De stilte van de winter rondt de cyclus af.

Weet je wat jullie mensen echt veel deugd kan doen?
En wat jullie nauwelijks nog durven doen?
Echte aarde opsnuiven.
Waarom zou je het niet proberen?
Je hoeft er maar voor op je knieën te gaan zitten,
een klein kuiltje te graven
en onze schimmelvrienden geven je hun geurigste parfums zomaar cadeau.

Voel jij je soms eenzaam?
Wees dan welkom hier bij mij.
Vlij je tegen mijn stam.
Kijk naar me.
Vertel me je diepste geheimen.
Wij bomen zijn grote mensenvrienden.
En wij zijn altijd op post.

Op een dag kreeg ik bezoek van een gids die een groep soortgenoten onderrichtte.
Hij nam een koffielepel aarde bij mijn wortels, toonde die en zei:
'Dit lepeltje grond bevat meer organismen dan er mensen zijn op aarde.'
Die dag leerde ik dat de bodem heel ver van jullie menselijke ervaring afstaat.
De minuscule netwerken van organismen in het donker lijken voor jullie meer op een buitenaardse planeet dan op een vertrouwde wereld.
Daarom kunnen jullie aarde zo gemakkelijk negeren, ondanks het feit dat jullie leven er volledig van afhankelijk is.
Deze aarde voorziet in al het leven dat bestaat op deze planeet.
Het levert ook driekwart van jullie medicijnen.
Van antibiotica en antidepressiva tot kankergeneesmiddelen.

Mijn wortelstelsel reikt zo diep en breed als mijn kruin.
Ik leef dankzij de aarde, de grond, de Aarde.
Voor jullie lijkt die aarde dood, vuil, vies.
Misschien is het wel omdat de aarde jullie angst inboezemt.
Angst voor de dood, voor de cyclus van begraven worden,
en zelf weer voedsel worden voor doodgravers, mestkevers, mollen en andere levende wezens die in de diepte wonen.

Hoeveel verhalen over zand, humus of bodem ken je?
Over de plek waarnaar je terugkeert?
Over de plek die je voedt?
Over de plek die je zuiver drinkwater geeft, dag na dag?
Heb je je al eens bedacht dat de bodem in staat is om met je te communiceren?
Wil je misschien eens proberen om je oor te luisteren te leggen bij de grond?

Onze terugkeer naar de aarde, naar alle organismen die in de bodem leven,
dat is wat ons verbindt.
Die terugkeer is een essentieel recht, of het nu gaat om onze lichamen in de aarde, om as in de zee, om deeltjes die verpulverd zijn en in de lucht zweven. Alles komt uiteindelijk tot rust op nieuwe grond.
Terugkeren naar de aarde is erkennen dat wij maar een deel van een cyclus zijn.
Allemaal draaien we mee in het wiel van de wereld.
Allemaal komen we terecht in de ondergrond.
Daar transformeren we in nieuwe vormen.
Daar groeien we weer naar het licht.

Lees nog snel even dit tekstje en dan is het tijd om je telefoon even weg te stoppen. Kijk eens rond. Zie je iets waarvan je graag een foto zou willen nemen? Probeer dat niet te doen. Neem een mentale foto in de plaats. Wat is het dat precies jouw aandacht trok? Bestudeer de details, de kleuren, de vormen. Maak een rechthoek met je duimen en wijsvingers. Welk perspectief kies je voor je mentale foto?
Klik! Wat je in je hoofd en je hart bewaart, raak je nooit meer kwijt.

In mijn gezelschap kan je je intuïtie oefenen. Ik nodig je uit om rond te kijken tot iets je aandacht trekt. Geef jezelf de tijd om ernaar toe te gaan en het zo bewust mogelijk te bekijken.
Kan je je inbeelden hoe het voelt, hoe het ruikt, hoe het meegeeft met je vingers? Als je het hele plaatje duidelijk hebt in je verbeelding, dan kan je mij, de plant of het voorwerp aanraken.
Klopt jouw intuïtie met hoe het echt aanvoelt?

Vogels, dieren, insecten, mensen in mijn omgeving volgen een heel eigen ritme. Ik stel voor dat je even je ogen sluit en oren spitst. Wat hoor je zoal? Heb je zin om ‘s avonds laat of ‘s ochtends vroeg nog even terug te komen? Dan zal je een heel ander concert te horen krijgen.

Jij kan hier luisteren naar de vogels, de zoemende insecten, de wind. Wist je dat wij ook naar jou kunnen luisteren? Je hoeft hier maar even te zitten. Je kan me jouw verhaal vertellen. Als je dat wenst kan ik het via mijn wortels en kruin doorgeven aan andere bomen in de buurt. Voor je het weet, zal je jezelf dan door ons gedragen voelen. Licht als een veer zal je zijn.

Sommige parkbezoekers verstaan de kunst om ons net na de regen te komen bezoeken. Tot een uur na een regenbui zit de lucht vol gezonde bacteriën uit de grond. Dankzij de regendruppels, die op de grond bij mijn stam vallen, zich vermengen met de moleculen van de Aarde en weer verdampen. Ze krijgen zo maar gratis en voor niets een inwendige douche.

Heb je zin om mij eens vroeg op de ochtend te bezoeken? Dan glinsteren hier overal dauwdruppels en is de lucht nog zwaar van net ontwaakte geuren. Misschien vind je dan wel een vroege slak of zie je de reiger op zijn eerste dagvlucht. Sowieso zal het hier rustig zijn, we zullen alleen zijn, met z’n tweeën.

'Komorebi' is het Japanse woord voor het zonlicht dat dagelijks door mijn kruin sijpelt. Ik nodig je uit om je dit woord eigen te maken. Dat kan je doen door op de grond te liggen of zitten en lang en aandachtig naar mijn bladerdak te kijken, naar het spel van de zon, de schaduw en de wind. Elke dag opnieuw.

Rond mijn kruin, stam en wortels zoemt, tsjilpt, fluit en ritselt het volop tijdens de zomer. Ik nodig je uit om te luisteren naar het verste geluid dat je kan horen. Misschien is het een vliegtuig? Of een auto? De wind in de bomen? Luister nu naar de geluiden dichterbij, telkens dichter naar jezelf toe, tot je eindigt bij jezelf, binnen in jou. Wat hoor je daar?

Ben je comfortabel? Heb je tijd? Kijk dan eens rond en bedenk waar je zoal dankbaar voor kan zijn? Het doet je vrolijk voelen!

Rond mijn stam en onder mijn kruin vind je talloze takjes, oude resten van vruchten, grassprietjes, misschien wel een bloemetje. Wij bomen houden heel erg van jullie creativiteit. Voel je daarom vrij om hier een kleine collage achter te laten, met een intentie erbij. Daar leef ik van op!
