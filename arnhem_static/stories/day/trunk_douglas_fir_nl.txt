De meeste wandelaars lopen aan ons voorbij.
We zijn zo talrijk in Nederland.
En nog altijd worden we migranten genoemd.
Gevestigde exoten.
We werden naar hier getransporteerd vanuit het Westen van de Verenigde Staten.
Daar worden we tot 100 meter hoog.

Samen met al mijn kompanen in dit stukje bos
werd ik aangeplant in 1962, door de vader van de eigenaar van de manège.
Wij waren toen in de mode.
We groeien snel. Ons hout is sterk.
Het werd veelvuldig gebruikt in de mijnen.
Niemand die zich toen kon inbeelden dat de mijnen op een dag zouden sluiten.
Het is ons geluk.
Al worden we ooit op een dag wel eens gekapt.

Mijn kernhout is roodbruin.
Eens je het gezien hebt, blijf je het herkennen.
Wij blijven na onze dood nog veel meer in jullie buurt:
in de daken van jullie huizen, de deuren, de kozijnen,
en heel waarschijnlijk ook in boekenkasten en goedkope meubels.
Want wij zijn de grote leveranciers van de triplex en multiplex platen.

Is het je al opgevallen dat ik omringd ben door stekelige bramen?
Net zoals alle andere aangeplante Douglassparren aan deze kant van het pad.
Wanneer je naar onze overburen kijkt, zie je iets heel anders.
Een dicht jong bos, vol gezonde jonge berken en lijsterbessen.
Je krijgt het mee als een raadsel:
wat is er met de ondergrond tussen onze wortels gebeurd?

Zoals alle gevestigde exoten draag ik in mijn houtcellen
de herinnering aan de man die ons over de oceaan voerde.
Zijn naam was David Douglas. Hij besliste om ons zijn naam te geven.
Hij was een Schotse tuinier die het ver schopte.
In totaal bracht hij 240 verschillende planten naar Engeland.
Hij stierf op 35-jarige leeftijd zijn poen...

Ik leef hier al vele jaren. Het is onmogelijk voor me om hier te vertrekken.
Wij planten zijn sendentair. We kunnen niet vluchten.
Dat maakt dat we anders gebouwd zijn dan jullie, mensen.
Om één hart te hebben op één welbepaalde plaats, of één long, of één plek voor één stel hersens,
dat zou al heel snel onze dood betekenen.
We zouden ons niet kunnen weren tegen een vijand die een stuk van ons lichaam afrukt.
Wij groeien dan ook duizenden geslachtsorganen, en evenveel neuzen als er bladeren, stam en wortels zijn.
Wij hebben altijd een Plan B.
Wij zijn modulair.
Wij zijn niet één ik, maar duizenden 'ikjes' tegelijk.
Wij kunnen 'ik' zeggen met eender welk deeltje van ons lichaam.

Hoe oud denk je dat ik ben? Geloof je dat ik nog jong ben omdat ik zo’n dunne stam heb? In een bos is zo’n gedachte misleidend. Als een boom veel groeiruimte heeft, en daardoor een grote kruin kan ontwikkelen, wordt hij dik. Maar ook dunne bomen kunnen heel erg oud zijn. Die telt dan evenveel jaarringen, alleen zijn ze veel fijner.

Vind je dit een bos? Wat is voor jou een bos? Een bos is een plek waar het barst van de bomen, hoorde ik een expert vertellen. In dit deel van Nederland zijn de meeste bossen aangeplant voor houtproductie. Ook wij, Douglassparren, zijn aangeplant. Maar als je de andere kant van het pad bekijkt, dan zie je een natuurlijk jong bos, met Berken en Lijsterbes, de klassieke pioniersbomen die maar weinig voeding nodig hebben om te groeien.

Ooit word ook ik gekapt. Het zal nog even duren. De meesten van jullie zal ik wel overleven. Kappen is niet verkeerd, zolang wij maar op tijd op de hoogte worden gebracht.
De dag waarop ik geveld word, geef ik meer groeiruimte en licht aan andere bomen, die op hun beurt groot en sterk kunnen worden. En jullie kunnen mijn hout verwerken, in een prachtige deur bijvoorbeeld.

Misschien vind je het hier een beetje rommelig, met die bramen en dode takken. Tot zo’n vijftig jaar geleden zou dit bos schoon zijn, aangeharkt. Intussen beseffen jullie mensen dat dood hout een belangrijke ecologische functie heeft. Door het te laten liggen, lever er weer Spechten, Kevers en Torren, Paddenstoelen en Mossen. Het bos wordt niet langer als een plantage gezien, maar als een ecosysteem. Dank daarvoor.
