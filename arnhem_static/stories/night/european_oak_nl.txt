Zoveel stilte om
van te houden
als je vleugels
krijgt je gehoord
aangeraakt voelt
zoveel stilte om
te doorbreken
als de bedreiging
binnenin aanzwelt 
monddood maakt
zoveel stilte om
te aanvaarden
als er niets is
om mee te denken 
om van te houden. 
door J.Bakx, 2020

Stilte die spreekt
Waar pracht en praal
Daag'lijks hoogtij vieren
En zelfs het duurste
Horloge met Kerst geen
Voldoening meer geeft
Daar is het heldere
Kaarslicht vanavond
Stille getuige in de tijd -
De stille nacht
Die warmte geeft
In kille eenzaamheid.
door Bert Weggemans, 2012

Na het slotakkoord 
houdt hij zijn baton hoog 
de instrumenten zwijgen
uit het ampul vloeit
een concentraat van stilte
recept voor de wereld.
door J.Bakx, 2024

Min de stilte in uw wezen,
 Zoek de stilte die bezielt,
Zij die alle stilte vrezen
Hebben nooit hun hart gelezen,
 Hebben nooit geknield.
Draag uw kleine levenszegen
 Naar het dromenloze land,
Lijk de golve' haar oogst bewegen -
Tot zij zachtjes breken tegen
 Het doodstille strand.
door C.S. Adama van Scheltema, 1928
 
 Leer u aan de stilte laven:
 Waar het leven u geleidt -
Zij is uwe veil'ge haven,
Want zij is de grote gave
 Van de Eeuwigheid.
Sluit de stilte in uw gaarde,
 Wees in haar gelukkig kind:
Al wie ze aan haar schoot vergaarde -
 Alle zaligen op aarde
 Hebben haar bemind.
door C.S. Adama van Scheltema, 1928

Wanneer men luistert
naar zijn ziel dan begint taal
zowaar te leven.
door Martin Valk, 2015

Laat woorden
Nooit een stilte kraken
die de liefde breken.
door cirkel der natuur, 2016

Het helse kabaal 
Achter ons gelaten
Pijlen die zinloos de
Hemel hebben bestormd -
Als uiteind'lijk al het
rumoer verdwenen is,
En het ogenblik van de 
Stilte aangebroken is,
Die zo weldadig over ons
Gevallen is - stilte, 
Vallende stilte die zwanger
Is van tijd die open staat
Voor een ieder die wacht
Op wat komen gaat.
door Bert Weggemans, 2012

Hoor het licht vallen 
op de velden, 
het verstrijken
van tijd
zacht schuren.
Zon schildert
in het voorbijgaan
kleuren in de bomen.
De lucht staat strak
blauw zonder een woord
te laten vallen.
Zelfs de vogels vliegen
geruisloos.
Luister, dit is 
stilte.
door Pia Wolters, 2014

Degene
die naast me staat
is stil van geest.
Hoeft niets 
te bewijzen,
schreeuwt noch overschreeuwt.
Pact van stilte,
voor het leven
verbondenheid
in bescheidenheid.
Gelijk ik: wachten
in eigen krachten.
door Petra Hermans, 2013

Jij hebt een bijzonder mooie geest:
de stilte erin
bekoort me
nog het meest,
wanneer jij
de liefde
in mijn ogen
leest.
door Petra Hermans, 2013

Hier, waar het onwerkelijk stil is.
Stilte, waarin een mens leert zwijgen.
Waar stille geluiden helder klinken.
Tonen die we zelden zuiver horen.
Hier, in onze eigen achtertuin,
dromen onverwachte dromen.
Vandaag heerst hier de vrede.
Nestelt zich een duivenpaar.
Leeft de werkelijkheid van hoop.
door Albert C M Weijman, 2020

Ik wil luisteren naar de stilte.
Of er nog iets leeft.
Waar het geluid van een vlinder,
Een donderslag geeft.
Ik wil luisteren naar de stilte.
Om uit te rusten van het drukke leven.
En die rust kan alleen de stilte geven.
door J.Ruigendijk, 2004


