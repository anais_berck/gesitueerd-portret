# Licence

Gesitueerd Portret, by Anaïs Berck, Brussels, August 2024.

All code, images, sounds and texts are published to a Creative Commons Attribution 4.0 licence (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). You may reuse, distribute, reproduce, and adapt this work in any medium, except for commercial purposes, provided you give attribution to the copyright holder, provide a link to the Creative Commons licence, and indicate if changes have been made.

All code, images, sounds and texts have also been released under a CC4r licence, available here: <https://constantvzw.org/wefts/cc4r.en.html>.

The data captured during this work is published to a Creative Commons 1.0 licence (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). You can copy, modify, distribute and perform the data, even for commercial purposes, all without asking permission.
