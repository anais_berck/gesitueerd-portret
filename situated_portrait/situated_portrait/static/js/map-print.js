// --- init the map
// note that we're going to resize and fit according to the geojson layer
// afterward so don't worry now :)
let map = L.map('map').setView([51.9904, 5.9069], 16);
// --- we create a layer so we can later get the bounding box of that layer and zoom accordingly

const translated_local_treenames = {
    'European oak': 'Boseik',
    'Douglas fir': 'Douglasspar',
    'Red beech': 'Rode Beuk',
    'Solitary oak': 'Allenige Eik',
    'Sweet chestnut': 'Kastanje',
    'European horse-chestnut': 'Paardenkastanje'
}

const label_positioning = JSON.parse(document.getElementById('tree-label-positioning-data').textContent);


let tree_layer = L.featureGroup();
// --- get the serialized tree data
const TREES = JSON.parse(JSON.parse(document.getElementById('trees-data').textContent));
for(let model of TREES){
    tree = model['fields']

    if (tree['latitude'] && tree['longitude']){
        let lat = parseFloat(tree['latitude']);
        let lon = parseFloat(tree['longitude']);

        let marker_html = '<div data-position="' + label_positioning[model['pk']] + '">' + translated_local_treenames[tree['local_name']] + '</div>';

        L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: '<div class="marker-box">' + marker_html + '</div>'
            })
        }).addTo(tree_layer);

        L.marker([lat, lon]).addTo(tree_layer);
    }
}

let label_layer = L.featureGroup();
const LABELS = JSON.parse(document.getElementById('label-data').textContent);
for(let label of LABELS){
    if (tree['latitude'] && tree['longitude']){
        let lat = parseFloat(label['latitude']);
        let lon = parseFloat(label['longitude']);

        let marker_html = '<div>' + label['label'] + '</div>';

        L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: '<div class="marker-box">' + marker_html + '</div>'
            })
        }).addTo(tree_layer);

        L.marker([lat, lon]).addTo(label_layer);
    }
}

// --- adding GEOjson from OSM
const POI = JSON.parse(document.getElementById('poi-data').textContent);
let poi = [];
let poi_legend = [];
let i = 1;

// sorting them by latitude
POI['features'] = POI['features'].sort(lat_sort);
function lat_sort(a,b){
    a_all_lat = a['geometry']['coordinates'].map(x => x[1])
    b_all_lat = b['geometry']['coordinates'].map(x => x[1])
    a_lat = Math.max(...a_all_lat);
    b_lat = Math.max(...b_all_lat);
    return a_lat > b_lat;
}

let poi_layer = L.geoJSON(POI, {
    noClip: true,
    onEachFeature : function(feature, layer){
        poi_legend.push([feature['properties']['name']])
        all_lat = feature['geometry']['coordinates'].map(x => x[1])
        all_lon = feature['geometry']['coordinates'].map(x => x[0])

        if('building' in feature['properties']){
            lat = all_lat.reduce((a, b) => a + b) / all_lat.length;
            lon = Math.max(...all_lon);
        }
        else{
            lat = all_lat.reduce((a, b) => a + b) / all_lat.length;
            lon = all_lon.reduce((a, b) => a + b) / all_lon.length;
        }
        let marker = L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: '<div class="marker-box poi">' + i + '</div>'
            })
        })
        i = i + 1;
        poi.push(marker);

    },
    style: function (feature) {
        let className = 'poi-layer';
        if('building' in feature['properties']){
            className = className + ' building';
        }
        return {
          className: className,
        };
    }
});
for(let p of poi){
    p.addTo(poi_layer);
}

const WALK = JSON.parse(document.getElementById('walk-data').textContent)
let walk_layer = L.polyline(WALK);
walk_layer.setStyle({'className': 'walk-layer walk-long-layer'});
walk_layer.setText('7 km →', { offset: -10, dx: 635, className: 'label-walk label-walk-long' })
const WALK_SHORT = JSON.parse(document.getElementById('walk-short-data').textContent)
let walk_short_layer = L.polyline(WALK_SHORT);
walk_short_layer.setStyle({'className': 'walk-layer walk-short-layer'}); 
const WALK_WHEEL = JSON.parse(document.getElementById('walk-wheel-data').textContent)
walk_short_layer.setText('4 km →', { offset: -10, dx: 33, className: 'label-walk label-walk-short'})
let walk_wheel_layer = L.polyline(WALK_WHEEL);
walk_wheel_layer.setStyle({'className': 'walk-layer walk-wheel-layer'}); 

const PARK = JSON.parse(document.getElementById('park-data').textContent)
let park_layer = L.geoJSON(PARK, {
    noClip: true,
});
park_layer.setStyle({'className': 'park-layer'});

const WATER = JSON.parse(document.getElementById('water-data').textContent)
let water_layer = L.geoJSON(WATER, {
    noClip: true,
});
water_layer.setStyle({'className': 'water-layer'});

const WAYS_OUTLINE = JSON.parse(document.getElementById('ways-data').textContent)
let ways_layer_outline = L.geoJSON(WAYS_OUTLINE, {
    noClip: true,
});
ways_layer_outline.setStyle({'className': 'ways-layer-outline'});


const WAYS = JSON.parse(document.getElementById('ways-data').textContent)
let ways_layer = L.geoJSON(WAYS, {
    noClip: true,
});
ways_layer.setStyle({'className': 'ways-layer'});

// order
park_layer.addTo(map);
water_layer.addTo(map);
ways_layer_outline.addTo(map);
ways_layer.addTo(map);
walk_layer.addTo(map);
walk_short_layer.addTo(map);
walk_wheel_layer.addTo(map);
poi_layer.addTo(map);
label_layer.addTo(map);
tree_layer.addTo(map);

// change the zoom level to fit the geojson layer with some padding
map.fitBounds(tree_layer.getBounds(),{
    padding: L.point([5, 5])
});

let legend = document.getElementById('legend');
legend_html = '<ol>';
for(let p of poi_legend){
    legend_html = legend_html  + '<li>' + p + '</li>';
}
legend_html = legend_html + '</ol>';
legend.innerHTML = legend.innerHTML + legend_html;