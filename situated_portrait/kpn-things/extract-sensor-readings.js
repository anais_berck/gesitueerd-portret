function listener(details) {
  const filter = browser.webRequest.filterResponseData(details.requestId);
  const decoder = new TextDecoder("utf-8");
  const encoder = new TextEncoder();

  const response_data = [];
  filter.ondata = (event) => {
    response_data.push(event.data);
  };

  filter.onstop = (event) => {
    let json_str = "";
    if (response_data.length === 1) {
      json_str = decoder.decode(response_data[0]);
    } else {
      for (let i = 0; i < response_data.length; i++) {
        const stream = i !== response_data.length - 1;
        json_str += decoder.decode(response_data[i], { stream });
      }
    }
    
    filter.write(encoder.encode(json_str));
    filter.close();

    try {
      let json = JSON.parse(json_str);
      console.debug(json);
      let records = json.records.filter((record) => record.messageDirection == "UP");

      if (records.length > 0) {
        console.log(records[records.length-1].timestamp);
        let deviceId = records[0].deviceIdentification;
        browser.storage.local.get(deviceId)
          .then((storage) => {
            if (!(deviceId in storage)) {
              storage[deviceId] = [];
            }
            storage[deviceId].push(records);
            browser.storage.local.set(storage)
          })
          .catch((e) => {
            console.log(e);
            let storage = {};
            storage[deviceId] = [ records ];
            browser.storage.local.set(storage)
         });
      }


      
      if (json.hasNextPage) {
        window.setTimeout(() => {
          browser.tabs.sendMessage(details.tabId, {
            command: "loadMoreRecords",
          });
        }, 2500 + Math.random() * 3000);
      }
    }
    catch {
      console.log("Could not parse JSON?", json_str);
    }
    
    
  };

  return {};
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  { urls: ["https://api.kpnthings.com/api/thingsgateway/wlogger/*"], types: ["xmlhttprequest"] },
  ["blocking"]
);
