## Copy over db to local machine (run on local machine)
`scp debian@91.134.100.3:/srv/bomenvertellen/gesitueerd-portret/situated_portrait/db.sqlite3 ./`

## Run manage.py
`sudo -u bomenvertellen /srv/bomenvertellen/venv/bin/python /srv/bomenvertellen/gesitueerd-portret/situated_portrait/manage.py`

## Update repository
`cd /srv/bomenvertellen/gesitueerd-portret && sudo -u bomenvertellen git pull`

# Registering new devices.

Milesights are automatically added when the flow is directed to the installation.

On other new devices run setup-devices management command.

Sensecap, decentlab and sensoterra devices are discovered. If there are no observations yet it is possible the fromdate has to be set by hand.

EMS Brno have to be added in the authentication details field of the dendrometer device type. After the devices and their sensors have been added the channel id's of the sensors have to be set. The id's can be found in the control panel of ems brno.

Have to set baseline. Take first observation, add it to extra data of circular increment sensor: {"baseline": 5.764000273775309}
/srv/bomenvertellen/venv/bin/python manage.py shell < situated_portrait/apply_ems_baseline.py 

--

