I am an Oak.
You call me a native species here in the Netherlands.
And yet I too once came from elsewhere.
After the last Ice Age, my ancestors travelled
from southern Spain and southern Italy here.
So we were once migrants.
Do you happen to know how long it takes for someone to stop being called a migrant?

I have lived here for 225 years.
Once, I was planted by an older man.
He was commissioned by the then owners of the Witte Villa.
For more than two centuries, I have been breathing and observing,
I receive and grow,
I enrich you with oxygen.
Imagine what the world looked like here then.
Can you list ten elements that were not around when I was a toddler?

I know happiness
living as a detached Oak.
Whoever sees me
remains enraptured for a moment.
I am an Oak that has been able to develop in all directions
to my liking and well-being
without any interference from other trees, buildings, sewers or underground car parks.
I am the paragon
of ideal personal development.
You are free at any time
to mirror me.

I live in this Marsh Meadow.
It is always nice and wet and soggy here.
The Beech down the road would not survive here.
Its superficial roots would soon begin to rot.
My roots reach deeper.
And just as one of my branches will always grow upwards like a central spear,
there is always a root that goes to the depths,
strong, powerful, long and thick.
It is my taproot, my durable anchor.

I have lived here for many years. It is impossible for me to leave here.
We plants are sedentary. We cannot flee.
That makes us built differently from you, humans.
To have one heart in one well-defined place, or one lung, or one place for one set of brains,
that would very soon kill us. 
We wouldn’t be able to defend ourselves against an enemy who tears off a piece of our body.
We therefore grow thousands of sex organs, and as many noses as there are leaves, trunk and roots.
We always have a Plan B. 
We are modular. 
We are not one I, but thousands of Is at once.
We can say I with any particle of our body.

Many park visitors like me because my appearance resembles that of the Tree of Life, an important symbol in almost all major religions and cultures. The cosmic tree makes the connection between Heaven and Earth, between gods and humans. You could say that my roots connect to the Underworld. And with my branches reaching to the Sky, I connect to the Universe. So you could engage me as a mediator between yourself and invisible beings in other dimensions.

My sprawling even branches remind park visitors of the Tree of Life. It symbolizes eternal life. You can read the changing of the seasons in it - buds sprouting in spring, growth in summer, fruit and decay in autumn, and dying off in winter. That dying off is only an apparent dying off. Every spring I sprout again and gain new green leaves. I become a symbol of victory over death.

Odysseus, the main character in Homer's Greek adventure novel, was the king of Ithaca. For years he travelled around. When he returned home, he looked like a beggar, a wanderer with dirty clothes and a long beard. His wife, Queen Penelope, couldn’t believe he was her long-lost husband. As a test, she told him she had had their bed moved. Odysseus, who had carved the bed himself from an Oak that had its roots in the foundation of the house itself, burst out in anger. Penelope cried with happiness.

As an Oak, many mythical stories are attributed to me. Let me share one more with you, about Wodan, the supreme Germanic god who was worshipped in Oaks. The Wodan Oaks of Wolfheze are named after him. Justice used to be administered under those giant Oaks, as the leaves of those trees would whisper justice to the plaintiffs.

Someday, when my branches bend down from fatigue and I will begin to meet my end, I hope to have the company of the mistletoe, even if it is a parasite that will take root in my wood and tap into water and minerals there.
Among the Celts and Germanic people, the mistletoe growing on an Oak was a sacred plant. With a golden sickle, the druid cut the mistletoe from the Oak in the midwinter ceremony. The cut plant was not allowed to touch the ground and was collected in a white cloth. Then the druid dipped the mistletoe in water. This was used as protection against disease and calamity. Wouldn't it be nice if future park visitors were to hang mistletoe in their houses again?

Once, nature was sacred to the peoples of these regions. The Celts did not build churches or temples, but laid a sacrificial stone in the forest. They organized their rituals in the open air, under the rustling foliage of natural temples. They worshipped their supreme god, Dagda, the good god who controlled the seasons, in an Oak tree.

An Oak like me, 200 years old, produced some ten million acorns. Can you imagine how important that rich harvest was for your ancestors? When grain was unknown, acorns were the staple food for humans, who ground them and made them into bread. Does it surprise you then that we Oaks were revered as sacred trees?

Once, long ago, visitors were in the habit of asking me for permission to use my leaves. They are very efficient. Take my fresh leaves, pound them, boil them in water and then use that water to wash a wound. It avoids inflammation, purifies and heals the wound.

Oak is an Old Germanic word meaning 'tree'. The tree clerk compared it to the word you use almost every day: google. The proper name becomes generic because it is so present.
The Greek word for Oak is 'drus'. It is similar to the word 'druid' for the Celtic priests, who were also called ‘Oak people’. Oaks were revered by the Celts as sacred trees.

For many park visitors, I am their favourite tree. A freestanding Oak like me symbolizes strength, sexuality, power and longevity. I am connected in your subconscious to fire and fertility. You warm yourselves and build with my wood, feed on my fruit, and you have done so for centuries. This also makes me a symbol of generosity.
