from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt, xframe_options_sameorigin
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.conf import settings
from django.db.models import Max, Min
from django.urls import reverse
from .models import Tree, Sensor, Observation, Installation
from markdown import markdown
import json
from django.shortcuts import render
import os
import datetime
from .dialogue import dialogue
from situated_portrait.subtitles.subtitles_tree_nl import generate_update_subtitle_for_tree as generate_update_subtitle_for_tree_nl
from situated_portrait.subtitles.subtitles_tree_de import generate_update_subtitle_for_tree as generate_update_subtitle_for_tree_de
from situated_portrait.subtitles.subtitles_tree_en import generate_update_subtitle_for_tree as generate_update_subtitle_for_tree_en
from situated_portrait.clock import generate_clock
# from functools import cache
from django.contrib.auth.decorators import login_required

from situated_portrait.discover_sound_files import discover_sound_files
from situated_portrait.stories import get_stories

from situated_portrait.map import get_poi, get_park, get_water, get_ways, get_focus, get_map_labels, get_walks
from django.utils.translation import gettext as _
from django.utils.translation import get_language, get_supported_language_variant

import re




#   INTERFACE VIEWS
#   ====================================================

# List of tuples with navigation items
# first enty is the text label
# second is the name of the view, or a tuple with name of the view
# and possibly a postfix
def menu_base ():
    return [
        (_('Terug naar de gids'), ('guide', '#p-5'))
    ]

def menu_tree (choices):
    return ((label, name) for label, name in (
        (_('Verhaal'), 'story'),
        (_('Verbeeld'), 'portrait'),
        (_('Symfonie'), 'soundscape'),
        (_('Data'), 'data')
    ) if name in choices)

def get_view_url (viewname, *args, **kwargs):
    # path = '/'
    # if viewname == 'about': 
    #     args.append(path)
    if type(viewname) is tuple:
        return reverse(viewname[0], args=args, kwargs=kwargs) + viewname[1]
    return reverse(viewname, args=args, kwargs=kwargs)

def menu_insert_urls (menu, *args, **kwargs):
    return [(i[0], get_view_url(i[1], *args, **kwargs)) for i in menu]

def get_markdown_content (folder):
    # it will return a dictionary
    # of parsed markdown inside of a specific folder
    article = {}
    possible_extensions = ['.md']
    directory = os.path.join(settings.PATH_CONTENT, folder)
    for file in os.listdir(directory):
        (basename, ext) = os.path.splitext(file)
        if ext in possible_extensions:
            path = os.path.join(directory, file)
            lang = basename.split('_')[-1]
            with open(path, 'r') as file:
                article[lang] = markdown(file.read(), extensions=['extra'])
    return article

def get_all_markdown_content (folder):
    # it will return a dictionary
    # of parsed markdown inside of a specific folder
    articles = {'en':[],'nl':[],'de':[]}
    possible_extensions = ['.md']
    directory = os.path.join(settings.PATH_CONTENT, folder)

    for root, dirs, files in os.walk(directory):
        for filename in files:
            (basename, ext) = os.path.splitext(filename)
            if ext in possible_extensions:
                path = os.path.join(root, filename)
                print(path, dirs)
                lang = basename.split('_')[-1]
                with open(path, 'r') as f:

                    article = markdown(f.read(), extensions=['extra'])
                    articles[lang].append(article)

    print(articles)
    return articles

def index (request):
    available_languages = request.site.installation.languages_as_tuples
    lang = get_supported_language_variant(get_language())
    if lang not in request.site.installation.languages:
        lang = request.site.installation.languages[0]

    # text part
    article = get_markdown_content(request.site.installation.slug + '/index')[lang]

    trees_data = [
        {
            'pk': tree.pk,
            'latitude': tree.latitude,
            'longitude': tree.longitude,
            'name': _(tree.local_name)
        } for tree in request.site.installation.trees.all()
    ]

    return render(request, 'interface/index.html', {
        'article' : article,
        'trees_data': trees_data,

        'focus': get_focus(request.site.installation.focus),
        'poi_data': get_poi(request.site.installation),
        'label_data': get_map_labels(request.site.installation),
        'walk_data': get_walks(request.site.installation),
        'available_languages': available_languages
    })

# @login_required(login_url="/")
@xframe_options_sameorigin
def interface_about (request, qr_id, path=None, at=None):
    path = list(filter(lambda p: p != '', path.split('/'))) if path else []
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    print(path)
    article = get_markdown_content(os.path.join(request.site.installation.slug, 'about', *path))

    view_url_kwargs = { 'qr_id': qr_id }
    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')
    
    # print(article)
    # generating back menu
    

    if len(path) > 0:
        menu_back = [(_('about'), reverse('about', kwargs=view_url_kwargs))]
        
        for i in range(0, len(path)-1):
            menu_back.append(
                (_(path[i]), reverse('about', kwargs={ **view_url_kwargs, 'path': '/'.join(path[:i+1]) }))
            )
    else:
        menu_back = False
    
    return render(request, 'interface/about.html', 
                {
                   'article': article[get_supported_language_variant(get_language())],
                   'title': 'Arnhemse Bomen Vertellen',
                   'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
                   'menu_back': menu_back
                })

@login_required(login_url="/")
def interface_print (request):
    directory = 'about'
    articles = get_all_markdown_content(directory)
    
    return render(request, 'interface/print.html', 
                {
                   'articles': articles[get_supported_language_variant(get_language())],
                   'title': 'Arnhemse Bomen Vertellen',
                   'lang': get_supported_language_variant(get_language()),
                })

# @login_required(login_url="/")
@xframe_options_sameorigin
def interface_guide(request, qr_id, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    views = request.site.installation.views
    available_languages = request.site.installation.languages_as_tuples

    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')

    def treat_dialogue_line(line):
        if type(line) is list or type(line) is tuple:
            if type(line[0]) is str and line[0].startswith('group-'):
                # This is a group.
                return list(filter(lambda l: l is not None, map(treat_dialogue_line, line)))
            else:
                # This is not a group. But a tuple which means that it is a link to a view
                # Make sure the view is active in this installation
                if line[1] in views:
                    return (treat_dialogue_line(line[0]), get_view_url(line[1], **view_url_kwargs))
                else:
                    return None
        else:
            return line.format(tree=_(tree.local_name), installation=request.site.installation.name)
    
    lang = get_supported_language_variant(get_language())
    
    if lang not in request.site.installation.languages:
        lang = request.site.installation.languages[0]

    filled_dialogue = filter(lambda l: l is not None, map(treat_dialogue_line, dialogue[request.site.installation.slug][lang]))
    return render(request, "interface/guide.html",{
        'id': 'guide',
        'tree': tree,
        'at': at.strftime('%d-%m-%Y %H:%M') if at else None,
        'dialogue': filled_dialogue,
        'available_languages': available_languages
    })

def time2angle(now, time):
    difference = now - time
    ratio = difference.total_seconds() / (24 * 3600)
    return str(ratio * -360) + 'deg'

@xframe_options_exempt
# @login_required(login_url="/")
def interface_clock(request, qr_id, day_from_now=0, date=None, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    kiosk = request.GET.get('kiosk', '') == '1'

    if at:
        now = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    elif request.user.is_authenticated and date:
        now = datetime.datetime.strptime(date, '%d-%m-%Y')+datetime.timedelta(hours=24)
    else:
        # --- CHOOSEN TIME WINDOWS
        now = datetime.datetime.now()
        now = now - datetime.timedelta(days = day_from_now)
    yesterday = now - datetime.timedelta(days=1)
    longtimeago = now - datetime.timedelta(days = 120)

    tree = generate_clock(tree, now, yesterday, longtimeago)
    # graph_link = '/multigraph/sensor/' + ','.join(used_sensors) + '/1'
    sunrise = now.replace(hour=6, minute=00)  #('05:55', '%H:%M')
    sunset = now.replace(hour=22, minute=00)  #('21:23', '%H:%M')
    times = {
        'now' : [now.time(), '0deg'],
        'sunrise' : [sunrise.time(), time2angle(now, sunrise)],
        'sunset' : [sunset.time(), time2angle(now, sunset)],
    }

    return render(request, "interface/clock.html", {
        'id': 'portrait',
        'tree': tree,
        'times': times,
        'title': _(tree.local_name),
        'kiosk': kiosk
        # 'graph_link': graph_link
    })

@xframe_options_exempt
# @login_required(login_url="/")
def interface_portrait(request, qr_id, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    kiosk = request.GET.get('kiosk', '') == '1'
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None

    lang = get_supported_language_variant(get_language())
    subtitle_gen = {
        'nl': generate_update_subtitle_for_tree_nl,
        'en': generate_update_subtitle_for_tree_en,
        'de': generate_update_subtitle_for_tree_de,
    }
    subtitles = ''
    try:
        subtitles = subtitle_gen[lang](tree, request.site.installation.trees.all())
    except:
        pass
        
    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')

    return render(request, "interface/portrait.html", {
            'id': 'portrait',
            'tree': tree,
            'at': at.strftime('%d-%m-%Y %H:%M') if at else None,
            'title': _(tree.local_name),
            'menu_tree': menu_insert_urls(menu_tree(request.site.installation.views), **view_url_kwargs),
            'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
            'subtitle': subtitles,
            'kiosk': kiosk,
            # 'subtitle': 'test'
        })

@xframe_options_exempt
# @login_required(login_url="/")
def interface_thumbnail(request, qr_id):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    tree_name = tree.local_name.lower().replace(' ', '_').replace('-', '_')
    thumbnail = '/static/img/molenplaats/' + tree_name + '.jpg'
        
    return render(request, "interface/thumbnail.html", {
            'id': 'portrait',
            'tree': tree,
            'title': _(tree.local_name),
            'thumbnail': thumbnail
        })

@xframe_options_exempt
# @login_required(login_url="/")
def interface_story(request, qr_id, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    story = get_stories(request.site.installation.slug, tree, get_supported_language_variant(get_language()), at=at)

    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')

    return render(request, "interface/story.html", {
        'id': 'story',
        'tree': tree,
        'at': at.strftime('%d-%m-%Y %H:%M') if at else None,
        'title': _(tree.local_name),
        'menu_tree': menu_insert_urls(menu_tree(request.site.installation.views), **view_url_kwargs),
        'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
        'story': story,
    })

def get_sunlight_clock (date_from, date_to):
    sensor = Sensor.objects.filter(type='sunlight').first()
    
    return {
        'key': sensor.key,
        'pk': sensor.pk,
        'brand': sensor.device.deviceType.brand,
        'type': sensor.type,
        'unit': sensor.unit,
        'serie': sensor.get_observations(date_from, date_to),
        'classes': 'ambient light'
    }

@xframe_options_exempt
# @login_required(login_url="/")
def interface_data(request, qr_id, days=1, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    kiosk = request.GET.get('kiosk', '') == '1'
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    date_to = datetime.datetime.now() if at is None else at
    max_delta = 365 if request.user.is_authenticated else 14

    if days == 0:
        delta = datetime.timedelta(hours=3)
    elif days == 1:
        delta = datetime.timedelta(hours=24)
    else: 
        delta = datetime.timedelta(days=min(max_delta, max(1, days)))
    date_from = date_to-delta
    sensors = tree.get_clock(date_from, date_to)

    if request.site.installation.slug == 'arnhem':
        sensors['sunlight'] = get_sunlight_clock(date_from, date_to)
    
    ticks = []
    if days == 0:
        tick_interval = datetime.timedelta(minutes=30)
        candidate = datetime.datetime.combine(date_to.date(), datetime.time(hour=date_to.hour, minute=30), tzinfo=settings.TZ_INSTALLATION)
    elif days == 1:
        # 6 hourly tick
        tick_interval = datetime.timedelta(hours=3)
        candidate = datetime.datetime.combine(date_to.date(), datetime.time(hour=18, minute=0), tzinfo=settings.TZ_INSTALLATION)
    elif days <= 3:
        # 12 hourly tick
        tick_interval = datetime.timedelta(hours=12)
        candidate = datetime.datetime.combine(date_to.date(), datetime.time(hour=12, minute=0), tzinfo=settings.TZ_INSTALLATION)
    else:
        tick_interval = datetime.timedelta(days=1)
        candidate = datetime.datetime.combine(date_to.date(), datetime.time(hour=0, minute=0), tzinfo=settings.TZ_INSTALLATION)
    
    while candidate > date_from.astimezone(settings.TZ_INSTALLATION):
        if candidate < date_to.astimezone(settings.TZ_INSTALLATION):
            ticks.append(candidate.strftime('%d-%m-%Y %H:%M:%S'))
        candidate -= tick_interval

    # Reverse ticks
    ticks = ticks[::-1]

    for sensor in sensors:
        # Convert entries into dicts
        # Convert to timezone of installation
        sensors[sensor]['serie'] = [{ 'timestamp': o.timestamp.astimezone(settings.TZ_INSTALLATION).strftime('%d-%m-%Y %H:%M:%S'), 'value': o.value } for o in sensors[sensor]['serie']][::-1]

    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')

    return render(request, "interface/data.html", {
        'id': 'data',
        'at': at.strftime('%d-%m-%Y %H:%M') if at else None,
        'tree': tree,
        'days': days,
        'ticks': ticks,
        'graph_extent': (date_from.strftime('%d-%m-%Y %H:%M:%S'), (date_from + delta * 1.05).strftime('%d-%m-%Y %H:%M:%S')),
        'sensor_data': list(sensors.values()),
        'title': _(tree.local_name),
        'menu_tree': menu_insert_urls(menu_tree(request.site.installation.views), **view_url_kwargs),
        'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
        'kiosk': kiosk
    })


# @login_required(login_url="/")
def interface_map(request, qr_id, interpolated_sensor_type=None, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    # Strictly speaking this operation is useless. But it's a form of data validation?
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None

    if interpolated_sensor_type not in settings.INTERPOLATED_SENSOR_TYPES:
        interpolated_sensor_type = settings.INTERPOLATED_SENSOR_TYPES[0]

    trees = list(request.site.installation.trees.all())
    trees_data = [
        {
            'pk': tree.pk,
            'latitude': tree.latitude,
            'longitude': tree.longitude,
            'name': _(tree.local_name)
        } for tree in trees
    ]

    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')


    return render(request, "interface/map.html", {
        'id': 'map',
        'tree': tree,
        'local_name': _(tree.local_name),
        'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
        'focus': get_focus(tree),
        'trees_data': trees_data,

        'poi_data': get_poi(request.site.installation),
        'label_data': get_map_labels(request.site.installation),
        'walk_data': get_walks(request.site.installation),

        # 'park_data': get_park(),
        # 'water_data': get_water(trees),
        # 'ways_data': get_ways(trees),

        'interpolated_sensor_types': settings.INTERPOLATED_SENSOR_TYPES
        # 'interpolated_data': interpolated_data,
    })

@xframe_options_exempt
# @login_required(login_url="/")
def interface_soundscape (request, qr_id, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    kiosk = request.GET.get('kiosk', '') == '1'

    SOUND_FILES = discover_sound_files(settings.SOUND_FILE_FOLDERS)
    now = at if at else datetime.datetime.now(tz=datetime.timezone.utc)
    trees = {}
    sensors = {}
    last_tree_observation = None

    for sensor in tree.sensors:
        observation_data = sensor.get_last_observation(now)
        observation = {
            'timestamp': int(observation_data.timestamp.timestamp()),
            'value': observation_data.value
        }

        if last_tree_observation is None or last_tree_observation['timestamp'] < observation['timestamp']:
            last_tree_observation = observation

        sensors[sensor.pk] = {
            'pk': sensor.pk,
            'name': _(sensor.type),
            'location': (tree.latitude, tree.longitude),
            'type': sensor.type.lower(),
            'observation': observation
        }

    trees[tree.pk] = {
        'pk': tree.pk,
        'name': tree.local_name,
        'location': (tree.latitude, tree.longitude)
    }

    view_url_kwargs = { 'qr_id': tree.qr_id }

    if at:
        view_url_kwargs['at'] = at.strftime('%d-%m-%Y %H:%M')

    return render(
        request,
        "interface/soundscape.html", {
            'soundscape': {
                'servertime': int(now.timestamp()),
                'window': settings.SOUNDSCAPE_SENSOR_OBSERVATION_WINDOW * 60,
                'api_url': reverse('soundscape-latest-observations', kwargs=view_url_kwargs),
                'tree': {
                    'pk': tree.pk,
                    'name': _(tree.local_name),
                    'location': (tree.latitude, tree.longitude),
                    'observation': last_tree_observation
                },
                'trees': trees,
                'sensors': sensors,
                'soundfiles': SOUND_FILES
            },
            'tree': tree,
            'local_name': _(tree.local_name),
            'menu_tree': menu_insert_urls(menu_tree(request.site.installation.views), **view_url_kwargs),
            'menu_base': menu_insert_urls(menu_base(), **view_url_kwargs),
            'kiosk' : kiosk,
            'title': _(tree.local_name)
        })


def interface_soundscape_latest_observations (request, qr_id, at=None):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    at = datetime.datetime.strptime(at, '%d-%m-%Y %H:%M').replace(tzinfo=settings.TZ_INSTALLATION) if at else None
    sensors = {}
    last_tree_observation = None

    for sensor in tree.sensors:
        observation_data = sensor.get_last_observation(at)
        observation = {
            'timestamp': int(observation_data.timestamp.timestamp()),
            'value': observation_data.value
        }

        if last_tree_observation is None or last_tree_observation['timestamp'] < observation['timestamp']:
            last_tree_observation = observation

        sensors[sensor.pk] = observation

    return JsonResponse({
        'tree': last_tree_observation,
        'sensors': sensors
    })


def interface_archive (request, qr_id):
    tree = get_object_or_404(Tree, qr_id=qr_id, installations=request.site.installation)
    return render(request, "interface/archive.html", { 'qr_id': tree.qr_id })

#   ARTWORK VIEWS
#   ====================================================

def compass (request):
    trees = []

    for tree in request.site.installation.trees.all():
        sensors = tree.sensors.all()

        for sensor in sensors:
            if sensor.type == 'moisture':
                moisture = sensor.get_last_observation().value

            if sensor.type == 'co2':
                co2 = sensor.get_last_observation().value

        trees.append({
            'name': str(tree),
            'latitude': tree.latitude,
            'longitude': tree.longitude,
            'moisture': moisture,
            'co2': co2
        })

    return render(request, 'compass.html', { 'trees': json.dumps(trees) })


from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
import numpy as np


def interpolated_compass (request):
    trees = []

    for tree in request.site.installation.trees.all():
        sensors = tree.sensors.all()

        for sensor in sensors:
            if sensor.type == 'moisture':
                last_observation = sensor.get_last_observation()

                if last_observation:
                    moisture = last_observation.value
                else:
                    moisture = None
            if sensor.type == 'co2':
                last_observation = sensor.get_last_observation()

                if last_observation:
                    co2 = last_observation.value
                else:
                    co2 = None
            if sensor.type == 'air temperature':
                last_observation = sensor.get_last_observation()

                if last_observation:
                    temperature = last_observation.value
                else:
                    temperature = None
            if sensor.type == 'air humidity':
                last_observation = sensor.get_last_observation()

                if last_observation:
                    humidity = last_observation.value
                else:
                    humidity = None

        trees.append({
            'name': str(tree),
            'latitude': tree.latitude,
            'longitude': tree.longitude,
            'moisture': moisture,
            'co2': co2,
            'temperature': temperature,
            'humidity': humidity
        })

    all_tree_longitudes = list(map(lambda tree: tree['longitude'], trees))
    all_tree_latitudes = list(map(lambda tree: tree['latitude'], trees))

    X = np.linspace(min(all_tree_longitudes), max(all_tree_longitudes), 100)
    Y = np.linspace(min(all_tree_latitudes), max(all_tree_latitudes), 100)
    X, Y = np.meshgrid(X, Y)  # 2D grid for interpolation

    interpolated_point_sets = []

    for sensorType in ['moisture', 'co2', 'humidity', 'temperature']:
        trees_with_observations = list(filter(lambda t: t[sensorType] is not None, trees))
        x = list(map(lambda tree: tree['longitude'], trees_with_observations))
        y = list(map(lambda tree: tree['latitude'], trees_with_observations))
        z = list(map(lambda tree: tree[sensorType], trees_with_observations))
        # interp = LinearNDInterpolator(list(zip(x, y)), z)

        Z = CloughTocher2DInterpolator(list(zip(x, y)), z)(X, Y).flatten()

        # Add interpolated ponts to the interpolated sets
        # Remove NaN values
        interpolated_point_sets.append(list(map(lambda z: False if np.isnan(z) else z, Z)))

    interpolated_points = list(map(lambda r: False if r[2] is False else r, zip(X.flatten(), Y.flatten(), *interpolated_point_sets)))

    return render(request, 'interpolated-compass.html', { 'trees': json.dumps(trees), 'interpolated_points': json.dumps(interpolated_points) })


#   BACKEND / TESTING VIEWS
#   ====================================================

def overview(request):

    trees = request.site.installation.trees.all()
    available_languages = request.site.installation.languages_as_tuples
    now = datetime.datetime.now()
    dates = [now - datetime.timedelta(days = day_from_now) for day_from_now in range(3)]

    lang = get_supported_language_variant(get_language())
    
    if lang not in request.site.installation.languages:
        lang = request.site.installation.languages[0]

    return render(request, "interface/overview.html",
                  {'trees': trees, 'dates': dates, 'available_languages': available_languages})

def map_view(request):

    trees = list(request.site.installation.trees.all())
    trees_data = [
        {
            'pk': tree.pk,
            'latitude': tree.latitude,
            'longitude': tree.longitude,
            'name': _(tree.local_name)
        } for tree in trees
    ]

    return render(request, "map.html", {
        'trees_data': trees_data,
        'poi_data': get_poi(request.site.installation),
        'park_data': get_park(request.site.installation),
        'water_data': get_water(trees),
        'walk_data': get_walks(request.site.installation),
        'ways_data': get_ways(trees),
        'label_data': get_map_labels(request.site.installation),
    })

@csrf_exempt
def api_emsbrno(request):

    body_unicode = request.body.decode('utf-8')
    body_data = json.loads(body_unicode)

    for amount, d in body_data["data"].items():
        url = d['url']
        print(amount, url)

    if not os.path.exists(settings.EMS_DATA_DIR):
        os.makedirs(settings.EMS_DATA_DIR)

    now = datetime.datetime.now()
    filename = re.sub('[^A-Za-z0-9]', '', body_data['locName']) + " - " + str(int(now.timestamp())) + '.txt'
    with open(os.path.join(settings.EMS_DATA_DIR, filename), 'a') as f:
        json.dump(body_data, f, ensure_ascii = False)

    filename = re.sub('[^A-Za-z0-9]', '', body_data['locName']) + " - " + str(int(now.timestamp())) + ' - (raw).txt'
    with open(os.path.join(settings.EMS_DATA_DIR, filename), 'a') as f:
        f.write(body_unicode)

    response = HttpResponse("ok")
    response.status_code = 200
    return response


@login_required()
def graph (request, sensor_pk, days=7):
    sensor = get_object_or_404(Sensor, pk=sensor_pk)
    today = datetime.date.today()
    delta = datetime.timedelta(days=min(30,max(1, days)))
    data = [{ 'timestamp': o.timestamp.strftime('%d-%m-%Y %H:%M:%S'), 'value': o.value } for o in sensor.observations.filter(timestamp__date__gte=today-delta)]
    data.reverse()
    return render(request, 'graph.html', { 'sensor': sensor, 'data': json.dumps(data, ensure_ascii=False) })


from situated_portrait.emsbrno.find_daily_dendrometer_delta import select_min_max_per_day


@login_required()
def dendrometer_growth (request, days=7):
    sensor_pks = [74, 77, 80, 83, 86]
    today = datetime.date.today()
    delta = datetime.timedelta(days=min(30,max(1, days)))
    since = today-delta
    sensors = []
    sensor_json = []

    for sensor_pk in sensor_pks:
        try:
            sensor = Sensor.objects.select_related('device__deviceType').get(pk=sensor_pk)
            sensors.append(sensor)
            sensor_min_max = {'min': -1.5, 'max': 1.5}

            data = [ { 'timestamp': '{} 12:00:00'.format(r['date'].strftime('%d-%m-%Y')), 'value': r['growth'] } for r in select_min_max_per_day(sensor_pk, since)]
            # data.reverse()
            sensor_json.append({
                'sensor': {
                    'name': str(sensor),
                    'pk': sensor.pk,
                    'unit': sensor.unit
                },
                'data': data,
                'type': sensor.device.deviceType.pk,
                'range': sensor_min_max
            })

        except Sensor.DoesNotExist:
            pass

    return render(request, 'multigraph.html', { 'sensors': sensors, 'sensor_json': json.dumps(sensor_json, ensure_ascii=False) })


@login_required()
def multigraph (request, sensor_pk_string, days=7):
    sensor_pks = list(map(int, sensor_pk_string.split(',')))
    today = datetime.date.today()
    delta = datetime.timedelta(days=min(30,max(1, days)))
    sensors = []
    sensor_json = []
    # data = []
    min_max = {}

    # Observations.objects.aggregate(Max("value"), Min("value"))

    # Retreive max and min value for observations within requested period
    # grouped by the device type and unit of the sensor.
    # This allows to define the range on the y-axes
    q = Observation.objects.filter(
            sensor__pk__in=sensor_pks,
            timestamp__date__gte=today-delta
        ).values(
            "sensor__unit", "sensor__device__deviceType__pk"
        ).annotate(max=Max("value"), min=Min("value"))

    for row in q:
        devicetype_pk = row['sensor__device__deviceType__pk']
        unit = row['sensor__unit']
        min_max['{}__{}'.format(devicetype_pk, unit)] = {
            'min': row['min'],
            'max': row['max']
        }

    for sensor_pk in sensor_pks:
        try:
            sensor = Sensor.objects.select_related('device__deviceType').get(pk=sensor_pk)
            sensors.append(sensor)
            try:
                sensor_min_max = min_max['{}__{}'.format(sensor.device.deviceType.pk, sensor.unit)]
            except KeyError:
                sensor_min_max = [0,100]

            data = [{ 'timestamp': o.timestamp.strftime('%d-%m-%Y %H:%M:%S'), 'value': o.value } for o in sensor.observations.filter(timestamp__date__gte=today-delta).order_by('timestamp')]
            # data.reverse()
            sensor_json.append({
                'sensor': {
                    'name': str(sensor),
                    'pk': sensor.pk,
                    'unit': sensor.unit
                },
                'data': data,
                'type': sensor.device.deviceType.pk,
                'range': sensor_min_max
            })

        except Sensor.DoesNotExist:
            pass

    return render(request, 'multigraph.html', { 'sensors': sensors, 'sensor_json': json.dumps(sensor_json, ensure_ascii=False) })

# View with data view for all trees
@login_required()
def data_dashboard (request):
    installations = Installation.objects.all()

    return render(request, 'data-dashboard.html', { 'installations': installations })