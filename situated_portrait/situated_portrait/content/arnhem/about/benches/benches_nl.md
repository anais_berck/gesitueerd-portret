# Bankjes en meubel

Floris van Hintum creëerde de bankjes in het park en het meubel in Molenplaats Sonsbeek met lokaal hout. Zo hebben ook deze voorwerpen hun eigen bomenverhaal.

## Bankjes: Iep & Robinia

![](/static/img/foto_bankje.jpg)

De stammetjes waarop de bankjes rusten zijn afkomstig van de Robinia's die geveld werden in de Parkstraat in het Spijkerkwartier. De meeste bomen waren ziek. Het riool wordt vervangen en de straat wordt opnieuw ingericht.

De planken zijn afkomstig van Iepen, die aangetast waren door de iepziekte. Het gevolg is de verwelking van takken en de dood van de boom. De ziekte wordt veroorzaakt door schimmels die in houtvaten van de boom groeien. De schimmel kan van het ene houtvat in het andere komen. Daardoor raken er zoveel houtvaten verstopt, dat de boom afsterft. Deze bomen werden 'geschild', wat een manier is om  de verspreiding van de iepziekte te voorkomen.

Wanneer dit project op 15 september afloopt, krijgen de bankjes nieuwe bestemmingen in publieke parken van Arnhem. Heb je interesse om een bankje in je buurt te plaatsen, neem dan contact op met anais@anaisberck.be.

## Meubel: Linde

De stam van het meubel in Molenplaats Sonsbeek, waarop een touchscreen is gemonteerd, is afkomstig van een Linde die leefde op Onderlangs, tegenover de kunstacademie Artez. Het is fijn dat een groot stuk van de boom een artistieke herbestemming krijgt.
