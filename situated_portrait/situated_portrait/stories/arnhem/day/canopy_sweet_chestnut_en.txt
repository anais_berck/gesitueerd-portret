We are light beings. We feed on light. 
Our cells, our wood, our fruits are made with sunlight.
Whenever you eat something vegetable, know that you are feeding on light. 
Let yourself be enlightened!

Every day, my leaves catch impressions from your peers. 'Those trees are obstacles’, says someone as a joke. 'How beautiful they are’, says another. Then they get quiet for a moment. Then invariably comes the question, 'How old are they, you think?' Many passers-by have read something about us. 'They grow to more than 350 years old’, they say, and they seem to think it's usual.
Can you imagine the changes we have seen in 370 years?

An old grump like me has skin that has split and grooved over the centuries.
My thick grooved bark is full of holes, hooks and cracks.
Spiders love that: they have a great choice to attach the points of their web.
I therefore house - not without some pride - a large number of webs.
Have you counted them?

Great Tits, Blue Tits, Blackbirds, Starlings, Finches, Wrens,
they all find their way to my branches.
They come here to rest, night after night.
Some also breed here.
Those hollows - where there were once branches - are the perfect hiding places.

Together with my comrades, we are the oldest Sweet Chestnuts in the Netherlands. We are erratic, and have many cavities.  
That is why we are popular with animals and
birds that like to come here to rest. 
I can say with some honour 
that I am one of the most attractive resting places of the Tawny Owls living here in the forest.
You can meet them here in the evening, when they come to sleep.
Occasionally, they also nest here.
Pine Martens also make eager use of the cavities. 
I feel like a giant guesthouse:
each of my cavities is used by different animals.

Despite my advanced age
I still make sweet chestnuts every year. 
Squirrels, Jays, Crows, Mice
all find my fruits delicious and nutritious.
You humans pick them up sometimes too.
Last autumn, a human told his young that he finds them delectable
roasted, and even raw.

The Spiders that weave their webs among my leaves and in my cavities are a treat for the Wrens. These birds make their nests in the low bushes nearby. You will barely get to see them, so small are they. But their song is remarkably loud, clear, with sharp vibrating trills. Once you have heard it, you will never forget it. They are also numerous. They can breed up to three times a year. 

Do you know that I shelter a Butterfly that feels so comfortable here that it lays about 400 eggs a week? The caterpillars of the Chestnut Leafroller live in my leaves, which they conveniently roll up or fold together. They feed on my foliage and buds. Fortunately, I have more than enough of these.

Under my leaves lives the Chestnut weevil. He lays eggs in what's left of my heartwood. They are one of the reasons I have become so hollow inside. His larvae feed on this soft, full wood. In autumn, they hatch and overwinter in the soil near my roots. In spring, I see them reappear, large and mature. There then follows the great migration back to my crown.

Finches are one of my faithful residents, year after year. In their feathers, they carry scents and images of other trees and animals in the far South. It is always a pleasure to hear their song again during spring and summer. They always sing the same intense and melodious tune, sometimes as many as ten times a minute.
