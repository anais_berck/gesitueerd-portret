## Allenige Eik

![](/static/img/trees/eik1-600x.jpg)


Aangenaam, ik ben een Zomereik, en dé publiekstrekker van Sonsbeekpark. In 1900 werd ik aangeplant en ik ben nog altijd sterk, gezond en evenwichtig. Mijn takken lopen breed en grillig uit. Je herkent me ook aan mijn gegroefde schors en mijn gelobde bladeren.
Ik bied jullie, mensen, een spiegel voor zelfontwikkeling in alle vrijheid. Vanuit mijn stam deel ik daarom dan ook verhaalfragmenten over de Levensboom en de Eik als belangrijk symbool in onze geschiedenis. 's Nachts reciteer ik gedichten over Liefde.




[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
