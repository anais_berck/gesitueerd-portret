## Forest Oak

![](/static/img/trees/european_oak-600x.jpg)

Dear visitor do you see me well? I stand a little in the back, beyond the newly planted trees. I'm more than 200 years old, like all the other big Oaks in this small forest. Don't confuse us, Oaks, with Beeches. The latter have a grey smooth bark. And our crowns have a far more sprawling and artistic pattern than the Beech's.
I am an adult tree and live very quietly. Together with my peers, we observe and reflect on the effects of climate change. The stories I share from my trunk are stories about the future. At night, I recite poems about Silence.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
