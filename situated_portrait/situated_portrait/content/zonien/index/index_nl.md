## Ontmoet een bijzondere Beuk

Van 3 tot 29 september 2024 kan je een bijzondere Beuk in Het Zoniënwoud op een unieke manier ervaren. Welke verhalen heeft een boomgriffier opgevangen? Groeit hij elke dag? Wat normaal voor ons verborgen blijft valt nu te ontdekken. Deze installatie is een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes.

### Bewegende boom
Bomen, we lopen er doorgaans gewoon voorbij. We vinden ze normaal en als we ze al opmerken, zien ze er hetzelfde uit als gisteren, als morgen en overmorgen. Onze huizen zijn gemaakt met hout. Bomen geven ons ook medicijnen, eten, warmte, zuurstof en we zoeken hun schaduw op tijdens hete zomers.

Tijdens deze installatie sta je stil bij bomen, zoals bij een kunstwerk. Aan de hand van sensoren die in een Beuk zijn geïnstalleerd, maakt deze installatie zichtbaar wat anders verborgen blijft voor het menselijke oog: het bewegen van de stam, de CO2- en wateraanvoer, de temperatuur in de lucht en bij de wortels.

### Altijd anders
Van uur tot uur zie je veranderingen in de stam van de boom, waardoor die boom gaat leven. De installatie nodigt je dan ook uit om deze bijzondere boom te waarderen als een actief, groeiend, veranderend levend wezens, waarmee je ook een relatie kan aangaan.

### QR-code
Bij de beuk staat een picnic-bank met daarop een QR-code. Als je die scant, word je naar een webpagina geleid, waar verhaalfragmenten en artistieke beelden je inzicht geven in het leven en de geschiedenis van de boom. Wat je te zien krijgt, hangt af van de data die de sensoren verzenden. Laat je verrassen, elk uur, elke dag.


## Praktisch
Je vndt de Beuk in het Zoniënwoud tegenover de picnic-bank aan de Graafdreef, net voorbij het kruispunt van de Tumuliweg, de Sint-Hubertusdreef en de Groene Spechtweg: [locatie](https://www.openstreetmap.org/#map=18/50.77950/4.41103)


## Credits

<small markdown>

Dit project wordt gerealiseerd door [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes:

- kunstenaars die meewerken aan dit project: An Mertens (concept, coördinatie, boomgriffier), Gijs de Heij & Doriane Timmermans (code, ontwerp & visualisaties - [Open Source Publishing](https://osp.kitchen/))
- boom die meewerkt aan dit project: beuk (Fagus sylvatica)
- algoritmes die meewerken aan dit project: EMS Brno banddendrometer, Sensoterra bodemvochtigheids- en temperatuursensor, Milesight CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programmeertaal, html, css.

Gesitueerd Portret is mogelijk dankzij de financiële steun van de Vlaamse Overheid, Kunsten & Erfgoed. Dit toonmoment kreeg de logistieke steun van Leefmilieu Brussel en het team van GC Wabo in Bosvoorde.

Gesitueerd Portret Zoniën, door Anaïs Berck, Arnhem, augustus 2024.
Alle code, afbeeldingen, geluiden en teksten zijn gepubliceerd onder een Creative Commons Attribution 4.0-licentie (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). Je kan dit werk op elk medium hergebruiken, verspreiden, reproduceren en aanpassen, maar niet voor commerciële doeleinden, en op voorwaarde dat je de auteur vermeldt, evenals een link naar de Creative Commons-licentie en vermelding van de wijzigingen die je maakte.

Alle code, afbeeldingen, geluiden en teksten zijn ook gepubliceerd onder een CC4r-licentie, die hier beschikbaar is: <https://constantvzw.org/wefts/cc4r.en.html>.

De gegevens die tijdens dit werk werden gegenereerd, zijn gepubliceerd onder een Creative Commons 1.0-licentie (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). Je kan deze data kopiëren, wijzigen, en opnieuw verspreiden, zelfs voor commerciële doeleinden, zonder toestemming te vragen.
</small>
