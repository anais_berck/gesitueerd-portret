We are light beings. We feed on light. 
Our cells, our wood, our fruits are made with sunlight.
Whenever you eat something vegetable, know that you are feeding on light. 
Let yourself be enlightened!

I have many residents and loyal visitors.
I am like the New York of the Netherlands, the tree in which the greatest number of different species of birds, insects, fungi, mosses, animals find shelter and food. If you were to imagine my roots, trunk and crown into human images, you would see one skyscraper next to another.
A newcomer like the Plane Tree has hardly any visitors or residents.
But an ancient species like me, which has thrived here for more than ten thousand years, is intertwined with everything that lives, down to the smallest fibres. 

The Jays are good friends of mine. You might find one of their blue-black striped feathers while visiting the park.
You humans have given him the name of the 'constantly scratching acorn seeker'.
As a result, he is very noisy, but that is mainly in autumn, when he hides my acorns in the ground for winter.
With luck, he will forget to dig them up again. So my little offspring can grow up in the most unexpected places.

This spring, I again had the pleasure of housing a nest of crows high up in my branches.
Twig by twig they lined and wove into a platform, which they further fortified with earth and thicker branches.
Year after year, they also surprise me with the materials they find to line their nests. They used to pluck pieces of wool from the fleece of the sheep that lived further away. Now they find wool, wire and paper worked by you. And, of course, a lot of feathers and grass, which spreads a special perfume so high up in my crown.

My fruits were once used to fatten pigs. They are highly nutritious and contain no less than a third of fat. ‘The best bacon grows on Oaks’, is an old Dutch saying which I keep faithfully in my cellular memory. Thanks to the pigs, we also enjoyed some privileges: we were not allowed to be felled, and we Pedunculate Oaks, who produce more and bigger acorns than the Sessile Oak, were planted more widely.

Every summer we get a visit from a couple of the Hobbies. They love the trees here around the deer park. They breed a bit later, taking advantage of the large quantities of dragonflies in this park. With these, they feed their young. When the peak of dragonflies is over by late August, the parents chase their young out of the territory. And then they have to find their own food. Some hunt Starlings, others Bats. If you come at dusk, you may well see a Hobby sitting on my branch, waiting for the Bats to appear. 

Squirrels are loyal friends of mine. No, I am not naive: I know that it is in part because they love my fruits. Unfortunately, they can't just pluck them from my crown. My acorns contain too much prussic acid for that, which is quite poisonous to them. Only wild Boars tolerate it. 
Squirrels bury my acorns as food for the winter. And that prussic acid is useful for that; it helps preserve my acorns. Over time, the hydrogen cyanide goes away. I sometimes see a Squirrel in winter dig up one of my acorns, smell it and nibble off a piece. To make sure it is edible!

The beautiful Jays with their striped blue-black feathers love my acorns. Some go about this slyly. They hide high up in my crown and observe how the Squirrel below digs up his stash of acorns. The Jay may then make an alarm sound, causing the Squirrel to flee. After two or three times, the Squirrel is no longer fooled that way. But the Jay is a master imitator. With the call of a Buzzard or Hawk, he can still get the Squirrel's stash.

Everything is always changing, including my rhythm. I used to make a lot of acorns every five years. You humans call that our mast year. I chose the warmer years for that. And then I would grow so many acorns that all the animals could feast on and there would still be some left over. It has to be said, since global warming, it has been this warm almost every summer. Since then, I can't help but produce so much fruit almost every year. It costs me a lot of energy, but it takes even more energy and time to adapt to this new environment.

A newcomer to this park is the Hoopoe. He is easily recognized by his red-brown feathers and his long black-tipped crest, which he raises when excited. The hoopoe likes to linger in southern Europe, but with global warming, he is starting to like it here too. He likes to make his nest in the hollows of Oaks. Sometimes he even pecks an old Woodpecker's hole.  

