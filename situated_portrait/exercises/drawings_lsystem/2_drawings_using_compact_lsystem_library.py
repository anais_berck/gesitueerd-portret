from TurtLSystems import *

## source: https://pypi.org/project/TurtLSystems/

## default arguments
# 1. start - The initial string or axiom of the L-system. Level 0 - str
# 2. rules - Dictionary that maps characters to what they are replaced with in the L-system expansion step.
# May also be a string where whitespace separated pairs of substrings correspond to the character and its replacement.
# For example {'A': 'AB', 'B': 'B+A'} and 'A AB B B+A' represent the same rules - Dict[str, str] | str
# 3. level - The number of L-system expansion steps to take, i.e. how many times to apply rules to start - int
# 4. angle - The angle to turn by on + or -. In degrees by default but the circle arg can change that - float
# 5. length - The distance in pixels to move forward by on letters. The length step - float
# 6. thickness - The line width in pixels. May be any non-negative number - float
# 7. colour - The line color. A 0-255 rgb tuple or None to hide all lines. Reselected on 0 - Tuple[int, int, int] | None
# 8. fill_colour - The fill color for {} polygons, @ dots, and turtle shapes.
# A 0-255 rgb tuple or None to hide all fills. Reselected on 1 - Tuple[int, int, int] | None
# 9. background color of the window - A 0-255 rgb tuple or None to leave unchanged - Tuple[int, int, int] | None
# 10. asap - When True the draw will happen as fast as possible, ignoring speed arg and the delay arg of init - bool

## and there are a series of costumization arguments

draw('A', 'A B-A-B B A+B+A,', 5, 60, 7, 2, (200, 220, 255), None, (36, 8, 107),
     heading=60, red_increment=2, gif='example.gif', max_frames=250, duration=30)
