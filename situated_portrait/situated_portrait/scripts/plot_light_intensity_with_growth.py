from situated_portrait.models import Observation
import zoneinfo

import matplotlib.pyplot as plt
import matplotlib

def time_as_minutes (time):
    return time.hour * 60 + time.minute

month_color_normalizer = matplotlib.colors.Normalize(vmin=1, vmax=12)
month_colormap = matplotlib.colormaps.get('terrain')

tzinfo = zoneinfo.ZoneInfo("Europe/Amsterdam")
intensity_observations = {}
growth_observations = {}


print('Loading intensity observations')
for observation in Observation.objects.filter(sensor__pk=54).order_by('timestamp'):
    timestamp = observation.timestamp.astimezone(tzinfo)
    date = timestamp.date()
    time = timestamp.time()

    if date not in intensity_observations:
        intensity_observations[date] = [(time, observation.value)]
    else:
        intensity_observations[date].append((time, observation.value))


last_observation = None

print('Loading growth observations')
for observation in Observation.objects.filter(sensor__pk=74):
    timestamp = observation.timestamp.astimezone(tzinfo)
    date = timestamp.date()
    time = timestamp.time()
    delta = observation.value - last_observation if last_observation is not None else 0
    last_observation = observation.value

    if date not in growth_observations:
        growth_observations[date] = [(time, delta)]
    else:
        growth_observations[date].append((time, delta))

width = 300 / 25.4
height = 150 / 25.4

print('Setting up plots')
fig, axes = plt.subplots(1, 12, sharey=True, figsize=(width*12, height),layout='constrained')
# Define second axis per graph
# Make twin_axes for growth
growth_axes = []

xticks = [0, 4*60, 8 * 60, 12*60,16*60,20*60, 24*60]
xticklabels = ['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00']

growth_ax_xticks = [-0.04, -0.02, 0, 0.02, 0.04]
growth_ax_xticklabels = ['-0.04 mm', '-0.02 mm', '0 mm', '0.02 mm', '0.04 mm']

# Set limits: -.005 - 0.005
for ax in axes:
    growth_ax = ax.twinx()
    growth_ax.set_ylim(-.05, .05)
    growth_ax.set_xlim(0, 24*60)
    growth_ax.set(xticks=xticks, xticklabels=xticklabels, yticks=growth_ax_xticks, yticklabels=growth_ax_xticklabels)
    middle_line = growth_ax.plot([0, 24 * 60], [0,0])
    middle_line[0].set(color='red', linewidth=0.1, linestyle=(0, (1, 10)))
    growth_axes.append(growth_ax) 

# Calculate delta per observation
# Use beech

# fig.

# intensity_fig, intensity_ax = plt.subplots(figsize=(width, height))

for idx, month in enumerate(['January 2024', 'February 2024', 'March 2024', 'April 2024', 'May 2024', 'June 2024', 'July 2024', 'August 2024', 'September 2024', 'October 2024', 'November 2024', 'December 2024']):
    axes[idx].set_title(month)

print("Plotting light intensity")
for date, observations in intensity_observations.items():
    # Unzip observation into list of x coordinates: time of day
    # and y coordinates: intensity
    x, y = zip(*[(time_as_minutes(time), value) for time, value in observations])
    intensity_line = axes[date.month - 1].plot(x, y)
    
    # intensity_line[0].set(color=month_colormap(month_color_normalizer(date.month)), linewidth=.05)
    intensity_line[0].set(color='black', linewidth=.05)


print("Plotting growth")
for date, observations in growth_observations.items():
    x, y = zip(*[(time_as_minutes(time), value) for time, value in observations])
    growth_line = growth_axes[date.month - 1].plot(x, y)
    growth_line[0].set(color='red', linewidth=0.05)


# intensity_fig.savefig('test.pdf')
print('Saving file')
plt.savefig('light_intensity_with_growth.pdf')