I like future scenarios.
This version was entrusted to me by author Jibbe Willems.
It is yet another warning to you:
our patience breaks concrete,
crumbles the thickest walls,
overgrows the tallest buildings,
our shoots will break through stone layers,
tearing out street and pavement.
And the scum who thought themselves
better than nature,
we will overgrow and suffocate,
we will pull the breath from their lungs
and feast on their decomposed corpses.
And then
all that was once two-legged
will return like grass.
Our patience breaks concrete
and 
we will inherit the Earth. 

I am an Oak.
I was planted here 225 years ago, in 1799, along with all the other similar Oaks you see here.
The owners of the estate hoped to make a lot of money from us.
It turned out differently. We are still here and will outlive you here.

Because we Oaks live so close together here, we grew narrow and long in height, looking for the light.
We are as old as the Solitary Oak that lives in the Marsh Meadow.
Its trunk and branches are so much thicker than ours but count the same number of annual rings.
He grows there alone, we form a group.

I would call life in this part of the park peaceful.
Especially since the wooded banks have been constructed, it has become comfortable here again.
The pure water flowing down from the Veluwe
is held back a little as a result, seeping into the soil, between our roots.
Gratefully, we transport an average of 800 litres of water to our crown every day.

In the early 1980s, a teacher here read a piece from a future story.
After the great floods, storms and extinctions, a new kingdom emerged in the far north.
The leadership was in the hands of women.
The climate there was Mediterranean. 
And we trees were worshipped as gods.

I have lived here for many years. It is impossible for me to leave here.
We plants are sedentary. We cannot flee.
That makes us built differently from you, humans.
To have one heart in one well-defined place, or one lung, or one place for one set of brains,
that would very soon kill us. 
We wouldn’t be able to defend ourselves against an enemy who tears off a piece of our body.
We therefore grow thousands of sex organs, and as many noses as there are leaves, trunk and roots.
We always have a Plan B. 
We are modular. 
We are not one I, but thousands of Is at once.
We can say I with any particle of our body.

I have lived here for 200 years and am very grateful for that. It is my intention to spend as many more years as possible in this place. Meanwhile, things are changing. News has reached me through my roots that many Oaks are in trouble because of soil acidification. Their roots are shorter and they can’t always get the necessary water and mineral supplies. That observation prompted my cells to start looking as far into the future as we can in the past: 224 years.

Can you see all those saplings in front of me? They were planted recently. Quite a few Beech trees had to make way for them. You can still find stumps of their trunks in the ground. Beech trees are quite dominant among trees. They take away all the light. Their leaves are so tanned that they take a long time to decompose. Those young Lindens, Maples, Rowans and Cherry trees have leaves that digest quickly. They make the soil airier and richer in minerals. I therefore welcome these saplings with all my being.

The rain carries stories from distant lands. For instance, I learned that you humans believe that we trees will last forever. You need not worry about us. The opposite is true. As long as you continue to generate nitrogen through your agriculture, exhaust fumes and industry, we are going to die. Oaks on sandy soils are already suffering from this. The soil is acidifying and minerals are being released, but the rain washes them away to depths those Oaks can no longer reach. And so famine and mortality ensue. 

Preferably, I would love to live here 224 more years, once as many, so to speak. Only, chances are that Oaks like me will disappear from the landscape, as will Beech and Spruce. Climate change and pollution are playing tricks on us. All it takes is one Bark Beetle to appear, and we will all be gone. Many of you think other trees are bound to replace it, like the Sweet Chestnut.

There is a real chance that one of your descendants will be the last one left on Earth, without any other living creature. The situation could also turn around. In Hawaii, for example, I heard from a Firebug with distant descendants there that many native species have gone extinct. You humans have also introduced many new species there. Despite the fact that they come from very different regions, they seem to form relationships with each other, so a new ecosystem is forming. You humans have composed that, as it were, partly by accident, partly on purpose, but it could work in a way.

Flowerpot management, a man in a suit called the trend in the Netherlands. He leaned against my trunk and addressed his colleague seriously: 'What we need to move towards is rewilding. That means letting nature take its course as much as possible and focusing on natural processes. It can’t all be about intensively managing this small patch of nature and trying to maintain it in the state we humans want it to be in.’ I sent that human my warmest energy. 

Finches, Starlings, Meadow Pipits, White-fronted Geese, Lapwings, Barn Swallows, Cormorants, Skylarks, Song Thrushes, they all migrate away in October. For them, the Netherlands doesn’t exist. They live on Earth. They look wide. That is also something you could do more of. Instead of trying to preserve or restore landscapes and species that are becoming rare in the Netherlands while they live spontaneously in other places in Europe and the world, you can guide nature in its transformation. And you get the surprise in return.
