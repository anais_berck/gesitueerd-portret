from turtle import *
import tkinter as tk

# Turtle Graphics -- Code by Isabel Beach
# Source: https://github.com/ibeach/ibeach.github.io/blob/master/code/turtle/turtle.m
# different formulas: https://ibeach.github.io/turtle/

# Plot the turtle curve determined by the options below. The vocabulary is
#     'F': go forward.
#     '-': turn left by a fixed number of degrees.
#     '+':  turn right by a fixed number of degrees.
#     'X', 'Y': do nothing.
# Warning: This code (especially the plotting portion) may be extremely
# slow for sufficiently long curves.
# -------------------------------------------------------------------------
#
# Options -----------------------------------------------------------------
# itersMax: maximum number of times to apply the string replacement rule.
# wordMax: maximum allowed length of the string.
# turnAngle: angle in radians to turn left/right.
# heading: 2-by-1 vector describing initial direction of the turtle.
# word: string of initial word.
# rule: 1-by-3 cell containing the string replacement rule.
#        rule{1}: string to replace 'F' with.
#        rule{2}: string to replace 'X' with.
#        rule{3}: string to replace 'Y' with.
# lineColor: 1-by-3 RGB vector of the curve color.

# Code --------------------------------------------------------------------

#  Apply the string replacement rule to word with specific amount of iterations and maximum length of word
def getTurtleString(word, rule, iterMax, wordMax):
    # declare rules
    rule_F = rule[0] # replaces F
    rule_X = rule[1] # replaces X
    rule_Y = rule[2] # replaces Y
    # replace characters in word
    iteration = 0
    while iteration < iterMax and len(word) < wordMax:
        # replace by lowercase temporarily so we dont overwrite the outcomes of the rules
        word = word.replace('F', 'f')
        word = word.replace('Y', 'y')
        word = word.replace('X', 'x')
        # apply rules
        word = word.replace('x', rule_F)
        word = word.replace('y', rule_X)
        word = word.replace('f', rule_Y)
        iteration += 1
    return word

# ------

def getTurtleCurve(word, len_forward, angle_left, angle_right):
#     Return the (x, y)-coordinates of the curve determined by giving the
#     instructions in word to the turtle with initial heading headingInit
#     and turning angle turnAngle.
#     word: string of instructions for the curve.
#     headingInit: initial direction of the turtle.
#     turnAngle: angle in radians to turn left/right.
#     x: real vector of x-coordinates of curve.
#     y: real vector of y-coordinates of curve.
#

    for character in word:
        if character == 'F':
            # go forward
            turtle.forward(len_forward)
        elif character == '-':
            # turn left
            turtle. left(angle_left)
        elif character == '+':
            # turn right
            turtle.right(angle_right)
        elif character == 'X':
            pass
        elif character == 'Y':
            pass
        else:
            print("invalid character")


# actions

## declare turtle variables
# set background canvas to transparent
turtle = getturtle()
root = (turtle
    ._screen
    .getcanvas()
    .winfo_toplevel())
root.attributes('-alpha', 0.3)
# declare canvas - turtle screen
s = turtle.getscreen()
# declare pen / turtle : You can program the turtle to move around the screen.
# The turtle has certain changeable characteristics, like size, color, and speed.
# It always points in a specific direction, and will move in that direction unless you tell it otherwise:
#t = turtle.Turtle()
# change form Turtle
turtle.shape("circle")
# increase size of turtle to see colours more clearly
turtle.shapesize(0.1,0.1,0.1)
# set colour turtle
turtle.fillcolor("green")
# set colour pen
turtle.pencolor("red")
# set speed pen
turtle.speed(9)

# sends the turtle back to the point (0,0)
turtle.home()
# draw a line from your current position to the point (100,100) on the screen.
#t.goto(100,100)

# iterMax: number of time to apply the string replacement rule.
iterMax = 200
# wordMax: maximum allowed length of the string.
wordMax = 5000
# word: string of initial word.
word = 'XY'
# formula to obtain certain generated pattern, see https://ibeach.github.io/turtle/
rule =  ['FY', 'XY-', 'F']
#lineColor = [0, 0, 0]
len_forward = 5
angle_left = 90
angle_right = 90

#  Apply the string replacement rule to word with specific amount of iterations and maximum length of word
word = getTurtleString(word, rule, iterMax, wordMax)
getTurtleCurve(word, len_forward, angle_left, angle_right)
s.getcanvas().postscript(file="drawing.eps")
