## Boseik

![](/static/img/trees/european_oak-600x.jpg)

Beste bezoeker, zie je me goed? Ik sta een beetje achterin, voorbij de jonge aangeplante boompjes. Meer dan tweehonderd jaar geleden werd ik aangeplant, net als alle grote Eiken in dit stukje bos. Verwar ons, Eiken, niet met Beuken. Die laatste hebben een grijze gladde schors. En de kruin van een Eik groeit veel grilliger en artistieker dan die van de beuk.
Ik ben intussen volwassen en leef erg rustig. Samen met mijn soortgenoten observeren we de effecten van de klimaatverandering en reflecteren daarover. De verhalen die ik deel vanuit mijn stam zijn toekomstverhalen. 's Nachts reciteer ik gedichten over Stilte.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
