# Kunstenaars en alle mensen die betrokken werden bij dit project

## Anaïs Berck
[Anais Berck](https://www.anaisberck.be/) is een pseudoniem en staat voor de samenwerking tussen mensen, algoritmes en bomen. Als collectief openen ze een ruimte waarin menselijke intelligentie een plaats krijgt in gezelschap van plantaardige intelligentie en artificiële intelligentie.

Voor dit project maken volgende mensen deel uit van de samenwerking.

### An Mertens (Brussel, BE)
An is mediakunstenaar en bomengriffier. An bedacht en coördineerde dit project, verzorgde onderzoek, literaire code en redactie van teksten en verhalen.

### Gijs de Heij (Brussel, BE) en Doriane Timmermans (Brussel, BE)
Beiden zijn lid van het experimentele ontwerperscollectief [Open Source Publishing](http://osp.kitchen/) in Brussel. Ze combineren grafisch ontwerp met programmeren.
[Gijs de Heij](https://gijsdeheij.com/) studeerde grafisch ontwerp aan ArtEZ in Arnhem. Hij programmeert al sinds zijn 11e.
Doriane Timmermans behaalde een master in de Wiskunde aan de Université Libre de Bruxelles en studeerde daarna digitale kunst aan de academie erg in Brussel. Ze doet onderzoek naar het literaire potentieel van css: [Declarations](https://declarations.style/).

### Bram Goots (Antwerpen, BE)
Bram is fotograaf. Binnen Agence Future heeft Bram ervaring met het vastleggen van uiteenlopende contexten op uiteenlopende plekken in de wereld, en dit altijd met een zuiver gevoel voor licht, detail en compositie.

### Christina Clar (Austria)
[Christina](https://christinaclar.net/) is geluidskunstenaar. Ze heeft 20 jaar ervaring met opnames in de natuur en het creëren van multi-layered soundscapes. Christina woonde tot enkele jaren geleden in Brussel, waar ze actief was bij organisaties als QO2 en Operahuis De Munt.

### Floris van Hintum (Arnhem, NL)
Floris noemt zich graag een verbindende boom, bos-, natuur-, en landschapsbeheerder. Hij maakte de bankjes en het meubel in Molenplaats Sonsbeek van lokaal hout. Met [Landbeeld](http://www.landbeeld.nl/) combineert hij projecten waarbij natuur en kunst gecombineerd zijn. Hij maakt ook soundsystems als WILLOW Hifi.

## Boomverzorgers van de Gemeente Arnhem
De sensoren en bankjes werden geïnstalleerd met de zeer fijne hulp van boomverzorgers van de Gemeente Arnhem. Dank voor jullie hulp en geduld: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

## Wetenschappelijk advies
Tijdens verschillende gesprekken voor en tijdens het creatieproces kregen we zeer waardevol advies van boomringenonderzoekster dr. Ute Sass-Klaassen en haar collega Linar Akhmetzyanov (Hogeschool Van Hall Larenstein, Leeuwarden). Met dank!

## Kennis en ervaringen over bomen en parken
Tijdens het creatieproces van dit project had An de eer en het genoegen om tijd door te brengen en te luisteren naar de kennis en ervaringen van volgende personen: parkgids Eddy van Heeckeren, bosdeskundige Simon Klingen, ecoloog Hans Van den Bos, kunstenaar Boudewijn Corstiaensen, beheerders Jan Floor en Rob Borst, Senior bestuursadviseur Groene Leefomgeving Marcel Wenker, natuurgids en bioloog Pim van Oers, parkgids Gerard Herbers, kunstenaar Albert van der Weide, boswachter Nina Kreisler, auteur Jibbe Willems, stadsecoloog Ineke Wesseling, kunstenaar Bartaku Vandeput (Baroa belaobara). Met dank!

## Meer advies
We konden ook rekenen op het praktische en morele advies van Hélène van Ginderachter, ir. Erik Mertens, Joost Verhagen (Cobra-Groen in zicht), Judith Van Ginneken, Marleen Verschueren, Erik De Wilde. Eindredactie van de teksten werd verzorgd door Patrick Lennon en Karin Ulmer. Met dank!

## Financiering, productie, communicatie
Dit project kwam tot stand dankzij ondersteuning van Marko van der Wel (wethouder Natuur, Milieu, Zorg, Lokaal Voedsel, Water), parkbeheerder Rob Borst en Senior bestuursadviseur Groene Leefomgeving Marcel Wenker. Communicatie werd verzekerd door Sanne Blok, Hanneke Busscher en William Hulstein; productie door Karlijn Michielsen. Met dank!

## Logo

<!-- ![](/static/img/logos/arnhemse-bomen-vertellen-groen.png) -->

Het logo van ‘Arnhemse Bomen Vertellen’ gebruikt Computers Regular.

Dit lettertype werd ontworpen door [Marianne Noordzij](https://www.mariannenoordzij.nl), die als grafisch ontwerpster afstudeerde aan Artez en vervolgens AI studeerde aan de Universiteit van Utrecht. Met het lettertype maakt ze de verbinding tussen machines en meer-dan-menselijke wezens als ‘companion species’.

We vinden het een fijn toeval dat ze van Arnhem afkomstig is en houden van de manier waarop het lettertype organische en technologische elementen combineert. In dat opzicht reflecteert het de identiteit van dit project.
