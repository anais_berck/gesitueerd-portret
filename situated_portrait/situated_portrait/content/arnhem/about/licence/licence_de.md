# Lizenz

Arnhemse Bomen Vertellen, von Anaïs Berck, Arnhem, Juni 2024.

Sämtlicher Code, alle Bilder, Sounds und Texte werden unter einer Creative Commons Attribution 4.0-Lizenz (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>) veröffentlicht. Sie dürfen dieses Werk außer für kommerzielle Zwecke in jedem beliebigen Medium wiederverwenden, verteilen, reproduzieren und anpassen, vorausgesetzt, Sie geben den Urheber des Werks an, stellen einen Link zur Creative Commons-Lizenz bereit und geben an, ob Änderungen vorgenommen wurden.

Sämtlicher Code, alle Bilder, Sounds und Texte wurden auch unter einer CC4r-Lizenz veröffentlicht, die hier verfügbar ist: <https://constantvzw.org/wefts/cc4r.en.html>.

Die während dieser Arbeit erfassten Daten werden unter einer Creative Commons 1.0-Lizenz (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>) veröffentlicht. Sie können die Daten kopieren, ändern, verteilen und aufführen, auch für kommerzielle Zwecke, und das alles, ohne um Erlaubnis zu bitten.
