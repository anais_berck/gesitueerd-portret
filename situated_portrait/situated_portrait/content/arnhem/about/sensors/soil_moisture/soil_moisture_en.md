## Soil moisture sensor

![](/static/img/sensors/sensoterra-600x.jpg)

### Recognizable patterns

Soil type
:   Solitary Oak is rooted in the very humid soil of the Moerasweide (Swamp Meadow), where the water is collected from the Hoge Veluwe

Rain
:   the more rain falls, the wetter the soil. The continuous rainfall of last winter & spring have created exceptionally flat lines in the graphics over time.

Hot & dry
:   on a hot day the soil is less humid.

### Soil moisture & Trees

Trees need a lot of water - and minerals - to produce sugars through photosynthesis (see CO2 sensor). The water management system of the parks, co-determined by the lateral moraine and water purified by the ground layers from the Hoge Veluwe, ensures that the participating trees are well supplied with mineral-rich water. Moreover, the municipality is constructing wooded banks here and there to slow down the too rapid drainage of water into the Moerasweide and Sint-Jansbeek.

The trees transport water from the roots to the crown via the xylem. These are wood vessels just beneath the bark, which become dead wood after each season, forming the tree's annual rings. On a hot summer day, an Oak, for example, can transport up to 800 litres of water a day.

### Sensor type

Soil moisture is measured with sensors from the Dutch start-up Sensoterra. They measure the humidity of the soil at a depth of 30cm.

The sensors are placed on the trees' 'drip line'. That is the imagined line that runs along the extreme leaves of the crown. It is along the drip line that the most lively surface roots grow.
