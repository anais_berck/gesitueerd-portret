## Rode Beuk

![](/static/img/trees/beuken-600x.jpg)

Hier, bij het kasteel van Zypendaal, lijkt één reusachtige Beuk te leven, maar we zijn met vijf. Onze flinterdunne schors verdraagt geen zon. Daarom ritsen wij ons dicht met een jas van bladeren. Tussen ons in is het als in een tempel, rustig en koel. Ik ben de grote Rode Beuk links vooraan.
Het onderzoek naar Sporen van Slavernij dat onlangs werd gepubliceerd, maakt oude herinneringen wakker in mijn stam. Ik heb Anna van Vossenburg persoonlijk gekend, en deel die ervaringen vanuit mijn stam. 's Nachts geef ik je gedichten over Magie.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was een artistieke installatie van 15 juni tot 15 december 2024. De sensoren zijn intussen verdwenen, maar dit bankje nodigt nog altijd uit om een relatie aan te gaan met de boom voor je.
Het kunstproject was een creatie van [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes, in opdracht van de Gemeente Arnhem.
