## Sapstroomsensor

![](/static/img/sensors/sapflow.jpg)

### Herkenbaar patroon

Zonlicht & luchtvochtigheid
:   de sapstroommetingen maken duidelijk hoe gevoelig bomen zijn voor hun omgeving. Een wolk, een regenbui, een zonnige periode, ze hebben onmiddellijk invloed op de sapstroom. Zo zie je dat die veel sneller stroomt op warme, zonnige dagen; en trager op kille, regenachtige dagen.

Dit heeft alles te maken met de fotosynthese: hoe meer zon, hoe meer suikers worden aangemaakt en hoe groter de watertoevoer vanuit de wortels naar de kruin. Eenmaal boven helpt het water met de aanmaak van de suikers in de bladeren en verdampt het. De suikers worden via een tweede sapstroom naar de wortels getransporteerd.

Let wel, op te warme dagen, wanneer het evenwicht tussen zonlicht en hoeveelheid water niet in balans is, sluiten de bomen hun stomata (huidmondjes op de bladeren), stopt de fotosynthese en vertraagt de sapstroom.

### Waarom deze sensor

De sapstroom van een boom kan je vergelijken met onze bloedsomloop, met dit verschil dat die in twee richtingen stroomt van de wortels naar de kruin (transport van water en mineralen) en van de kruin naar de wortels (transport van suikers). De hoeveelheid water is cruciaal voor een groot aantal processen in planten, zoals fotosynthese, transpiratie, voortplanting, groei, orgaanontwikkeling, celstructuur en celwerking, en nog veel meer.

De hoeveelheid en de snelheid van de sapstroom geven ook inzicht in het vermogen van bomen om te reageren op watertekorten in de bodem, droogte en extreme weersomstandigheden.

### Type sensor

De sapstroom wordt berekend op basis van de snelheid van de stroom in de transportcellen net onder de bast van de boom. De sensor bestaat uit drie naalden die in de stam worden geboord. De middelste naald geeft een warmtesignaal. De naalden boven en onder het warmtesignaal meten de temperatuur, stroomafwaarts en stroomopwaarts van de warmtebron. De reistijd van het warmtesignaal wordt zo op twee plekken gemeten. Water is een sterke geleider. Door de tijd te meten die het warmtesignaal nodig heeft om beide posities te bereiken, wordt de snelheid van het warmtesignaal bepaald.

Vervolgens past de sensor een aantal wiskundige formules toe: de warmtesignaal-snelheden worden omgezet in sapstroom-dichtheden en toegepast over de doorsnede van de stam om een idee te krijgen van de volledige sapstroom. Op online fora wordt actief gediscussieerd over welke wiskundige formules de beste resultaten opleveren.

Wij plaatsten één sapstroomsensor bij de Douglasspar, omdat de sensor vereist om drie gaatjes in de bast te boren. Douglassparren maken snel hars aan, wanneer hun bast verwond wordt, waardoor een invasie van bacteriën en schimmels minder kans krijgt dan bij oude loofbomen.
