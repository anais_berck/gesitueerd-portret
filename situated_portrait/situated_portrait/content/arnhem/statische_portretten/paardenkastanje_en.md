## Horse Chestnut

![](/static/img/trees/european_horse_chestnut-600x.jpg)

Hello, I'm a White Horse Chestnut, living near Witte Villa. You can recognize me by my wounded trunk. I had a lot of uninvited visitors, fungi and bacteria. But you can see how healthy and vibrant my crown still is. And did you see my beautiful white flowers last spring?
Over the years, the villa's residents and the park's users changed. From my trunk, I shares story fragments about that history. At night, I recite poems about Death.


[Arnhemse Bomen Vertellen](https://arnhemsebomenvertellen.nl/) was an artistic installation from 15 June till 15 December 2024. The sensors have since disappeared, but this bench still invites you to enter into a relationship with the tree in front.
This art project was a commission by the municipality of Arnhem, created by [Anaïs Berck](https://www.anaisberck.be/), an art collective of people, trees and algorithms.
