# Artists and all the people involved in the project

## Anaïs Berck
[Anais Berck](https://www.anaisberck.be/) is a pseudonym and represents the collaboration between people, algorithms and trees. As a collective, they open up a space in which human intelligence is given a place alongside plant intelligence and artificial intelligence.

For this project, the following people are part of the collaboration:

### An Mertens (Brussels, BE)
An is a media artist and tree clerk. An conceived and coordinated this project, provided research, literary code and editing of texts and stories.

### Gijs de Heij (Brussels, BE) and Doriane Timmermans (Brussels, BE)
Both are members of the experimental graphic design studio [Open Source Publishing](http://osp.kitchen/) in Brussels. They combine graphic design with programming and artistic creation.
[Gijs de Heij](https://gijsdeheij.com/) studied graphic design at ArtEZ in Arnhem. He has been programming the age of 11.
Doriane Timmermans holds a master's degree in mathematics from Université libre de Bruxelles and studied digital art at ERG in Brussels. She researches the literary potential of css: [Declarations](https://declarations.style/).

## Arborists from Leefmilieu Brussel / Bruxelles Environnement
The sensors were installed with the kind help of the forest guard and tree care staff from Leefmilieu Brussel/Bruxelles Environnement: Willy Vandevelde, Sébastien Billon, Didier Noben and Guillaume van Cutsem.

## Scientific advice
During various conversations before and during the creation process, we received very valuable advice from tree ring researcher Dr. Ute Sass-Klaassen and her colleague Linar Akhmetzyanov (Van Hall Larenstein University of Applied Sciences, Leeuwarden).

## Knowledge and experience about trees and parks
During the creation process of this project, An had the pleasure to spend time and listen to the knowledge and experiences of Frederik Vaes, Head of Department Leefmilieu Brussel/Bruxelles Environnement, Building Heritage Division - Forest and Nature.

## More advice
We could also count on the practical and moral advice of Hélène van Ginderachter, engineer Erik Mertens, Judith Van Ginneken, Marleen Verschueren, Erik De Wilde. Proofreading of the texts was taken on by Patrick Lennon.

## Financing, production, communication
This project came about thanks to the financial support of the Flemish Government, Arts & Heritage. This presentation moment received the logistical support of Leefmilieu Brussel/Bruxelles Environnement and the GC Wabo team in Bosvoorde/Boitsfort.
