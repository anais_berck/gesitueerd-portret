## Mouvements trunk

![](/static/img/sensors/ems-600x.jpg)

### Recognizable patterns

Day & night
:   during the day, the trunk shrinks during photosynthesis, due to the rapid transport of sap from the crown to the leaves and vice versa. At night, the stem refills with water from the roots and expands. This regular pattern is visible only in the Red Beech.

Sun & rain
:   in old trees with a thick bark, the dendrometer primarily measures the movement of the dead wood of the bark. When it rains, it expands; when the Sun shines, it shrinks. We are familiar with this phenomenon from wooden doors that 'stick' in autumn and winter.

We could measure growth in old trees with thick bark by removing a piece of bark around the circumference of the trunk. It is not in the nature of this project to cut into the bark of old deciduous trees. Most studies with dendrometers are done on beeches, which have very thin bark.

Season
:   on the Red Beech, you can see a steady growth per week; on the other trees, you notice the growth over a much longer period of time.

### Why dendrometers

The dendrometer makes it possible to record the ‘breathing’ of a tree, something we can't observe with the naked eye. The tree is constantly moving. During the day, when it is actively producing sugars through photosynthesis, the trunk shrinks. At night, when it is resting, the trunk fills up with water and expands again.

The production of sugars combined with water and minerals from the soil ensures cell production and the growth of the tree. In a year with plenty of sunshine and sufficient rain, no illnesses or unexpected events, the tree grows more. It then creates a wider tree ring. When a tree is felled, you can count the annual rings. A thin annual ring means a difficult year for the tree. The weather was then too dry or too wet.

Dendrochronologists drill a sample from the trunk of a living tree to know exactly how old it is, and to analyse past weather conditions. So trees are living climate archives!

### Sensor type
For this project, we use tape dendrometers. A metal band is tightened around the trunk. The black cylinder is the measuring instrument that moves up and down and transmits the measurements via the LTE network or mobile phone network.
