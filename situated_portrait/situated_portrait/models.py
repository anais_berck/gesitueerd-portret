from django.db import models
from django.db.models import Max, Min, Avg
from multiselectfield import MultiSelectField
import random
import string
import datetime
from django.conf import settings  
from django.contrib.sites.models import Site
from django.template.defaultfilters import slugify

def generate_qr_id ():
  out = ''

  while len(out) < 5:
    out += string.ascii_letters[random.randint(0,len(string.ascii_letters)-1)]

  return out

class Tree (models.Model):
  # Only the local_name and environment are required to add a tree

  # -- context
  latitude = models.FloatField(blank=True, null=True)
  longitude = models.FloatField(blank=True, null=True)

  qr_id = models.CharField(max_length=10, default=generate_qr_id)

  # -- name
  local_name = models.CharField(max_length=250)                                # ...
  species = models.CharField(max_length=250, blank=True, null=True)            # latin name
  environment = models.CharField(max_length=250)                               # parc, forest, city, etc
  remote_identifier = models.TextField(blank=True, null=True)

  @property
  def sensors (self):
    return Sensor.objects.filter(device__tree = self)

  # Return last observation per sensor, with an optional
  # since
  def observations_since (self, since=None, synthetic=None):
    qs = Observation.objects.filter(
      sensor__device__tree=self
    ).prefetch_related('sensor').order_by('-timestamp')

    if synthetic is not None:
      qs = qs.filter(sensor__synthetic=synthetic)

    if since:
      qs = qs.filter(timestamp__gte=since)

    return qs

  def observations_for_sensor_type (self, type, synthetic=None):
    if synthetic is not None:
      return Observation.objects.filter(
        sensor__type=type,
        sensor__device__tree=self,
        sensor__synthetic=synthetic
      )
    else:
      return Observation.objects.filter(
        sensor__type=type,
        sensor__device__tree=self
      )

  def get_clock(self, date_from, date_to):
    # a tree clock contains the observations
    # linked to a tree
    # over a choosen window of time
    # alongside its min, max and average over that window of time
    # and sorted by sensor type

    # if any of those keyword are in a sensor key,
    # the sensor will be excluded

    sensors = Sensor.objects.filter(device__tree = self, synthetic = False)

    excluding = ['battery', 'pressure', 'bosch-bmp280', 'senseair-lp8', 'sunlight', 'system', 'index', 'implexx']
    def remove_excluded(sensor):
      return not any(ex in sensor.key for ex in excluding)
    sensors = list(filter(remove_excluded, sensors))

    for sensor in sensors:

        sensor.clock = {}
        sensor.clock['key'] = sensor.key
        sensor.clock['pk'] = sensor.pk
        sensor.clock['brand'] = sensor.device.deviceType.brand
        sensor.clock['type'] = sensor.type
        sensor.clock['unit'] = sensor.unit
        sensor.clock['serie'] = sensor.get_observations(date_from, date_to)

        # creating CSS classes here for the clock
        # this can be regarded as a simplified jargon for characterizing the sensors on every tree
        sensor.clock['classes'] = []

        if 'circular' in sensor.key:
          sensor.clock['classes'].append('contact')
          sensor.clock['classes'].append('circular')
        elif 'sapflow' in sensor.key:
          sensor.clock['classes'].append('contact')
          sensor.clock['classes'].append('sapflow')
        else:
          sensor.clock['classes'].append('ambient')

        if 'sensoterra' in sensor.key:
          sensor.clock['classes'].append('soil')
        if 'air' in sensor.key or 'co2' in sensor.key:
          sensor.clock['classes'].append('air')
        if 'temperature' in sensor.key:
          sensor.clock['classes'].append('temperature')
        if 'humidity' in sensor.key or 'moisture' in sensor.key:
          sensor.clock['classes'].append('humidity')
        if 'co2' in sensor.key:
          sensor.clock['classes'].append('co2')

        if 'serie' in sensor.clock and len(sensor.clock['serie']) > 0:
          sensor.clock['last'] = round(sensor.clock['serie'][0].value, 3)
          sensor.clock['hour_last'] = sensor.clock['serie'][0].timestamp.strftime("%H:%M")
          sensor.clock['second_last'] = round(sensor.clock['serie'][1].value, 3)
          sensor.clock['hour_second_last'] = sensor.clock['serie'][1].timestamp.strftime("%H:%M")
          sensor.clock['sensor_quality'] = sensor.clock['serie'][0].sensor.type
          sensor.clock['min'] = round(min([o.value for o in sensor.clock['serie']]), 3)
          sensor.clock['max'] = round(max([o.value for o in sensor.clock['serie']]), 3)
          sensor.clock['average'] = round(sum([o.value for o in sensor.clock['serie']]) / len(sensor.clock['serie']), 3)
        
    return { sensor.clock['key'] : sensor.clock for sensor in sensors }

  def __str__ (self):
    return f"{self.local_name} ({self.environment})"


class DeviceType (models.Model):
  # Type of device, used to retrieve data

  brand = models.CharField(max_length=250)      # Sensoterra
  model = models.CharField(max_length=250)      # name of this model
  url = models.CharField(max_length=250)        # url of the api endpoint
  authentication = models.JSONField()           # email, password, auth

  def __str__ (self):
    return f"[{self.brand}] {self.model}"


class Device (models.Model):
  # Instance of a device
  # serial_number and deviceType are the only required

  # -- context
  latitude = models.FloatField(blank=True, null=True)
  longitude = models.FloatField(blank=True, null=True)
  tree = models.ForeignKey(Tree, blank=True, null=True, related_name='devices', on_delete=models.PROTECT)
  placement = models.TextField(blank=True, null=True)

  # -- data
  active = models.BooleanField(default=True)
  deviceType = models.ForeignKey(DeviceType, related_name='instances', on_delete=models.PROTECT)
  remote_identifier = models.TextField(blank=True, null=True)
  serial_number = models.TextField()

  start_date_data_fetch = models.DateTimeField(blank=True, null=True, verbose_name="Start date data fetch", help_text="From this date data will be requested from the data provider.") 

  @property
  def name (self):
    tree = self.tree.local_name if self.tree else "No tree"
    return f"{tree} [{self.deviceType.brand}] {self.placement} ({self.serial_number})"

  @property
  def short_name (self):
    tree = self.tree.local_name if self.tree else "No tree"
    return f"{tree} [{self.deviceType.brand}]"
  
  def __str__ (self):
    return self.name


class Sensor (models.Model):
  # One device can have multiple sensors,
  # usually one sensor measure one thing

  device = models.ForeignKey(Device, related_name='sensors', on_delete=models.PROTECT)
  type = models.CharField(max_length=250)       # humidity, CO2, etc
  unit = models.CharField(max_length=250)       # %, m, etc
  remote_identifier = models.TextField(blank=True, null=True)
  extra_data = models.JSONField(blank=True, null=True)

  # this say if this sensor is physical or not
  synthetic = models.BooleanField()
  # if non-physical this explain what type of extrapolation this is
  synthetic_type = models.CharField(max_length=250, blank=True, null=True) 
  
  def get_observations(self, date_from, date_to):
    # get observation between two range of time
    return self.observations.filter(
      timestamp__gte = date_from,
      timestamp__lte = date_to
    ).order_by('-timestamp')

  def get_last_observation (self, before=None):
    qs = self.observations.order_by('-timestamp')
    
    if before:
      qs = qs.filter(timestamp__lt=before)

    return qs.first()
  
  class Meta:
    unique_together = [["device", "type", "synthetic_type"]]

  # generate an id, that is CSS or anchor compatible
  # this doesn't take in account the tree at all
  @property
  def key (self):
    unique = [self.device.deviceType.brand, self.type]
    if self.synthetic:
      unique.append(self.synthetic_type)
    return str("-".join(unique)).lower().replace(' ', '_')

  @property
  def name (self):
    name = ''

    if self.synthetic:
      name = name + "[synthetic]"
    name = name + " " + self.type
    if self.synthetic:
      name = name + " (" + self.synthetic_type + ")"
    name = name + " - " + self.device.short_name

    return name
  
  def __str__ (self):
    return self.name


class Observation (models.Model):
  # Reading of a sensor

  sensor = models.ForeignKey(Sensor, related_name='observations', on_delete=models.PROTECT)
  timestamp = models.DateTimeField()
  created_at = models.DateTimeField(auto_now_add=True)
  value = models.FloatField()
  extra_data = models.JSONField(blank=True, null=True)
  ordering = ["-timestamp"]

  class Meta:
    unique_together = [["sensor", "timestamp"]]

  def __str__ (self):
    return f"[{self.timestamp}] - {self.value} - {self.sensor}"

  pass

# get the range and average around
# a common brand and type of sensors
# over a defined period of type
def get_range_uncached(date_from, date_to):
  
  if not isinstance(date_from, datetime.datetime):
    date_from = datetime.datetime.fromtimestamp(date_from)

  if not isinstance(date_to, datetime.datetime):
    date_to = datetime.datetime.fromtimestamp(date_to)

  sensors = Sensor.objects.filter(synthetic = False)

  # creating the keys
  ranges = {}
  for sensor in sensors:
    ranges[sensor.key] = sensor

  for key, sensor in ranges.items():
    range = Observation.objects.filter(
              sensor__type = sensor.type,
              sensor__device__deviceType__brand = sensor.device.deviceType.brand,
              sensor__synthetic = False,
              timestamp__gte = date_from,
              timestamp__lte = date_to
            ).aggregate(max=Max("value"), min=Min("value"), average=Avg("value"))
                        
    range['unit'] = sensor.unit
    ranges[key] = range
  
  print('Finished aggregating')

  return ranges

try:
    from functools import cache
    
    @cache
    def get_range (date_from, date_to):
        return get_range_uncached(date_from, date_to)
except ImportError:
    get_range = get_range_uncached



class Installation (models.Model):
  name = models.CharField(max_length=200)
  site = models.OneToOneField(to=Site, on_delete=models.CASCADE, related_name='installation')
  languages = MultiSelectField(choices=settings.LANGUAGES)
  views = MultiSelectField(choices=settings.AVAILABLE_VIEWS)
  trees = models.ManyToManyField(to=Tree, related_name='installations', blank=True)
  
  slug = models.SlugField(blank=True, null=True)
  def save(self, *args, **kwargs):
      self.s = slugify(self.name)
      super(Installation, self).save(*args, **kwargs)

  POI = models.JSONField(blank=True, null=True)
  focus = models.ForeignKey(Tree, blank=True, null=True, related_name='focus', on_delete=models.PROTECT)

  def __str__ (self):
    return str(self.name)
  
  """
    The languages multiselectfield remains a list of language codes.
    (though when printed it is transformed to a list of display values)
    Often interested in both display value, as well as the language code.
    This function returns that based on the value of languages field and the choices
  """
  @property
  def languages_as_tuples (self):
    return [
      (code, label) for code, label in settings.LANGUAGES if code in self.languages
    ]

  
# -------------- deprecated ---------------------

  # aggr = Observation.objects.filter(
  #       timestamp__gte = date_from,
  #       timestamp__lte = date_to
  #     ).values(
  #       "sensor__device__deviceType__brand", "sensor__type", "sensor__unit",
  #     ).annotate(max=Max("value"), min=Min("value"), average=Avg("value"),
  #                 brand= F("sensor__device__deviceType__brand"),
  #                 type = F("sensor__type"),
  #                 unit = F("sensor__unit"))

  # for trees model
  #
  # def get_ranges(self, date_from, date_to):
  #   return  Observation.objects.filter(
  #             timestamp__gte = date_from,
  #             timestamp__lte = date_to,
  #             sensor__device__tree = self
  #           ).values(
  #             "sensor__device__deviceType__brand", "sensor__type", "sensor__unit",
  #           ).annotate(max=Max("value"), min=Min("value"), average=Avg("value"), type = F("sensor__type"))
