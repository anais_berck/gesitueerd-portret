De dood wees mij op kleine, interessante dingen:
	dit is een spijker – zie de Dood - en dit is een touw
	Ik zie hem aan, een kind. Hij is mijn meester
	omdat ik hem bewonder en vertrouw,
de Dood
	Hij wees mij alles: dranken, pillen,
	pistolen, gaskraan, steile daken,
	een bad, een scheermes, een wit laken
	‘zomaar’ – voor als ik eens zou willen
de dood.
	En voor hij ging, gaf hij me nog een klein portretje…
	‘Ik weet niet, of je ’t al vergeten was,
	het komt misschien nog wel te pas
	voor als je eens niet meer zou willen
sterven,	
	maar wat let je?’
	zei de Dood
door M. Vasalis, 2021

Ik vraag je
Ik vraag je, blauwe lucht,
met een diepe zucht
En jou, volle maan,
wat heb je met hem gedaan.
En u, fonkelende ster,
hij is weg, zó ontzettend ver.
Ik vraag jou, mooi leven,
wil je hem terug geven?
Door Willeke Tjassens, 2023

Een foto, zes jaar
oud begraaf ik in de tuin
onze geheimen
door Zywa, 2023

Daar waar ik leefde
en bestond
voor jou,
waren wij samen
op aarde,
al wat ik je gaf
speelde tussen ons,
neem ik mee in mijn graf
door Petra Hermans, 2013

Ik zit op mijn balkonnetje
En geniet er van het zonnetje.
Op de straat aan de overkant
Lopen twee mensen, hand in hand.
Het vertedert mij als ik er naar kijk;
Ik denk wat zijn die twee toch rijk!
Ze zijn al oud en schuifelen voorbij,
Nog samen, zij aan zij.
Wie weet al jaren bij elkaar.
Ook al was het soms wel zwaar.
Maar geluk had de overhand,
Daarom lopen zij nog steeds hand in hand
Tot de dood hen scheidt
Joop Bonnemaijers, 2017

De toenadering
houdt ook het afscheid in
het einde vervolledigt
de cirkel en legt aan
tot het begin
door Petra Hermans, 2013

Of je levend bent of dood
maakt niet veel uit
je hebt mij aan je vastgeplakt
je ogen bij mij ingeprent
je nagels in mijn vlees gezet
je stem verborgen in mijn oren
of je levend bent of dood
ik raak je nooit meer kwijt
door Serval, 2016

Dood hout zorgt voor nieuw leven
ooit was hij oersterk
reeën schuurden vaak hun vacht
aan de bast van deze pracht
een kanjer van een zilverberk
maar hij moest het zwaar bekopen
stond voor velen in de weg
de kettingzaag werd in zijn stam gezet
zo triestig kan het bomenleven lopen
zo, het dode hout is wat er rest
enkele zwammen proberen te overleven
tevens wat gekraak, rotte splinters die plots bewegen
ja, de wormen doen hier ook hun best
er is nieuw leven na de dood
door jonhy donovan, 2018

Ik zag je vermoeide ogen,
	ik zag de vele pijn.
Hetgeen ik zag heeft mij erg bewogen,
	wat kan het leven meedogenloos zijn.
Maar jouw krachtige hand bij het afscheid nemen,
	heeft me toch ook weer meegegeven.
Dat de mens die het ondergaat,
	tot de laatste snik zijn mannetje staat.
Door John Hamers, 2022

De dood fluisterde zacht, kom neem mijn hand
Ik breng je naar de bergen of de zee
Of naar enig ander land.
Ik riep op luide toon
een tuinman verwacht je in Ispahaan.
ga weg dood, je kunt me niet bekoren
Oké, dan niet, zei de dood
Dit keer heb ik verloren 
Ik wacht wel af,
je zult ooit nog van mij horen.
Door Ingrid van Belkom, 2019

Als ik dood ben
zal er niets veranderd
zijn
de lege straat
het warme asfalt
je naakte schouder
de jeugd van mijn zoon
zal eeuwig zijn
en hoe ik aan mijn
zachte wang
je tranen weet
er zal niet veel veranderd zijn
berust in mij.
door Geert Loman, 2003

Helder water
dat kabbelt
en klatert
en stroomt
en dan
plotseling
zomaar ineens
tot stilstand komt
in een poel
die  langzaam 
troebel wordt.
door Hennie van Ee, 2003

Als het leven je ontglipt
en de dood is zo dichtbij
dan verdwijnt de woede
en voel je je vrij
De strijd is gestreden
je hebt je best gedaan
de pijn zou verdwijnen
je wist dat je moest gaan.
door Ikke, 2004

Al wat leeft, sterft, vergeet
zal immer bloeien
onverholen begeleid
door tekens op een graf
herinnering zal nimmer wijken
al wat jij gaf, nam en bracht
jij
jij bent leven
jij bent de Dood
leven is de naam die jij bracht
door Mike, 2003

En altijd is daar, ongezien
Na jaren, soms een tel misschien
die ongenode zwarte gast,
waardoor het leven wordt verrast
De dood, hij volgt met zachte schreden
aan hen die baden, hen die leden
vaak ongewenst, soms zelf gezocht
Wat mooi, zo ieder kiezen mocht
door Joop van Santen, 2002

De dood klopt
aan de deur van het leven
Het eind van het aardse 
het begin van het vervolg
wordt prijsgegeven
De eeuwigheid klopt
aan de deur van het tijdelijke
en confronteert stervelingen
met het onvermijdelijke
door reminder, 2002

Dood. Heb geen angst. Talm niet
voor mijn deur. Kom binnen.
Lees mijn boeken. In negen van de tien
kom je voor. Je bent geen onbekende.
Hou mij niet voor de gek met kwalen
waarvan niemand de namen durft te noemen.
Leg mij niet in een bed tussen kwijlende
kinderen die van ouderdom niet weten wat ze zeggen.
Klop mij geen geld uit de zak
voor nutteloze uren in chique klinieken.
Veeg je voeten en wees welkom.
door Eddy van Vliet, 2023

Niets is zo somber 
niets is zo triest
niets is zo kleurloos
als je iemand verliest
je moet het maar leren
de dood te accepteren
door Mathilde, 2002

Leven is leven
tot het overgaat
dan is er alleen nog de dood
tot het stof is weggewaaid
Stof is stof
tot het weer samenkomt
dan is er de geboorte
waarna de dood zich weer samendromt
door Gerard, 2003

De dood is
een gebarsten raam
dat breekt
desnoods
in duizend stukjes
De dood is
definitief zwart
een opluchting
voor de man die het al
meegemaakt had
door Ed Franck, 2006

Als iemand dood gaat 
hou je dan op met houden van? 
en als dat zo is 
waar blijft dat dan?
door Kimberley, 2007

Door twijfel verreten
  aan benen geknaagd
  ben ik ten hemel gegaan 
  ik heb de strijd gewaagd
door speci, 2007

Met je ogen wil ik leven
al mijn liefde zal ik geven
maar mijn handen blijven kleven
aan de dingen die ik gaf.
In je ogen zal ik leven
al je liefdes wil ik vergeven
maar mijn handen blijven beven
bij de dingen die jij gaf.
Heel mijn leven moet ik geven
aan de dingen die wij kregen
maar vandaag geef ik je leven
met de bloemen op je graf.
door J.J.v.Verre, 2008

Was, ik durf het niet uit te spreken.
Is, dat klinkt al beter.
Maar, dat klopt niet!
Je bent dood,
Maar leeft nog in m'n hart.
door Zoë, 2008

Dood is 
Bindend loslaten
Banden verbreken
Buiten spel gezet worden
Overgave alom
Binnentreden in het heelal
Onfeilbare energie!
door Maria B.V., 2024

Laat me maar slapen
eindelijk mijn eigen winterslaap
en wek me
wek me maar
als de lente komt
en het leven weer begint.
door Marjanne, 2010

Plat en uitgerekt
ingewanden uitwendig
zo werd groen met
rood vermengd
arme kikker op de weg.
door Julia, 2010

Dood
neem mij mee.
ik heb hier afgedaan.
ik wil op de rotsen te pletter slaan
en versplintren in open zee...
neem mij mee,
dood.
door Hendrik Marsman, 1934

En dan de dood
onsterfelijk fenomeen
wie je ook bent
wie je ook kent
de mens gaat heen
de een met een knal
een ander vindt rust
het leven was een bal
en ik ben blij
dat ik je toch nog
heb gekust.
door Fred, 2012

Na mijn dood
Strooi mijn as uit op het wad.
Ik spoel weer aan,
ik land weer aan,
zo kom ik weer in de stad
door Stadjer, 2014

De dood
nooit ben ik alleen
hij eet met me, slaapt met me
zwijgt tegen me, gaapt, zet de
tijd vast van mijn heen-
gaan
geen traan
die hij daar om vergiet
hij reist met mij door de dagen
over zeeën van vreugde, langs vlagen
van verdriet, het raakt hem niet
als een giftige termiet
bouwt hij heuvels in hiernadagen
waaroverheen hij me zal dragen
als het leven uit mij vliedt.
door Jacqueline Pronk, 2017
