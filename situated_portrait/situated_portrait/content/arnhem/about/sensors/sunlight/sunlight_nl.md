## Zonlichtsensor

![](/static/img/sensors/solar_radiation.jpg)

### Herkenbaar patroon

Dag & nacht
:   de Aarde draait om de Zon in ongeveer 365 dagen. Ondertussen draait de Aarde ook om zijn eigen as, in 24 uur of in één dag.

In Nederland is het veel langer licht in de zomer dan in de winter. Dit komt doordat de Aarde een klein beetje schuin staat ten opzichte van de Zon.

In de zomer staat de Zon loodrecht op - en daardoor ook het dichtst bij - het noordelijk halfrond. Hierdoor is er meer zon op dit halfrond in de zomer en zijn de dagen dus ook langer. In de winter gebeurt precies het omgekeerde.

### Zonlicht & Bomen

Bomen groeien letterlijk naar het licht. Hun bladeren vangen zoveel mogelijk zonlicht op om suikers te kunnen aanmaken via fotosynthese. Als je erover nadenkt, dan vormen de Zon, water en CO2 de basis van ons leven, onze voeding, ons materiaal en medicijnen. Elke plant groeit dankzij de Zon en de fotosynthese. Mensen en dieren voeden zich met planten.

### Type sensor

De zonneschijnsensor is een klein bolletje, zo groot als een oog, dat horizontaal in een open ruimte is gemonteerd. Het meet de bestralingssterkte van de zon. De eenheid van bestralingssterkte is joule per seconde per vierkante meter (W/m-²). De eenheid die vaak wordt gebruikt in de zonne-energiesector is kilowattuur per vierkante meter (kWh/m²). Joule drukt de hoeveelheid energie uit, terwijl Watt het vermogen aangeeft. Als je het vermogen gedurende een bepaalde tijd toepast (bijvoorbeeld per uur), dan heb je een bepaalde hoeveelheid joule verbruikt of gegenereerd.

De sensor gebruikt als eenheid joule per seconde per vierkante meter (W/m-2). Gedeeld door 1000 geeft dit kWh/m².

Zonneschijnduur is de periode waarin de directe zonnestraling de limiet van 120 W/m² overschrijdt. De stralingsintensiteit varieert sterk, van ongeveer 50 W/m² op een zwaar bewolkte winterdag tot meer dan 1000 W/m² bij een heldere hemel in de zomer.
