from situated_portrait.models import Tree, Sensor, Observation, get_range
import datetime
import pytz
from collections import defaultdict
import random

## download laatste versie van database:
# scp debian@91.134.100.3:/srv/bomenvertellen/gesitueerd-portret/situated_portrait/db.sqlite3 ./

## run script
# ./manage.py shell < exercises/subtitles_test.py

translations = {'circular increment': ['stambeweging', 'houtcellen', 'ervaren'], 'co2': ['co2', 'bladeren', 'ervaren'], 'air humidity': ['luchtvochtigheid', 'bladeren', 'ervaren'], 'air pressure': ['luchtdruk', 'bladeren', 'ervaren'], 'air temperature': ['temperatuur', 'bladeren', 'ervaren'], 'moisture': ['bodemvochtigheid', 'wortels', 'ervaren'], 'temperature': ['bodemtemperatuur', 'wortels', 'ervaren'], 'sunlight': ['zonlicht', 'bladeren', 'ervaren'], 'sapflow': ['sapstroom', 'houtcellen', 'ervaren'], 'Sweet chestnut': 'Kastanje', 'European oak': 'Boseik', 'Douglas fir': 'Douglasspar', 'Red beech': 'Rode Beuk', 'Solitary oak': 'Allenige Eik', 'European horse-chestnut': 'Paardenkastanje'}

# --- CHOOSE TIME WINDOWS
def find_time(nr):
    now = datetime.datetime.now(pytz.utc)
    yesterday = now - datetime.timedelta(days = nr)
    return now, yesterday

# Find last and second last values for all sensors at all trees using the tree clock function
# Returns a list of tuples consisting of a sensortype and a list of trees, for each tree
# first entry is local tree name, second entry is the unit, last two pairs are value and time stamp of observation.
# [
#   (str:sensorType, [
#       [ str:Tree.local_name, str:unit, number:value, str:time, number:value, str:time ])
# ]
def last_values_all_trees(nr):
    collection = []
    last_observations = defaultdict(list)
    # find time
    now, yesterday = find_time(nr)
    # select all trees
    trees = Tree.objects.all()
    # get clock observations for all trees in set time
    for tree in trees:
        tree.clock = tree.get_clock(yesterday, now)
        # get tree names and all last and second last values of all trees with their timestamp and unit
        for sensor_type, observation_per_tree in tree.clock.items():
            if 'serie' in observation_per_tree and len(observation_per_tree['serie']) > 0:
                data = [tree.local_name, observation_per_tree["unit"], observation_per_tree["last"], observation_per_tree["hour_last"], observation_per_tree["second_last"],  observation_per_tree["hour_second_last"]]
                couple = observation_per_tree["sensor_quality"], data
                collection.append(couple)
    for k, v in collection:
        last_observations[k].append(v)
    last_observations = list(last_observations.items())
    return(last_observations)

# get values of last day (nr) for specific tree and specific sensor
def one_tree_one_sensor(nr, tree_focus, update):
    collection = []
    values = []
    # find time
    now, yesterday = find_time(nr)
    # select 1 tree
    tree = Tree.objects.filter(local_name = tree_focus)[0]
    # get clock observations for this tree in set time
    tree.clock = tree.get_clock(yesterday, now)
    # get last values of specific tree and specific sensor
    for sensor_type, observation_per_tree in tree.clock.items():
        if observation_per_tree['type'] == update:
            values = observation_per_tree['serie']
    # order observations by value
    ordered_values = values.order_by("-value")
    # collect latest value, time latest value, highest value, time highest value, lowest value, time lowest value
    collection = [values.first().value, values.first().timestamp.strftime("%H:%M"), ordered_values.first().value, ordered_values.first().timestamp.strftime("%H:%M"), ordered_values.last().value, ordered_values.last().timestamp.strftime("%H:%M")]
    return collection

# get all values of last day (nr) for all trees and specific sensor
def all_trees_one_sensor(nr, update):
    collection = []
    values = []
    # find time
    now, yesterday = find_time(nr)
    # select 1 tree
    trees = Tree.objects.all()
    # get clock observations for all trees in set time
    for tree in trees:
        tree.clock = tree.get_clock(yesterday, now)
    # get last values of specific tree and specific sensor
    for sensor_type, observation_per_tree in tree.clock.items():
        if observation_per_tree['type'] == update:
            values = observation_per_tree['serie']
    # order observations by value
    ordered_values = values.order_by("-value") # FIXME: does a double ordering, values is a queryset returned by the get_observations method of a Sensor, and this queryset is already ordered.
    # collect highest value, time highest value, tree highest value, lowest value, time lowest value, tree lowest value, unit
    collection = [ordered_values.first().value, ordered_values.first().timestamp.strftime("%H:%M"), ordered_values.first().sensor.device.tree.local_name, ordered_values.last().value, ordered_values.last().timestamp.strftime("%H:%M"), ordered_values.last().sensor.device.tree.local_name, ordered_values.last().sensor.unit]
    return collection


# returns a list with specific [name_tree, unit, last, hour_last, second_last, hour_second_last]
def matching_observation(list, update, tree_focus):
    related = []
    for item in list:
        if item[0] == update:
            for element in item[1]:
                if element[0] == tree_focus:
                    related = element
    return related

# compare 2 values
def compare(value_1, value_2):
    if value_1 >= value_2:
        difference = value_1 - value_2
        ratio = "meer"
    else:
        difference = value_2 - value_1
        ratio = "minder"
    return difference, ratio

# write general subtitles that apply to all sensor qualities mentioned in dictionary above
def subtitles_gen(nr, tree_focus, update):
    # # Find last and second last values for all sensors at all trees using the tree clock function
    # nr sets amount of days you want to go back
    # returns list with sensor quality + [name_tree, unit, last, hour_last, second_last, hour_second_last]
    last_observations = last_values_all_trees(nr)
    # find related observation [name_tree, unit, la st, hour_last, second_last, hour_second_last]
    related = matching_observation(last_observations, update, tree_focus)
    # compare 2 values last & second last
    difference, ratio = compare(related[2], related[4])
    # write subtitles
    sent = "Op dit moment {} mijn {} een {} van {} {}. Dat is {} {} {} dan de vorige meting om {} u.".format(translations[update][2], translations[update][1], translations[update][0], "{:.2f}".format(related[2]), related[1], "{:.2f}".format(difference), related[1], ratio, related[5])
    
    if update == "sapflow":
        sent += "Hoe meer zon, hoe meer fotosynthese in mijn bladeren, hoe sneller mijn sapstroom."

    return sent

def high_low_all(nr, tree_focus, update):
    # find tree with highest value of the day
    # nr sets amount of days you want to go back
    # returns list with sensor quality + [name_tree, unit, last, hour_last, second_last, hour_second_last]
    last_observations = last_values_all_trees(nr)
    # find related observation [name_tree, unit, last, hour_last, second_last, hour_second_last]
    related = matching_observation(last_observations, update, tree_focus)
    # collect highest value, time highest value, tree highest value, lowest value, time lowest value, tree lowest value, unit
    collection = all_trees_one_sensor(1, update)
    # compare values
    difference_high, ratio_high = compare(collection[0], related[2])
    difference_low, ratio_low = compare(collection[3], related[2])
    difference, ratio = compare(related[2], collection[0])
    sents = ["Van alle bomen die deelnemen aan dit project, ervaart {} de hoogste verandering bij hun {} vandaag: {} {} {} om {}. Dat is {} {} {} dan mijn laatste meting om {} u.".format(translations[collection[2]], translations[update][1], "{:.2f}".format(collection[0]), collection[6], translations[update][0], collection[1], "{:.2f}".format(difference_high), collection[6], ratio_high, related[3]), "Van alle bomen die deelnemen aan dit project, ervaart {} de kleinste verandering bij hun {} vandaag: {} {} {} om {}.  Dat is {} {} {} dan mijn laatste meting om {} u.".format(translations[collection[5]], translations[update][1], "{:.2f}".format(collection[3]), collection[6],translations[update][0], collection[4], "{:.2f}".format(difference_low), collection[6], ratio_low, related[3]), "Mijn {} {} {} {} {} dan op het hoogst gemeten moment vandaag, om {} uur.".format(translations[update][1], translations[update][2], "{:.2f}".format(difference), collection[6], ratio, collection[1])]
    picked_sent = random.choice(sents)
    return picked_sent

def trunk_mouvement(nr, tree_focus, update):
    # nr sets amount of days you want to go back in time
    # collect highest value, time highest value, tree highest value, lowest value, time lowest value, tree lowest value, unit
    collection = one_tree_one_sensor(nr, tree_focus, update)
    # compare values
    difference, ratio = compare(collection[0], collection [2])
    # write subtitles
    trunk_sents = ["Mijn stam ervaarde vandaag de grootste verandering om {} uur: {} mm.".format(collection[3], "{:.2f}".format(collection[2])), "Mijn stam ervaart nu {} mm {} beweging dan op het hoogst gemeten moment vandaag, om {} uur. 's Nachts zet mijn stam uit door grote opname van water. Overdag krimpt mijn stam wanneer mijn sap flink stroomt.".format("{:.2f}".format(difference), ratio, collection[3])]
    picked_sent = random.choice(trunk_sents)
    return picked_sent

def sun_mouvement(nr, update):
    # sunlight sensor is only connected to Douglas fir
    tree_focus = 'Douglas fir'
    # find month
    now, yesterday = find_time(1)
    month = now.strftime("%m")
    # gemiddelde waarden van voorbije 20 jaar
    # source: https://www.meteo.be/resources/climatology/climateCity/pdf/climate_INS21004_9120_nl.pdf
    # uitgedrukt in kWh/m², om te vergelijken moeten we onze waarden delen door 1000
    mean_values_per_month = {"04": 3.9, "05": 4.8, "06": 5.2, "07": 5, "08": 4.3, "09": 3.1}
    # find mean value for this month
    mean_radiation = mean_values_per_month[month]
    months_str = {"04": 'april', "05": "mei", "06": "juni", "07": "juli", "08": "augustus", "09": "september"}
    month_str = months_str[month]
    # collect latest value, time latest value, highest value, time highest value, lowest value, time lowest value
    collection = one_tree_one_sensor(1, tree_focus, update)
    # compare latest
    difference, ratio = compare(collection[0], collection[4])
    difference_high, ratio_high = compare(collection[0], collection[2])
    # write subtitles
    sun_sents = ["Mijn kruin ervaarde vandaag het meeste zonlicht om {} uur. De Zon scheen op dat moment aan {} kWh/m². De gemiddelde hoeveelheid zonlicht in {} tijdens de voorbije 20 jaar, was {} kWh/m².".format(collection[3], "{:.2f}".format(collection[2]/1000), month_str, mean_radiation),\
    "Op dit moment ervaar ik een zonnekracht van {} kWh/m². Dat is {} kWh/m² {} dan op het laagst gemeten moment vandaag, om {} uur.".format("{:.2f}".format(collection[0]/1000), "{:.2f}".format(difference/1000), ratio, collection[5]),\
    "Mijn kruin ervaart {} kWh/m² zonne-energie {} dan op het meest zonnige moment vandaag, om {} uur.".format("{:.2f}".format(difference_high/1000), ratio_high, collection[3])
    ]
    picked_sent = random.choice(sun_sents)
    return picked_sent

def co2_mouvement(nr, tree_focus, update):
    # collect latest value, time latest value, highest value, time highest value, lowest value, time lowest value
    collection = one_tree_one_sensor(1, tree_focus, update)
    # write subtitle
    return "Mijn bladeren ervaarden vandaag het meest CO2 om {} uur. Het ging om {} ppm. De gemiddelde hoeveelheid CO2 in de buitenlucht ligt rond 400 ppm. In 1700 was dat maar de helft.".format(collection[3], "{:.2f}".format(collection[2]))   



# ---- -------------------------------------------


sentence_generators = {
    'circular increment': [
    subtitles_gen, high_low_all, trunk_mouvement
    ],
    'air humidity':[
    subtitles_gen, high_low_all
    ],
    'air pressure':[
    subtitles_gen, high_low_all
    ],
    'air temperature':[
    subtitles_gen, high_low_all
    ],
    'co2':[
    subtitles_gen, high_low_all, co2_mouvement
    ],
    'moisture':[
    subtitles_gen, high_low_all
    ],
    'temperature':[
    subtitles_gen, high_low_all
    ],
    'sunlight':[
    sun_mouvement
    ],
    'sapflow':[
    subtitles_gen
    ]
}

### NOTES:
# - air temperature gives a very strange C as unit....
# - sun_mouvement is supposed to be combinable with all trees, I don't know whether my solution works
# - Douglas fir has a new Sensoterra sensor that is not encoded in the db yet... so no 'moisture' or 'temperature' data for Douglas fir
# - this code takes for granted that an update of a sensor comes with a specific tree - so f.ex. air pressure will not come with Horse chestnut, circular increment will not come with Douglas fir, etc (with sunlight as an exception)
def generate_update_subtitle (sensor):
    update_type = sensor.type
    if update_type in sentence_generators:
        return random.choice(sentence_generators[update_type])(1, sensor.device.tree.local_name, update_type)
    else:
        return None

# Returns a subtitle for a random sensor on given tree
def generate_update_subtitle_for_tree (tree):
    # Loop through a shuffled list of sensors for this tree.
    # Return as soon as one worked
    tree_sensors = list(tree.sensors)
    random.shuffle(tree_sensors)
    for sensor in tree_sensors:
        sentence = generate_update_subtitle(sensor)

        if sentence:
            return sentence

# --- -----------------------------------------------------

# updates can be (type sensor): circular increment, air humidity, air pressure, air temperature, co2, moisture, temperature, sapflow, sunlight
# trees can be: Sweet chestnut, European oak, Douglas fir, Red beech, Solitary oak, European horse-chestnut

if __name__ == '__main__' or __name__ == 'django.core.management.commands.shell':
    sensors = list(Sensor.objects.all())

    while True:
        sentence = generate_update_subtitle(random.choice(sensors))

        if sentence:
            print(sentence)
            exit()