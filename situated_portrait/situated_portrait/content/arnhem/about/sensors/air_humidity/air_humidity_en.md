## Air humidity sensor

![](/static/img/sensors/milesight-600x.jpg)

### Recognizable patterns

Rain
:   as humans, we are very familiar with this sensor. Apps like Rainviewer provide guidance. Rain determines our activities, clothing style and often our mood. Rain is part of the water cycle, in which trees are also important players.

Day & night
:   humidity undulates with the rhythm of day and night. At night it cools down. The warm air from daytime condenses. That humidity is deposited on solid elements, such as grass or benches in the park.

### Air humidity & Trees

Water is the lifeblood of our planet. From rainforest to desert, prairie to tundra, everywhere the amount of water available determines what grows. No living organism can do without water as the basis for all life processes.
As soon as rain falls on the ground, plants start absorbing the water. Trees like beeches have tanned leaves that conduct the water to the trunk, from where it runs to the roots. This is how they optimize the rain. However, plants need to absorb much more water than they need for their metabolism because plants lose a lot of water through evaporation and transpiration. Water is their internal means of transport.

The importance of water for trees is a cyclical story. Trees are essential to the water cycle. Transpiration through their leaves is the main mechanism by which the air is kept moist. This air flows inland from the ocean coasts. If plants did not give off so much water through transpiration, the interior of continents would be mostly desert land. There is a direct link between deforestation and desertification.

Among plants, trees are the most effective evaporators. Next to the oceans, trees make up the other half of the global system known as the water or rain cycle. An average tree can evaporate 100 litres of water per day through the huge surface area of its leaves.

Even close to the ocean, trees can be decisive for humidity and rain. When European settlers razed the forests full of tall trees from the island of Maui, the neighbouring, once entirely forested island of Kahoolawe quickly became a desert, as rain there came from the trees on Maui - not from the ocean surrounding both islands.

### Sensor type

Air humidity is measured by the sensor that also measures CO2 and air temperature. That sensor is suspended in the canopy of the tree.
