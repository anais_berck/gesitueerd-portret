## Sapflow sensor

![](/static/img/sensors/sapflow.jpg)

### Recognizable pattern

Sunlight & humidity
:   sap flow measurements reveal how sensitive trees are to their environment. A cloud, a rain shower, a sunny period, immediately affect the sap flow. You can see, for example, that the sap flows much faster on warm, sunny days - and more slowly on chilly, rainy days.

This has to do with photosynthesis: the more sunshine, the more sugars are produced and the greater the water flow from the roots to the canopy. Once at the top, the water helps produce sugars in the leaves and evaporates. The sugars are transported to the roots via a second sap stream.

Note that on too hot days, when the balance between sunlight and amount of water is not even, trees close their stomata (small openings on the back of the leaves), photosynthesis stops and the sap flow slows down.

### Why this sensor?

A tree's sap flow can be compared to our circulatory blood system, except that it flows in two directions: from the roots to the canopy (transport of water and minerals) and from the canopy to the roots (transport of sugars). The quantity of water is crucial for a large number of processes in plants: photosynthesis, transpiration, reproduction, growth, organ development, cell structure and cell function, and much more.

The amount and rate of sap flow also provide insight into the ability of trees to respond to soil water shortages, drought and extreme weather conditions.

### Type of sensor

Sap flow is calculated based on the velocity of the flow in the transport cells just beneath the bark of the tree. The sensor consists of three needles drilled into the trunk. The middle needle gives a heat signal. The needles above and below the heat signal measure the temperature, downstream and upstream of the heat source. The travel time of the heat signal is thus measured in two places. Water is a strong conductor. By measuring the time it takes for the heat signal to reach both positions, we can determine the speed of the heat signal.

The sensor then applies some mathematical formulas: the heat signal velocities are converted into sap flow densities and applied across the cross-section of the trunk to get an idea of the full sap flow. On online forums, developers are actively discussing which mathematical formulas give the best results.

We placed one sap flow sensor on the Douglas Fir because the sensor requires drilling three holes in the bark. Douglas firs produce resin quickly when their bark is injured, giving less opportunity for invasion by bacteria and fungi than old deciduous trees.
