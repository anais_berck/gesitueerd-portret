With 'Arnhemse Bomen Vertellen', you can experience the parks and their trees in an innovative way. You will enter into a relationship with the trees and become more aware of the natural and cultural heritage the parks have to offer. This art project was created by Anaïs Berck, a collective of people, trees and algorithms.

![](/static/img/trees/eik1-600x.jpg)

<!-- More information about: -->

- [the project](project/)
- [the web app](webapp/)
- [the sensors](sensors/)
- [the trees](trees/)
- [the benches](benches/)
- [the artists and all the people involved in the project](artists/)
- [the licence](licence/)
