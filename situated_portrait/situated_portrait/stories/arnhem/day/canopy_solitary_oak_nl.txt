Op warme zomeravonden strijken honderden jongeren neer in dit deel van het park.
Ze spelen, vleien, lachen, drinken, dansen.
Sommigen vinden zelfs een donker plekje om te paren.
Het is als een festival bij mijn wortels.
Hoog in mijn kruin vindt een ander festival plaats, dat van de eikenpages.
De mannetjesvlinders houden zich schuil tussen mijn bladeren.
De vrouwtjes vliegen hoog op zoek naar een partner.
De mannetjes fladderen achter de vrouwtjes aan en voeren luchtgevechten met rivalen.
Honderden vlinders duikelen en draaien om mijn bladeren heen, soms tot laat in de nacht.
En sommigen vinden in mijn kruin een donker plekje om te paren.

Ik ken vele bewoners en trouwe bezoekers.
Ik ben als het New York van Nederland, de boom waarin het grootst aantal verschillende soorten vogels, insecten, zwammen, mossen, dieren huisvesting en voedsel vindt. Als je mijn wortels, stam en kruin naar menselijke beelden zou vertalen, zou je de ene wolkenkrabber naast de andere zien.
Een nieuwkomer als de Plataan kent nauwelijks bezoekers of inwoners.
Maar een oude soort als ik, die hier al meer dan tienduizend jaar gedijt, is tot in de kleinste vezels verweven met al wat leeft.

De Gaaien zijn goeie vrienden van me. Misschien vind je wel een van hun blauwzwart gestreepte veren tijdens een bezoek aan het park.
Jullie mensen hebben hem de naam gegeven van de 'voortdurend krassende eikelzoeker'.
Hij is dan ook erg luidruchtig, maar dat is vooral in de herfst, wanneer hij mijn eikels verstopt in de grond voor de winter.
Met wat geluk vergeet hij ze weer op te graven. Zo kunnen mijn kleine nakomelingen opgroeien op de meest onverwachte plekken.

Deze lente had ik opnieuw het genoegen om een nest kraaien te huisvesten hoog in mijn takken.
Twijg per twijg voerden ze aan en verweefden ze tot een platform, dat ze verder met aarde en dikkere takken aansterken.
Jaar na jaar verrassen ze me ook met de materialen die ze vinden om hun nest te bekleden. Vroeger plukten ze stukjes wol uit de vacht van de schapen die verderop leefden. Nu vinden ze door jullie bewerkte wol, ijzerdraad en papier. En natuurlijk ook een hoop veren en gras, wat een bijzonder parfum verspreidt zo hoog in mijn kruin.

Bijzonder fier ben ik op de jonge vlier die een gunstig plekje heeft gevonden bij mijn stam.
Zij houdt van de vochtige aarde. Haar zaad belandde hier dankzij de Koolmees, een andere trouwe bezoeker, die een van mijn lagere takken graag gebruikt om er zijn behoefte te doen. Vlieren zijn grote beschermers tegen negatieve energieën en zwarte magie. Met de toename van het vermoeiende verkeerslawaai wachtte ik jarenlang geduldig op de komst van een vlier, om de sfeer wat te helpen verlichten. Ik moedig haar dan ook aan om snel te groeien en veel plaats in te nemen op en onder de grond.

Wij zijn lichtwezens.
Wij voeden ons met licht.
Onze cellen, ons hout, onze vruchten zijn gemaakt met zonlicht.
Telkens wanneer je iets plantaardig eet, weet dan dat je je voedt met licht.
Laat je verlichten!

Mijn vruchten worden al jarenlang niet meer geoogst.
Ooit was het anders.
Een nobele oude vrouw, die hier gisteren met korte pasjes passeerde,
herinnerde het zich nog.
Als kind kwam ze in de herfst dagelijks mijn eikels rapen.
Ze droogden en roosterden die,
vermaalden ze,
en maakten er een soort koffie van.
Die smaakte vreselijk.
Het was dan ook oorlog.

Ooit werden met mijn vruchten varkens vetgemest. Ze zijn zeer voedzaam en bevatten wel meer dan een derde vet. Op Eiken groeit het beste spek, is een oud gezegde, dat ik trouw bewaar in mijn celgeheugen. Dankzij de varkens genoten wij ook enkele privileges: we mochten niet gekapt worden, en wij Zomereiken die meer en grotere eikels aanmaken dan de Wintereik, werden veel meer aangeplant.

Zie je de jonge Lijsterbes bij de voet van mijn stam? Hij is een nieuwkomer. Hij groeit op eender welke grond en in eender welk weer. Omdat hij zo goed tegen de koude kan, was hij een van de eerste bomen die na de ijstijd in Nederland voorkwamen. Ik kijk uit naar de dag waarop hij bloemen en bessen zal dragen. Dan krijg ik nog meer vogels op bezoek, en jullie kunnen van de bessen heerlijke jam en thee maken.

Bij mijn stam groeit een plant met fijne witte bloemetjes: kleefkruid of ook wel plakgras, dé favoriet van kinderen in de zomer. De plant blijft vast zitten aan alles wat erlangs strijkt. Zijn stengel en vruchten zitten boordevol haakjes. Ze blijven hangen in de vacht van dieren en ook op jullie kleding. Zo verspreidt die slimme plant zijn vruchten over grote afstand.

Als je dichterbij durft te komen, zal je zien dat in het gras onder mijn kruin een massa klavertjes groeien. Daar ben ik dankbaar voor: klaver is in staat om stikstof uit de atmosfeer te halen via bacteriën in hun wortels. Het zijn de perfecte bodemverbeteraars. Per hectare kunnen ze 55 tot 170 kilogram stikstof toevoegen aan de bodem. Bovendien zijn er parkbezoekers die hun bloemetjes verzamelen voor een heerlijke thee.

Tussen de takken in mijn kruin leven heel wat spinnen. Hun weefkunst verbaast me dag na dag. Er zit er nu eentje ongeveer halverwege mijn bladerdak. Ongestoord spint ze een lange zijden draad en laat die meevoeren door de Wind naar een parallelle tak iets hogerop. Wanneer ze voelt dat de lijn stevig vastzit, glijdt ze sereen naar de overkant, waar ze de draad bijstelt, verstevigt en onverstoorbaar verder spint. Ze bouwt een fijn nieuw web dat zachtjes golft in de wind terwijl het groeit. Straks, wanneer het klaar is, vangt ze daar muggen en vliegen als lekkere hapjes. En in de draden registreert ze ook zowat alles wat hier in deze hoogte gebeurt. Ze is de archivaris van de ongewervelden.

Sommige van mijn gasten zit overdag verscholen tussen mijn bladeren. ‘S nachts komen ze tevoorschijn en vliegen op het licht af. Het zijn de Eikenbladrollers, kleine groene nachtvlindertjes. Na de winter, wanneer de rupsen uitkomen, kan het wel eens lastig worden. Ze vreten dan mijn jonge uitlopers aan. Later maken ze een inkeping in een van mijn bladeren, rollen zich erin en verpoppen daar. Eens ze vlinder zijn, slapen ze overdag nog in mijn bladeren. ‘s Nachts gaan ze uit. Dan verzamelen ze in grote groene zwermen.

Mag ik je aan mijn trouwe gasten voorstellen? De Eikelboorders zijn snuitkevers die mijn vruchten ombouwen tot kraamkamers. Ze hebben een lange snuit, waarmee ze een gaatje boren in mijn eikels. Daar legt het vrouwtje haar eitjes in. Het uitgekomen larfje voelt er zich veilig en vindt in mijn kiem ook heerlijk voedsel. Het larfje verpopt en na een tijdje zie ik uit mijn vrucht een nieuw kevertje verschijnen. Gelukkig kan ik eikels in overvloed aanmaken.

Zag je hier laatst mijn meimaand-gasten? De Meikevers vinden mijn bladeren verrukkelijk. Ten benijden zijn ze niet, hoor. Ze leven wel drie jaar onder de grond. Daar eten ze de wortels van grassen en soms knabbelen ze ook wel aan de mijne. Wanneer ze eenmaal verpoppen, leven ze nog een paar weken. En dan ben ik hun lievelingsrestaurant. Er zijn al jaren geweest dat ze het echt te bont maakten. Met zoveel waren ze, dat ik wel moest ingrijpen. Dan maak ik stoffen aan die ze niet lekker vinden. De Eik hier naast me ruikt dat dan en is op zijn beurt verwittigd. Zo zie je Meikevers wel eens migreren naar andere bomen.

Sinds een dertigtal jaren krijg ik ook bezoek van de Eikenprocessierups, die ontpopt tot een onopvallende bruine nachtvlinder. De rupsen kunnen gevaarlijk zijn, ook voor jullie, door de brandharen die ze als verdediging gebruiken. Sommige van mijn andere trouwe bezoekers, de Koolmezen, mispakken zich er soms wel aan. Als ze zo'n rups met hun korte snaveltje beetpakken, dan raakt hun kop de brandharen. Pijnlijk is dat. De bezoekers die wel goed met de rupsen om kunnen, zijn de Boomkruipers. Met hun lange, gebogen pincetsnavel grijpen ze de rupsen achter hun kop, schuren de haren eraf en eten ze op. 
