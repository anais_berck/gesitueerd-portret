# Banke und Stand

Floris van Hintum hat die Bänke im Park und den Stand am Molenplaats Sonsbeek aus lokalem Holz hergestellt. So haben auch diese Objekte ihre eigene Baumgeschichte.

## Bänke: Ulme & Robinie

![](/static/img/foto_bankje.jpg)

Die Stämme, auf denen die Bänke ruhen, stammen von den Robinien, die in der Parkstraat im Spijkerkwartier gefällt wurden. Die meisten Bäume waren krank. Die Kanalisation wird ersetzt und die Straße neugestaltet.

Die Bretter stammen von Ulmen, die vom Ulmensterben befallen sind. Die Folge ist das Welken der Äste und der Tod des Baumes. Die Krankheit wird durch Pilze verursacht, die in den Holzgefäßen des Baumes wachsen. Wenn sich der Pilz von einem Gefäß in ein anderes ausbreitet, verstopfen die Gefäße und der Baum stirbt. Um die Ausbreitung des Ulmensterbens zu verhindern, wurden diese Bäume 'geschält'.

Wenn dieses Projekt am 15. September 2024 endet, werden die Bänke in den öffentlichen Parks von Arnheim wiederverwendet. Wenn Sie Interesse daran haben, in Ihrer Nachbarschaft eine Bank aufzustellen, wenden Sie sich bitte an anais[att]anaisberck[dot]be.

## Stand: Linde

Der Stamm des Möbelstücks im Molenplaats Sonsbeek, auf dem ein Touchscreen montiert ist, stammt von einer Linde, die auf Onderlangs gegenüber der Artez-Kunstakademie stand. Wie schön, dass ein großer Teil des Baumes eine künstlerische Neuverwendung erfährt.
