# Dit project

### Bewegende bomen
Arnhemse bomen, we lopen er doorgaans gewoon voorbij. We vinden ze normaal en als we ze al opmerken, zien ze er hetzelfde uit als gisteren, als morgen en overmorgen. Onze huizen zijn gemaakt met hout. Bomen geven ons ook medicijnen, eten, warmte, zuurstof en we zoeken hun schaduw op tijdens hete zomers. Wist je dat een boom van uur tot uur beweegt? Dat kan je met ‘Arnhemse Bomen Vertellen' ervaren.

Tijdens ‘Arnhemse Bomen Vertellen’ sta je stil bij bomen, zoals bij een kunstwerk. In de drie parken leven zeer bijzondere en oude bomen. Aan de hand van sensoren die in zes bomen zijn geïnstalleerd, maakt ‘Arnhemse Bomen Vertellen’ zichtbaar wat anders verborgen blijft voor het menselijke oog: het bewegen van de stam, de CO2- en wateraanvoer, de temperatuur in de lucht en bij de wortels, de hoeveelheid zonlicht die de bomen helpt bij hun fotosynthese.

### Altijd anders
Van uur tot uur zie je veranderingen in de stam van de boom, waardoor die boom gaat leven. De installatie nodigt bezoekers dan ook uit om deze bijzondere bomen te waarderen als actieve, groeiende, veranderende levende wezens, waarmee je ook een relatie kan aangaan.

Bezoekers maken kennis met het intieme leven van de Allenige Eik in de Moerasweide, de Paardenkastanje bij de Witte Villa, de Boseik onderweg naar de Belvedère-toren, één van de Rode Beuken bij het Kasteel van Zypendaal, één van de Poortwachters bij Gulden Bodem en een Douglasspar in Schaarsbergen-Zuid.

### Bankjes met QR-code
Bij elk van de bomen is een houten bankje geplaatst met daarop een QR-code. Als je die scant, word je naar een webpagina geleid, waar verhaalfragmenten, een artistieke soundscape en digitale beelden je inzicht geven in het leven en de geschiedenis van de boom. Wat je te zien en te horen krijgt, hangt af van je locatie en van de data die de sensoren verzenden. Laat je verrassen, elk uur, elke dag.


## Praktisch

Het vertrekpunt is [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), waar je een kaart met de route vindt:

- wandeling van +-7km langs de 6 bomen
- wandeling van 4km langs 5 bomen, toegankelijk voor mindervaliden.

Voor regelmatige bezoekers van de parken bestaat de mogelijkheid om één of meerdere bomen dagelijks te ontmoeten, hen te leren kennen en nieuwe, levenslange vriendschappen te sluiten. Bezoekers die geen smartphone hebben of liever niet wandelen, kunnen de portretten van de zes bomen bezoeken via een touchscreen dat opgesteld staat in bezoekerscentrum Molenplaats Sonsbeek. Daar vind je ook een kleine tentoonstelling met artistieke visualisaties op basis van de data die de sensoren uitsturen.


## Credits

<small markdown>

Dit project wordt gerealiseerd door [Anaïs Berck](https://www.anaisberck.be/), een collectief van mensen, bomen en algoritmes:

- kunstenaars die meewerken aan dit project: An Mertens (concept, coördinatie, boomfluisteraar), Gijs de Heij & Doriane Timmermans (code, ontwerp & visualisaties - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (geluid), Bram Goots (foto’s), Loïc Lachaize (mastering), Floris van Hintum (bankjes/meubel), Patrick Lennon & Karin Ulmer (eindredactie EN/D)
Meer over de kunstenaars en de mensen die betrokken zijn bij dit project (link artists page)
- bomen die meewerken aan dit project: Poortwachter Kastanje, Rode Beuk Zijpendaal, Paardenkastanje Witte Villa, Allenige Eik Sonsbeek, Boseik Ronde Weide Sonsbeek, Douglasspar Schaarsbergen-Zuid
- algoritmes die meewerken aan dit project: Decentlab sapstroomsensor, zonlichtsensor, CO2 & temperatuursensor; EMS Brno banddendrometers, Sensoterra bodemvochtigheids- en temperatuursensors, Milesight CO2-sensors, SenseCap CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programmeertaal, html, css.

“Arnhemse Bomen Vertellen” is dankbaar voor de medewerking en ondersteuning van de gemeente Arnhem en Natuurcentrum Arnhem.



</small>
