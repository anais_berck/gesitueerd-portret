# Trees

<!-- Six trees of interest are participating in this project: -->

## Solitary Oak

![](/static/img/trees/eik1-600x.jpg)

This majestic Oak in Moerasweide is one of the main attractions of Sonsbeekpark. Strong, healthy and balanced. His sprawling branches grow far and wide. You can also recognize him by his grooved bark and lobed leaves.
The Solitary Oak offers us a mirror for self-development in all freedom. From his trunk, therefore, this tree shares story fragments about the Tree of Life and the Oak as an important symbol in our history. At night, he recites poems about Love.

## Horse Chestnut

![](/static/img/trees/european_horse_chestnut-600x.jpg)

You can recognize this tree near Witte Villa by her wounded trunk. She had a lot of uninvited visitors, fungi and bacteria. But you can see how healthy and vibrant her crown still is. And did you see her beautiful white flowers this spring?
The Horse Chestnut has lived at the White Villa for 188 years. Over the years, the villa's residents and the park's users changed. From her trunk, this tree shares story fragments about that history. At night, she recites poems about Death.

## Forest Oak

![](/static/img/trees/european_oak-600x.jpg)

This Oak, on the path to the Belvédère-tower, stands a little in the back, beyond the newly planted trees. He is 200 years old, like all the other big Oaks in this small forest. Don't confuse Oaks with Beeches. The latter have a grey smooth bark. And the Oak's crown has a far more sprawling and artistic pattern than the Beech's.
This Oak is old and lives very quietly. Together with his peers, he observes and reflects on the effects of climate change. The stories he shares from his trunk are stories about the future. At night, he recites poems about Silence.

## Red Beech

![](/static/img/trees/beuken-600x.jpg)

At Zijpendaal Castle, it looks like there is one giant Beech, but there are five. Their thin bark can’t tolerate the Sun. So they zip themselves up in a coat of leaves. The space between their trunks is like a temple, quiet and cool.
The research on Traces of Slavery recently published awakens old memories in this Red Beech. He has known Anna van Vossenburg personally, and shares those experiences from his trunk. At night, he gives you poems on Magic.

## Sweet Chestnut

![](/static/img/trees/3watchers-600x.jpg)

These three Chestnuts were planted three hundred and seventy-five years ago as a tribute to the Romans who brought the Sweet Chestnut to the North. They have been here almost as long as the Church of Eusebius. Rembrandt visited them. Once you have met them, they will gladly travel with you in your memory.
This Sweet Chestnut is a teacher of Time in all its aspects. From his cellular memory, he shares stories about Trees in world myths and traditions. At night, he recites poems about Time.

## Douglas Fir

![](/static/img/trees/douglas_fir-600x.jpg)

This Fir belongs to one of the most planted tree species in the Netherlands. Its wood was used to shore up tunnels in mines. You can recognize this tree by his cork-like bark and his pine cones with their funny mousetails at each scale.
From his heartwood, this Douglas Fir shares story fragments about planting and managing forests, about exotics like him that bring much profit. At night, he recites poems about Nature.
