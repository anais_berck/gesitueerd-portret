# Das Projekt

### Baum Bewegung

Normalerweise gehen wir einfach vorbei an den Bäumen von Arnheim. Sie kommen uns ziemlich gewöhnlich vor, und wenn wir sie überhaupt wahrnehmen, sehen sie immer gleich aus - ob gestern, heute oder morgen. Unsere Häuser sind aus Holz gebaut. Bäume geben uns auch Medizin, Nahrung, Wärme, Sauerstoff, und wir suchen ihren Schatten in heißen Sommern.

Bei 'Arnhemse Bomen Vertellen' begegnen Sie den Bäumen wie einem Kunstwerk. Die drei Parks beherbergen ganz besondere und alte Bäume. Mit Hilfe von Sensoren, die in sechs Bäumen installiert sind, macht "Arnhemse Bomen Vertellen" sichtbar, was dem menschlichen Auge sonst verborgen bleibt: die Bewegungen des Stammes, die CO2- und Wasserversorgung, die Temperatur in der Luft und an den Wurzeln sowie die Menge an Sonnenlicht, die den Bäumen bei der Photosynthese hilft.

### Immer anders

Von Stunde zu Stunde kann man Veränderungen im Stamm des Baumes beobachten. Die Installation lädt dazu ein, diese besonderen Bäume als aktive, wachsende, sich verändernde Lebewesen zu betrachten - Wesen, denen man begegnen und auch eine Beziehung eingehen kann.

Sie lernen das intime Leben der Einsamen Eiche in der Moerasweide, der Rosskastanie bei der Witte-Villa, der Waldeiche auf dem Weg zum Belvedere-Turm, einer der Rotbuchen beim Schloss Zijpendaal, eines der Pförtnerkastanie am Gulden Bodem und einer Douglasie in Schaarsbergen-Zuid kennen.

### QR-Code auf der Bank

In der Nähe eines jeden Baumes wurde eine Holzbank mit einem QR-Code aufgestellt. Diese Bänke sind aus Holz von Bäumen aus Arnheim gefertigt. Wenn Sie den QR-Code scannen, werden Sie auf eine Webseite weitergeleitet, auf der Fragmente von Geschichten, eine künstlerische Klanglandschaft und digitale Bilder einen Einblick in das Leben und die Geschichte des Baumes geben. Was Sie zu sehen und zu hören bekommen, hängt von Ihrem Standort und den von den Sensoren gesendeten Daten ab. Lassen Sie sich überraschen - jede Stunde, jeden Tag neu.

## Praktisch

Der Startpunkt ist [Molenplaats Sonsbeek](https://molenplaatssonsbeek.nl/), wo Sie einen Papierabzug mit der Route finden:

- eine 7 km lange Wanderung entlang der sechs Bäume
- ein 4 km langer Spaziergang entlang von fünf Bäumen, der auch für Behinderte zugänglich ist

Regelmäßige Besucher der Parks haben die Möglichkeit, täglich einen oder mehrere Bäume zu treffen, sie kennenzulernen und neue, lebenslange Freundschaften zu schließen. Besucher, die kein Smartphone haben oder nicht laufen möchten, können die Porträts der sechs Bäume über den Touchscreen im Besucherzentrum am Molenplaats Sonsbeek kennenlernen. Hier gibt es auch eine kleine Ausstellung mit künstlerischen Visualisierungen auf der Grundlage der von den Sensoren gesendeten Daten.

## Impressum

<small markdown>

Dieses Projekt wurde von [Anaïs Berck](https://www.anaisberck.be/), einem Kollektiv aus Menschen, Bäumen und Algorithmen, realisiert.

- Künstler, die an diesem Projekt mitarbeiten: An Mertens (Konzept, Koordination, Baumgriffier), Gijs de Heij & Doriane Timmermans (Code, Design & Visualisierungen - [Open Source Publishing](https://osp.kitchen/)), Christina Clar (Ton), Bram Goots (Fotos), Loïc Lachaize (Mastering), Floris van Hintum (Bänke/Stand), Patrick Lennon & Karin Ulmer (Korrekturlesen)

Mehr über die [Künstler und andere Menschen, die an diesem Projekt mitgearbeitet haben](artists/)

- An diesem Projekt beteiligte Bäume: Pförtnerkastanie, Rotbuche Zijpendaal, Rosskastanie Witte Villa, Einsame Eiche Sonsbeek, Waldeiche Ronde Weide Sonsbeek, Douglasie Schaarsbergen-Zuid

- Algorithmen, die an diesem Projekt mitarbeiten: Decentlab Saftstromsensor, Sonnenlichtsensor, CO2- und Temperatursensor; EMS Brno Bandmessgeräte, Sensoterra Bodenfeuchte- und Temperatursensoren, Milesight CO2-Sensoren, SenseCap CO2-Sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

‘Arnhemse Bomen Vertellen’ bedankt sich für die Zusammenarbeit und Unterstützung der Gemeinde Arnheim und des Natuurcentrum Arnheim.

</small>
