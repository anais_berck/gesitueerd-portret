Gesitueerd Portret is een artistiek onderzoeksproject dat stil wil staan bij bomen, zoals bij een kunstwerk. Aan de hand van sensoren die we in bomen plaatsen, proberen we op een artistieke manier zichtbaar te maken wat anders verborgen blijft voor het menselijke oog: het bewegen van de stam, de CO2- en wateraanvoer, de temperatuur in de lucht en bij de wortels.

Met de steun van de Vlaamse Overheid, Kunsten & Erfgoed.

![](/static/img/trees/beuk.jpg)

<!-- Meer informatie: -->

- [Over dit project](project/)
- [Over de webapp](webapp/)
- [Over de sensors](sensors/)
- [Over de boom](trees/)
- [Over de kunstenaars en alle mensen die betrokken werden bij dit project](artists/)
- [Over de licentie](licence/)

<!--
    note for An:
    in between parentheses is the name of the next subfolder you want to go into
-->
