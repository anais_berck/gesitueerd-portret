from django.contrib.staticfiles import finders
import glob
import os
from django.templatetags.static import static

"""
    Uses the folder structure / mapping as defined in settings to discover
    mp3-files for the sensor and the trees in the static folder.
"""
def discover_sound_files (folders):
    data = {}

    # Loop throught categories, like trees, sensors
    for category, category_folders in folders.items():
        data[category] = {}

        # Loop through the indivual trees and sensors
        for key, foldername in category_folders.items():
            # Try to find its folder and search for mp3's in there
            local_folder = finders.find(foldername)

            if local_folder:
                mp3_files = glob.glob(os.path.join(local_folder, '*.mp3'))
                data[category][key] = [static(os.path.join(foldername, os.path.basename(mp3_file))) for mp3_file in mp3_files]

    return data
