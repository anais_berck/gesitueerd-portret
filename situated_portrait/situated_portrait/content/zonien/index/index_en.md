## Meet a remarkable Beech

From 3 till 29 September 2024, you can meet a remarkable Beech in the Sonian Forest in a unique way. What stories did a tree clerck pick up? Does the Beech grow every day? What is normally hidden from us can now be discovered. This installation is a creation by [Anaïs Berck](https://www.anaisberck.be/), a collective of humans, trees and algorithms.

### Moving trees

We usually just walk past trees. They seem pretty regular to us and if we notice them at all, they always look the same – whether yesterday, today or tomorrow. Our houses are made with wood. Trees also give us medicine, food, warmth, oxygen, and we seek their shade during hot summers.

During this installation, you will meet a tree as you meet a work of art. Using sensors installed in the Beech, this installation makes visible what is otherwise hidden from the human eye: the movements of the trunk, the CO2 and water supply, the temperature in the air and at the roots.

### Always different

From hour to hour, you can see changes in the trunk of the tree. The installation invites you to appreciate this special Beech as an active, growing, changing living being – a being you can also enter into a relationship with.

### QR code

Near the Beech there is picnic bench with a QR code on it. By scanning the QR code, you will be guided to a web page where story fragments and artistic images will provide insight into the life and history of the tree. What you get to see depends on the data sent by the sensors. Let yourself be surprised – every hour, every day.

## Practical

You find the beech opposite the picnic bench on Graafdreef/Drève du Comte, just past the intersection of Tumuliweg/Chemin des Tumuli, Sint-Hubertusdreef/Drève St-Hubert and Groene Spechtweg/Chemin du Pivert: [location](https://www.openstreetmap.org/#map=18/50.77950/4.41103)

## Credits

<small markdown>

This project was realized by [Anaïs Berck](https://www.anaisberck.be/), a collective of people, trees and algorithms:

- Artists collaborating on this project: An Mertens (concept, coordination, tree whisperer), Gijs de Heij & Doriane Timmermans (code, design & visualizations - [Open Source Publishing](https://osp.kitchen/))
- Trees participating in this project: beech (Fagus sylvatica)
- algorithms collaborating on this project: EMS Brno tape dendrometer, Sensoterra soil moisture and soil temeprature sensor, Milesight CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

'Situated Portrait' is is possible thanks to the financial support of Vlaamse Overheid, Kunsten & Erfgoed. This presentation moment received the logistical support of Leefmilieu Brussel/Bruxelles Environnement and the GC Wabo team in Bosvoorde/Boitsfort.

Situated Portrait Sonian, by Anaïs Berck, Arnhem, August 2024.
All code, images, sounds and texts are published to a Creative Commons Attribution 4.0 licence (CC BY 4.0, <https://creativecommons.org/licenses/by/4.0/>). You may reuse, distribute, reproduce, and adapt this work in any medium, except for commercial purposes, provided you give attribution to the copyright holder, provide a link to the Creative Commons licence, and indicate if changes have been made.

All code, images, sounds and texts have also been released under a CC4r licence, available here: <https://constantvzw.org/wefts/cc4r.en.html>.

The data captured during this work is published to a Creative Commons 1.0 licence (CC0 1.0, <https://creativecommons.org/publicdomain/zero/1.0/>). You can copy, modify, distribute and perform the data, even for commercial purposes, all without asking permission.

</small>
