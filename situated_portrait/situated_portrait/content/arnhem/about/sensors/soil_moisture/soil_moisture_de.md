## Feuchtigkeit des Bodens

![](/static/img/sensors/sensoterra-600x.jpg)

### Erkennbare Muster

Bodenart
:   Die Einsame Eiche ist in den sehr feuchten Böden der Moerasweide verwurzelt, wo das Wasser aus der Hoge Veluwe gesammelt wird.

Regen
:   Je mehr Regen fällt, desto feuchter ist der Boden. Die anhaltenden Regenfälle des letzten Winters und Frühjahres haben im Laufe der Zeit zu außergewöhnlich flachen Linien in den Grafiken geführt.

Heiß und trocken
:   An heißen Tagen ist der Boden weniger feucht.

### Feuchtigkeit des Bodens & Baume

Bäume brauchen viel Wasser - und Mineralien - um durch Photosynthese Zucker zu produzieren (siehe CO2-Sensor). Das Wassermanagementsystem der Parks, das von der Seitenmoräne und dem durch die Bodenschichten der Hoge Veluwe gereinigten Wasser mitbestimmt wird, sorgt dafür, dass die beteiligten Bäume gut mit mineralreichem Wasser versorgt werden. Außerdem legt die Gemeinde hier und da bewaldete Dämme an, um den zu schnellen Abfluss des Wassers in die Moerasweide und Sint-Jansbeek zu verlangsamen.

Die Bäume transportieren das Wasser über das Xylem von den Wurzeln zur Krone. Xylem sind Holzgefäße direkt unter der Rinde, die nach jeder Jahreszeit zu Totholz werden und die Jahresringe des Baumes bilden. An einem heißen Sommertag kann zum Beispiel eine Eiche bis zu 800 Liter Wasser pro Tag transportieren.

### Sensortype

Die Bodenfeuchtigkeit wird mit Sensoren des niederländischen Start-ups Sensoterra gemessen. Sie messen die Bodenfeuchtigkeit in einer Tiefe von 30 cm.

Die Sensoren werden an der "Tropflinie" der Bäume angebracht. Das ist die gedachte Linie, die entlang der äußersten Blätter der Krone verläuft. Entlang der Tropflinie wachsen die lebendigsten Oberflächenwurzeln.
