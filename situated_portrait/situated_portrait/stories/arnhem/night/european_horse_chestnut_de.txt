Ein Samenkorn fiel auf die Erde, 
die Leute fragten, was es werde, 
ein grüner Sproß schoß bald hervor, 
und wuchs gen Himmel schnell empor. 
Gegen ihn wurd' früh entschieden,
 im Wind soll sich der Baum nicht wiegen,
 für ihn gab's keine friedliche Ruhe, 
er wurde eine Wäschetruhe. 
Die Motorsäge nahm ihm den Saft, 
die Hobelbank die letzte Kraft, 
schon formte man aus seinem Fleisch, 
ein hohles Ding zu hohem Preis. 
Als letztes kam die größte Pein, 
doch für die Optik muß das sein, 
die zarte Haut gequält mit Beize,
 entlockt man ihm die letzten Reize. 
Nun steht er da in großer Wut, 
gefüllt mit lauter fremden Gut, 
das eigene Ich ward ihm genommen, 
dafür hat er ja Stil bekommen. 
Denkt man zurück an ihn als Baum, 
und an den zarten Blätterflaum, 
und an die Nester, die er barg, 
weil er so zärtlich, aber stark.
___ Petra Ewering, 1962

Ich möchte in dir hochwellen 
Grüner Baum! 
Ich möchte treibfroh in deinen Markzellen 
Aufschwellen 
Bis in den Wipfeltraum 
Lichtoben – 
Ich möchte in die Lichtweiten
Hundert Arme breiten 
Wie Zweige – 
Armzweige mit Blätterfingern 
Und dann fühlen wie Mittagsgluten, 
Wie Lichtfluten 
Durch sie schlingern –
Ich möchte aus deinem Wirbelkopf, 
Lebensbaum, 
aus dem Laubtraum 
Wie Lichtgetropf, 
Wie Windsingen 
Mich aufschwingen In den Weltraum!
___ Gerit Engelke

Ich bin der Wald 
Ich bin uralt 
Ich hege den Hirsch
Ich hege das Reh 
Ich schütz Euch vor Sturm 
Ich schütz Euch vor Schnee
Ich wehre dem Frost 
Ich wahre die Quelle 
Ich hüte die Scholle 
Bin immer zur Stelle 
Ich bau Euch das Haus 
Ich heiz Euch den Herd 
Drum ihr Menschen 
Haltet mich wer
___ Unbekannt

Mietegäste vier im Haus 
Hat die alte Buche. 
Tief im Keller wohnt die Maus, 
Nagt am Hungertuche. 
Stolz auf seinen roten Rock 
Und gesparten Samen 
Sitzt ein Protz im ersten Stock; 
Eichhorn ist sein Namen. 
Weiter oben hat der Specht 
Seine Werkstatt liegen, 
Hackt und zimmert kunstgerecht, 
Daß die Späne fliegen. 
Auf dem Wipfel im Geäst 
Pfeift ein winzig kleiner 
Musikante froh im Nest. 
Miete zahlt nicht einer.
___ Rudolf Baumback

Es wächst ein Baum 
Und seine Äste greifen 
Kühn in den Raum, 
Daß Sterne seinen Wipfel streifen. 
Er weiß es, daß 
An seinem Wurzelende 
Der Neid, der Haß 
Sich reichen brüderlich die Hände 
Und mit dem Dorn 
Der Schelsucht ihn verwunden. 
Jedoch kein Zorn 
Stiehlt ihm das Glück der frohen Stunden. 
Er lächelt bloß 
In ruhigem Verstehen: 
Dies ist das Los
 Von allen, die in Sterne sehen
___ Alfons Petzold

