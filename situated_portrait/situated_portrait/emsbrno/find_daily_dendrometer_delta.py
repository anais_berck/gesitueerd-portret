from situated_portrait.models import Observation
from django.db.models import Max, Min, Window, DateField, F
from django.db.models.functions import FirstValue, LastValue, Cast

window = {
    'partition_by': 'date',
    'order_by': 'date'
}

def select_min_max_per_day (sensor_pk, since=None):
  # Select min, max, first and last values for observations attached to a sensor
  # use windowing
  # Separate annotations: first to set the date, then the aggregate functions is necessary

  qs = Observation.objects.filter(sensor__pk=sensor_pk)

  if since:
    qs = qs.filter(timestamp__gte=since)

  return qs.distinct()\
    .annotate(date=Cast('timestamp', DateField()))\
    .annotate(
      min=Window(Min("value"), **window),
      max=Window(Max("value"), **window),
      first=Window(FirstValue('value'), **window),
      last=Window(LastValue('value'), **window),
    )\
    .annotate(
      delta=F('max')-F('min'),
      growth=F('last')-F('first'),
    ).values('date', 'first', 'last', 'min', 'max', 'delta', 'growth')