from django.contrib import admin  
from situated_portrait.models import Observation, Sensor, Device, DeviceType, Tree, Installation
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
import datetime
from django import forms

from django.urls import reverse
from django.utils.html import format_html

# admin.site.register(Observation)
# admin.site.register(Sensor)
# admin.site.register(Device)
admin.site.register(DeviceType)
admin.site.register(Installation)
# admin.site.register(Tree)

class TreeFilter (admin.SimpleListFilter):
  title = _('Tree')
  parameter_name = 'tree'

  def lookups(self, request, model_admin):
    return ((tree.pk, str(tree)) for tree in Tree.objects.all())
  
  def queryset(self, request, queryset):
    if self.value():
      return queryset.filter(sensor__device__tree__pk=self.value())
    else:
      return queryset

class TimestampFilter(admin.SimpleListFilter):
  # Human-readable title which will be displayed in the
  # right admin sidebar just above the filter options.
  title = _('from the')

  # Parameter for the filter that will be used in the URL query.
  parameter_name = 'timestamp'

  def lookups(self, request, model_admin):
    return (
      ('1h', _('last hour')),
      ('2h', _('last 2 hours')),
      ('6h', _('last 6 hours')),
      ('12h', _('last 12 hours')),
      ('24h', _('last day')),
      ('48h', _('last 2 days')),
      ('1w', _('last week')),
      ('2w', _('last 2 weeks')),
      ('3w', _('last 3 weeks')),
      ('1m', _('last month')),
      ('2m', _('last 2 months')),
      ('6m', _('last 6 months')),
      ('12m', _('last year')),
    )

  def queryset(self, request, queryset):
    # Compare the requested value (either '80s' or '90s')
    # to decide how to filter the queryset.
    deltas = {
      '1h': datetime.timedelta(hours=1),
      '2h': datetime.timedelta(hours=2),
      '6h': datetime.timedelta(hours=12),
      '12h': datetime.timedelta(hours=12),
      '24h': datetime.timedelta(days=1),
      '48h': datetime.timedelta(days=2),
      '1w': datetime.timedelta(weeks=1),
      '2w': datetime.timedelta(weeks=2),
      '3w': datetime.timedelta(weeks=3),
      '1m': datetime.timedelta(weeks=4),
      '2m': datetime.timedelta(weeks=8),
      '6m': datetime.timedelta(weeks=26),
      '12m': datetime.timedelta(weeks=52)
    }

    key = self.value()

    if key in deltas:
      delta = deltas[key]
      return queryset.filter(timestamp__gte=(timezone.now() - delta))
    else:
      return queryset


@admin.register(Observation)
class ObservationAdmin (admin.ModelAdmin):
  list_filter = [TimestampFilter, ("sensor__device__tree", admin.RelatedOnlyFieldListFilter), ("sensor", admin.RelatedOnlyFieldListFilter)]
  ordering = ("-timestamp",)

@admin.register(Tree)
class TreeAdmin (admin.ModelAdmin):
  readonly_fields = ('list_devices', 'graph_sensors')

  def list_devices (self, tree):
    if tree and tree.pk:
      links = []

      for device in tree.devices.all():
        links.append(mark_safe(format_html('<li><a href="{}">{}</a></li>', reverse('admin:situated_portrait_device_change', args=[device.pk]), device)))
    
      return mark_safe('<ul style="margin-left:0">{}</ul>'.format(''.join(links)))
    else:
      return 'Devices are visible after creation'  

  list_devices.short_description = 'Devices linked to this tree'

  def graph_sensors (self, tree):
    sensor_pks = [o['pk'] for o in Sensor.objects.filter(device__tree=tree).values('pk')]
    if sensor_pks:
      return format_html('<a href="{}">Graph for this tree</a>', reverse('multigraph', kwargs={ 'sensor_pk_string': ','.join(map(str, sensor_pks)), 'days': 7 } ))
    else:
      return 'Tree does not have any linked devices.'
  
  graph_sensors.short_description = 'Graph of sensors'



# @admin.register(DeviceType)
# class DeviceTypeAdmin (admin.ModelAdmin):
#   readonly_fields = ('list_sensors','graph_sensors')

#   def list_sensors (self, device):
#     if device and device.pk:
#       links = []

#       for sensor in device.sensors.all():
#         links.append(mark_safe(format_html('<li><a href="{}">{}</a></li>', reverse('admin:situated_portrait_sensor_change', args=[sensor.pk]), sensor)))
    
#       return mark_safe('<ul style="margin-left:0">{}</ul>'.format(''.join(links)))
#     else:
#       return 'Sensors are visible after creation'

#   list_sensors.short_description = 'Sensors of this device'



#   def graph_sensors (self, device):
#     sensor_pks = [o['pk'] for o in Sensor.objects.filter(device=device).values('pk')]
#     return format_html('<a href="{}">Graph for this device</a>', reverse('multigraph', kwargs={ 'sensor_pk_string': ','.join(map(str, sensor_pks)), 'days': 7 } ))
  
#   graph_sensors.short_description = 'Graph of sensors'



@admin.register(Device)
class DeviceAdmin (admin.ModelAdmin):
  list_display = ['__str__', 'active']
  readonly_fields = ('list_sensors','graph_sensors')

  def list_sensors (self, device):
    if device and device.pk:
      links = []

      for sensor in device.sensors.all():
        links.append(mark_safe(format_html('<li><a href="{}">{}</a></li>', reverse('admin:situated_portrait_sensor_change', args=[sensor.pk]), sensor)))
    
      return mark_safe('<ul style="margin-left:0">{}</ul>'.format(''.join(links)))
    else:
      return 'Sensors are visible after creation'

  list_sensors.short_description = 'Sensors of this device'



  def graph_sensors (self, device):
    if device and device.pk:
      sensor_pks = [o['pk'] for o in Sensor.objects.filter(device=device).values('pk')]
      if sensor_pks:
        return format_html('<a href="{}">Graph for this device</a>', reverse('multigraph', kwargs={ 'sensor_pk_string': ','.join(map(str, sensor_pks)), 'days': 7 } ))
      else:
        return 'Device does not have sensors yet.'
    else:
      return ''
    
  graph_sensors.short_description = 'Graph of sensors'


@admin.register(Sensor)
class SensorAdmin (admin.ModelAdmin):
  readonly_fields = ('last_observations','all_observations','graph_sensor','graph_similar_sensors')
  
  # Read only fields
  def last_observations (self, sensor):
    if sensor and sensor.pk:
      observations = sensor.observations.order_by('-timestamp')[:25]
      observation_snippets = [f'{observation.timestamp}: {observation.value}' for observation in observations]
      # observation_data = {}

      # for observation in observations:
      #   if sensor.unit not in observation_data:
      #     observation_data[sensor.unit] = []

      #   observation_data[observation.unit].append({
      #     'timestamp': observation.timestamp.timestamp(),
      #     'value': observation.value
      #   })

      return mark_safe('<br>'.join(observation_snippets))
    else:
      return 'Observations are visible after creation'

  def all_observations (self, sensor):
    if sensor and sensor.pk:
      return format_html('<a href="{}?sensor__id__exact={}">All observations</a>', reverse('admin:situated_portrait_observation_changelist'), sensor.pk)
    else:
      return 'Observations are visible after creation'

  last_observations.short_description = 'The last 25 observations collected for this sensor'


  def graph_sensor (self, sensor):
    if sensor and sensor.pk:
      return format_html('<a href="{}">Graph for this sensor</a>', reverse('multigraph', kwargs={ 'sensor_pk_string': str(sensor.pk), 'days': 7 } ))
    else:
      return ''

  graph_sensor.short_description = 'Graph for this sensor'

  
  def graph_similar_sensors (self, sensor):
    if sensor and sensor.pk:
      sensor_pks = [o['pk'] for o in Sensor.objects.filter(unit=sensor.unit).values('pk')]
      return format_html('<a href="{}">Graph</a>', reverse('multigraph', kwargs={ 'sensor_pk_string': ','.join(map(str, sensor_pks)), 'days': 7 } ))
    else:
      return ''
    
  graph_similar_sensors.short_description = 'Graph of similar sensors'


