Ich sah den Wald im Sonnenglanz, 
Vom Abendrot beleuchtet, 
Belebt von düstrer Nebel Tanz, 
Vom Morgentau befeuchtet: 
Stets blieb er ernst, stets blieb er schön, 
Und stets mußt' ich ihn lieben. 
Die Freud' an ihm bleibt mir besteh'n, 
Die andern all zerstieben. 
Ich sah den Wald im Sturmgebraus, 
Vom Winter tief umnachtet, 
Die Tannen sein in wirrem Graus, 
Vom Nord dahingeschlachtet; 
Und lieben mußt' ich ihn noch mehr, 
Ihn meiden könnt' ich nimmer. 
Schön ist er, düsterschön und hehr, 
Und Heimat bleibt er immer. 
Ich sah mit hellen Augen ihn, 
Und auch mit tränenvollen; 
Bald sänftigt' er mein Grollen. 
In Sommersglut, in Winterfrost, –
 Konnt' er mir mehr nicht geben, – 
So gab er meinem Herzen Trost; 
Und drum: Mein Wald, mein Leben!
___ Emerenz Meier

Was weckt denn Wehschrei? was verdroß 
Euch Raben? hab‘ ich es gefunden? 
Gefällt, ein wahrer Waldkoloß, 
Liegt hier ein Eichbaum, abgeschunden, 
Sein Riesenastwerk ohne Rinde, 
Entsetzlich mir, dem Menschenkinde. 
Was Wunder, daß der Schrecken packt 
Des Waldes wanderfrohe Raben,
 Sehn sie den Alten tot und nackt, 
Bei dem sie oft geherbergt haben!
___ Karl Mayer

leise spielt der Wind
in den Zweigen
flüstert mir zu 
alte Geschichten 
seltsame Erinnerungen 
ich lege meine Hand 
sachte auf die Rinde 
unter meiner Haut 
atmet er Zärtlichkeit
___ Anke Maggauer-Kirsche, 1948

Den Ebnen gibst du Schatten, ernste Eiche, 
Und steilen Höhn, doch lieb ich dich nicht mehr, 
Seit du Zerstörern vieler Stadt und Reiche 
Geschmückt mit grünen Zweigen Helm und Wehr. 
Noch bist du, eitler Lorbeer, mein Begehr; 
Seis, daß dein Laub das winterliche bleiche 
Gefild beschäme, oder daß zur Ehr 
Es kahler Römerkaiser Stirn gereiche. 
Dich, Rebe, lieb ich, die aus braunem Stein 
So üppig lacht und reift, mir zu bereiten 
Des weisen Selbstvergessens Lebenstrank; 
Mehr noch lieb ich die Tanne: glatt und blank 
Schließt sie einst in vier Bretter all mein Streiten
Und Grübeln, all mein eitles Wünschen ein.
___ Giosue Carducci

Die hohen dichtgedrängten Wälder thronen 
Auf Hügeln sanft gewölbt und abgedacht – 
In Heimatschwermut rauschen ihre Kronen. 
Sie sind erfüllt von Flucht und Wetterweben 
Der zündenden Gewölke, die bei Nacht 
Mit schwerem Flügelschlage drüber schweben. 
Zu ihren Füßen, wo die breiten Pflüge 
Gleichmäßig Furchen ziehn im Ackerland,
Baut still ein enges Dasein sich Genüge. 
Und von der Spanne Leben und dem Sterben 
Webt Jahr um Jahr geheimnisvoll ein Band 
Zu ihrem Blätterprangen und Verfärben.
___ Hedwig Lachmann
