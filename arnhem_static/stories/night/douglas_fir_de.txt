Wer einen Baum pflanzt,
wird den Himmel gewinnen.
___ Konfuzius

Es grünen die Bäume des Waldes,
Es kündigt der Frühling sich an,
Hinweg mit dem frostigen Winter,
Der Frühling ist ein sanfter Mann!
Die langen goldnen Strahlen,
Sie sind wie ein langes Haar!
Die Veilchen im tiefen Grase
Sind blau, wie ein Augenpaar!
___ Friederike

Entblößt, wohl langsam,
Baumriese um Baumriese
Weißes Gestänge, löst sich
Die geknopft-gefurchte Rinde,
Bis der letzte Zweig stirbt.
Leichnam um Leichnam
Wird, gefällt und verbrannt
Von uns, ihren Bestattern.
___ Michael Hamburger

Ehe die Farben sich trüben, 
Verschwimmen Baumform und Blattbild. 
Die bunten Grüne der Farne 
Verschmelzen als erste im Halblicht, 
Scharlach, Malve, Blau harren aus. 
Die Nachtkerze 
Den ganzen Tag schlaff 
Öffnet vierblättrige Blüten
Eines so reinen Gelb, 
Sie erstrahlen, sie scheinen
Als hätte die Erde ein Licht 
Das sie nicht Sonne, Mond, Sternen verdankt.
___ Michael Hamburger

Ich lebe mein Leben in wachsenden Ringen,
die sich über die Dinge ziehn.
Ich werde den letzten vielleicht nicht vollbringen,
aber versuchen will ich ihn.
Ich kreise um Gott, um den uralten Turm,
und ich kreise jahrtausendelang;
und ich weiß noch nicht: bin ich ein Falke, ein Sturm
oder ein großer Gesang. 
___ Rainer Maria Rilke
