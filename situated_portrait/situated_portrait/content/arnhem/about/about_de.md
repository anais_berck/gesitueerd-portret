Mit 'Arnhemse Bomen Vertellen' können Sie die Parks und ihre Bäume auf eine neuartige Weise erleben. Sie treten in eine Beziehung zu den Bäumen und werden sich des natürlichen und kulturellen Erbes, das die Parks zu bieten haben, bewusst. Dieses Kunstprojekt wurde von Anaïs Berck entwickelt, einem Kollektiv aus Menschen, Bäumen und Algorithmen.

![](/static/img/trees/eik1-600x.jpg)

<!-- Mehr Informationen über: -->

- [das Projekt](project/)
- [die Webanwendung](webapp/)
- [die Sensoren](sensors/)
- [die Baume](trees/)
- [die Bänke](benches/)
- [die Künstler und alle an dem Projekt beteiligten Personen](artists/)
- [die Lizenz](licence/)
