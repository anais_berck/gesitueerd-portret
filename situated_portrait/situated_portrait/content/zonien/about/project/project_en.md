# The project

### Moving trees

We usually just walk past trees. They seem pretty regular to us and if we notice them at all, they always look the same whether yesterday, today or tomorrow. Our houses are made with wood. Trees also give us medicine, food, warmth, oxygen and we seek their shade during hot summers.

During this installation, you meet a tree as you meet a work of art. The Sonian Forest/Forêt de Soignes is home to special and old trees. Using sensors installed in this beech tree, 'Situated Portrait' makes visible what is otherwise hidden from the human eye: the mouvements of the trunk, the CO2 and water supply, the temperature in the air and at the roots.

### Always different

From hour to hour, you can see changes in the trunk of the tree. The installation invites you to appreciate these special trees as active, growing, changing living beings, beings you can also enter into a relationship with.

### QR code on bench

Near the tree there is a picnic bench, installed with a QR code. By scanning the QR code you will be guided to a web page where story fragments and artistic digital images will provide insight into the life and history of the tree. What you get to see and hear depends on the data sent by the sensors. Let yourself be surprised every hour, every day.

## Practical

You find the beech opposite the picnic bench on Graafdreef/Drève du Comte, just past the intersection of Tumuliweg/Chemin des Tumuli, Sint-Hubertusdreef/Drève St-Hubert and Groene Spechtweg/Chemin du Pivert: [location](https://www.openstreetmap.org/#map=18/50.77950/4.41103)


## Credits

<small markdown>

This project was realized by [Anaïs Berck](https://www.anaisberck.be/), a collective of people, trees and algorithms:

- Artists collaborating on this project: An Mertens (concept, coordination, tree whisperer), Gijs de Heij & Doriane Timmermans (code, design & visualizations - [Open Source Publishing](https://osp.kitchen/))
More about the artists and other humans who collaborated in the project (link artists page)
- Trees participating in this project: beech (Fagus sylvatica)
- algorithms collaborating on this project: EMS Brno tape dendrometer, Sensoterra soil moisture and soil temeprature sensor, Milesight CO2-sensor; django, leaflet, sqlite, D3 javascript module, Python programming language, html, css.

'Situated Portrait' is is possible thanks to the financial support of Vlaamse Overheid, Kunsten & Erfgoed. This presentation moment received the logistical support of Leefmilieu Brussel/Bruxelles Environnement and the GC Wabo team in Bosvoorde/Boitsfort.

</small>
