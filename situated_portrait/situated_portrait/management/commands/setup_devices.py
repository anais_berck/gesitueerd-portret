from django.core.management.base import BaseCommand
from situated_portrait.models import DeviceType, Device, Sensor
from situated_portrait.decentlab.discover_device_sensors import discover_and_add_device_sensors as decentlab_discover_and_add_device_sensors
from django.conf import settings
import requests
from django.db.utils import IntegrityError
import os
import json

# COMMON
# ====================================================================

def add_sensor(device, type, unit, synthetic=False):

  # --- check if exist
  if not Sensor.objects.filter(device=device, type=type).exists():

    sensor = Sensor(
      device = device,
      type = type,
      unit = unit,
      synthetic=synthetic
    )
    try:
      sensor.save()
    except IntegrityError:
      print('Tried to store double sensor')

    print(f'  new sensor added {sensor}')


# SENSOTERRA
# ====================================================================

def add_sensoterra(probe, device_type):

  # --- check if exist
  if not Device.objects.filter(serial_number = probe['serial']).exists():
  
    # --- add it to the DB if not there
    device = Device(
      latitude = probe['latitude'],
      longitude = probe['longitude'],
      deviceType = device_type,
      remote_identifier = probe['id'],
      serial_number = probe['serial']
    )
    try:
      device.save()
    except IntegrityError:
      print('Tried to store double device')

    print(f'new device added {device}')

  else:
    device = Device.objects.filter(serial_number = probe['serial'])[0]

  # --- create each sensors associated to a sensoterra device (if doesn't exist)
  add_sensor(device, "temperature", "°C")
  add_sensor(device, "moisture", "%")
  add_sensor(device, "sensoterra index", "SI")


def add_all_sensoterra(device_type):

  # --- SENSOTERRA AUTHENTIFICATION
  headers = {
    'accept': 'application/json',
    'language': 'en'
  }
  auth = requests.post(device_type.url + 'customer/auth', headers=headers, json=device_type.authentication)
  authdata = auth.json()
  api_key = authdata['api_key']
  headers['api_key'] = api_key

  # --- getting all the sensoterra devices we registered
  probes = requests.get(device_type.url + 'probe', headers=headers, params={
      'limit': 100,
      'skip': 0,
      'include_shared_data': 'NO' 
  })

  # --- looping on those
  probesdata = probes.json()
  print(f'devices detected: {len(probesdata)}')
  for probe in probesdata:
    add_sensoterra(probe, device_type)


# SENSECAP
# ====================================================================

def add_sensecap(probe, device_type):

  # --- check if exist
  if not Device.objects.filter(serial_number = probe['device_eui']).exists():

    # --- add it to the DB if not there
    device = Device(
      deviceType = device_type,
      remote_identifier = probe['device_eui'],
      serial_number = probe['device_eui']
    )
    try:
      device.save()
      print(f'new device added {device}')
    except IntegrityError:
      print('Tried to store double device')

  else:
    device = Device.objects.filter(serial_number = probe['device_eui'])[0]

  # --- create each sensors associated to a sensecap device (if doesn't exist)
  add_sensor(device, "air temperature", "°C")
  add_sensor(device, "air humidity", "%RH")
  add_sensor(device, "co2", "ppm")


def add_all_sensecap(device_type):

  # --- listing all sensecap devices
  headers = {
    'accept': 'application/json',
    'language': 'en'
  }
  probe_response =  requests.get(device_type.url + 'list_devices',
                                 headers=headers,
                                 auth=( device_type.authentication['id'], device_type.authentication['key']))
  if probe_response.ok:
    probe_data = probe_response.json()
    print(f'devices detected: {len(probe_data["data"])}')
    for probe in probe_data['data']:
      add_sensecap(probe, device_type)

  else:
    print(probe_response.status_code, probe_response.reason)

# EMS Brno
# ====================================================================

def add_emsbrno(device_type, locName, device):

  """
    device is a list containing [
      str:serial,
      str:?,
      str:location,
      str:locname,
      str:latitude,
      str:longitude
    ]
  """

  # --- check if exist
  if not Device.objects.filter(serial_number=device[0]).exists():
  
    # --- add it to the DB if not there
    device = Device(
      latitude = device[4],
      longitude = device[5],
      deviceType = device_type,
      remote_identifier = device[3] + ' - ' + locName,
      serial_number = device[0]
    )
    try:
      device.save()
    except IntegrityError:
      print('Tried to store double device')

    print(f'new device added {device}')

  else:
    device = Device.objects.filter(serial_number = device[0])[0]

  # --- create each sensors associated to a emsbrno device (if doesn't exist)
  # --- !!! Sensors are created. But a channel id should be set by hand. Entered in remote id. !!!
  add_sensor(device, "System temperature", "°C")
  add_sensor(device, "System RH", "%RH")
  add_sensor(device, "circular increment", "mm")


def add_all_emsbrno(device_type):

  # devices are hardcoded as a string in the authentification field of the deviceType
  # this informations was taken manually from an email sent by Michal from EMS Brno
  for locName, device_data in device_type.authentication['devices'].items():
        add_emsbrno(device_type, locName, device_data)


# GLOBAL COMMAND WITH SWITCH CASES
# ====================================================================

class Command(BaseCommand):
  args = ''
  help = 'Setup (and automatically add db entries) for each devices from each brand.'

  def handle(self, *args, **options):
    for device_type in DeviceType.objects.all():

      if device_type.brand == 'Sensoterra':
        print("[Sensoterra]")
        add_all_sensoterra(device_type)

      elif device_type.brand == 'Sensecap':
        print("[Sensecap]")
        add_all_sensecap(device_type)

      elif device_type.brand == 'Decentlab':
        print("[Decentlab]")
        for device in device_type.instances.all():
          # Discover and add sensors for this device
          decentlab_discover_and_add_device_sensors(device)

      elif device_type.brand == 'EMS Brno':
        print("[EMS Brno]")
        add_all_emsbrno(device_type)
       
          