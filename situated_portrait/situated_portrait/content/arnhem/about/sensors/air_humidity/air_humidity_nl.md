## Luchtvochtigheidsensor

![](/static/img/sensors/milesight-600x.jpg)

### Herkenbare patronen

Regen
:   als mensen zijn we erg vertrouwd met deze sensor. Apps als Buienradar geven richtlijnen. Regen bepaalt onze activiteiten, kledingstijl en vaak ook ons humeur. Regen maakt deel uit van de waterkringloop, waarin ook bomen belangrijke spelers zijn.

Dag & nacht
:   de luchtvochtigheid golft mee op het ritme van dag en nacht. 's Nachts koelt het af. De warme lucht van overdag condenseert. Die vochtigheid wordt afgezet op vaste elementen, zoals gras of bankjes in het park.

### Luchtvochtigheid & Bomen

Water is het levensbloed van onze planeet. Van regenwoud tot woestijn, van prairie tot toendra, overal is de beschikbare hoeveelheid water bepalend voor wat er groeit. Geen enkel levend organisme kan zonder water als basis voor alle levensprocessen.

Zodra regen op de grond terecht komt beginnen planten het water op te nemen. Bomen als beuken hebben zelfs gelooide bladeren die het water geleiden naar de stam, vanwaar het naar de wortels loopt. Zo optimaliseren ze de regen. Planten moeten echter veel meer water opnemen dan ze nodig hebben voor hun stofwisseling, omdat planten veel water verliezen door verdamping en transpiratie. Het water is hun inwendige transportmiddel.

Het belang van water voor bomen is een cyclisch verhaal. Bomen zijn essentieel voor de waterkringloop. Transpiratie via hun bladeren is het belangrijkste mechanisme waardoor de lucht vochtig wordt gehouden. Deze lucht stroomt landinwaarts vanaf de oceaankusten. Als planten niet zo veel water afgaven via hun transpiratie zouden de binnenlanden van continenten vooral woestijngrond zijn. Er is een directe link tussen ontbossing en verwoestijning.

Bomen zijn onder de planten de meest effectieve verdampers. Naast de oceanen vormen de bomen de andere helft van het wereldwijde systeem dat bekend staat als de water- of regenkringloop. Een gemiddelde boom kan per dag 100 liter water verdampen via het enorme oppervlak van zijn bladeren.

Zelfs dicht bij de oceaan kunnen bomen van beslissend belang zijn voor de luchtvochtigheid en regen. Toen Europese kolonisten de wouden vol hoge bomen verwijderden van het eiland Maui, werd het naburige, ooit geheel beboste eiland Kahoolawe snel een woestijn, want de regen daar kwam van de bomen op Maui – en niet van de oceaan rondom beide eilanden.

### Type sensor

De luchtvochtigheid wordt gemeten door de sensor die ook CO2 en luchttemperatuur meet. Die sensor is opgehangen in de kruin van de boom.
