from situated_portrait.models import Device, DeviceType, Observation, Sensor
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from situated_portrait.milesight.decoder import decode_message, hexstring_to_bytes
import json
import re
import datetime

deveui_regex = re.compile('^urn:dev:DEVEUI:(\w+):$')

# [
#   {
#     "bn": "urn:dev:DEVEUI:24E124126D281191:",
#     "bt": 1713360398.547,
#     "n": "payload",
#     "vs": "057dce0106735d27036725000468a1"
#   },
#   {
#     "n": "port",
#     "v": 85
#   },
#   {
#     "n": "timeOrigin",
#     "vs": "NETWORK"
#   }
# ]

# [
#   {
#     "bn": "urn:dev:DEVEUI:24E124126D156268:",
#     "bt": 1713360695.709,
#     "n": "payload",
#     "vs": "057dfb0106738227036728000468ac"
#   },
#   {
#     "n": "port",
#     "v": 85
#   },
#   {
#     "n": "timeOrigin",
#     "vs": "NETWORK"
#   }
# ]


# Extract deveui from basename
#
# From: https://docs.kpnthings.com/kpn-things/components/data-processing-flows/thingsml-and-senml/senml#base-name-bn-required-in-pack
#   urn stands for unique resource name.
#   dev means we are identifying a device.
#   DEVEUI is the type of device identifier used to identify the device.
#   01234567890ABCDEF is the DevEUI, the device identifier.
def extract_device_id (basename):
  m = deveui_regex.match(basename)

  if m:
    return m.group(1)

"""
  Setup a milesight device with given device_id
"""
def setup_milesight_device (device_id):
  deviceType = DeviceType.objects.get(brand='Milesight', model='EM500-CO2-868M')
  device = Device(
    deviceType=deviceType,
    remote_identifier = device_id,
    serial_number = device_id
  )
  device.save()

  Sensor.objects.bulk_create([
    Sensor(device=device, type='air temperature', unit='°C', synthetic=False),
    Sensor(device=device, type='air humidity', unit='%RH', synthetic=False),
    Sensor(device=device, type='co2', unit='ppm', synthetic=False),
    Sensor(device=device, type='air pressure', unit='hPa', synthetic=False)
  ])

  return device

"""
  Process a json package from KPN Things platform
"""
@csrf_exempt
def kpn_things_get_milesight_package (request):
  if 'x-auth-token' in request.headers and request.headers['x-auth-token'] == settings.KPN_THINGS_TOKEN:

    body_unicode = request.body.decode('utf-8')
    package = json.loads(body_unicode)
    
    # loop through all chunks
    for chunk in package:
      if 'n' in chunk and chunk['n'] == 'payload':
        payload_raw = chunk['vs']
        data = decode_message(hexstring_to_bytes(payload_raw))

      if 'bn' in chunk:
        device_id = extract_device_id(chunk['bn'])

      if 'bt' in chunk:
        timestamp = datetime.datetime.fromtimestamp(chunk['bt'], datetime.datetime.now().astimezone().tzinfo)

    if data and device_id:
      try:
        device = Device.objects.get(remote_identifier=device_id)
      except Device.DoesNotExist:
        # Cannot find a device for this identifier so make it
        device = setup_milesight_device(device_id)

      if device.active:
        for (sensor_type, value) in data.items():
          sensor = device.sensors.get(type=sensor_type)
          observation = Observation(sensor=sensor, value=value, timestamp=timestamp, extra_data=json.dumps(data))
          observation.save()  

  response = HttpResponse("ok")
  response.status_code = 202
  return response

