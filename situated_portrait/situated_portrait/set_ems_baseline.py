from situated_portrait.models import Sensor, Observation
import datetime
import json

dendrometers = Sensor.objects.filter(type='circular increment', device__deviceType__brand='EMS Brno')
baseline_datetime = datetime.datetime(2024, 4, 5, 0, 0, tzinfo=datetime.timezone.utc)


print('Using {} as timestamp to set baseline.'.format(baseline_datetime))
print()

for dendrometer in dendrometers:
    print(dendrometer)
    # Fetch observation at April 5th 00:00
    baseline = Observation.objects.get(sensor=dendrometer, timestamp=baseline_datetime)
    print('Found baseline: {}'.format(baseline.value))
    dendrometer.extra_data = { 'baseline': baseline.value }
    dendrometer.save()
    print('Encoded baseline on the sensor.')
    print()