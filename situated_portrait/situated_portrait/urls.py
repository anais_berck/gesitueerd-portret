"""
URL configuration for situated_portrait project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from situated_portrait import views
from situated_portrait.milesight import views as milesight_views

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),

    path("i18n/", include("django.conf.urls.i18n")),

    path('T/<str:qr_id>', views.interface_guide),
    path('T/<str:qr_id>/guide', views.interface_guide, name='guide'),
    path('T/<str:qr_id>/portrait', views.interface_portrait, name='portrait'),
    path('T/<str:qr_id>/thumbnail', views.interface_thumbnail, name='thumbnail'),
    path('T/<str:qr_id>/story', views.interface_story, name='story'),
    path('T/<str:qr_id>/map', views.interface_map, name='map'),
    path('T/<str:qr_id>/clock/<int:day_from_now>', views.interface_clock, name='clock'),
    path('T/<str:qr_id>/clock/of/<str:date>', views.interface_clock),
    path('T/<str:qr_id>/map/<str:interpolated_sensor_type>', views.interface_map, name='map_interpolated_sensor_type'),
    path('T/<str:qr_id>/data', views.interface_data, name='data'),
    path('T/<str:qr_id>/data/<int:days>', views.interface_data, name='data'),
    # path('T/<str:qr_id>/data/<int:days>/<str:date_to>', views.interface_data, name='data'),
    path('T/<str:qr_id>/soundscape', views.interface_soundscape, name='soundscape'),
    path('T/<str:qr_id>/soundscape/latest', views.interface_soundscape_latest_observations, name='soundscape-latest-observations'),

    path('T/<str:qr_id>/about/at/<str:at>/', views.interface_about, name='about'),   
    path('T/<str:qr_id>/clock/at/<str:at>/', views.interface_clock, name='clock'),
    path('T/<str:qr_id>/data/at/<str:at>/', views.interface_data, name='data'),
    path('T/<str:qr_id>/data/<int:days>/at/<str:at>/', views.interface_data, name='data'),
    path('T/<str:qr_id>/guide/at/<str:at>/', views.interface_guide, name='guide'),
    path('T/<str:qr_id>/map/at/<str:at>/', views.interface_map, name='map'),
    path('T/<str:qr_id>/portrait/at/<str:at>/', views.interface_portrait, name='portrait'),
    path('T/<str:qr_id>/story/at/<str:at>/', views.interface_story, name='story'),
    path('T/<str:qr_id>/soundscape/at/<str:at>/', views.interface_soundscape, name='soundscape'),
    path('T/<str:qr_id>/soundscape/latest/at/<str:at>/', views.interface_soundscape_latest_observations, name='soundscape-latest-observations'),

    # even if the about pages are the same for each trees
    # we keep this URL scheme for back navigation support 
    # (going from about to guide again)
    path('T/<str:qr_id>/about/', views.interface_about, name='about'),
    path('T/<str:qr_id>/about/at/<str:at>/', views.interface_about, name='about'),
    path('T/<str:qr_id>/about/at/<str:at>/<path:path>/', views.interface_about, name='about'),
    path('T/<str:qr_id>/about/<path:path>/', views.interface_about, name='about'),

    path('T/<str:qr_id>/archive/', views.interface_archive, name='archive'),

    path('overview', views.overview),
    path('map', views.map_view),
    path('data-dashboard', views.data_dashboard),
    path('print', views.interface_print, name='print'),

    # path('compass', views.compass),
    # path('interpolated-compass', views.interpolated_compass),

    path('api/emsbrno', views.api_emsbrno),
    path('api/kpn_things_get_milesight_package', milesight_views.kpn_things_get_milesight_package),
    path('graph/sensor/<int:sensor_pk>', views.graph),
    path('graph/sensor/<int:sensor_pk>/<int:days>', views.graph),
    path('graph/dendrometer_growth/<int:days>', views.dendrometer_growth),
    path('multigraph/sensor/<str:sensor_pk_string>/<int:days>', views.multigraph, name='multigraph'),
]

