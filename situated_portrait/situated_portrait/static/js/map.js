// --- init the map
// note that we're going to resize and fit according to the geojson layer
// afterward so don't worry now :)
let map = L.map('map').setView([51.9904, 5.9069], 16);

// --- add the tiles,
// note that attribution is mandatory by OSM, and for other provider
// such as Mapbox, 
let oms = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

let aerial = L.tileLayer('https://service.pdok.nl/hwh/luchtfotorgb/wmts/v1_0/Actueel_orthoHR/EPSG:3857/{z}/{x}/{y}.jpeg', {
    maxZoom: 21,
    attribution: 'Kaartgegevens &copy; <a href="https://www.kadaster.nl">Kadaster</a> | <a href="https://www.verbeterdekaart.nl">Verbeter de kaart</a>'
})

document.getElementById('switch-to-aerial').addEventListener('click', function(){
    map.addLayer(aerial);
    map.removeLayer(oms);
    document.getElementById('switch-to-aerial').hidden = true;
    document.getElementById('switch-to-map').hidden = false;
    document.getElementById('map').dataset.mapStyle = 'aerial';
});

document.getElementById('switch-to-map').addEventListener('click', function(){
    map.removeLayer(aerial);
    map.addLayer(oms);
    document.getElementById('switch-to-aerial').hidden = false;
    document.getElementById('switch-to-map').hidden = true;
    document.getElementById('map').dataset.mapStyle = 'map';
});

// --- we create a layer so we can later get the bounding box of that layer and zoom accordingly
let tree_layer = L.featureGroup();

// --- get the serialized tree data
const TREES = JSON.parse(document.getElementById('trees-data').textContent);
for(let tree of TREES){

    if (tree['latitude'] && tree['longitude']){
        let lat = parseFloat(tree['latitude']);
        let lon = parseFloat(tree['longitude']);

        L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: `<div class="marker-box"><div>${ tree['name'] }</div></div>`
            })
        }).addTo(tree_layer);

        L.marker([lat, lon]).addTo(tree_layer);
    }
}

// --- labels ---
const LABELS = JSON.parse(document.getElementById('label-data').textContent);
let label_layer = L.featureGroup();
for(let label of LABELS){
    if (label['latitude'] && label['longitude']){
        let lat = parseFloat(label['latitude']);
        let lon = parseFloat(label['longitude']);

        let marker_html = '<div>' + label['label'] + '</div>';

        L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: '<div class="marker-box">' + marker_html + '</div>'
            })
        }).addTo(label_layer);

        L.marker([lat, lon]).addTo(label_layer);
    }
}


// --- adding GEOjson from OSM
const POI = JSON.parse(document.getElementById('poi-data').textContent);
let poi = [];
let poi_legend = [];
let i = 1;

// sorting them by latitude
POI['features'] = POI['features'].sort(lat_sort);
function lat_sort(a,b){
    a_all_lat = a['geometry']['coordinates'].map(x => x[1])
    b_all_lat = b['geometry']['coordinates'].map(x => x[1])
    a_lat = Math.max(...a_all_lat);
    b_lat = Math.max(...b_all_lat);
    return a_lat > b_lat;
}

let poi_layer = L.geoJSON(POI, {
    noClip: true,
    onEachFeature : function(feature, layer){
        poi_legend.push([feature['properties']['name']])
        all_lat = feature['geometry']['coordinates'].map(x => x[1])
        all_lon = feature['geometry']['coordinates'].map(x => x[0])

        if('building' in feature['properties']){
            lat = all_lat.reduce((a, b) => a + b) / all_lat.length;
            lon = Math.max(...all_lon);
        }
        else{
            lat = all_lat.reduce((a, b) => a + b) / all_lat.length;
            lon = all_lon.reduce((a, b) => a + b) / all_lon.length;
        }
        let marker = L.marker([lat, lon], {
            icon: new L.DivIcon({
                html: '<div class="marker-box poi">' + feature['properties']['name'] + '</div>'
            })
        })
        i = i + 1;
        poi.push(marker);

    },
    style: function (feature) {
        let className = 'poi-layer';
        if('building' in feature['properties']){
            className = className + ' building';
        }
        return {
          className: className,
        };
    }
});
for(let p of poi){
    p.addTo(poi_layer);
}

const WALKS = JSON.parse(document.getElementById('walk-data').textContent)
console.log(WALKS)
for(let [name, walk] of Object.entries(WALKS)){
    let walk_layer = L.polyline(walk);
    walk_layer.setStyle({'className': 'walk-layer'}); 
    if(name.includes('complement')){
        walk_layer.setStyle({'className': 'walk-layer walk-complement-layer'});
    }
    walk_layer.addTo(map);
}

poi_layer.addTo(map);
label_layer.addTo(map);
tree_layer.addTo(map);

// the tree we're on right now
const focus_tree = document.getElementById('focus')
if (focus_tree){
    const TREE = JSON.parse(focus_tree.textContent);
    map.setView([TREE.latitude, TREE.longitude], 17)
}
else{
    // change the zoom level to fit the geojson layer with some padding
    map.fitBounds(tree_layer.getBounds(),{
        padding: L.point([5, 5])
    });
    let bounds = new L.LatLngBounds([TREE.latitude, TREE.longitude], [TREE.latitude, TREE.longitude]);
    console.log(bounds)
    map.fitBounds(bounds);
}




// const interpolated_data = JSON.parse(document.getElementById('interpolated-data').textContent),
//     grid_size = interpolated_data.size,
//     contourLineCount = 50,
//     // get bounding box of trees
//     lon = d3.interpolateNumber.apply(this, d3.extent(TREES, tree => tree.fields.longitude)),
//     lat = d3.interpolateNumber.apply(this, d3.extent(TREES, tree => tree.fields.latitude));
  
// const contourLines = 50,
//     contourGenerator = d3.contours()
//     .size(grid_size)
//     .thresholds(d3.range(interpolated_data.range[0], interpolated_data.range[1], (interpolated_data.range[1] - interpolated_data.range[0]) / contourLineCount));

// // Generate the contours based on the interpolated points
// let contours = contourGenerator(interpolated_data.data.map(p => p ? p[2] : -100)) // Retrieve value per point. Or insanely large negative.
//     .map(contour => {
//         // Loop through all contours to map them back to geo-coordinates.
//         contour.coordinates = contour.coordinates.map(polygon => (
//             polygon.map(ring => (
//                 // Coordinates are remapped by d3 on the gridsize
//                 // so from 0 up to width and 0 up to height
//                 ring.map(coord => [lon(coord[0]/grid_size[0]), lat(coord[1]/grid_size[1])])
//             ))));
            
//         return contour;
//     });

// let contoursLayer = L.geoJSON(contours);
// contoursLayer.setStyle({'className': 'contour-layer'}); 
// contoursLayer.addTo(map);

// document.getElementById('interpolation-selector')
//     .addEventListener('change', function (e) {
//         window.location = e.target.value;
// });