# Artists and all the people involved in the project

## Anaïs Berck
[Anais Berck](https://www.anaisberck.be/) is a pseudonym and represents the collaboration between people, algorithms and trees. As a collective, they open up a space in which human intelligence is given a place alongside plant intelligence and artificial intelligence.

For this project, the following people are part of the collaboration:

### An Mertens (Brussels, BE)
An is a media artist and tree clerk. An conceived and coordinated this project, provided research, literary code and editing of texts and stories.

### Gijs de Heij (Brussels, BE) and Doriane Timmermans (Brussels, BE)
Both are members of the experimental graphic design studio [Open Source Publishing](http://osp.kitchen/) in Brussels. They combine graphic design with programming and artistic creation.
[Gijs de Heij](https://gijsdeheij.com/) studied graphic design at ArtEZ in Arnhem. He has been programming the age of 11.
Doriane Timmermans holds a master's degree in mathematics from Université libre de Bruxelles and studied digital art at ERG in Brussels. She researches the literary potential of css: [Declarations](https://declarations.style/).

### Bram Goots (Antwerp, BE)
Bram is a photographer. Within Agence Future, Bram has experience capturing diverse contexts in diverse places around the world, always with a pure sense of light, detail and composition.

### Christina Clar (Austria)
[Christina](https://christinaclar.net/) is a sound artist. She has twenty years of experience recording in nature and creating multi-layered soundscapes. Christina lived in Brussels until a few years ago, where she was active with organizations such as QO2 and opera house De Munt. Although she moved to Austria, she often comes to Brussels.

### Floris van Hintum (Arnhem, NL)
Floris likes to call himself a connecting tree, forest, nature, and landscape manager. He made the benches and furniture in Molenplaats Sonsbeek from local wood. With [Landbeeld](http://www.landbeeld.nl/), he develops projects combining nature and art. He also makes sound systems with WILLOW Hifi.

## Arborists from the Municipality of Arnhem
The sensors and benches were installed with the very kind help of the tree care staff from the municipality of Arnhem. Thank you for your help and patience: Ruud Krabbenborg, Arthur Wanders, Tony Berendsen, Lucas Willen Beemster, Dave Bobeldijk, Tim van Leeuwen, Jan Floor.

## Scientific advice
During various conversations before and during the creation process, we received very valuable advice from tree ring researcher Dr. Ute Sass-Klaassen and her colleague Linar Akhmetzyanov (Van Hall Larenstein University of Applied Sciences, Leeuwarden). Thank you!

## Knowledge and experience about trees and parks
During the creation process of this project, An had the honour and pleasure to spend time and listen to the knowledge and experiences of the following people: park guide Eddy van Heeckeren, forest expert Simon Klingen, ecologist Hans Van den Bos, artist Boudewijn Corstiaensen, administrators Jan Floor and Rob Borst, senior management advisor green living environment Marcel Wenker, nature guide and biologist Pim van Oers, park guide Gerard Herbers, artist Albert van der Weide, forester Nina Kreisler, author Jibbe Willems, urban ecologist Ineke Wesseling, artist Bartaku Vandeput (Baroa belaobara). Thank you!

## More advice
We could also count on the practical and moral advice of Hélène van Ginderachter, engineer Erik Mertens, Joost Verhagen (Cobra-Groen in Zicht), Judith Van Ginneken, Marleen Verschueren, Erik De Wilde. Proofreading of the texts was taken on by Patrick Lennon & Karin Ulmer. Thank you!

## Financing, production, communication
This project was made possible thanks to the support of Marko van der Wel (Nature, Environment, Care, Local Food, Water), park manager Rob Borst and senior management advisor green living environment Marcel Wenker. Communication was ensured by Sanne Blok, Hanneke Busscher and William Hulstein; production by Karlijn Michielsen. Thank you!

## Logo

<!-- ![](/static/img/logos/arnhemse-bomen-vertellen-groen.png) -->

The logo of ‘Arnhemse Bomen Vertellen’ uses Computers Regular.

This font was designed by [Marianne Noordzij](https://www.mariannenoordzij.nl), who graduated as a graphic designer from Artez and then studied AI at Utrecht University. The font makes the connection between machines and more-than-human beings as ‘companion species’.

We like the coincidence that she is from Arnhem and love the way the typeface combines organic and technological elements. In that respect, it reflects nicely the identity of the project.
