## Soil moisture sensor

![](/static/img/sensors/sensoterra-600x.jpg)

### Recognizable patterns

Soil type
:   each sensor is set to the specific soil type in which the measurements are taken, in this case a loamy soil.

Rain
:   the more rain falls, the wetter the soil. The continuous rainfall of last winter & spring have created exceptionally flat lines in the graphics over time.

Hot & dry
:   on a hot day the soil is less humid.

### Soil moisture & Trees

Trees need a lot of water - and minerals - to produce sugars through photosynthesis (see CO2 sensor). The trees transport water from the roots to the crown via the xylem. These are wood vessels just beneath the bark, which become dead wood after each season, forming the tree's annual rings. On a hot summer day, a Beech, for example, can transport up to 800 litres of water a day.

### Sensor type

Soil moisture is measured with sensors from the Dutch start-up Sensoterra. They measure the humidity of the soil at a depth of 30cm.

The sensors are placed on the trees' 'drip line'. That is the imagined line that runs along the extreme leaves of the crown. It is along the drip line that the most lively surface roots grow.
